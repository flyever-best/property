let common={
    getThisFormatDate:function() {
        var date = new Date();
        var seperator1 = "-";
        var seperator2 = ":";
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
            + " " + date.getHours() + seperator2 + date.getMinutes()
            + seperator2 + date.getSeconds();
        return currentdate;
    },
    /**
     * 时间戳格式化
     * @param date  '时间戳'
     * @param extra 'Y-m-d H:i:s'
     * @return '2019-03-19 18:23:01'
     */
    time_format:function(date = new Date().getTime(),extra = 'Y-m-d H:i:s') {
        const D = new Date(date);
        const time = {
            'Y': D.getFullYear(),
            'm': D.getMonth() + 1,
            'd': D.getDate(),
            'H': D.getHours(),
            'i': D.getMinutes(),
            's': D.getSeconds()
        };
        const key = extra.split(/\W/);
        let _date;
        for (const k of key) {
            time[k] = time[k] < 10 ? '0' + time[k] : time[k];
            _date = extra.replace(k, time[k]);
            extra = _date;
        }
        return _date;
    },



};
exports.common = common;