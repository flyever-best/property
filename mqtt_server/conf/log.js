const log4js = require('log4js');
log4js.configure({
    appenders: { MqttServer: { type: 'file', filename: 'logs.log' } },
    categories: { default: { appenders: ['MqttServer'], level: 'trace' } }
});

const logger = log4js.getLogger('MqttServer');
exports.logger=logger;
