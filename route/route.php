<?php

// //前台地址
Route::domain('www.ht.com', 'index');

// //后台地址
Route::domain('sys.wuyegl.com', 'admin');
// //api 地址
Route::domain('api.wuyegl.com', 'api')->allowCrossDomainAll();

//刷新缓存地址
Route::get('cache/clean/:password', 'cache/clean/index');