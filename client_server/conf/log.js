const log4js = require('log4js');
log4js.configure({
    appenders: { wuyeClient: { type: 'file', filename: 'logs.log' } },
    categories: { default: { appenders: ['wuyeClient'], level: 'trace' } }
});

const logger = log4js.getLogger('wuyeClient');
exports.logger=logger;
