var mqtt = require('mqtt');
var http = require('http');
var url =require('url');
var mysql=require('./conf/mysql').pool_dev;
// var redis = require("./conf/redis").client;
var common=require('./conf/common').common;
var loger=require('./conf/log').logger;

function exesql(sql,cb){
    mysql.query(sql,function(err,rows,fields){
        if(err) throw err;
        
		typeof cb == "function" && cb(rows);
        
    })
}
function aloneExe(getData,keys){
	exesql(getData[keys],function(){
		keys++;
		console.log(getData.length)
		if(getData.length>=keys){
			setTimeout(() => {
				aloneExe(getData,keys)
			}, 100);
			
		}
	})
}
http.createServer((req,res) => {
	var pathname=url.parse(req.url).pathname;
	if(req.method == 'GET'){
		res.writeHead(404);
		res.end();
		return false;
	}
	if(pathname=='/add_copy_table'){
		let jsonData = '';
		// 监听数据
		req.on('data',(data) => {
			jsonData += data;
		});
		// 事件结束
		req.on('end', () => {
			var getData = JSON.parse(jsonData);
			res.end(JSON.stringify({sta:'1',msg:'成功'}))
			let keys=0	
			try {
				aloneExe(getData,keys)
			}catch (e){
				loger.error(common.getThisFormatDate()+"-"+e)
			}
		})
    }
}).listen('2233');
process.on('uncaughtException', function (err) {
    loger.error(common.getThisFormatDate()+"-"+'Caught exception: ' + err);
});