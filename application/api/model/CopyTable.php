<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;
use tool\Curl;
class CopyTable extends ModelService {
    protected $table = 'wy_copy_table';
    protected $errorInfo='';
    protected $append = [
        'bd_info_id_text',
        'building_id_text',
        'unit_id_text',
        'house_id_text',
        'project_id_text',
        'project_detial_id_text',
    ];
    public static function getList($ar_id,$cm_mg_id,$bid,$page,$search = [],$pageSize=10) {
        if(@$cm_mg_id){
            $where[]=['bd_info_id','in',$cm_mg_id];
        }else {
            $getMgIds=model('wy_bd_info')->dbIds($ar_id);
            if($getMgIds){
                $where[]=['bd_info_id','in',$getMgIds];
            }else {
                $where[]=['bd_info_id','=',0];
            }
            
        }
        $where[] = ['is_deleted', '=', 0];
        $list = self::where($where)->where($search)->order(['id' => 'desc'])->page($page,$pageSize)->select();
        $count = self::where($where)->where($search)->count();
        return ['data'=>$list,'pageSize'=>$pageSize,'pageNo'=>(int)$page,'totalPage'=>ceil($count / $pageSize),'totalCount'=>(int)$count];
    }
    public function getError(){
        return $this->errorInfo;
    }
    public function getBdInfoIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['bd_info_id'];
        $infos=model('wy_bd_info')->where('id',$value)->value('name');
        return $infos;
    }
    public function getBuildingIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['building_id'];
        $infos=db('wy_building')->where('id',$value)->value('name');
        return $infos;
    }
    public function getUnitIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['unit_id'];
        $infos=model('wy_unit')->where('id',$value)->value('name');
        return $infos;
    }
    public function getHouseIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['house_id'];
        $infos=db('wy_houses')->where('id',$value)->value('name');
        return $infos;
    }
    public function getProjectIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['project_id'];
        $infos=model('toll_project')->where('id',$value)->value('name');
        return $infos;
    }
    public function getProjectDetialIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['project_detial_id'];
        $infos=model('toll_project_detial')->where('id',$value)->value('name');
        return $infos;
    }
    public function getNormalMoney($infos,$type,$toll_pro,$toll_info){
       
        $money=0;
        $toll=model('TollProject')->where('id',$toll_info['project_id'])->find();
        switch ($toll_info['style']){
            case 1:
                $money=formatPrice($toll_info['price']*$infos['area']*$toll_info['count_week'],$toll['dec_type'],$toll['dec_num']);
                break;
            case 2:
                $money=formatPrice($toll_pro['money'],$toll['dec_type'],$toll['dec_num']);
                break;
            case 3:
                $money=formatPrice($toll_info['fixed_price'],$toll['dec_type'],$toll['dec_num']);
                break;
            case 4:
                $gsinfo=db('wy_toll_formula_user')->alias('u')
                ->join('wy_toll_formula_name n','u.wy_toll_formula_name_id=n.id')
                ->field('u.*,n.unit,var_count')
                ->where('u.id',$toll_info['formula_id'])
                ->find();
                // $fun,$id,$floor=0,$area=0,$power=0,$count=0,$areaAll=0,$num=0,$numAll=0,$ratio=0,$kes=0
                if($type==1){
                    $pay_price=getGSgo($gsinfo['unit'],$toll_info['formula_id'],$infos['floor'],$infos['area']);
                    $money=formatPrice($pay_price,$toll['dec_type'],$toll['dec_num']);
                }elseif ($type==2) {
                    $pay_price=getGSgo($gsinfo['unit'],$toll_info['formula_id'],0,$infos['area']);
                    $money=formatPrice($pay_price,$toll['dec_type'],$toll['dec_num']);
                }else{
                    $pay_price=getGSgo($gsinfo['unit'],$toll_info['formula_id'],0,0,$infos['horsepower']);
                    $money=formatPrice($pay_price,$toll['dec_type'],$toll['dec_num']);
                }
                break;
        }
        return $money;
    }
    public function addCopyTable($data){
        if($data['addType']==1){
            $det_id = $data['toll'][1];
            $price = Db::name('wy_toll_project_detial')->where('id',$det_id)->value('price') ?: 0;
            $ratio = ($data['end_key'] - $data['start_key']) * $price * $data['times'];
            $addinfo=[
                'user_id'=>$data['user_id'],
                'bd_info_id'=>$data['houseIds'][0],
                'building_id'=>$data['houseIds'][1],
                'unit_id'=>$data['houseIds'][2],
                'house_id'=>$data['houseIds'][3],
                'project_id'=>$data['toll'][0],
                'project_detial_id'=>$data['toll'][1],
                'start_time'=>$data['start_time'],
                'end_time'=>$data['end_time'],
                'start_k'=>$data['start_key'],
                'end_k'=>$data['end_key'],
                'times'=>$data['times'],
                'ratio'=>$ratio,
                'remarks'=>$data['remarks'],
                'type'=>1,
            ];
            $ret=$this->insertGetId($addinfo);
            if($ret){
                // $months=getMonthNum($addinfo['start_time'],$addinfo['end_time']);
                $pay_price=0;
                $toll_info=model('TollProject')->where('id',$data['toll'][0])->find();
                $toll_pro_info=model('TollProjectDetial')->where('id',$data['toll'][1])->find();
                switch($toll_pro_info['style']){
                    case 1:
                        $pay_price = ($addinfo['end_k']-$addinfo['start_k'])*$toll_pro_info['price'];
                        break;
                    case 2:
                        $pay_price = 0;
                        break;
                    case 3:
                        $pay_price =$toll_pro_info['fixed_price'];
                        break;
                    case 4:
                        $houseinfo=model('wy_houses')->where('id',$addinfo['house_id'])->find();
                        $gsinfo=db('wy_toll_formula_user')->alias('u')
                        ->join('wy_toll_formula_name n','u.wy_toll_formula_name_id=n.id')
                        ->field('u.*,n.unit,var_count')
                        ->where('u.id',$toll_pro_info['formula_id'])
                        ->find();
                        $pay_price=getGSgo($gsinfo['unit'],$toll_pro_info['formula_id'],$houseinfo['floor'],$houseinfo['area']);
                }
                $pay_price= formatPrice($pay_price,$toll_info['dec_type'],$toll_info['dec_num']);
                // if($months>0){
                //     $addArr=[];
                //     $midtime=$addinfo['start_time'];
                //     for ($i=1; $i <=$months; $i++) { 
                //         $addNoPay=[
                //             'bd_info_id'=>$data['houseIds'][0],
                //             'building_id'=>$data['houseIds'][1],
                //             'house_id'=>$data['houseIds'][3],
                //             'createtime'=>date('Y-m-d H:i:m'),
                //             'price'=>$pay_price,
                //             'money'=>$pay_price,
                //             'name'=>$toll_pro_info['name'],
                //             'toll_project_detial_id'=>$data['toll'][1],
                //             'start_time'=>$midtime,
                //             'end_time'=>date('Y-m-d',strtotime('-1 day',strtotime('+1 month',strtotime($midtime)))),
                //             'Batch_num'=>'sd_'.$ret
                //         ];
                //         $midtime=date('Y-m-d',strtotime('+1 day',strtotime($addNoPay['end_time'])));
                //         $addArr[]=$addNoPay;
                //     }
                //     //todo判断重复
                //     $re=db('wy_no_payment')->insertAll($addArr);
                //     return $re;
                // }else {
                    $addNoPay=[
                        'bd_info_id'=>$data['houseIds'][0],
                        'building_id'=>$data['houseIds'][1],
                        'house_id'=>$data['houseIds'][3],
                        'createtime'=>date('Y-m-d H:i:m'),
                        'price'=>$pay_price,
                        'money'=>$pay_price,
                        'name'=>$toll_pro_info['name'],
                        'toll_project_detial_id'=>$data['toll'][1],
                        'start_time'=>$addinfo['start_time'],
                        'end_time'=>$addinfo['end_time'],
                        'Batch_num'=>'sd_'.$ret
                    ];
                    if($toll_pro_info['style']==1){
                        $addNoPay['num']=$addinfo['end_k']-$addinfo['start_k'];
                        $addNoPay['one_money']=$toll_pro_info['price'];
                    }
                    //todo判断重复
                $addinfo['house_user_id'] = $addinfo['user_id'];
                $addinfo['toll_project_detial_id'] = $addinfo['project_detial_id'];
                unset($addinfo['user_id']);
                unset($addinfo['unit_id']);
                unset($addinfo['start_k']);
                unset($addinfo['end_k']);
                unset($addinfo['times']);
                unset($addinfo['ratio']);
                unset($addinfo['project_id']);
                unset($addinfo['project_detial_id']);
                    $re=db('wy_no_payment')->insert($addNoPay);
                    return $re;
                // }
                

            }
        }else {
            $addinfo=[
                'user_id'=>$data['user_id'],
                'bd_info_id'=>$data['houseIds'][0],
                'building_id'=>0,
                'unit_id'=>0,
                'house_id'=>0,
                'project_id'=>$data['toll'][0],
                'project_detial_id'=>$data['toll'][1],
                'start_time'=>$data['start_time'],
                'end_time'=>$data['end_time'],
                'remarks'=>$data['remarks'],
                'ratio'=>$data['ratio'],
                'type'=>2,
            ];
            $ret=$this->insertGetId($addinfo);
            if($ret){
                // $months=getMonthNum($addinfo['start_time'],$addinfo['end_time']);
                $pay_price=0;
                $toll_info=model('TollProject')->where('id',$data['toll'][0])->find();
                $toll_pro_info=model('TollProjectDetial')->where('id',$data['toll'][1])->find();
                if($toll_pro_info['style']!=4){
                    $this->errorInfo='必须为自定义公式';
                    return false;
                }
                $gsinfo=db('wy_toll_formula_user')->alias('u')
                ->join('wy_toll_formula_name n','u.wy_toll_formula_name_id=n.id')
                ->field('u.*,n.unit,var_count')
                ->where('u.id',$toll_pro_info['formula_id'])
                ->find();
                if($gsinfo['unit']!='buildAreaGT'){
                    $this->errorInfo='自定义公式使用不正确';
                    return false;
                }
                $housesModel=model('wy_houses');
                // $areaAll=$housesModel->where('bd_info_id',$data['houseIds'][0])->where('over_house',1)->where('is_gt',1)->sum('area');
                $areaAll=0;
                $numAll=$housesModel->where('bd_info_id',$data['houseIds'][0])->where('over_house',1)->where('is_gt',1)->sum('num');
                $houseIds=$housesModel->where('bd_info_id',$data['houseIds'][0])->where('over_house',1)->where('is_gt',1)->field('id,ratio,building_id,num,area')->select();
                foreach($houseIds as $k=>$v){
                    $areaAll+=$v['ratio']*$v['area'];
                }
                // $pay_price=getGSgo($gsinfo['unit'],$data['ratio'],$houseinfo['floor'],$houseinfo['area']);
                $addArr=[];
                // if($months>0){
                //     foreach($houseIds as $k=>$v){
                //         $pay_price=getGSgo($gsinfo['unit'],$toll_pro_info['formula_id'],0,$v['area'],0,0,$areaAll,$v['num'],$numAll,$data['ratio'],$v['ratio']);
                //         $pay_price= formatPrice((float)$pay_price/$months,$toll_info['dec_type'],$toll_info['dec_num']);
                //         $midtime=$addinfo['start_time'];
                //         for ($i=1; $i <=$months; $i++) { 
                //             $addNoPay=[
                //                 'bd_info_id'=>$data['houseIds'][0],
                //                 'building_id'=>$v['building_id'],
                //                 'house_id'=>$v['id'],
                //                 'createtime'=>date('Y-m-d H:i:m'),
                //                 'price'=>$pay_price,
                //                 'money'=>$pay_price,
                //                 'name'=>$toll_pro_info['name'],
                //                 'toll_project_detial_id'=>$data['toll'][1],
                //                 'start_time'=>$midtime,
                //                 'end_time'=>date('Y-m-d',strtotime('-1 day',strtotime('+1 month',strtotime($midtime)))),
                //                 'Batch_num'=>'gt_'.$ret
                //             ];
                            
                //             $midtime=date('Y-m-d',strtotime('+1 day',strtotime($addNoPay['end_time'])));
                //             $addArr[]="INSERT INTO `wy_no_payment` (`type`,`bd_info_id` , `building_id` , `house_id` , `createtime` , `price` , `money` , `name` , `toll_project_detial_id` , `start_time` , `end_time` , `Batch_num`) VALUES (1,".$addNoPay['bd_info_id']." , ".$addNoPay['building_id']." , ".$addNoPay['house_id']." , '".$addNoPay['createtime']."' , ".$addNoPay['price']." , ".$addNoPay['money']." , '".$addNoPay['name']."' , ".$addNoPay['toll_project_detial_id']." , '".$addNoPay['start_time']."' , '".$addNoPay['end_time']."' , '".$addNoPay['Batch_num']."')";
                //         }
                //     }   
                // }else {
                    foreach($houseIds as $k=>$v){
                        $pay_price=getGSgo($gsinfo['unit'],$toll_pro_info['formula_id'],0,$v['area'],0,0,$areaAll,$v['num'],$numAll,$data['ratio'],$v['ratio']);
                        $pay_price= formatPrice((float)$pay_price,$toll_info['dec_type'],$toll_info['dec_num']);
                        $midtime=$addinfo['start_time'];
                       
                            $addNoPay=[
                                'bd_info_id'=>$data['houseIds'][0],
                                'building_id'=>$v['building_id'],
                                'house_id'=>$v['id'],
                                'createtime'=>date('Y-m-d H:i:m'),
                                'price'=>$pay_price,
                                'money'=>$pay_price,
                                'name'=>$toll_pro_info['name'],
                                'toll_project_detial_id'=>$data['toll'][1],
                                'start_time'=>$addinfo['start_time'],
                                'end_time'=>$addinfo['end_time'],
                                'Batch_num'=>'gt_'.$ret,
                                'type'=>1
                            ];
                            
                            $addArr[]="INSERT INTO `wy_no_payment` (`type`,`bd_info_id` , `building_id` , `house_id` , `createtime` , `price` , `money` , `name` , `toll_project_detial_id` , `start_time` , `end_time` , `Batch_num`) VALUES (1,".$addNoPay['bd_info_id']." , ".$addNoPay['building_id']." , ".$addNoPay['house_id']." , '".$addNoPay['createtime']."' , ".$addNoPay['price']." , ".$addNoPay['money']." , '".$addNoPay['name']."' , ".$addNoPay['toll_project_detial_id']." , '".$addNoPay['start_time']."' , '".$addNoPay['end_time']."' , '".$addNoPay['Batch_num']."')";
                        
                    }
                // } 
                $upmode=db('wy_copy_table');
                $retdd=$upmode->where('id',$ret)->update(['exc_num'=>count($addArr)]);
                
                // echo $$upmode->getlastsql();
                $resd =Curl::request('http://127.0.0.1:2233/add_copy_table',$addArr);
                if($resd){
                    return count($addArr);
                }else {
                    $this->errorInfo='执行错误';
                    return false;
                }
            }else {
                $this->errorInfo='插入数据错误';
                return false;
            }
            
        }
    }
}