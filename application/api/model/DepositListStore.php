<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;

class DepositListStore extends ModelService {
    protected $table = 'wy_deposit_list_store';

    public static function getList($page,$search,$house,$mobile,$pageSize) {
        if ($house == 1 && $mobile) {
            $us = Db::name('wy_house_user')->where('name|phone','like','%'.$mobile.'%')->column('id');
            $search[] = ['house_user_id','in',implode(',',$us)];
        } elseif ($house == 2 && $mobile) {
            $hou = model('wy_houses')->where('name','like','%'.$mobile.'%')->column('id');
            $uids = Db::name('wy_house_user')->where('houses_id','in',implode(',',$hou))->column('id');
            $search[] = ['house_user_id','in',implode(',',$uids)];
        }
        $list = self::where($search)->order(['id' => 'desc'])->page($page,$pageSize)->select();
        $count = self::where($search)->count();
        $search[] = ['type','=',1];
        $count1 = self::where($search)->field('sum(price) as ps,sum(money) as ms')->find();
        $search[] = ['type','=',2];
        $count2 = self::where($search)->sum('price');
        return ['data'=>$list,'pageSize'=>$pageSize,'pageNo'=>(int)$page,'totalPage'=>ceil($count / $pageSize),'totalCount'=>(int)$count,'p_all'=>$count1['ps'],'m_all'=>$count1['ms'],'d_all'=>$count2];
    }
}