<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;
class WyBdInfo extends ModelService {
    protected $table = 'wy_bd_info';

    public function dbIds($ar_id){
        return $this->where('pms_ar_id','=',$ar_id)->column('id');
    }
    public function getIdList($ar_id,$cm_mg_id=0){
        $where[] = ['is_deleted','=',0];
        $where[] = ['pms_ar_id','=',$ar_id];
        if($cm_mg_id){
            $where[] = ['id','in',$cm_mg_id];
        }
        $list=self::where($where)->field('id,name')->select();
        return $list;
    }
    public function isHasName($name,$area){
        // dump();die;
        $res=$this->where('name','=',$name)->where('area',$area)->find();
        return $res;
    }
}