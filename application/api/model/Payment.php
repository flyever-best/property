<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;

class Payment extends ModelService {
    protected $table = 'wy_payment';

    public static function getList($page,$search,$mobile,$pageSize,$del = false,$lab_id=0) {
        if ($del) {
            $search[] = ['is_deleted', '=', 1];
        } else {
            $search[] = ['is_deleted', '=', 0];
        }
        if (!empty($lab_id)) {
            $hid = Db::name('wy_houses')->where('labels','=',$lab_id)->column('id');
            $search[] = ['houses_id','in',implode(',',$hid)];
        }
        $where = $search;
        $where2 = $search;
        $where3 = $search;
        if ($mobile) {
            $us = Db::name('wy_houses')->where('name','like','%'.$mobile.'%')->column('id');
            $car = Db::name('wy_car')->where('name','like','%'.$mobile.'%')->column('id');
            $cara = Db::name('wy_car_area')->where('name','like','%'.$mobile.'%')->column('id');
            $user = Db::name('wy_house_user')->where('phone|name','like','%'.$mobile.'%')->column('id');
            if (!empty($us)) {
                $search[] = ['houses_id','in',implode(',',$us)];
            }
            if (!empty($car)) {
                $where[] = ['car_id','in',implode(',',$car)];
            }
            if (!empty($cara)) {
                $where2[] = ['car_area_id','in',implode(',',$cara)];
            }
            if (!empty($user)) {
//                if ($lab_id && $user) {
//                    $uids = Db::name('wy_labels_middle')->where('labels_id',$lab_id)->column('house_user_id');
//                    $user = array_merge($user,$uids);
//                } elseif ($lab_id) {
//                    $uids = Db::name('wy_labels_middle')->where('labels_id',$lab_id)->column('house_user_id');
//                    $user = $uids;
//                }
                $hid = Db::name('wy_house_user')->where('id','in',implode(',',$user))->column('houses_id');
                $where3[] = ['houses_id','in',implode(',',$hid)];
            }
            if ($us && $car && $cara && $user) {
                $list = self::where($search)->whereOr($where)->whereOr($where2)->whereOr($where3)->order(['id' => 'desc'])->page($page,$pageSize)->select();
//                $count = self::where($search)->whereOr($where)->whereOr($where2)->whereOr($where3)->count();
                $sum = self::where($search)->whereOr($where)->whereOr($where2)->whereOr($where3)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($us && $car && $cara) {
                $list = self::where($search)->whereOr($where)->whereOr($where2)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $sum = self::where($search)->whereOr($where)->whereOr($where2)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($car && $cara && $user) {
                $list = self::where($where3)->whereOr($where)->whereOr($where2)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $sum = self::where($where3)->whereOr($where)->whereOr($where2)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($us && $cara && $user) {
                $list = self::where($search)->whereOr($search)->whereOr($where2)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $sum = self::where($search)->whereOr($search)->whereOr($where2)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($us && $car && $user) {
                $list = self::where($search)->whereOr($where3)->whereOr($where2)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $sum = self::where($search)->whereOr($where3)->whereOr($where2)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($us && $car) {
                $list = self::where($search)->whereOr($where)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $sum = self::where($search)->whereOr($where)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($us && $cara) {
                $list = self::where($search)->whereOr($where2)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $sum = self::where($search)->whereOr($where2)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($us && $user) {
                $list = self::where($search)->whereOr($where3)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $sum = self::where($search)->whereOr($where3)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($car && $cara) {
                $list = self::where($where)->whereOr($where2)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $sum = self::where($where)->whereOr($where2)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($car && $user) {
                $list = self::where($where)->whereOr($where3)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $sum = self::where($where)->whereOr($where3)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($cara && $user) {
                $list = self::where($where2)->whereOr($where3)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $sum = self::where($where2)->whereOr($where3)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } else {
                if ($us) {
                    $where4[] = ['houses_id','in',implode(',',$us)];
                } elseif ($car) {
                    $where4[] = ['car_id','in',implode(',',$car)];
                } elseif ($cara) {
                    $where4[] = ['car_area_id','in',implode(',',$cara)];
                } elseif ($user) {
                    $where4[] = ['house_user_id','in',implode(',',$user)];
                } else {
                    $where4 = $search;
                }
                $list = self::where($where4)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $sum = self::where($where4)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            }
        } else {
            $list = self::where($search)->order(['id' => 'desc'])->page($page,$pageSize)->select();
            $sum = self::where($search)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
        }
        return ['data'=>$list,'pageSize'=>$pageSize,'pageNo'=>(int)$page,'totalPage'=>ceil($sum['cou'] / $pageSize),'totalCount'=>(int)$sum['cou'],'ps'=>round($sum['ps'],2),'ms'=>round($sum['ms'],2)];
    }
}