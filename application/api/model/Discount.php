<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;
class Discount extends ModelService {
    protected $table = 'cm_discount';

    // 追加属性
    protected $append = [
        'pms_ar_id_text',
        'cm_message_id_text',
        'type_text'
    ];

    public static function getList($search = []) {
        $where = [['is_deleted', '=', 0]];
        $data = self::where($where)->where($search)->order(['id' => 'desc'])->select();
        return $data;
    }
    public function getPmsArIdList()
    {
        if(cache('pms_ar')){
            return cache('pms_ar');
        }else{
            $pms_ar =  db('pms_ar')->where(array('status'=>0,'is_deleted'=>0))->column('id,name');
            cache('pms_ar',$pms_ar);
            return $pms_ar;
        }
    }


    public function getCmMessageIdList()
    {
        if(cache('cm_message')){
            return cache('cm_message');
        }else{
            $cm_message =  db('cm_message')->where(array('status'=>0,'is_deleted'=>0))->column('id,name');
            cache('cm_message',$cm_message);
            return $cm_message;
        }
    }

    public function getTypeList()
    {
        return ['1' =>'优惠金额','2' =>'赠送期限'];
    }


    public function getPmsArIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['pms_ar_id'];
        $list = $this->getPmsArIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getCmMessageIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['cm_message_id'];
        $list = $this->getCmMessageIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['type'];
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }

}