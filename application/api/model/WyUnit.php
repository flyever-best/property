<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;
class WyUnit extends ModelService {
    protected $table = 'wy_unit';

    protected $append = [
        'bd_info_id_text',
        'building_id_text',
    ];
    public static function getList($ar_id,$cm_mg_id,$bid,$page,$search = []) {
        if(@$cm_mg_id){
            $where[]=['bd_info_id','in',$cm_mg_id];
        }else {
            $getMgIds=model('wy_bd_info')->dbIds($ar_id);
            if($getMgIds){
                $where[]=['bd_info_id','in',$getMgIds];
            }else {
                $where[]=['bd_info_id','=',0];
            }
            
        }

        $where[] = ['is_deleted', '=', 0];
        $where[] = ['building_id', '=', $bid];
        $list = self::where($where)->where($search)->order(['id' => 'desc'])->page($page,10)->select()->each(function($item,$key){
            $item['houses_number'] = Db::name('wy_houses')->where(['bd_info_id'=>$item['bd_info_id'],'building_id'=>$item['building_id'],'unit_id'=>$item['id']])->count();
        });
        $count = self::where($where)->where($search)->count();
        return ['data'=>$list,'pageSize'=>10,'pageNo'=>(int)$page,'totalPage'=>ceil($count / 10),'totalCount'=>(int)$count];
    }
    public function getBdInfoIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['bd_info_id'];
        $infos=model('wy_bd_info')->where('id',$value)->value('name');
        return $infos;
    }
    public function getBuildingIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['building_id'];
        $infos=model('wy_building')->where('id',$value)->value('name');
        return $infos;
    }
    public function delInfo($ar_id,$cm_mg_id,$id)
    {
        if(@$cm_mg_id){
            $where[]=['bd_info_id','in',$cm_mg_id];
        }else {
            $getMgIds=model('wy_bd_info')->dbIds($ar_id);
            if($getMgIds){
                $where[]=['bd_info_id','in',$getMgIds];
            }else {
                $where[]=['bd_info_id','=',0];
            }      
        }
        
        $info=self::where($where)->where('id','in',$id)->update(['is_deleted'=>1]);
        return $info;
    }
    public function ishasname($bd_info_id,$building_id,$name,$id=0){
        $where[]=['bd_info_id','=',$bd_info_id];
        $where[]=['building_id','=',$building_id];
        $where[]=['is_deleted','=',0];
        $where[]=['name','=',$name];
        $info=self::where($where)->find();
        if($info){
            if($info['id']==$id){
                return false;
            }
            return $info;
        }else {
            return false;
        }
       
    }
    public function editInfo($bd_info_id,$building_id,$id,$name,$layers)
    {
        $where[]=['bd_info_id','=',$bd_info_id];
        $where[]=['building_id','=',$building_id];
        $info=self::isUpdate(true)->save(['name'=>$name,'id'=>$id,'layers'=>$layers]);
        return $info;
    }
    public function addInfo($bd_info_id,$building_id,$name,$layers)
    {
        $addinfo['bd_info_id']=$bd_info_id;
        $addinfo['building_id']=$building_id;
        $addinfo['layers']=$layers;
        $addinfo['is_deleted']=0;
        $addinfo['name']=$name;
        $info=self::insert($addinfo);
        return $info;
    }
    public function autoAddInfo($bd_info_id,$building_id,$num,$layers)
    {
        $where[]=['bd_info_id','=',$bd_info_id];
        $where[]=['building_id','=',$building_id];
        $where[]=['is_deleted','=',0];
        $info=self::where($where)->find();
        $info=false;
        if(!$info){
            $addArr=[];
            for($i=0;$i<$num;$i++){
                $params['bd_info_id']=$bd_info_id;
                $params['building_id']=$building_id;
                $params['is_deleted']=0;
                // $params['middle_layers']=$middle_layers;
                $params['layers']=$layers;
                $numsd=$i+1;
                $params['name']=$numsd.'单元';
                $addArr[]=$params;
            }
            $info=$this->insertAll($addArr);
        }
        return $info;
    }
}