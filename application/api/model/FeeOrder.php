<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;
class Houses extends ModelService {
    protected $table = 'Fee_order';

     // 追加属性
     protected $append = [
        'bd_message_id_text',
        'period_message_id_text',
        'unit_message_id_text',
        'houses_id_text'
    ];

    public function getBdMessageIdList($cm_message_id)
    {
        if(cache('bd_message_'.$cm_message_id)){
            return cache('bd_message_'.$cm_message_id);
        }else{
            $bd_message =  db('bd_message')->where(array('status'=>1,'is_deleted'=>0,'cm_message_id'=>$cm_message_id))->column('id,name');
            cache('bd_message_'.$cm_message_id,$bd_message);
            return $bd_message;
        }
    }

    public function getPeriodMessageIdList($cm_message_id){
        if(cache('period_message_'.$cm_message_id)){
            return cache('period_message_'.$cm_message_id);
        }else{
            $period_message =  db('period_message')->where(array('status'=>1,'is_deleted'=>0,'cm_message_id'=>$cm_message_id))->column('id,name');
            cache('period_message_'.$cm_message_id,$period_message);
            return $period_message;
        }
    }

    public function getUnitMessageIdList($cm_message_id){
        if(cache('unit_message_'.$cm_message_id)){
            return cache('unit_message_'.$cm_message_id);
        }else{
            $unit_message =  db('unit_message')->where(array('status'=>1,'is_deleted'=>0,'cm_message_id'=>$cm_message_id))->column('id,unit');
            cache('unit_message_'.$cm_message_id,$unit_message);
            return $unit_message;
        }
    }

    public function getHousesIdList($cm_message_id){
        if(cache('houses_'.$cm_message_id)){
            return cache('houses_'.$cm_message_id);
        }else{
            $houses =  db('houses')->where(array('status'=>1,'is_deleted'=>0,'cm_message_id'=>$cm_message_id))->column('id,name');
            cache('houses_'.$cm_message_id,$houses);
            return $houses;
        }
    }

    public function getBdMessageIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['bd_message_id'];
        $list = $this->getBdMessageIdList($data['cm_message_id']);
        return isset($list[$value]) ? $list[$value] : '';
    }
    public function getPeriodMessageIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['period_message_id'];
        $list = $this->getPeriodMessageIdList($data['cm_message_id']);
        return isset($list[$value]) ? $list[$value] : '';
    }
    public function getUnitMessageIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['unit_message_id'];
        $list = $this->getUnitMessageIdList($data['cm_message_id']);
        return isset($list[$value]) ? $list[$value] : '';
    }
    public function getHousesIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['unit_message_id'];
        $list = $this->getUnitMessageIdList($data['cm_message_id']);
        return isset($list[$value]) ? $list[$value] : '';
    }

}