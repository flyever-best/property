<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;
class CmFee extends ModelService {
    protected $table = 'cm_fee';

    // 追加属性
    protected $append = [
        'pms_ar_id_text',
        'cm_message_id_text',
    ];

    public static function getList($search = []) {
        $where = [['is_deleted', '=', 0]];
        $data = self::where($where)->where($search)->order(['id' => 'desc'])->select();
        return $data;
    }
    public function getPmsArIdList()
    {
        if(cache('pms_ar')){
            return cache('pms_ar');
        }else{
            $pms_ar =  db('pms_ar')->where(array('status'=>0,'is_deleted'=>0))->column('id,name');
            cache('pms_ar',$pms_ar);
            return $pms_ar;
        }
    }


    public function getCmMessageIdList()
    {
        if(cache('cm_message')){
            return cache('cm_message');
        }else{
            $cm_message =  db('cm_message')->where(array('status'=>0,'is_deleted'=>0))->column('id,name');
            cache('cm_message',$cm_message);
            return $cm_message;
        }
    }



    public function getPmsArIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['pms_ar_id'];
        $list = $this->getPmsArIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getCmMessageIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['cm_message_id'];
        $list = $this->getCmMessageIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getMoneys($houseInfo,$value,$feeType)
    {
        $money=0;
        if($value<=5){
            $money=$feeType['price'];
        }else{

        }
        return $money;
    }
    public function getHouseFee($houseid,$cm_mg_id){
        $houseFeeAll=$this->where('houses_id',$houseid)
        ->where('cm_message_id',$cm_mg_id)
        ->where('status','0')
        ->where('is_deleted','0')
        ->order('start_day asc')->select()->toArray();
        $newFeeData=[];
        foreach($houseFeeAll as $k=>$v){
            $newFeeData[$v['fee_items_id']][]=$v;
        }
        $feeOrders=[];
        $feeOrderKey=0;
        foreach($newFeeData as $key=>$value){
            foreach($value as $kk=>$vv){
                if($kk==0){
                    $temarr['name']=$vv['name'];
                    $temarr['start_day']=$vv['start_day'];
                    $temarr['end_day']=$vv['end_day'];
                    $temarr['price']=$vv['money'];
                    $temarr['cm_fee_ids'][]=$vv['id'];
                    $feeOrders[$feeOrderKey]=$temarr;
                }else{
                    if(strtotime('-1 days',strtotime($vv['start_day']))==strtotime($feeOrders[$feeOrderKey]['end_day'])){
                        $feeOrders[$feeOrderKey]['end_day']=$vv['end_day'];
                        $feeOrders[$feeOrderKey]['price']=(float)$feeOrders[$feeOrderKey]['price']+(float)$vv['money'];
                        $feeOrders[$feeOrderKey]['cm_fee_ids'][]=$vv['id'];
                    }else{
                       $feeOrderKey++;
                       $temarr['name']=$vv['name'];
                       $temarr['start_day']=$vv['start_day'];
                       $temarr['end_day']=$vv['end_day'];
                       $temarr['price']=$vv['money'];
                       $temarr['cm_fee_ids'][]=$vv['id'];
                       $feeOrders[$feeOrderKey]=$temarr;
                    }
                }    
            }
            $feeOrderKey++;         
        }
        return $feeOrders;
    }
}