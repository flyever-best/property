<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;

class DepositList extends ModelService {
    protected $table = 'wy_deposit_list';

    public static function getList($page,$search,$house,$mobile,$pageSize) {
        $where = $search;
        if ($house == 1) {
            $us = Db::name('wy_house_user')->where('phone|name','like','%'.$mobile.'%')->column('id');
//            $ids = Db::name('wy_house_user')->where('name','like','%'.$mobile.'%')->column('id');
            $search[] = ['house_user_id','in',implode(',',$us)];
//            $where[] = ['house_user_id','in',implode(',',$ids)];
            if ($us) {
                $list = self::where($search)->order(['id' => 'desc'])->page($page,$pageSize)->group('house_user_id')->field('*,sum(balance) as balance')->select();
//                $count = self::where($where)->whereOr($search)->count();
                $count = self::where($search)->field('count(id) as cou,sum(balance) as ps')->find();
            }
//            elseif ($ids) {
//                $list = self::where($where)->order(['id' => 'desc'])->page($page,$pageSize)->group('house_user_id')->field('*,sum(balance) as balance')->select();
//                $count = self::where($where)->field('count(id) as cou,sum(balance) as ps')->find();
//            }
            else {
                $list = self::where($search)->order(['id' => 'desc'])->page($page,$pageSize)->group('house_user_id')->field('*,sum(balance) as balance')->select();
                $count = self::where($search)->field('count(id) as cou,sum(balance) as ps')->find();
            }
        } elseif ($house == 2) {
            $where2 = $search;
            if ($mobile) {
                $us = Db::name('wy_houses')->where('name','like','%'.$mobile.'%')->column('id');
                $car = Db::name('wy_car')->where('name','like','%'.$mobile.'%')->column('id');
                $cara = Db::name('wy_car_area')->where('name','like','%'.$mobile.'%')->column('id');
                if ($us && $car && $cara) {
                    $search[] = ['houses_id','in',implode(',',$us)];
                    $where[] = ['car_id','in',implode(',',$car)];
                    $where2[] = ['car_area_id','in',implode(',',$cara)];
                    $list = self::where($search)->whereOr($where)->whereOr($where2)->order(['id' => 'desc'])->page($page,$pageSize)->group('house_user_id')->field('*,sum(balance) as balance')->select();
                    $count = self::where($search)->whereOr($where)->whereOr($where2)->field('count(id) as cou,sum(balance) as ps')->find();
                } elseif ($us && $car) {
                    $search[] = ['houses_id','in',implode(',',$us)];
                    $where[] = ['car_id','in',implode(',',$car)];
                    $list = self::where($search)->whereOr($where)->order(['id' => 'desc'])->page($page,$pageSize)->group('house_user_id')->field('*,sum(balance) as balance')->select();
                    $count = self::where($search)->whereOr($where)->field('count(id) as cou,sum(balance) as ps')->find();
                } elseif ($us && $cara) {
                    $search[] = ['houses_id','in',implode(',',$us)];
                    $where2[] = ['car_area_id','in',implode(',',$cara)];
                    $list = self::where($search)->whereOr($where2)->order(['id' => 'desc'])->page($page,$pageSize)->group('house_user_id')->field('*,sum(balance) as balance')->select();
                    $count = self::where($search)->whereOr($where2)->field('count(id) as cou,sum(balance) as ps')->find();
                } elseif ($car && $cara) {
                    $where[] = ['car_id','in',implode(',',$car)];
                    $where2[] = ['car_area_id','in',implode(',',$cara)];
                    $list = self::where($where)->whereOr($where2)->order(['id' => 'desc'])->page($page,$pageSize)->group('house_user_id')->field('*,sum(balance) as balance')->select();
                    $count = self::where($where)->whereOr($where2)->field('count(id) as cou,sum(balance) as ps')->find();
                } else {
                    if ($us) {
                        $search[] = ['houses_id','in',implode(',',$us)];
                    } elseif ($car) {
                        $search[] = ['car_id','in',implode(',',$car)];
                    } elseif ($cara) {
                        $search[] = ['car_area_id','in',implode(',',$cara)];
                    }
                    $list = self::where($search)->order(['id' => 'desc'])->page($page,$pageSize)->group('house_user_id')->field('*,sum(balance) as balance')->select();
                    $count = self::where($search)->field('count(id) as cou,sum(balance) as ps')->find();
                }
            } else {
                $list = self::where($search)->order(['id' => 'desc'])->page($page,$pageSize)->group('house_user_id')->field('*,sum(balance) as balance')->select();
                $count = self::where($search)->field('count(id) as cou,sum(balance) as ps')->find();
            }
        } else {
            $list = self::where($search)->order(['id' => 'desc'])->page($page,$pageSize)->group('house_user_id')->field('*,sum(balance) as balance')->select();
            $count = self::where($search)->field('count(id) as cou,sum(balance) as ps')->find();
        }
        return ['data'=>$list,'pageSize'=>$pageSize,'pageNo'=>(int)$page,'totalPage'=>ceil($count['cou'] / $pageSize),'totalCount'=>(int)$count['cou'],'ps'=>$count['ps']];
    }
}