<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;

class WyTollProject extends ModelService {
    protected $table = 'wy_toll_project';

    public static function getList($page,$search) {
        $list = self::where($search)->order(['id' => 'desc'])->page($page,10)->select();
        $count = self::where($search)->count();
        return ['data'=>$list,'pageSize'=>10,'pageNo'=>(int)$page,'totalPage'=>ceil($count / 10),'totalCount'=>(int)$count];
    }
}