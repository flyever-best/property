<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;
class FeeItems extends ModelService {
    protected $table = 'fee_items';

    public function getValue($id)
    {
        return $this->where('id',$id)->value('value');
    }
}