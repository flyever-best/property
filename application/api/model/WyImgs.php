<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 2019-10-17
 * Time: 16:35
 */

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;
class WyImgs extends ModelService {
    protected $table = 'wy_imgs';

    // 获取图片信息
    public function getImgsList($where) {
        return $this->where($where)->field('id uid,file_name name,url')->select();
    }
}