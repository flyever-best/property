<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;

class NoPay extends ModelService {
    protected $table = 'wy_no_payment';
    
    public function addinfo($data){
        return $this->insertGetId($data);
    }
    public static function getList($page,$search,$mobile,$pageSize,$lab_id) {
        $search[] = ['is_deleted', '=', 0];
        if (!empty($lab_id)) {
            $hid = Db::name('wy_houses')->where('labels','=',$lab_id)->column('id');
            $search[] = ['house_id','in',implode(',',$hid)];
        }
        $wheret[] = ['remarks', 'like', '%'.$mobile.'%'];
        $where = $search;
        $where2 = $search;
        $where3 = $search;
        if ($mobile) {
            $us = Db::name('wy_houses')->where('name','like','%'.$mobile.'%')->column('id');
            $car = Db::name('wy_car')->where('name','like','%'.$mobile.'%')->column('id');
            $cara = Db::name('wy_car_area')->where('name','like','%'.$mobile.'%')->column('id');
            $user = Db::name('wy_house_user')->where('phone|name','like','%'.$mobile.'%')->column('id');
            if (!empty($us)) {
                $search[] = ['house_id','in',implode(',',$us)];
            }
            if (!empty($car)) {
                $where[] = ['car_id','in',implode(',',$car)];
            }
            if (!empty($cara)) {
                $where2[] = ['car_area_id','in',implode(',',$cara)];
            }
            if (!empty($user)) {
                $hid = Db::name('wy_house_user')->where('id','in',implode(',',$user))->column('houses_id');
                $where3[] = ['house_id','in',implode(',',$hid)];
            }
            if ($us && $car && $cara && $user) {
                $list = self::where($search)->whereOr($where)->whereOr($where2)->whereOr($where3)->whereOr($wheret)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $count = self::where($search)->whereOr($where)->whereOr($where2)->whereOr($where3)->whereOr($wheret)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($us && $car && $cara) {
                $list = self::where($search)->whereOr($where)->whereOr($where2)->whereOr($wheret)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $count = self::where($search)->whereOr($where)->whereOr($where2)->whereOr($wheret)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($car && $cara && $user) {
                $list = self::where($where3)->whereOr($where)->whereOr($where2)->whereOr($wheret)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $count = self::where($where3)->whereOr($where)->whereOr($where2)->whereOr($wheret)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($us && $cara && $user) {
                $list = self::where($search)->whereOr($search)->whereOr($where2)->whereOr($wheret)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $count = self::where($search)->whereOr($search)->whereOr($where2)->whereOr($wheret)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($us && $car && $user) {
                $list = self::where($search)->whereOr($where3)->whereOr($where2)->whereOr($wheret)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $count = self::where($search)->whereOr($where3)->whereOr($where2)->whereOr($wheret)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($us && $car) {
                $list = self::where($search)->whereOr($where)->whereOr($wheret)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $count = self::where($search)->whereOr($where)->whereOr($wheret)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($us && $cara) {
                $list = self::where($search)->whereOr($where2)->whereOr($wheret)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $count = self::where($search)->whereOr($where2)->whereOr($wheret)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($us && $user) {
                $list = self::where($search)->whereOr($where3)->whereOr($wheret)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $count = self::where($search)->whereOr($where3)->whereOr($wheret)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($car && $cara) {
                $list = self::where($where)->whereOr($where2)->whereOr($wheret)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $count = self::where($where)->whereOr($where2)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($car && $user) {
                $list = self::where($where)->whereOr($where3)->whereOr($wheret)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $count = self::where($where)->whereOr($where3)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } elseif ($cara && $user) {
                $list = self::where($where2)->whereOr($where3)->whereOr($wheret)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $count = self::where($where2)->whereOr($where3)->whereOr($wheret)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            } else {
                if ($us) {
                    $where4[] = ['house_id','in',implode(',',$us)];
                } elseif ($car) {
                    $where4[] = ['car_id','in',implode(',',$car)];
                } elseif ($cara) {
                    $where4[] = ['car_area_id','in',implode(',',$cara)];
                } elseif ($user) {
                    $hid = Db::name('wy_house_user')->where('id','in',implode(',',$user))->column('houses_id');
                    $where4[] = ['house_id','in',implode(',',$hid)];
                } else {
                    $where4 = $search;
                }
                $list = self::where($where4)->where($search)->whereOr($wheret)->order(['id' => 'desc'])->page($page,$pageSize)->select();
                $count = self::where($where4)->where($search)->whereOr($wheret)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
            }
        } else {
            $list = self::where($search)->order(['id' => 'desc'])->page($page,$pageSize)->select();
            $count = self::where($search)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
        }
        return ['data'=>$list,'pageSize'=>$pageSize,'pageNo'=>(int)$page,'totalPage'=>ceil($count['cou'] / $pageSize),'totalCount'=>(int)$count['cou'],'ps'=>round($count['ps'],2),'ms'=>round($count['ms'],2)];
    }
    public static function getList2($page,$house,$car,$area,$pageSize) {
        $search[] = ['is_deleted', '=', 0];
        $where = $search;
        $where2 = $search;
        $where3 = $search;
        if ($house) {
            $where[] = ['house_id','in',implode(',',$house)];
        }
        if ($car) {
            $where2[] = ['car_id','in',implode(',',$car)];
        }
        if ($area) {
            $where3[] = ['car_area_id','in',implode(',',$area)];
        }
        if ($house && $car && $area) {
            $list = self::where($where)->whereOr($where2)->whereOr($where3)->order(['id' => 'desc'])->page($page,$pageSize)->select();
            $count = self::where($where)->whereOr($where2)->whereOr($where3)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
        } elseif ($house && $car) {
            $list = self::where($where)->whereOr($where2)->order(['id' => 'desc'])->page($page,$pageSize)->select();
            $count = self::where($where)->whereOr($where2)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
        } elseif ($house && $area) {
            $list = self::where($where)->whereOr($where3)->order(['id' => 'desc'])->page($page,$pageSize)->select();
            $count = self::where($where)->whereOr($where3)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
        } elseif ($car && $area) {
            $list = self::where($where2)->whereOr($where3)->order(['id' => 'desc'])->page($page,$pageSize)->select();
            $count = self::where($where2)->whereOr($where3)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
        } elseif ($house) {
            $list = self::where($where)->order(['id' => 'desc'])->page($page,$pageSize)->select();
            $count = self::where($where)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
        } elseif ($car) {
            $list = self::where($where2)->order(['id' => 'desc'])->page($page,$pageSize)->select();
            $count = self::where($where2)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
        } elseif ($area) {
            $list = self::where($where3)->order(['id' => 'desc'])->page($page,$pageSize)->select();
            $count = self::where($where3)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
        } else {
            $list = self::where('id','<',0)->order(['id' => 'desc'])->page($page,$pageSize)->select();
            $count = self::where('id','<',0)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
        }
//        $list = self::where($search)->whereOr($where)->order(['id' => 'desc'])->page($page,$pageSize)->select();
//        $count = self::where($search)->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
        return ['data'=>$list,'pageSize'=>$pageSize,'pageNo'=>(int)$page,'totalPage'=>ceil($count['cou'] / $pageSize),'totalCount'=>(int)$count['cou'],'ps'=>round($count['ps'],2),'ms'=>round($count['ms'],2)];
    }
}