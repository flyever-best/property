<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;
class User extends ModelService {
    protected $table = 'pms_employee';

    public function login($account,$password){  
        $login = $this->where('mobile', $account)->find();
        if (empty($login)) return ['code' => 1, 'msg' => '账号不存在，请重新输入！', 'user' => $login];
        if ($login['password'] != password($password)) return ['code' => 1, 'msg' => '密码不正确，请重新输入！', 'user' => $login];
        if ($login['status'] == 0) return ['code' => 1, 'msg' => '该账户已被停用，请联系管理员！', 'user' => $login];
        unset($login['password']);
        // if(!$login['cm_message_id']){
        //     return ['code' => 1, 'msg' => '未绑定任何小区！', 'user' => $login];;
        // }
        $msg = [];
        $msg['token'] = buildToken($login['id']);   // Token
        $msg['id'] = $login['id'];                  // ID
        $msg['name'] = $login['name'];              // 登录员工姓名
        $msg['cm_message_id'] = $login['cm_message_id'];     // 酒店ID
        $msg['message_list'] = db('cm_message')->where('id','IN',$login['cm_message_id'])->field('id,name')->select();
        return ['code' => 0, 'msg' => '登录成功，正在进入系统！', 'user' => $msg, 'info' => $login];
    }
}