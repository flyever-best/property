<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;

class PaymentStore extends ModelService {
    protected $table = 'wy_payment_store';

    public static function getList($page,$search = [],$name='',$pageSize=10,$lab_id=0,$where = []) {
        if ($name) {
            $hous = Db::name('wy_houses')->where('name','like','%'.$name.'%')->column('id');
            $user_id = Db::name('wy_house_user')->where('name|phone','like','%'.$name.'%')->column('id');
            $car_id = Db::name('wy_car')->where('name','like','%'.$name.'%')->column('id');
            $car_area_id = Db::name('wy_car_area')->where('name','like','%'.$name.'%')->column('id');
            if ($hous) {
                $pid = Db::name('wy_payment')->where('houses_id','in',implode(',',$hous))->column('id');
                $where[] = ['payment_id','in',implode(',',$pid)];
            } elseif ($user_id) {
//                if ($lab_id) {
//                    $uids = Db::name('wy_labels_middle')->where('labels_id',$lab_id)->column('house_user_id');
//                    $user = array_merge($user_id,$uids);
//                } else {
                    $user = $user_id;
//                }
                $hid = Db::name('wy_house_user')->where('id','in',implode(',',$user))->column('houses_id');

                $pid = Db::name('wy_payment')->where('house_user_id','in',implode(',',$user_id))->whereOr('houses_id','in',implode(',',$hid))->column('id');
                $where[] = ['payment_id','in',implode(',',$pid)];
            } elseif ($car_id){
                $pid = Db::name('wy_payment')->where('car_id','in',implode(',',$car_id))->column('id');
                $where[] = ['payment_id','in',implode(',',$pid)];
            } elseif ($car_area_id) {
                $pid = Db::name('wy_payment')->where('car_area_id','in',implode(',',$car_area_id))->column('id');
                $where[] = ['payment_id','in',implode(',',$pid)];
            }else {
                $where[] = ['houses_name|car_area_name|car_name|user_name|user_phone','eq',$name];
            }
        }
        if (!empty($lab_id)) {
            $hid = Db::name('wy_houses')->where('labels',$lab_id)->column('id');
//            $uids = Db::name('wy_labels_middle')->where('labels_id',$lab_id)->column('house_user_id');
//            $hid = Db::name('wy_house_user')->where('id','in',implode(',',$uids))->column('houses_id');
            $pid = Db::name('wy_payment')->where('houses_id','in',implode(',',$hid))->column('id');
            $where[] = ['payment_id','in',implode(',',$pid)];
        }
        $list = self::where($search)->where($where)
            ->order(['id' => 'desc'])->page($page,$pageSize)->select();
        $sum = self::where($search)->where($where)
            ->field('count(id) as cou,sum(price) as ps,sum(money) as ms')->find();
        $pid = self::where($search)->where($where)
            ->column('payment_id');
        $deu = model('payment')->where('id','in',$pid)->sum('deduction');
        $pidu = self::where($search)->where($where)
            ->page($page,$pageSize)->column('payment_id');
        $ud = model('payment')->where('id','in',$pidu)->sum('deduction');
        $count = $sum['cou'];
        return ['data'=>$list,'pageSize'=>$pageSize,'pageNo'=>(int)$page,'totalPage'=>ceil($count / $pageSize),'totalCount'=>(int)$count,'ps'=>round($sum['ps'],2),'ms'=>round($sum['ms'],2),'deu'=>round($deu,2),'xd'=>round($ud,2)];
    }
}