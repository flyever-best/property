<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;
class Fee extends ModelService {
    protected $table = 'fee_type';

    // 追加属性
    protected $append = [
        'pms_ar_id_text',
        'cm_message_id_text',
        'fee_items_id_text',
        'type_text',
        'house_type_text'
    ];
    public static function getFeeTypeList($search = []) {
        $where = [['is_deleted', '=', 0]];
        $data = self::where($where)->where($search)->order(['id' => 'desc'])->select();
        $ret['skip']=[];
        $ret['go']=[];
        foreach ($data as $k=>$v){
            if($v['type']==1){
                $ret['skip'][]=$v;
            }else{
                $ret['go'][]=$v;
            }
        }
        return $ret;
    }
    public function getPmsArIdList()
    {
        if(cache('pms_ar')){
            return cache('pms_ar');
        }else{
            $pms_ar =  db('pms_ar')->where(array('status'=>0,'is_deleted'=>0))->column('id,name');
            cache('pms_ar',$pms_ar);
            return $pms_ar;
        }
    }


    public function getCmMessageIdList()
    {
        if(cache('cm_message')){
            return cache('cm_message');
        }else{
            $cm_message =  db('cm_message')->where(array('status'=>0,'is_deleted'=>0))->column('id,name');
            cache('cm_message',$cm_message);
            return $cm_message;
        }
    }


    public function getFeeItemsIdList()
    {
        if(cache('fee_items')){
            return cache('fee_items');
        }else{
            $fee_items =  db('fee_items')->where(array('is_deleted'=>0,'type'=>1))->column('value,name');
            cache('fee_items',$fee_items);
            return $fee_items;
        }
    }
    public function getFeeItemsIdGoList(){
        return retGoValue();
    }
    public function getTypeList()
    {
        return ['1' => '周期行费用','2' => '走表费用'];
    }

    public function getHouseTypeList()
    {
        return ['1' => '商场','2' => '住宅','3'=>'写字楼'];
    }
    public function getPmsArIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['pms_ar_id'];
        $list = $this->getPmsArIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getCmMessageIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['cm_message_id'];
        $list = $this->getCmMessageIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getFeeItemsIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['fee_items_id'];
        if($data['type']=='1'){
            $list = $this->getFeeItemsIdList();
        }else{
            $list = $this->getFeeItemsIdGoList();
        }
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['type'];
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }
    public function getHouseTypeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['house_type'];
        $list = $this->getHouseTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }
}