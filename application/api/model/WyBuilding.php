<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;
class WyBuilding extends ModelService {
    protected $table = 'wy_building';

    // 追加属性
    protected $append = [
        'bd_info_id_text',
    ];

    public static function getList($ar_id,$cm_mg_id,$page,$search = []) {
        if(@$cm_mg_id){
            $where[]=['bd_info_id','in',$cm_mg_id];
        }else {
            $getMgIds=model('wy_bd_info')->dbIds($ar_id);
            if($getMgIds){
                $where[]=['bd_info_id','in',$getMgIds];
            }else {
                $where[]=['bd_info_id','=',0];
            }
            
        }
        $where[] = ['is_deleted', '=', 0];
        $list = self::where($where)->where($search)->order(['id' => 'desc'])->page($page,10)->select()->each(function($item,$key) {
            $item['unit_num'] = Db::name('wy_unit')->where(['building_id'=>$item['id'],'bd_info_id'=>$item['bd_info_id'],'is_deleted'=>0])->count();
            $item['user_num'] = Db::name('wy_house_user')->where(['building_id'=>$item['id'],'bd_info_id'=>$item['bd_info_id']])->count();
            return $item;
        });
        foreach ($list as $key=>$value){
            $list[$key]['status'] = ['0'=>'禁用','1'=>'正常','2'=>'拆除','3'=>'出售','4'=>'其它'][$value['status']];
        }
        $count = self::where($where)->where($search)->count();
        return ['data'=>$list,'pageSize'=>10,'pageNo'=>(int)$page,'totalPage'=>ceil($count / 10),'totalCount'=>(int)$count];
    }
    public function getInfo($ar_id,$cm_mg_id,$id){
        if(@$cm_mg_id){
            $where[]=['bd_info_id','in',$cm_mg_id];
        }else {
            $getMgIds=model('wy_bd_info')->dbIds($ar_id);
            if($getMgIds){
                $where[]=['bd_info_id','in',$getMgIds];
            }else {
                $where[]=['bd_info_id','=',0];
            }      
        }
        $info=self::where($where)->where('id',$id)->find();
        return $info;
    }
    public function delInfo($ar_id,$cm_mg_id,$id){
        if(@$cm_mg_id){
            $where[]=['bd_info_id','in',$cm_mg_id];
        }else {
            $getMgIds=model('wy_bd_info')->dbIds($ar_id);
            if($getMgIds){
                $where[]=['bd_info_id','in',$getMgIds];
            }else {
                $where[]=['bd_info_id','=',0];
            }      
        }
        $info=self::where($where)->where('id','in',$id)->update(['is_deleted'=>1]);
        $unitIds = model('wy_unit')->where($where)->where('building_id','in',$id)->column('id');
//        $unitIds = implode(',',$unitIds);
        $housesIds = model('wy_houses')->where($where)->where('building_id','in',$id)->where('unit_id','in',$unitIds)->column('id');
        Db::name('wy_houses_user_middle')->where('houses_id','in',$housesIds)->delete();
        return $info;
    }
    public function getBdInfoIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['bd_info_id'];
        $infos=model('wy_bd_info')->where('id',$value)->value('name');
        return $infos;
    }
}