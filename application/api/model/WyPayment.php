<?php

namespace app\api\model;

use app\common\service\ModelService;
use think\Db;
class WyPayment extends ModelService {
    protected $table = 'wy_payment';
    
    public function addOhters($data,$type){
        $start_time=$data['start_time'];
        $end_time=$data['end_time'];
        unset($data['start_time']);
        unset($data['end_time']);
        unset($data['time']);
        $getid=$this->insertGetId($data);

        $pay_store['payment_id'] = $getid;
        $pay_store['status'] = "已缴";
        $pay_store['type'] = $type==1?"其它收费":'临时停车费';
        $pay_store['name'] = $data['name'];
        $pay_store['start_time'] = $start_time;
        $pay_store['end_time'] = $end_time;
        $pay_store['price'] = $data['price'];
        $pay_store['money'] = $data['money'];
        if($type==1){
            $pay_store['user_id'] = $data['house_user_id'];
            $pay_store['user_name'] = model('wy_houses')->where('id',$data['house_user_id'])->value('name');
        }
       
        $pay_store['action_user'] = $data['action_user'];
        $pay_store['pay_style'] = $data['pay_style'];
        model('wy_payment_store')->insert($pay_store);
        return $getid;
    }
}