<?php
if (!function_exists('exportExcel')) {
    /**
     * excel 导出台账
     * @param $expTitle     表头名
     * @param $expCellName  表字段数据
     * @param $expTableData 表格数据
     * @param $download     是否保存下载
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Reader_Exception
     */
    function exportExcel($expTitle,$expCellName, $expTableData,$extendTitle=[], $download = true){
        $fileName = $expTitle.date('_Y年m月d日H点i分s秒');//文件名
        $cellNum = count($expCellName);//数据总列数
        $dataNum = count($expTableData);//数据总行数
        // 实例化一个Excel文档
        $objPHPExcel = new PHPExcel();
        $BasecellName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
        $cellName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
        foreach($BasecellName as $v){
            foreach($BasecellName as $vv){
                $cellName[]=$v.$vv;
            }
        }
        // dump($expCellName);die;
        $objPHPExcel->getActiveSheet(0)->mergeCells('A1:'.$cellName[15].'1');//合并单元格
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $expTitle);//第一表头
        $topTempKey=16;
        foreach($extendTitle as $k=>$v){
            $objPHPExcel->getActiveSheet(0)->mergeCells($cellName[$topTempKey].'1:'.$cellName[$topTempKey+11].'1');//年合并单元格
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$topTempKey].'1', $v);//第一表头

            $topTempKey+=12;
            
        }
        
        // 第一/二表头数据居中
        $objPHPExcel->getActiveSheet(0)->getStyle('A1:'.$cellName[$cellNum-1].'1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet(0)->getStyle('A2:'.$cellName[$cellNum-1].'2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        // 第一/二表头字体加粗
        $objPHPExcel->getActiveSheet(0)->getStyle('A1:'.$cellName[$cellNum-1].'1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet(0)->getStyle('A2:'.$cellName[$cellNum-1].'2')->getFont()->setBold(true);
        for($i=0;$i<$cellNum;$i++){
            // 表头赋值
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i].'2', $expCellName[$i]);
            // 设置列自动宽度
            $objPHPExcel->getActiveSheet(0)->getColumnDimension($cellName[$i])->setAutoSize(true);
            // 设置列宽度
            // $objPHPExcel->getActiveSheet(0)->getColumnDimension($cellName[$i])->setWidth(14);
        }
        // dump($expTableData);
        // dump($cellNum);die;
        // 循环导出数据
        for($i=0;$i<$dataNum;$i++){
            for($j=0;$j<$cellNum;$j++){
                $objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j].($i+3), $expTableData[$i][$j]);
            }
        }
        // 声明下载类型
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        // 下载形式及文件名
        // attachment新窗口打印 - inline本窗口浏览
        header("Content-Disposition: attachment;filename=".$fileName.".xls");
        // 禁止缓存
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        if($download === true){
            // 保存到服务器本地
            $path = ROOT_PATH.'public/static/excel/';
            if(!is_dir($path)){
                mkdir($path, 0777, true);
            }
            $file = $expTitle.'_'.date('Y年m月d日H点i分s秒').'.xls';
            $filePath = $path.$file;
            $objWriter->save($filePath);
            return '/static/excel/'.$file;
            // return request()->domain().'/static/excel/'.$file;
        }else{
            $objWriter->save('php://output');
            exit;
        }
    }
}
function payTypeTxt($id=0){
    $style_text = [
        '0' => '',
        '1' => '线下支付-现金',
        '2' => '线下支付-支票',
        '3' => '线下支付-银行转账',
        '4' => '线下支付-pos机刷卡',
        '5' => '线下支付-支付宝直接转账',
        '6' => '线下支付-微信直接转账',
        '7' => '小区收款码-支付宝',
        '8' => '小区收款码-微信',
        '9' => '网上缴费1',
        '10' => '网上缴费2',
        '11' => '网上缴费3'
    ];
    return $style_text[$id];
}
function strFilter($str){
    $str = str_replace('`', '', $str);
    $str = str_replace('·', '', $str);
    $str = str_replace('~', '', $str);
    $str = str_replace('!', '', $str);
    $str = str_replace('！', '', $str);
    $str = str_replace('@', '', $str);
    $str = str_replace('#', '', $str);
    $str = str_replace('$', '', $str);
    $str = str_replace('￥', '', $str);
    $str = str_replace('%', '', $str);
    $str = str_replace('^', '', $str);
    $str = str_replace('……', '', $str);
    $str = str_replace('&', '', $str);
    $str = str_replace('*', '', $str);
    $str = str_replace('(', '', $str);
    $str = str_replace(')', '', $str);
    $str = str_replace('（', '', $str);
    $str = str_replace('）', '', $str);
    $str = str_replace('-', '', $str);
    $str = str_replace('_', '', $str);
    $str = str_replace('——', '', $str);
    $str = str_replace('+', '', $str);
    $str = str_replace('=', '', $str);
    $str = str_replace('|', '', $str);
    $str = str_replace('\\', '', $str);
    $str = str_replace('[', '', $str);
    $str = str_replace(']', '', $str);
    $str = str_replace('【', '', $str);
    $str = str_replace('】', '', $str);
    $str = str_replace('{', '', $str);
    $str = str_replace('}', '', $str);
    $str = str_replace(';', '', $str);
    $str = str_replace('；', '', $str);
    $str = str_replace(':', '', $str);
    $str = str_replace('：', '', $str);
    $str = str_replace('\'', '', $str);
    $str = str_replace('"', '', $str);
    $str = str_replace('“', '', $str);
    $str = str_replace('”', '', $str);
    $str = str_replace(',', '', $str);
    $str = str_replace('，', '', $str);
    $str = str_replace('<', '', $str);
    $str = str_replace('>', '', $str);
    $str = str_replace('《', '', $str);
    $str = str_replace('》', '', $str);
    $str = str_replace('.', '', $str);
    $str = str_replace('。', '', $str);
    $str = str_replace('/', '', $str);
    $str = str_replace('、', '', $str);
    $str = str_replace('?', '', $str);
    $str = str_replace('？', '', $str);
    return trim($str);
}