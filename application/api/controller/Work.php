<?php

namespace app\api\controller;
use app\common\controller\ApiController;
use think\Db;
class Work extends ApiController
{
    public function getList(){
        $param = $this->request->post();
        if(@$param['bd_info_id']){
            $where[] = ['bd_info_id','=',$param['bd_info_id']];
        }else{
            $ar_id = $this->userinfo['pms_ar_id'];
            $cm_mg_id =  $this->userinfo['cm_message_id'];
            if(@$cm_mg_id){
                $where[]=['bd_info_id','in',$cm_mg_id];
            }else {
                $getMgIds=model('wy_bd_info')->dbIds($ar_id);
                if($getMgIds){
                    $where[]=['bd_info_id','in',$getMgIds];
                }else {
                    $where[]=['bd_info_id','=',0];
                }      
            }
        }
        if(@$param['start_time'] && @$param['end_time']){
            $where[] = ['createtime','>=',$param['start_time']];
            $where[] = ['createtime','<=',$param['end_time']];
        }
        $progressArr=[
            '1'=>'待分配',
            '2'=>'待完成',
            '3'=>'已完成',
        ];
        if(!empty($param['name'])) $where[] = ['name','like','%'.$param['name'].'%'];        
        $where[] = ['is_deleted','=',0];
        $where[] = ['type','=',$param['type']];
        $models=model('wy_work');
        $list = $models->where($where)->page($param['pageNo'],$param['pageSize'])->order('id desc')->select();
        foreach($list as $k => $v) {
            if($v['bd_info_id'] > 0) $list[$k]['bd_info_id_text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name');
            $list[$k]['progress_text']=$progressArr[$v['progress']];
        }
        $count = $models->where($where)->count();
        return __success('ok',['data'=>$list,'pageSize'=>$param['pageSize'],'pageNo'=>$param['pageNo'],'totalCount'=>$count,'totalPage'=>ceil($count / $param['pageSize'])]);

    }
    public function operation(){
        $param = $this->request->post();
        unset($param['token']);
        $imgsarr=[];
        foreach($param['work_image'] as $K=>$v){
            $imgsarr[]=$v['uid'];
        }
        $models=model('wy_work');
        $param['work_image']=implode(',',$imgsarr);
        if(@$param['id']){
            $param['actiontime']=date('Y-m-d');
            $res=$models->isUpdate(true)->save($param);
            if($res){
                return __success('修改成功！');
            }else{
                return __error('修改失败！');
            }
        }else{
            $param['createtime']=date('Y-m-d');
            $param['actiontime']=date('Y-m-d');
            $res=$models->isUpdate(false)->save($param);
            if($res){
                return __success('新增成功！');
            }else{
                return __error('新增失败！');
            }
        }
    }
    public function deleted(){
        $id = (int)$this->request->post('id',0);
        if(@$id){
            $models=model('wy_work');
            $res=$models->isUpdate(false)->save(['is_deleted'=>1]);
            if($res){
                return __success('删除成功！');
            }else{
                return __error('删除失败！');
            }
        }else{
            return __error('参数错误！');
        }
    }
}