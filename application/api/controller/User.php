<?php
namespace app\api\controller;
use app\common\controller\ApiController;
class User extends ApiController{
    
    public function info(){
        $userinfo=$this->userinfo;
        $userinfo['message_list'] = db('cm_message')->where('id','IN',$userinfo['cm_message_id'])->field('id,name')->select();
        $userinfo['role'] = [];
        if($userinfo['wy_auth_id'] !== 1){
            // 如果用户不是管理员-> 获取组对应的权限菜单
            $groupInfo = db('wy_auth')->where('id',$userinfo['wy_auth_id'])->find();
            if(empty($groupInfo)) return __error('未设置权限');
            $userinfo['role'] = $this->getMenu(['id','in',explode(',',$groupInfo['menus'])]);
        }else $userinfo['role'] = $this->getMenu();
        return __success('获取成功',$userinfo);
    }

    public function editPwd() {
        $param = $this->request->param();
        $userInfo = db('pms_employee')->where('id',$this->userinfo['id'])->find();
        if($userInfo['password'] !== password($param['oldpwd'])) return __error('旧密码不正确');
        if($param['pwd'] !== $param['fitpwd']) return __error('两次密码不一致');
        $upRes = db('pms_employee')->where('id',$this->userinfo['id'])->update(['password'=>password($param['pwd'])]);
        return $upRes !== false ? __success('修改成功') : __error('修改失败');
    }

    function getMenu($where = ['id','>',0]) {
        $map[] = $where;
        $menu = db('wy_web_menu')->where($map)->order('id,sort')->select();
        $oneMenu = [];
        foreach ($menu as $key=>$value) if($value['pid'] === 0) array_push($oneMenu,$value);
        foreach ($oneMenu as $k=>$v){
            $oneMenu[$k]['children'] = [];
            foreach ($menu as $mk=>$mv) {
                if($mv['pid'] == $v['id']) array_push($oneMenu[$k]['children'],$mv);
            }
            foreach ($oneMenu[$k]['children'] as $ck=>$cv){
                $oneMenu[$k]['children'][$ck]['children'] = [];
                foreach ($menu as $mk=>$mv) {
                    if($mv['pid'] == $cv['id']) array_push($oneMenu[$k]['children'][$ck]['children'],$mv);
                }
            }
        }
        return [['path'=>'/','name'=>'index','component'=>'BasicLayout','meta'=>['title'=>'首页'],'redirect'=>'/dashboard','children'=>$oneMenu]];
    }
}
