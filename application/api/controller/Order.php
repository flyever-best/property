<?php
namespace app\api\controller;
use app\common\controller\ApiController;
class Order extends ApiController{
    protected $ar_id='';
    protected $cm_mg_id='';
    protected $model='';
    public function __construct() {
        parent::__construct();
        $this->model=model('fee');
        $this->ar_id = request()->param('ar_id');
        $this->cm_mg_id = request()->param('cm_mg_id');
        if(!$this->ar_id){
            echo json_encode(['code' => 1, 'msg' => '物业id不能为空', 'data' => []]);die;
        }
        if(!$this->cm_mg_id){
            echo json_encode(['code' => 1, 'msg' => '小区id不能为空', 'data' => []]);die;
        }
    }

   public function make_order(){
       if($this->request->isPost()){
        $start_day=$this->request->post('start_day');
        $end_day=$this->request->post('end_day');
        $houseIds=$this->request->post('houseIds');
        $payType=$this->request->post('payType');
        $page=$this->request->post('page',1);
        $cmModel=model('cm_fee');
        $feeItemsModel=model('fee_items');
        $feeModel=model('fee');
        $houseModel=model('houses');
        $feeTypeInfo=$feeModel->where('id',$payType)->find();
        $feeItemValue=$feeItemsModel->getValue($feeTypeInfo['fee_items_id']);
        if($page==1){
            $ifHasCm=$cmModel
            ->where('fee_items_id',$feeTypeInfo['fee_items_id'])
            ->where('start_day','<=',$start_day)
            ->where('end_day','>=',$end_day)
            ->where('houses_id','in',$houseIds)
            ->where('is_deleted','0')
            ->where('status','>=','0')
            ->find();
            if($ifHasCm){
                return __error('选择的住户已经有这个时间段的收费订单');
            }
        }
       
        $houseInfoArr=$houseModel->where('id','in',$houseIds)->page($page,5)->select();
        $houseInfoArrCount=$houseModel->where('id','in',$houseIds)->count();
        $allPages=ceil($houseInfoArrCount/5);
        if($page< $allPages){
            foreach($houseInfoArr as $k => $v){
                /************构建时间 start*********************/
                if(strtotime($v['get_time'])>0){
                    $starttime=$v['get_time'];
                }else{
                    $starttime=$start_day;
                }
                $days=date('d',strtotime($starttime));
                $start_days=date('d',strtotime($start_day));
                $time1=strtotime($starttime);
                $time2=strtotime($end_day);
                if($days>$start_days){
                    $time1 = strtotime('+1 month', $time1);
                }
                $temp['pms_ar_id']=$this->ar_id;
                $temp['cm_message_id']=$this->cm_mg_id;
                $temp['houses_id']=$v['id'];
                $temp['fee_items_id']=$feeTypeInfo['fee_items_id'];
                $temp['fee_type_id']=$feeTypeInfo['id'];
                $temp['name']=$feeTypeInfo['name'];
                $temp['start_day']=date('Y-m-d',$time1);
                $temp['end_day']=date('Y-m-d',strtotime('-1 days',strtotime('+1 month',$time1)));
                $temp['money']=$cmModel->getMoneys($v,$feeItemValue,$feeTypeInfo);
                $temp['status']='0';
                $temp['is_deleted']='0';
                $insterData[]=$temp;
        
                while( ($time1 = strtotime('+1 month', $time1)) <= $time2){  
                    $temp['pms_ar_id']=$this->ar_id;
                    $temp['cm_message_id']=$this->cm_mg_id;
                    $temp['houses_id']=$v['id'];
                    $temp['fee_items_id']=$feeTypeInfo['fee_items_id'];
                    $temp['fee_type_id']=$feeTypeInfo['id'];
                    $temp['name']=$feeTypeInfo['name'];
                    $temp['start_day']=date('Y-m-d',$time1);
                    $temp['end_day']=date('Y-m-d',strtotime('-1 days',strtotime('+1 month',$time1)));
                    $temp['money']=$cmModel->getMoneys($v,$feeItemValue,$feeTypeInfo);
                    $temp['status']='0';
                    $temp['is_deleted']='0';
                    $insterData[]=$temp;
                }
                /************构建时间 end*********************/  
            }
            $cmModel->startTrans();
            try{
                $cmModel->saveAll($insterData);
                $cmModel->commit();
                return __success('生成成功！',$allPages,'1000');
            }catch(\Exception $e){
                $cmModel->rollback();
                return __error('生成失败！');
            }
        }else{
            return __success('生成成功！');
        }
       
       }else{
           return __error('请求不正确');
       }
   }
   /**
    * 缴费
    */
   public function submit_order(){
       if($this->request->isPost()){
        $addOrder['cm_message_id']=$this->cm_mg_id;
        $addOrder['bd_message_id']=$this->request->post('bd_message_id',0);
        $addOrder['period_message_id']=$this->request->post('period_message_id',0);
        $addOrder['unit_message_id']=$this->request->post('unit_message_id',0);
        $addOrder['order_num']= 'wy-'.time();
        $addOrder['houses_id']=$this->request->post('houses_id',0);
        $addOrder['moeny']=$this->request->post('moeny',0);
        $addOrder['dicount_type']=$this->request->post('dicount_type','0');
        $addOrder['dicount_content']=$this->request->post('dicount_content','');
        $addOrder['create_time']=date('Y-m-d H:i:s');
        $addOrder['pay_type']=$this->request->post('pay_type');
        $addOrder['status']='1';
        $addOrder['is_deleted']='0';
        $insterId=db('fee_order')->insertGetId($addOrder);
        if($insterId){
            $extOrder=$this->request->post('ext_order');
            $temp=[];
            foreach($extOrder as $k=>$v){
                $temp[$k]['fee_order_id']=$insterId;
                $temp[$k]['name']=$v['name'];
                $temp[$k]['start_day']=$v['start_day'];
                $temp[$k]['end_day']=$v['end_day'];
                $temp[$k]['cm_fee_ids']=implode(',',$v['cm_fee_ids']);
                model('cm_fee')->where('id','in',$v['cm_fee_ids'])->update(['status'=>'1']);
            }
            $resd=db('fee_order_ext')->insertAll($temp);
           
            if($resd){
                return __success('缴费成功！');
            }else{
                return __error('添加缴费扩展失败！');
            }
        }else{
            return __error('写入订单失败！');
        }
       }else{
           return __error('参数错误！');
       }
   }
}
