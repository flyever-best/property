<?php
namespace app\api\controller;
use app\common\controller\ApiController;
class Discount extends ApiController{
    protected $ar_id='';
    protected $cm_mg_id='';
    protected $model='';
    public function __construct() {
        parent::__construct();
        $this->model=model('discount');
        $this->ar_id = request()->param('ar_id');
        $this->cm_mg_id = request()->param('cm_mg_id');
        if(!$this->ar_id){
            echo json_encode(['code' => 1, 'msg' => '物业id不能为空', 'data' => []]);die;
        }
        if(!$this->cm_mg_id){
            echo json_encode(['code' => 1, 'msg' => '小区id不能为空', 'data' => []]);die;
        }
    }

    /**
     * 获取交费项目
     * @return \think\response\Json
     */
    public function get_list(){
        $map=[['is_deleted','=',0],['pms_ar_id','=',$this->ar_id],['cm_message_id','=',$this->cm_mg_id]];
        $list=$this->model->getList($map);
        return __success('获取成功！',$list);
    }


    /**
     * 保存修改交费项目
     * @return \think\response\Json
     */
    public function save_item(){
        if($this->request->isPost()){
            $savedata=$this->request->post();
            if(@$savedata['saveData']){
                $savedata['saveData']['create_time']=date('Y-m-d H:i:s');
                $res=$this->model->save($savedata['saveData']);
                if($res){
                    return __success('添加成功！');
                }else{
                    return __success('添加失败！');
                }
            }else{
              return __error('参数缺失！');
            }
        }else{
            $savedata=$this->request->put();
            if(@$savedata['saveData']){
                $res=$this->model->isUpdate(true)->save($savedata['saveData']);
                if($res){
                    return __success('修改成功！');
                }else{
                    return __success('修改失败！');
                }
            }else{
                return __error('参数缺失！');
            }
        }
    }

    /**
     * 删除交费项目
     * @return \think\response\Json
     */
    public function del_item(){
        $id=$this->request->delete('id');
        if(@$id){
            $res=$this->model->isUpdate(true)->save(['id'=>$id,'is_deleted'=>1]);
            if($res){
                return __success('删除成功！');
            }else{
                return __success('删除失败！');
            }
        }else{
            return __error('参数缺失！');
        }
    }
}
