<?php
namespace app\api\controller;
use think\Controller;
use think\Db;

class Index extends Controller
{
    public function index()
    {
       echo 111;die;  
    }
    public function Login(){
        if($this->request->isPost()){
            $data=$this->request->post();
            if(@$data){
                if(!@$data['username']){
                    return __error('用户不能为空');
                }
                if(!@$data['password']){
                    return __error('密码不能为空');
                }
                $user = model('User')->login($data['username'],$data['password']);
                if($user['code'] == 1){
                    return __error($user['msg']);
                }
                //储存session数据
    //            $login['user']['login_time'] = time(); // 登录时间
                cache('employee_'.$user['user']['token'],$user['user']['token'],7 * 24 * 60 * 60);
                cache($user['user']['token'], $user['info'],7 * 24 * 60 * 60);
                return __success($user['msg'], $user['user']);
            }else {
                return __error('参数不能为空！');
            }
        }else {
          return __error('methond错误');
        }
    }
    public function clearDataBase(){
        Db::query('truncate table wy_bd_info');
        Db::query('truncate table wy_building');
        Db::query('truncate table wy_car');
        Db::query('truncate table wy_car_area');
        Db::query('truncate table wy_his_payment');
        Db::query('truncate table wy_house_user');
        Db::query('truncate table wy_houses');
        Db::query('truncate table wy_houses_user_middle');
        Db::query('truncate table wy_labels');
        Db::query('truncate table wy_labels_middle');
        Db::query('truncate table wy_no_payment');
        Db::query('truncate table wy_payment');
        Db::query('truncate table wy_payment_store');
        Db::query('truncate table wy_work');
        Db::query('truncate table wy_unit');
        Db::query('truncate table wy_toll_bz_middle');
        Db::query('truncate table wy_copy_table');
        Db::query('truncate table wy_work');
        // Db::query('truncate table wy_toll_formula_user');
        // Db::query('truncate table wy_toll_model');
        // Db::query('truncate table wy_toll_project');
        // Db::query('truncate table wy_toll_project_detial');
        // Db::query('truncate table wy_deposit');
        // Db::query('truncate table wy_deposit_list');
        // Db::query('truncate table wy_deposit_list_store');
        // Db::query('truncate table wy_deposit_set');
        return __success('清空成功');
    }
}