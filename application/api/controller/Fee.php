<?php
namespace app\api\controller;
use app\common\controller\ApiController;
class Fee extends ApiController{
    protected $ar_id='';
    protected $cm_mg_id='';
    protected $model='';
    public function __construct() {
        parent::__construct();
        $this->model=model('fee');
        $this->ar_id = request()->param('ar_id');
        $this->cm_mg_id = request()->param('cm_mg_id');
        if(!$this->ar_id){
            echo json_encode(['code' => 1, 'msg' => '物业id不能为空', 'data' => []]);die;
        }
        if(!$this->cm_mg_id){
            echo json_encode(['code' => 1, 'msg' => '小区id不能为空', 'data' => []]);die;
        }
    }

    /**
     * 获取交费项目
     * @return \think\response\Json
     */
    public function get_c_type(){
        $map=[['is_deleted','=',0],['pms_ar_id','=',$this->ar_id],['cm_message_id','=',$this->cm_mg_id]];
        $list=$this->model->getFeeTypeList($map);
        return __success('获取成功！',$list);
    }

    /**
     * 获取列表交费项目
     * @return \think\response\Json
     */
    public function get_type_item(){
        $res['skipItem']=$this->model->getFeeItemsIdList();
        $res['goItem']=$this->model->getFeeItemsIdGoList();
        return __success('获取成功！',$res);
    }

    /**
     * 保存修改交费项目
     * @return \think\response\Json
     */
    public function save_fee_type(){
        if($this->request->isPost()){
            $savedata=$this->request->post();
            if(@$savedata['saveData']){
                $res=$this->model->save($savedata['saveData']);
                if($res){
                    return __success('添加成功！');
                }else{
                    return __success('添加失败！');
                }
            }else{
              return __error('参数缺失！');
            }
        }else{
            $savedata=$this->request->put();
            if(@$savedata['saveData']){
                $res=$this->model->isUpdate(true)->save($savedata['saveData']);
                if($res){
                    return __success('修改成功！');
                }else{
                    return __success('修改失败！');
                }
            }else{
                return __error('参数缺失！');
            }
        }
    }

    /**
     * 删除交费项目
     * @return \think\response\Json
     */
    public function del_fee_type(){
        $id=$this->request->delete('id');
        if(@$id){
            $res=$this->model->isUpdate(true)->save(['id'=>$id,'is_deleted'=>1]);
            if($res){
                return __success('删除成功！');
            }else{
                return __success('删除失败！');
            }
        }else{
            return __error('参数缺失！');
        }
    }
    /**
     * 获取小区树状列表
     * @return \think\response\Json
     */
    public function getTree(){
        $feeType=$this->request->get('feeType');
        $commonMap=[['is_deleted','=','0'],['status','=','1']];
        $periodinfo=db('period_message')->where('cm_message_id',$this->cm_mg_id)->where($commonMap)->select();
        $baseinfo=[];
        foreach ($periodinfo as $k=>$v){
            $flag=1;
            $bdInfo=db('bd_message')->where('period_message_id',$v['id'])->where('cm_message_id',$this->cm_mg_id)->where($commonMap)->field('id,name')->select();
            $newbdInfo=[];
            foreach ($bdInfo as $Ks=>$Vs){
                $newbdInfo[$Ks]['title']=$Vs['name'];
                $newbdInfo[$Ks]['key']=$Vs['id'];
            }
            foreach ($newbdInfo as $key=>$value){
                $unInfo=db('unit_message')->where('bd_message_id',$value['key'])->field('id,info')->where($commonMap)->select();
                $newunInfo=[];
                foreach ($unInfo as $Kds=>$Vds){
                    $newunInfo[$Kds]['title']=$Vds['info'];
                    $newunInfo[$Kds]['key']=$Vds['id'];
                    $houseInfo=db('houses')->where('unit_message_id',$Vds['id'])->where('type',$feeType)->where($commonMap)->field('id,name')->select();
                    $newhouseinfo=[];
                    if($houseInfo){
                        foreach ($houseInfo as $kll=>$vll){
                            $newhouseinfo[$kll]['title']=$vll['name'];
                            $newhouseinfo[$kll]['key']=$vll['id'];
                        }
                        $newunInfo[$Kds]['children']=$newhouseinfo;
                        $flag=1;
                    }else{
                        $flag=0;
                    }
                    
                }
                $newbdInfo[$key]['children']=$newunInfo;
            }
            if($flag){
                $baseinfo[]=array(
                    'title'=>$v['name'],
                    'key'=>$v['id'],
                    'children'=>$newbdInfo
                );
            }
        }
       return __success('成功',$baseinfo);

    }
    /**
     * 缴费搜索
     * 
     */
    public function search_fee(){
        $key=$this->request->get('keyword');
        $type=$this->request->get('type');
        switch ($type){
            case '1':
                $runModel=model('houses');
                $info=$runModel->where('name','like',$key.'%')->where('cm_message_id',$this->cm_mg_id)->limit(10)->select();
                return __success('成功！',$info);
                break;
            // TODO 待加入其它方式
        }
    }
    /**
     * 房间信息
     */
    public function house_info()
    {
        $id=$this->request->get('id');
        $runModel=model('houses');
        $houseInfo=$runModel->where('cm_message_id',$this->cm_mg_id)->where('id',$id)->find();
        $houseInfo['labels_id_text']=$runModel->GetLabInfo($houseInfo['labels_id']);
        if($houseInfo){
            return __success('成功',$houseInfo);
        }else{
            return __error('未找到数据');
        }
       
    }
    /**
     * 缴费列表
     */
    public function house_info_fee_list(){
        $id=$this->request->get('id');
        $runModel=model('cm_fee');
        $data=$runModel->getHouseFee($id,$this->cm_mg_id);
        foreach($data as $k=>$v){
            $data[$k]['feeTimes']=$v['start_day'].'至'.$v['end_day'];
            $data[$k]['id']=$k+1;
        }
        $retarr=[
            'data'=>$data,
            'pageSize'=>10,
            'pageNo'=>1,
            'totalPage'=>1,
            'totalCount'=>10
        ];
        return json($retarr);
    }
    public function get_fee_order_base(){
        $cmDiscount=db('cm_discount')->where('is_deleted','0')->select();
        $payItem=model('fee_items')->where('is_deleted','0')->where('type','2')->select();
        return json(['payItem'=>$payItem,'cmDiscount'=>$cmDiscount]);
    }
}
