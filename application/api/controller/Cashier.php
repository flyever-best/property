<?php
/**
 * Created by PhpStorm.
 * User: chenzhidong
 * Date: 2019/10/4
 * Time: 12:58 AM
 */
namespace app\api\controller;
use app\common\controller\ApiController;
use think\Db;
use function ytwl\e;

class Cashier extends ApiController {

    // 收银台搜索
    public function search_info() {
        $data = $this->request->post();
        unset($data['token']);
        if($data['value']){
            switch (intval($data['type'])) {
                case 1:
                    $info = model('wy_houses')->alias('h')
                        ->join('wy_bd_info b','h.bd_info_id = b.id')
                        ->join('wy_building u','h.building_id = u.id')
                        ->join('wy_unit n','h.unit_id = n.id')
                        ->where('h.name', 'LIKE', $data['value'].'%')
                        ->where('h.is_deleted',0)
                        ->field('h.id as value,b.name as text,h.name as text1, u.name as text2, n.name as text3')
                        ->limit(20)
                        ->select()->each(function($item,$key){
                            $item['text'] =$item['text'].'-'.$item['text1'];
                        });
                    
                    break;
                case 2:
                    $info = model('wy_car')
                        ->where('name', 'LIKE', '%'.$data['value'].'%')
                        ->where('is_deleted',0)
                        ->field('id as value, name as text, bd_info_id')
                        ->limit(20)
                        ->select();
                    foreach($info as $k => $v) {
                        $info[$k]['text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name').'-'.$v['text'];
                        unset($info[$k]['bd_info_id']);
                    }
                    break;
                case 3:
                    $info = model('wy_car_area')
                        ->where('name', 'LIKE', '%'.$data['value'].'%')
                        ->where('is_deleted',0)
                        ->field('id as value, name as text, bd_info_id')
                        ->limit(20)
                        ->select();
                    foreach($info as $k => $v) {
                        $info[$k]['text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name').'-'.$v['text'];
                        unset($info[$k]['bd_info_id']);
                    }
                    break;
                case 4:
                    $info = model('wy_house_user')
                        ->where('name', 'LIKE', '%'.$data['value'].'%')
                        ->whereOr('phone', 'LIKE', '%'.$data['value'].'%')
                        ->field('id as value,name,bd_info_id,houses_id,phone')
                        ->limit(20)
                        ->select();
                    foreach($info as $k => $v) {
                        if (@$v['bd_info_id'] > 0 && @$v['houses_id'] > 0) {
                            $bd = Db::name('wy_bd_info')->where('id',$v['bd_info_id'])->value('name') ?: '';
                            $hou = Db::name('wy_houses')->where('id',$v['houses_id'])->value('name') ?: '';
                            $info[$k]['text'] = $bd.'-'.$hou.'-'.$v['name'].'-'.$v['phone'];
                        } else {
                            $info[$k]['text'] = '非住户-'.$v['name'].'-'.$v['phone'];
                        }
                        unset($info[$k]['bd_info_id']);
                    }
                    break;
            }
            if(empty($data)) {
                return __error('暂无数据');
            } else {
                return __success('查询成功', $info);
            }
        }else {
            return __success('查询成功', []);
        }
        
    }
    public function get_no_pay_info(){
        $data = $this->request->post();
        unset($data['token']);
        if($data['type']==2){
            // 欠费信息
            $noPayFields = 'id, house_id, car_id, car_area_id, name, start_time, end_time, one_money, num, price, offer, overdue, deduction, remarks, type, money';
            $info = Db::name('wy_no_payment')
            ->field($noPayFields)
            ->where('car_area_id', $data['id'])
            ->where('pay_status',0)
            ->where('is_deleted',0)
            ->order('id desc')
            ->page($data['pageNo'],$data['pageSize'])
            ->select();
            $count= Db::name('wy_no_payment')
            ->where('car_area_id', $data['id'])
            ->where('pay_status',0)
            ->where('is_deleted',0)
            ->count();
            $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
            foreach($info as $k => $v) {
                $remark_list = Db::name('wy_no_payment_remarks')->where('no_payment_id',$v['id'])->column('name');
                $info[$k]['remark_list'] = !empty($remark_list) ? $remark_list : ['还没有备注内容'];
                $info[$k]['type_text'] = '车位';
                $info[$k]['time'] = $v['start_time'].'/'.$v['end_time'];
                if(!empty($v['car_area_id'])) {
                    $info[$k]['info'] = Db::name('wy_houses')->where('id', $v['car_area_id'])->value('name');
                    unset($info[$k]['house_id']);
                }
            }
        }elseif($data['type']==3){
            // 欠费信息
            $noPayFields = 'id, house_id, car_id, car_area_id, name, start_time, end_time, one_money, num, price, offer, overdue, deduction, remarks, type, money';
            $info = Db::name('wy_no_payment')
            ->field($noPayFields)
            ->where('car_id', $data['id'])
            ->where('pay_status',0)
            ->where('is_deleted',0)
            ->order('id desc')
            ->page($data['pageNo'],$data['pageSize'])
            ->select();
            $count=Db::name('wy_no_payment')
            ->where('car_id', $data['id'])
            ->where('is_deleted',0)
            ->where('pay_status',0)
            ->count();
            $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
            foreach($info as $k => $v) {
                $remark_list = Db::name('wy_no_payment_remarks')->where('no_payment_id',$v['id'])->column('name');
                $info[$k]['remark_list'] = !empty($remark_list) ? $remark_list : ['还没有备注内容'];
                $info[$k]['type_text'] = '车辆';
                $info[$k]['time'] = $v['start_time'].'/'.$v['end_time'];
                if(!empty($v['car_id'])) {
                    $info[$k]['info'] = Db::name('wy_car')->where('id', $v['car_id'])->value('name');
                    unset($info[$k]['house_id']);
                }
            }
        }elseif($data['type']==4){
            // 欠费信息
            $noPayFields = 'id, house_id, car_id, car_area_id, name, start_time, end_time, one_money, num, price, offer, overdue, deduction, remarks, type, money';
            $info = Db::name('wy_no_payment')
            ->field($noPayFields)
            ->where('house_user_id', $data['id'])
            ->where('pay_status',0)
            ->where('is_deleted',0)
            ->order('id desc')
            ->page($data['pageNo'],$data['pageSize'])
            ->select();
            $count=Db::name('wy_no_payment')
            ->where('house_user_id', $data['id'])
            ->where('pay_status',0)
            ->where('is_deleted',0)
            ->count();
            $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
            foreach($info as $k => $v) {
                $remark_list = Db::name('wy_no_payment_remarks')->where('no_payment_id',$v['id'])->column('name');
                $info[$k]['remark_list'] = !empty($remark_list) ? $remark_list : ['还没有备注内容'];
                $info[$k]['type_text'] = '住户';
                $info[$k]['time'] = $v['start_time'].'/'.$v['end_time'];
                if(!empty($v['house_id'])) {
                    $info[$k]['info'] = Db::name('wy_houses')->where('id', $v['house_id'])->value('name');
                    unset($info[$k]['house_id']);
                }
            }
        }else{
             // 欠费信息
             $noPayFields = 'id, house_id, car_id, car_area_id, name, start_time, end_time, one_money, num, price, offer, overdue, deduction, remarks, type, money';
             $info = Db::name('wy_no_payment')
             ->field($noPayFields)
             ->where('house_id', $data['id'])
             ->where('pay_status',0)
             ->where('is_deleted',0)
             ->order('id desc')
             ->page($data['pageNo'],$data['pageSize'])
             ->select();
            //  echo Db::name('wy_no_payment')->getlastsql();die;
             $count=Db::name('wy_no_payment')
             ->where('house_id', $data['id'])
             ->where('pay_status',0)
             ->where('is_deleted',0)
             ->count();
             $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
             // dump($info);
             foreach($info as $k => $v) {
                 $remark_list = Db::name('wy_no_payment_remarks')->where('no_payment_id',$v['id'])->column('name');
                 if ($remark_list) $info[$k]['remark_list'] = !empty($v['remarks']) ? array_merge([$v['remarks']],$remark_list) : $remark_list;
                 if (!$remark_list) $info[$k]['remark_list'] = !empty($v['remarks']) ? [$v['remarks']] : ['还没有备注内容'];
                 $info[$k]['type_text'] = '房屋';
                 $info[$k]['time'] = $v['start_time'].'/'.$v['end_time'];
                 if(!empty($v['house_id'])) {
                     $info[$k]['info'] = Db::name('wy_houses')->where('id', $v['house_id'])->value('name');
                     unset($info[$k]['house_id']);
                 }
             }
        }
        return __success('查询成功', ['data'=>$info,'pageSize'=>$data['pageSize'],'pageNo'=>$data['pageNo'],'totalCount'=>$count,'totalPage'=>ceil($count / $data['pageSize'])]);
    }

    // 收银台房屋详情
    public function house_info() {
        $data = $this->request->post();
        unset($data['token']);
        if($data['type']==2){
            $housemodel=model('wy_car_area');
            $info = $housemodel->where('id', $data['id'])->find()->toArray();
            if ($info['labels_id']) {
                $info['labels'] = Db::name('wy_labels')->where('id',$info['labels_id'])->value('name');
            } else {
                $info['labels'] = '';
            }
            $info['bd_info_text'] = Db::name('wy_bd_info')->where('id', $info['bd_info_id'])->value('name');
            $info['users'] = Db::name('wy_houses_user_middle m')
            ->join('wy_house_user u','u.id=m.house_user_id')
            ->where('m.car_area_id', $data['id'])
            ->field('u.id,u.name,u.phone,u.money')->group('m.house_user_id')->select();
            $info['user_text']='';
            foreach($info['users'] as $k =>$v){
                if (empty($v['money'])) {
                    $info['users'][$k]['money'] = 0;
                }
                if( $info['user_text']){
                    $info['user_text'].=','.$v['name'].'('.$v['phone'].')';
                }else {
                        $info['user_text'].=$v['name'].'('.$v['phone'].')';
                }
            }
            // 欠费信息
            $noPayFields = 'id, house_id, car_id, car_area_id, name, start_time, end_time, one_money, num, price, offer, overdue, deduction, remarks, type, money';
            $info['no_pay'] = Db::name('wy_no_payment')->field($noPayFields)->where('car_area_id', $data['id'])->where('pay_status',0)->select();
            $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
            foreach($info['no_pay'] as $k => $v) {
                $info['no_pay'][$k]['type_text'] = $type_text[$v['type']];
                $info['no_pay'][$k]['time'] = $v['start_time'].'/'.$v['end_time'];
                if(!empty($v['car_area_id'])) {
                    $info['no_pay'][$k]['info'] = Db::name('wy_houses')->where('id', $v['car_area_id'])->value('name');
                    unset($info['no_pay'][$k]['house_id']);
                }
            }
            $modelField = 'id,name,type,paper_type,layout,num,range_t,range_r,range_b,range_l,width,remark';
            $info['model'] = Db::name('wy_toll_model')->where('status',0)->field($modelField)->select();
            foreach ($info['model'] as $k=>$v) {
                $paper_det = $this->model_paper_detail($v['paper_type']);
                $info['model'][$k]['p_width'] = $paper_det['width'];
                $info['model'][$k]['p_height'] = $paper_det['height'];
            }
        }elseif ($data['type']==3) {
            $housemodel=model('wy_car');
            $info = $housemodel->where('id', $data['id'])->find()->toArray();
            $info['bd_info_text'] = Db::name('wy_bd_info')->where('id', $info['bd_info_id'])->value('name');
            $info['users'] = Db::name('wy_houses_user_middle m')
            ->join('wy_house_user u','u.id=m.house_user_id')
            ->where('m.car_id', $data['id'])
            ->field('u.id,u.name,u.phone,u.money')->group('m.house_user_id')->select();
            $info['user_text']='';
            foreach($info['users'] as $k =>$v){
                if (empty($v['money'])) {
                    $info['users'][$k]['money'] = 0;
                }
                if( $info['user_text']){
                    $info['user_text'].=','.$v['name'].'('.$v['phone'].')';
                }else {
                    $info['user_text'].=$v['name'].'('.$v['phone'].')';
                }
            }
            // 欠费信息
            $noPayFields = 'id, house_id, car_id, car_area_id, name, start_time, end_time, one_money, num, price, offer, overdue, deduction, remarks, type, money';
            $info['no_pay'] = Db::name('wy_no_payment')->field($noPayFields)->where('car_id', $data['id'])->where('pay_status',0)->select();
            $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
            foreach($info['no_pay'] as $k => $v) {
                $info['no_pay'][$k]['type_text'] = $type_text[$v['type']];
                $info['no_pay'][$k]['time'] = $v['start_time'].'/'.$v['end_time'];
                if(!empty($v['car_id'])) {
                    $info['no_pay'][$k]['info'] = Db::name('wy_car')->where('id', $v['car_id'])->value('name');
                    unset($info['no_pay'][$k]['house_id']);
                }
            }
            $modelField = 'id,name,type,paper_type,layout,num,range_t,range_r,range_b,range_l,width,remark';
            $info['model'] = Db::name('wy_toll_model')->where('status',0)->field($modelField)->select();
            foreach ($info['model'] as $k=>$v) {
                $paper_det = $this->model_paper_detail($v['paper_type']);
                $info['model'][$k]['p_width'] = $paper_det['width'];
                $info['model'][$k]['p_height'] = $paper_det['height'];
            }
        }elseif ($data['type']==4) {
            $housemodel=model('wy_house_user');
            $info = $housemodel->where('id', $data['id'])->find()->toArray();
            // 欠费信息
            $noPayFields = 'id, house_id, car_id, car_area_id, name, start_time, end_time, one_money, num, price, offer, overdue, deduction, remarks, type, money';
            $info['no_pay'] = Db::name('wy_no_payment')->field($noPayFields)->where('house_user_id', $data['id'])->where('pay_status',0)->select();
            $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
            foreach($info['no_pay'] as $k => $v) {
                $info['no_pay'][$k]['type_text'] = $type_text[$v['type']];
                $info['no_pay'][$k]['time'] = $v['start_time'].'/'.$v['end_time'];
                if(!empty($v['house_id'])) {
                    $info['no_pay'][$k]['info'] = Db::name('wy_houses')->where('id', $v['house_id'])->value('name');
                    unset($info['no_pay'][$k]['house_id']);
                }
            }
            $modelField = 'id,name,type,paper_type,layout,num,range_t,range_r,range_b,range_l,width,remark';
            $info['model'] = Db::name('wy_toll_model')->where('status',0)->field($modelField)->select();
            foreach ($info['model'] as $k=>$v) {
                $paper_det = $this->model_paper_detail($v['paper_type']);
                $info['model'][$k]['p_width'] = $paper_det['width'];
                $info['model'][$k]['p_height'] = $paper_det['height'];
            }
        }else{
            $housemodel=model('WyHouses');
            $info = $housemodel->where('id', $data['id'])->find()->toArray();
            if ($info['labels']) {
                $info['labels'] = Db::name('wy_labels')->where('id',$info['labels'])->value('name');
            } else {
                $info['labels'] = '';
            }
            // // 所属小区
            $info['bd_info_text'] = Db::name('wy_bd_info')->where('id', $info['bd_info_id'])->value('name');
            // // 所属楼宇
            $info['building_text'] = Db::name('wy_building')->where('id', $info['building_id'])->value('name');
            // 住户
            $info['users'] = Db::name('wy_houses_user_middle m')
            ->join('wy_house_user u','u.id=m.house_user_id')
            ->where('m.houses_id', $data['id'])
            ->field('u.id,u.name,u.phone,u.money')->group('m.house_user_id')->select();
            $info['user_text']='';
            foreach($info['users'] as $k =>$v){
                if (empty($v['money'])) {
                    $info['users'][$k]['money'] = 0;
                }
                if( $info['user_text']){
                    $info['user_text'].=','.$v['name'].'('.$v['phone'].')';
                }else {
                        $info['user_text'].=$v['name'].'('.$v['phone'].')';
                }
            }

            // 欠费信息
            $noPayFields = 'id, house_id, car_id, car_area_id, name, start_time, end_time, one_money, num, price, offer, overdue, deduction, remarks, type, money';
            $info['no_pay'] = Db::name('wy_no_payment')->field($noPayFields)->where('house_id', $data['id'])->where('pay_status',0)->order('id DESC')->select();
            $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
            // dump($info['no_pay']);
            foreach($info['no_pay'] as $k => $v) {
                $info['no_pay'][$k]['type_text'] = $type_text[$v['type']];
                $info['no_pay'][$k]['time'] = $v['start_time'].'/'.$v['end_time'];
                if(!empty($v['house_id'])) {
                    $info['no_pay'][$k]['info'] = Db::name('wy_houses')->where('id', $v['house_id'])->value('name');
                    unset($info['no_pay'][$k]['house_id']);
                }
            }
            $modelField = 'id,name,type,paper_type,layout,num,range_t,range_r,range_b,range_l,width,remark';
            $info['model'] = Db::name('wy_toll_model')->where('status',0)->field($modelField)->select();
            foreach ($info['model'] as $k=>$v) {
                $paper_det = $this->model_paper_detail($v['paper_type']);
                $info['model'][$k]['p_width'] = $paper_det['width'];
                $info['model'][$k]['p_height'] = $paper_det['height'];
            }
        }
        
        return __success('查询成功', $info);
    }

    // 获取固定收费规则
    public function get_pay_formula_list() {
        $id=$this->request->get('id');
        if(@$id){
            $info=Db::name('wy_toll_formula_user')->where('id',$id)->find();
            $data = Db::name('wy_toll_formula_name')->where('id',$info['wy_toll_formula_name_id'])->find();
            $data['ext']=$info;
            if(empty($data)) {
                return __error('暂无数据');
            } else {
                return __success('查询成功', $data);
            }
        }else{
            $data = Db::name('wy_toll_formula_name')->select();
            if(empty($data)) {
                return __error('暂无数据');
            } else {
                return __success('查询成功', $data);
            }
        }
        
    }

    // 获取自定义公式列表
    public function get_toll_formula() {
        $param = $this->request->post();
        $where = [];
        if(!empty($param['name'])) $where[] = ['name', 'LIKE', '%'.$param['name'].'%'];
        $data = Db::name('wy_toll_formula_user')->field('id, name, remarks')->where($where)->select();
        if(empty($data)) {
            return __error('暂无数据');
        } else {
            return __success('查询成功', $data);
        }
    }

    // 添加保存自定义公式
    public function save_toll_formula() {
        $data = $this->request->post();
        unset($data['token']);
        if(@$data['id']){
            $wy_toll_formula_name_id = Db::name('wy_toll_formula_name')->where('unit', $data['for_name'])->value('id');
            $params = $data;
            $params['wy_toll_formula_name_id'] = $wy_toll_formula_name_id;
            unset($params['for_name']);
            $res=model('TollFormulaUser')->isUpdate(true)->save($params);
            if(empty($res)) {
                return __error('修改失败');
            } else {
                return __success('修改成功');
            }
        }else{
            $id = Db::name('wy_toll_formula_name')->where('unit', $data['for_name'])->value('id');
            $params = $data;
            $params['wy_toll_formula_name_id'] = $id;
            unset($params['for_name']);
            $adStatus = Db::name('wy_toll_formula_user')->insert($params);
            if(empty($adStatus)) {
                return __error('保存失败');
            } else {
                return __success('保存成功');
            }
        }
        
        
    }

    // 获取未缴费账单
    public function get_no_pay_list() {
        $page = $this->request->get('pageNo','1');
        $pageSize = $this->request->get('pageSize','10');
        $mobile = $this->request->get('name');
        $type = $this->request->get('type');
        $lab_id = $this->request->get('lab_id');
        $pro_det_id = $this->request->get('pro_det_id');
        $where = [];
        if ($type) {
            if ($type == 1) {
                $where[] = ['type','in','0,1'];
            } else {
                $where[] = ['type','=',$type];
            }
        }
        if ($pro_det_id) {
            $where[] = ['toll_project_detial_id','=',$pro_det_id];
        }
        $start = $this->request->get('start_time');
        $end = $this->request->get('end_time');
        if ($start && $end) {
            $where[] = ['pay_time','between',$start.','.$end];
            $where[] = ['pay_status','=',1];
        } else {
            $where[] = ['pay_status', '=', 0];
        }
        $jine_max = $this->request->get('jine_max');
        $jine_min = $this->request->get('jine_min');
        if ($jine_max && $jine_min >= 0) {
            $where[] = ['price','between',$jine_min.','.$jine_max];
        }
        $gj_name_value = $this->request->get('gj_name_value');
        if ($gj_name_value) {
//            $where[] = ['name','=',$gj_name_value];
            $mobile = $gj_name_value;
        }
        $status = $this->request->get('status');
        if ($status) {
            $where[] = ['status','=',$status];
        }
        $sou = $this->request->get('sou');
        if ($sou) {
            $where[] = ['pay_style','=',$sou];
        }
        $bd_id = $this->request->get('bd_id');
        if ($bd_id) {
            $where[] = ['bd_info_id','in',$bd_id];
        }
        $models = model('no_pay');
        $data = $models->getList($page,$where,$mobile,$pageSize,$lab_id);
        $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
        $data['x_p'] = 0;
        $data['x_m'] = 0;
        foreach($data['data'] as $k => $v) {
            $remark_list = Db::name('wy_no_payment_remarks')->where('no_payment_id',$v['id'])->column('name');
            if ($v['remarks']) {
                $data['data'][$k]['remark_list'] = !empty($remark_list) ? array_merge([$v['remarks']],$remark_list) : [$v['remarks']];
            } else {
                $data['data'][$k]['remark_list'] = !empty($remark_list) ? $remark_list : ['还没有备注内容'];
            }
            $data['x_p'] += $v['price'];
            $data['x_m'] += $v['money'];
            $data['data'][$k]['price'] = '¥'.$v['price'];
            $data['data'][$k]['money'] = '¥'.$v['money'];
            $data['data'][$k]['bd_bu_text'] = '';
            $data['data'][$k]['user_all_info'] = '';
            $data['data'][$k]['name_time'] = '';
            $data['data'][$k]['type_text'] = $type_text[$v['type']];
            if($v['bd_info_id'] > 0) $data['data'][$k]['bd_bu_text'] = $data['data'][$k]['bd_bu_text'].' '.Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name');
//            if($v['building_id'] > 0) $data['data'][$k]['bd_bu_text'] = $data['data'][$k]['bd_bu_text'].' '.Db::name('wy_building')->where('id', $v['building_id'])->value('name');
            if($v['house_id'] > 0) {
                $house = Db::name('wy_houses')->where('id', $v['house_id'])->field('area,name')->find();
                $data['data'][$k]['user_all_info'] = $data['data'][$k]['user_all_info'].' '.$house['name'];
                if ($v['num'] > 0) {
                } else {
                    $data['data'][$k]['num'] = $house['area'];
                }
            }
            if ($v['one_money'] == 0) {
                $data['data'][$k]['num'] = 0;
            }
            if ($v['house_id'] > 0) {
                $data['data'][$k]['info'] = '房屋:';
            } elseif ($v['car_area_id'] > 0) {
                $data['data'][$k]['info'] = '车位:';
            } elseif ($v['car_id'] > 0) {
                $data['data'][$k]['info'] = '车辆:';
            } else {
                $data['data'][$k]['info'] = '';
            }
            $data['data'][$k]['label'] = '还没有标签';
            if($v['house_id'] > 0) {
//                $uid = Db::name('wy_house_user')->where('houses_id','=',$v['house_id'])->column('id');
                $hs = Db::name('wy_houses')->where('id', $v['house_id'])->field('building_id,unit_id,name,labels')->find();
                $bd = Db::name('wy_building')->where('id', $hs['building_id'])->value('name') ?: '';
                $ut = Db::name('wy_unit')->where('id', $hs['unit_id'])->value('name') ?: '';
                $data['data'][$k]['info'] = $data['data'][$k]['info'].$bd.'|'.$ut.'|'.$hs['name'];
                $data['data'][$k]['label'] = Db::name('wy_labels')->where('id',$hs['labels'])->value('name') ?: '还没有标签';
//                    Db::name('wy_labels_middle')->alias('m')->join('wy_labels s','s.id=m.labels_id')
//                    ->where('s.type_name',1)
//                    ->where('m.house_user_id',implode(',',$uid))->value('s.name') ?: '还没有标签';
            }
            if($v['car_area_id'] > 0) {
                $car_area_det = Db::name('wy_car_area')->where('id', $v['car_area_id'])->field('name,labels_id')->find();
                $car_area_name = $car_area_det['name'];
                $data['data'][$k]['info'] = $data['data'][$k]['info'].' '.$car_area_name;
                $data['data'][$k]['user_all_info'] = $data['data'][$k]['user_all_info'].' '.$car_area_name;
                $data['data'][$k]['label'] = Db::name('wy_labels')->where('id',$car_area_det['labels_id'])->value('name') ?: '还没有标签';
//                    Db::name('wy_labels_middle')->alias('m')->join('wy_labels s','s.id=m.labels_id')
//                    ->where('s.type_name',2)
//                    ->where('m.house_user_id',$v['house_user_id'])->value('s.name') ?: '还没有标签';
            }
            if($v['car_id'] > 0) {
                $car_det = Db::name('wy_car')->where('id', $v['car_id'])->field('name,labels_id')->find();
                $car_name = $car_det['name'];
                $data['data'][$k]['info'] = $data['data'][$k]['info'].' '.$car_name;
                $data['data'][$k]['user_all_info'] = $data['data'][$k]['user_all_info'].' '.$car_name;
                $data['data'][$k]['label'] = Db::name('wy_labels')->where('id',$car_det['labels_id'])->value('name') ?: '还没有标签';
//                    Db::name('wy_labels_middle')->alias('m')->join('wy_labels s','s.id=m.labels_id')
//                    ->where('s.type_name',3)
//                    ->where('m.house_user_id',$v['house_user_id'])->value('s.name') ?: '还没有标签';
            }
            $data['data'][$k]['house_user_list'] = [];
            if($v['house_user_id'] > 0) {
                $user = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name');
                $data['data'][$k]['user_all_info'] = $data['data'][$k]['user_all_info'].' '.$user;
                $data['data'][$k]['house_user_text'] = $user;
                $data['data'][$k]['label'] = Db::name('wy_labels_middle')->alias('m')->join('wy_labels s','s.id=m.labels_id')
                    ->where('s.type_name',4)
                    ->where('m.house_user_id',$v['house_user_id'])->value('s.name') ?: '还没有标签';
            } else {
                $user=Db::name('wy_houses_user_middle')->alias('m')
                    ->join('wy_house_user u','u.id = m.house_user_id')
                    ->join('wy_houses h','m.houses_id = h.id')
                    ->join('wy_bd_info bd','h.bd_info_id = bd.id')
                    ->join('wy_building b','h.building_id = b.id')
                    ->join('wy_unit un','h.unit_id = un.id')
                    ->where('m.houses_id','=',$v['house_id'])
                    ->where(['m.middle_type'=>1])
                    ->field('u.*,bd.name bd_info_name,b.name building_name,un.name unit_name,h.name houses_name,h.id as houses_ids')
                    ->order('u.id','desc')->select();
//                $user = Db::name('wy_house_user')->where('houses_id', $v['house_id'])->field('name,phone')->select();
                $data['data'][$k]['house_user_list'] = $user;
                $data['data'][$k]['house_user_text'] = '';
//                $data['data'][$k]['tel'] = $user['phone'];
            }
            $data['data'][$k]['name_time'] = $v['name'].' '.$v['createtime'];
        }
        $data['x_p'] = round($data['x_p'],2);
        $data['x_m'] = round($data['x_m'],2);
        if(empty($data['data'])) {
            return __error('暂无数据');
        } else {
            return __success('查询成功',  $data);
        }
    }

    // 获取已缴费账单
    public function get_pay_list() {
        $page = $this->request->get('pageNo','1');
        $pageSize = $this->request->get('pageSize','10');
        $mobile = $this->request->get('name');
        $type = $this->request->get('type');
        $where = [];
        if ($type) {
            if ($type == 1) {
                $where[] = ['houses_id','gt',0];
            } elseif ($type == 2) {
                $where[] = ['car_area_id','gt',0];
            } elseif ($type == 3) {
                $where[] = ['car_id','gt',0];
            } elseif ($type == 4) {
                $where[] = ['house_user_id','gt',0];
            } elseif ($type == 5) {
                $pid = Db::name('wy_payment_store')->where('type','=','其它收费')->column('payment_id');
                $where[] = ['id','in',implode(',',$pid)];
            } elseif ($type ==  6) {
                $pid = Db::name('wy_payment_store')->where('type','=','临时停车费')->column('payment_id');
                $where[] = ['id','in',implode(',',$pid)];
            }
        }
        $start = $this->request->get('start_time');
        $end = $this->request->get('end_time');
        if ($start && $end) {
            $where[] = ['pay_time','between',$start.','.$end];
        }
        $jine_max = $this->request->get('jine_max');
        $jine_min = $this->request->get('jine_min');
        if ($jine_max && $jine_min >= 0) {
            $where[] = ['price','between',$jine_min.','.$jine_max];
        }
        $jine_max_s = $this->request->get('jine_max_s');
        $jine_min_s = $this->request->get('jine_min_s');
        if ($jine_max_s && $jine_min_s >= 0) {
            $where[] = ['money','between',$jine_min_s.','.$jine_max_s];
        }
        $gj_name_value = $this->request->get('gj_name_value');
        if ($gj_name_value) {
            $mobile = $gj_name_value;
        }
        $status = $this->request->get('status');
        if ($status) {
//            $all_sta = ['1'=>'已缴','2'=>'退款','3'=>'撤销'];
            $where[] = ['status','=',$status];
        }
        $admin = $this->request->get('admin');
        if ($admin && $admin != '所有操作员') {
            $where[] = ['action_user','=',$admin];
        }
        $sou = $this->request->get('sou');
        if ($sou) {
            $where[] = ['pay_style','=',$sou];
        }
        $bd_id = $this->request->get('bd_id');
        if ($bd_id) {
//            $hous = model('wy_houses')->where('bd_info_id',$bd_id)->column('id');
            $where[] = ['bd_info_id','=',$bd_id];
        }
        $lab_id = $this->request->get('lab_id');
        $models = model('payment');
        $data = $models->getList($page,$where,$mobile,$pageSize,false,$lab_id);
        $data['x_p'] = 0;
        $data['x_m'] = 0;
        foreach($data['data'] as $k => $v) {
            $remark_list = Db::name('wy_no_payment_remarks')->where('no_payment_id',$v['no_payment_id'])->column('name');
            if ($v['remarks']) {
                $data['data'][$k]['remark_list'] = !empty($remark_list) ? array_merge([$v['remarks']],$remark_list) : [$v['remarks']];
            } else {
                $data['data'][$k]['remark_list'] = !empty($remark_list) ? $remark_list : ['还没有备注内容'];
            }
            if (!$v['remarks']) {
                $data['data'][$k]['remarks'] = '还没有备注内容';
            }
            $data['x_p'] += $v['price'];
            $data['x_m'] += $v['money'];
            $data['data'][$k]['style_text'] = $this->style_text($v['pay_style']);
            $data['data'][$k]['status_text'] = $this->status_text($v['status']);
            if ($v['houses_id'] > 0) {
                $data['data'][$k]['info'] = '房屋:';
            } elseif ($v['car_area_id'] > 0) {
                $data['data'][$k]['info'] = '车位:';
            } elseif ($v['car_id'] > 0) {
                $data['data'][$k]['info'] = '车辆:';
            } else {
                $data['data'][$k]['info'] = $v['name'];
            }
            $data['data'][$k]['price_n'] = $v['price'];
            $data['data'][$k]['price'] = '¥'.$v['price'];
            $data['data'][$k]['money'] = '¥'.$v['money'];
//            if (date('H',strtotime($v['pay_time'])) == 0 && date('i',strtotime($v['pay_time'])) == 0) {
//                $data['data'][$k]['pay_time'] = date('Y年m月d日',strtotime($v['pay_time']));
//            } else {
//                $data['data'][$k]['pay_time'] = date('Y年m月d日H时i分',strtotime($v['pay_time']));
//            }
            $data['data'][$k]['label'] = '还没有标签';
            if($v['bd_info_id'] > 0) $data['data'][$k]['bd_info_id_text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name');
            if($v['houses_id'] > 0) {
                $hs = Db::name('wy_houses')->where('id', $v['houses_id'])->field('building_id,unit_id,name')->find();
                $data['data'][$k]['houses_id_name'] = $hs['name'];
                $bd = Db::name('wy_building')->where('id', $hs['building_id'])->value('name') ?: '';
                $ut = Db::name('wy_unit')->where('id', $hs['unit_id'])->value('name') ?: '';
                $data['data'][$k]['info'] = $data['data'][$k]['info'].$bd.'|'.$ut.'|'.$hs['name'];
                $data['data'][$k]['label'] = Db::name('wy_labels_middle')->alias('m')->join('wy_labels s','s.id=m.labels_id')->where('s.type_name',1)->where('m.house_user_id',$v['house_user_id'])->value('s.name') ?: '还没有标签';
            } else {
                $data['data'][$k]['houses_id_name'] = '';
            }
            if($v['car_area_id'] > 0) {
                $data['data'][$k]['info'] = $data['data'][$k]['info'].' '.Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
                $data['data'][$k]['label'] = Db::name('wy_labels_middle')->alias('m')->join('wy_labels s','s.id=m.labels_id')->where('s.type_name',2)->where('m.house_user_id',$v['house_user_id'])->value('s.name') ?: '还没有标签';
            }
            if($v['car_id'] > 0) {
                $data['data'][$k]['info'] = $data['data'][$k]['info'].' '.Db::name('wy_car')->where('id', $v['car_id'])->value('name');
                $data['data'][$k]['label'] = Db::name('wy_labels_middle')->alias('m')->join('wy_labels s','s.id=m.labels_id')->where('s.type_name',3)->where('m.house_user_id',$v['house_user_id'])->value('s.name') ?: '还没有标签';
            }
            if($v['house_user_id'] > 0) {
                $usr = Db::name('wy_house_user')->where('id', $v['house_user_id'])->field('name,phone')->find();
                $data['data'][$k]['user_text'] = $usr['name'].'('.$usr['phone'].')';
                $data['data'][$k]['u_name'] = $usr['name'];
                $data['data'][$k]['u_phone'] = empty($usr['phone']) ? '' : '('.$usr['phone'].')';
                $data['data'][$k]['label'] = Db::name('wy_labels_middle')->alias('m')->join('wy_labels s','s.id=m.labels_id')->where('s.type_name',4)->where('m.house_user_id',$v['house_user_id'])->value('s.name') ?: '还没有标签';
            } elseif ($v['houses_id'] > 0 && !$v['house_user_id']) {
                $hu_id = Db::name('wy_houses_user_middle')->where('houses_id',$v['houses_id'])->value('house_user_id');
                $usr = Db::name('wy_house_user')->where('id', $hu_id)->field('name,phone')->find();
                $data['data'][$k]['user_text'] = $usr['name'].'('.$usr['phone'].')';
                $data['data'][$k]['u_name'] = $usr['name'];
                $data['data'][$k]['u_phone'] = empty($usr['phone']) ? '' : '('.$usr['phone'].')';
                $data['data'][$k]['u_phone_n'] = empty($usr['phone']) ? '' : $usr['phone'];
            }
            $data['data'][$k]['action_user'] = is_numeric($v['action_user']) ? Db::name('pms_employee')->where('id', $v['action_user'])->value('name') : $v['action_user'];
        }
        $data['x_p'] = round($data['x_p'],2);
        $data['x_m'] = round($data['x_m'],2);
        if(empty($data['data'])) {
            return __error('暂无数据');
        } else {
            return __success('查询成功',  $data);
        }
    }
    public function pay_det(){
        $id = $this->request->post('id');
        $data = Db::name('wy_payment')->where('id',$id)->find();
        if(empty($data)) {
            return __error('暂无数据');
        } else {
            $sta_arr = ['1'=>'已缴','2'=>'退款','3'=>'撤销'];
            if (@$data['houses_id']) {
                $houseinfo=Db::name('wy_houses')->where('id',$data['houses_id'])->find();
                $data['house_text'] = $houseinfo['name']? $houseinfo['name']:'';
            }
            if (@$data['house_user_id']) {
                $data['house_user'] = Db::name('wy_house_user')->where('id',$data['house_user_id'])->find() ?: '';
            } else {
                $data['house_user'] = ['name'=>''];
            }
            if (@$data['bd_info_id']) $data['bd_info_text'] = Db::name('wy_bd_info')->where('id',$data['bd_info_id'])->value('name') ?: '';
            if (@$houseinfo) $data['buiding_text'] = Db::name('wy_building')->where('id',$houseinfo['building_id'])->value('name') ?: '';
            if ($data['houses_id'] > 0) {
                $data['info'] = '房屋:';
            } elseif ($data['car_area_id'] > 0) {
                $data['info'] = '车位:';
            } elseif ($data['car_id'] > 0) {
                $data['info'] = '车牌:';
            } else {
                $data['info'] = '';
            }
            if($data['houses_id'] > 0) {
                $hs = Db::name('wy_houses')->where('id', $data['houses_id'])->field('building_id,unit_id,name')->find();
                $bd = Db::name('wy_building')->where('id', $hs['building_id'])->value('name') ?: '';
                $ut = Db::name('wy_unit')->where('id', $hs['unit_id'])->value('name') ?: '';
                $data['info'] = $data['info'].$bd.'|'.$ut.'|'.$hs['name'];
            }
            if($data['house_user_id'] > 0) {
                $usr = Db::name('wy_house_user')->where('id', $data['house_user_id'])->field('name,phone')->find();
                $data['user_text'] = $usr['name'].'('.$usr['phone'].')';
            }
            if($data['car_area_id'] > 0) $data['info'] = $data['info'].' '.Db::name('wy_car_area')->where('id', $data['car_area_id'])->value('name');
            if($data['car_id'] > 0) $data['info'] = $data['info'].' '.Db::name('wy_car')->where('id', $data['car_id'])->value('name');
            $data['pay_style_text'] = $this->style_text($data['pay_style']);
            $data['status_text'] = $this->status_text($data['status']);
            $data['action_user'] = is_numeric($data['action_user']) ? Db::name('pms_employee')->where('id', $data['action_user'])->value('name') : $data['action_user'];
            return __success('查询成功',  $data);
        }
    }
    public function get_pay_del_list() {
        $page = $this->request->get('pageNo','1');
        $pageSize = $this->request->get('pageSize','1');
        $mobile = $this->request->get('name');
        $where = [];
        $models = model('payment');
        $data = $models->getList($page,$where,$mobile,$pageSize,true);
        foreach($data['data'] as $k => $v) {
            $data['data'][$k]['style_text'] = $this->style_text($v['pay_style']);
            $data['data'][$k]['status_text'] = $this->status_text($v['status']);
            $data['data'][$k]['info'] = '';
            if($v['bd_info_id'] > 0) $data['data'][$k]['bd_info_id_text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name');
            if($v['houses_id'] > 0) $data['data'][$k]['info'] = $data['data'][$k]['info'].' '.Db::name('wy_houses')->where('id', $v['houses_id'])->value('name');
            if($v['car_area_id'] > 0) $data['data'][$k]['info'] = $data['data'][$k]['info'].' '.Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
            if($v['car_id'] > 0) $data['data'][$k]['info'] = $data['data'][$k]['info'].' '.Db::name('wy_car')->where('id', $v['car_id'])->value('name');
            if($v['house_user_id'] > 0) $data['data'][$k]['user_text'] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name');
            $data['data'][$k]['action_user'] = is_numeric($v['action_user']) ? Db::name('pms_employee')->where('id', $v['action_user'])->value('name') : $v['action_user'];
        }
        if(empty($data['data'])) {
            return __error('暂无数据');
        } else {
            return __success('查询成功',  $data);
        }
    }
    private function style_text($style){
        $style_text = [
            '1' => '线下支付-现金',
            '2' => '线下支付-支票',
            '3' => ' 银行转账',
            '4' => '线下支付-pos机刷卡',
            '5' => '线下支付-支付宝直接转账',
            '6' => '线下支付-微信直接转账',
            '7' => '小区收款码-支付宝',
            '8' => '小区收款码-微信',
            '9' => '网上缴费1',
            '10' => '网上缴费2',
            '11' => '网上缴费3',
            '12' => '用户余额',
        ];
        if (in_array($style,[1,2,3,4,5,6,7,8])) {
            return $style_text[$style];
        } else {
            return '';
        }
    }
    private function status_text($status){
        $status_text = [
            '1' => '已缴',
            '2' => '已退',
            '3' => '撤销'
        ];
        if (in_array($status,[1,2,3])) {
            return $status_text[$status];
        } else {
            return '';
        }
    }
    private function type_text($type){
        $type_text = [
            '1' => '房屋',
            '2' => '车辆',
            '3' => '车位',
            '4' => '住户'
        ];
        if (in_array($type,[1,2,3,4])) {
            return $type_text[$type];
        } else {
            return '';
        }
    }
    // 获取缴费明细
    public function get_pay_store_list() {
        $page = $this->request->get('pageNo','1');
        $pageSize = $this->request->get('pageSize','10');
        $name = $this->request->get('name');
        $type = $this->request->get('type');
        $pid = $this->request->get('payment_id');
        $where = [];
        if ($type) {
            $all_type = ['1'=>'房屋','2'=>'车位','3'=>'车辆','4'=>'住户','5'=>'其它收费','6'=>'临时停车费'];
            $where[] = ['type','=',$all_type[$type]];
        }
        if ($pid) {
            $where[] = ['payment_id','=',$pid];
            $where[] = ['status','in',['已缴','已退']];
        }
        $start = $this->request->get('start_time');
        $end = $this->request->get('end_time');
        if ($start && $end) {
            if ($start == $end) {
                $end = date('Y-m-d',strtotime('+1 days',strtotime(date('Y-m-d',strtotime($start)))));
            }
            $pays = model('payment')->where('pay_time','between',$start.','.$end)->column('id');
            $where[] = ['payment_id','in',implode(',',$pays)];
        }
        $jine_max = $this->request->get('jine_max');
        $jine_min = $this->request->get('jine_min');
        if ($jine_max && $jine_min >= 0) {
            if ($jine_min == $jine_max) {
                $where[] = ['price','=',$jine_min];
            } else {
                $where[] = ['price','between',$jine_min.','.$jine_max];
            }
        }
        $jine_max_s = $this->request->get('jine_max_s');
        $jine_min_s = $this->request->get('jine_min_s');
        if ($jine_max_s && $jine_min_s >= 0) {
            if ($jine_min_s == $jine_max_s) {
                $where[] = ['money','=',$jine_min_s];
            } else {
                $where[] = ['money','between',$jine_min_s.','.$jine_max_s];
            }
        }
        $gj_name_value = $this->request->get('gj_name_value');
        if ($gj_name_value) {
            $name = $gj_name_value;
        }
        $status = $this->request->get('status');
        if ($status) {
            $all_sta = ['1'=>'已缴','2'=>'已退','3'=>'撤销'];
            $where[] = ['status','=',$all_sta[$status]];
        }
        $admin = $this->request->get('admin');
        if ($admin && $admin != '所有操作员') {
            $where[] = ['action_user','=',$admin];
        }
        $sou = $this->request->get('sou');
        if ($sou) {
            $where[] = ['pay_style','=',$sou];
        }
        $bd_id = $this->request->get('bd_id');
        $label = $this->request->get('lab_id');
        if ($bd_id) {
            $hous = model('wy_houses')->where('bd_info_id',$bd_id)->column('id');
            $where[] = ['houses_id','in',implode(',',$hous)];
        }
        $models = model('payment_store');
        $data = $models->getList($page,$where,$name,$pageSize,$label);
        $data['x_p'] = 0;
        $data['x_m'] = 0;
        foreach ($data['data'] as $k=>$v) {
            $data['x_p'] += $v['price'];
            $data['x_m'] += $v['money'];
            $data['data'][$k]['start_end'] = $v['start_time'].'/'.$v['end_time'];
            $pay = Db::name('wy_payment')->where('id', $v['payment_id'])->field('pay_time,bd_info_id,remarks,deduction,no_payment_id')->find();
            $remark_list = Db::name('wy_no_payment_remarks')->where('no_payment_id',$pay['no_payment_id'])->column('name');
            $data['data'][$k]['remark_list'] = !empty($remark_list) ? $remark_list : ['还没有备注内容'];
            $bd_info_id = $pay['bd_info_id'];
            $data['data'][$k]['bd_info_id_text'] = Db::name('wy_bd_info')->where('id', $bd_info_id)->value('name');
            if ($v['houses_id'] > 0) {
                $data['data'][$k]['info'] = '房屋:';
            } elseif ($v['car_area_id'] > 0) {
                $data['data'][$k]['info'] = '车位:';
            } elseif ($v['car_id'] > 0) {
                $data['data'][$k]['info'] = '车辆:';
            } else {
                $data['data'][$k]['info'] = '';
            }
            $data['data'][$k]['label'] = '还没有标签';
            if($v['houses_id'] > 0) {
                $hs = Db::name('wy_houses')->where('id', $v['houses_id'])->field('building_id,unit_id,name,labels')->find();
                $bd = Db::name('wy_building')->where('id', $hs['building_id'])->value('name') ?: '';
                $ut = Db::name('wy_unit')->where('id', $hs['unit_id'])->value('name') ?: '';
                $data['data'][$k]['bd_info_id_text_t'] = $bd.$ut;
                $data['data'][$k]['data_info'] = $hs['name'];
                $data['data'][$k]['info'] = $data['data'][$k]['info'].$bd.'|'.$ut.'|'.$hs['name'];
                $data['data'][$k]['label_hos'] = $hs;
                $data['data'][$k]['label'] = Db::name('wy_labels')->where('id','=',$hs['labels'])->value('name') ?: '还没有标签';
//                dump($data['data'][$k]['label']);die();
//                    Db::name('wy_labels_middle')->alias('m')->join('wy_labels s','s.id=m.labels_id')->where('s.type_name',1)->where('m.house_user_id',$v['user_id'])->value('s.name') ?: '还没有标签';
            }
            if($v['car_id'] > 0) {
                $car_det = Db::name('wy_car')->where('id', $v['car_id'])->field('name,labels_id')->find();
                $car_name = $car_det['name'];
                $data['data'][$k]['data_info'] = $car_name;
                $data['data'][$k]['info'] = $data['data'][$k]['info'].' '.$car_name;
                $data['data'][$k]['label'] = Db::name('wy_labels')->where('id','=',$car_det['labels_id'])->value('name') ?: '还没有标签';
            }
            if($v['car_area_id'] > 0) {
                $car_area_det = Db::name('wy_car_area')->where('id', $v['car_area_id'])->field('name,labels_id')->find();
                $car_area_name = $car_area_det['name'];
                $data['data'][$k]['data_info'] = $car_area_name;
                $data['data'][$k]['info'] = $data['data'][$k]['info'].' '.$car_area_name;
                $data['data'][$k]['label'] = Db::name('wy_labels')->where('id','=',$car_area_det['labels_id'])->value('name') ?: '还没有标签';
//                    Db::name('wy_labels_middle')->alias('m')->join('wy_labels s','s.id=m.labels_id')->where('s.type_name',2)->where('m.house_user_id',$v['user_id'])->value('s.name') ?: '还没有标签';
            }
            if($v['user_id'] > 0) {
                $usr = Db::name('wy_house_user')->where('id', $v['user_id'])->field('name,phone,labels_id')->find();
                $data['data'][$k]['user_id_name'] = $usr['name'].'('.$usr['phone'].')';
                $data['data'][$k]['u_name'] = $usr['name'];
                $data['data'][$k]['u_phone'] = empty($usr['phone']) ? '' : '('.$usr['phone'].')';
//                $data['data'][$k]['label'] = Db::name('wy_labels')->where('id','=',$usr['labels_id'])->value('name') ?: '还没有标签';
//                    Db::name('wy_labels_middle')->alias('m')->join('wy_labels s','s.id=m.labels_id')->where('s.type_name',4)->where('m.house_user_id',$v['user_id'])->value('s.name') ?: '还没有标签';
            } elseif ($v['houses_id'] > 0 && !$v['user_id']) {
                $hu_id = Db::name('wy_houses_user_middle')->where('houses_id',$v['houses_id'])->value('house_user_id');
                $usr = Db::name('wy_house_user')->where('id', $hu_id)->field('name,phone')->find();
                $data['data'][$k]['user_id_name'] = $usr['name'].'('.$usr['phone'].')';
                $data['data'][$k]['u_name'] = $usr['name'];
                $data['data'][$k]['u_phone'] = empty($usr['phone']) ? '' : '('.$usr['phone'].')';
//                $data['data'][$k]['label'] = Db::name('wy_labels_middle')->alias('m')->join('wy_labels s','s.id=m.labels_id')->where('s.type_name',4)->where('m.house_user_id',$hu_id)->value('s.name') ?: '还没有标签';
            }
            if($v['pay_style'] > 0) $data['data'][$k]['pay_style_text'] = $this->style_text($v['pay_style']);
            $data['data'][$k]['action_user'] = is_numeric($v['action_user']) ? Db::name('pms_employee')->where('id', $v['action_user'])->value('name') : $v['action_user'];
            if (date('H',strtotime($pay['pay_time'])) == 0 && date('i',strtotime($pay['pay_time'])) == 0) {
                $data['data'][$k]['pay_time'] = date('Y-m-d',strtotime($pay['pay_time']));
            } else {
                $data['data'][$k]['pay_time'] = date('Y-m-d H:i:s',strtotime($pay['pay_time']));
            }
            $data['data'][$k]['stc'] = $v['status'].date('Y-m-d',strtotime($pay['pay_time']));
            $data['data'][$k]['remarks'] = $pay['remarks'];
            if (empty($pay['deduction']) || $pay['deduction'] == 0) {
                $data['data'][$k]['deduction'] = '无';
            } else {
                $data['data'][$k]['deduction'] = $pay['deduction'];
            }
        }
        $data['x_p'] = round($data['x_p'],2);
        $data['x_m'] = round($data['x_m'],2);
        if(empty($data['data'])) {
            return __error('暂无数据');
        } else {
            return __success('查询成功',  $data);
        }
    }
    public function pay_store_exp_se() {
        $param = $this->request->post();
        $name = @$param['name'];
        $type = @$param['type'];
        $pid = @$param['payment_id'];
        $info = [];
        $where = [];
        $bd_id = @$param['bd_id'];
        $label = @$param['lab_id'];
        if ($bd_id) {
            $hous = model('wy_houses')->where('bd_info_id',$bd_id)->column('id');
            $where[] = ['houses_id','in',implode(',',$hous)];
            $bd_name = Db::name('wy_bd_info')->where('id',$bd_id)->value('name');
            $info = array_merge($info,['小区：'.$bd_name.'小区;']);
        }
        if ($type) {
            $all_type = ['1'=>'房屋','2'=>'车位','3'=>'车辆','4'=>'住户','5'=>'其它收费','6'=>'临时停车费'];
            $where[] = ['type','=',$all_type[$type]];
            $info = array_merge($info,['类型：'.$all_type[$type].';']);
        }
        if ($pid) {
            $where[] = ['payment_id','=',$pid];
            $where[] = ['status','in',['已缴','已退']];
        }
        $start = @$param['start_time'];
        $end = @$param['end_time'];
        if ($start && $end) {
            if ($start == $end) {
                $end = date('Y-m-d',strtotime('+1 days',strtotime(date('Y-m-d',strtotime($start)))));
            }
            $pays = model('payment')->where('pay_time','between',$start.','.$end)->column('id');
            $where[] = ['payment_id','in',implode(',',$pays)];
            $info = array_merge($info,['支付时间：'.$start.'到'.$end.';']);
        }
        if ($label > 0) {
            $label_name = Db::name('wy_labels')->where('id',$label)->value('name') ?: '还没有找到标签';
            $info = array_merge($info,['数据标签：'.$label_name.';']);
        }
        $jine_max = @$param['jine_max'];
        $jine_min = @$param['jine_min'];
        if ($jine_max && $jine_min >= 0) {
            if ($jine_min == $jine_max) {
                $where[] = ['price','=',$jine_min];
            } else {
                $where[] = ['price','between',$jine_min.','.$jine_max];
            }
            $info = array_merge($info,['应收金额区间：'.$jine_min.'-'.$jine_max.';']);
        }
        $jine_max_s = @$param['jine_max_s'];
        $jine_min_s = @$param['jine_min_s'];
        if ($jine_max_s && $jine_min_s >= 0) {
            if ($jine_min_s == $jine_max_s) {
                $where[] = ['money','=',$jine_min_s];
            } else {
                $where[] = ['money','between',$jine_min_s.','.$jine_max_s];
            }
            $info = array_merge($info,['实收金额区间：'.$jine_min_s.'-'.$jine_max_s.';']);
        }
        $status = @$param['status'];
        if ($status) {
            $all_sta = ['1'=>'已缴','2'=>'已退','3'=>'撤销'];
            $where[] = ['status','=',$all_sta[$status]];
            $info = array_merge($info,['状态：'.$all_sta[$status].';']);
        }
        $admin = @$param['admin'];
        if ($admin && $admin != '所有操作员') {
            $where[] = ['action_user','=',$admin];
            $info = array_merge($info,['操作员：'.$admin.';']);
        }
        $sou = @$param['sou'];
        if ($sou) {
            $where[] = ['pay_style','=',$sou];
            $info = array_merge($info,['收款方式：'.$this->style_text($sou).';']);
        }
        if ($name) {
            $info = array_merge($info,['关键字：'.$name.';']);
        }
        $models = model('payment_store');
        $nod = $models->getList(1,$where,$name,10,$label);
        $count = $nod['totalCount'];
        if ($count <= 0) {
            return json_encode(['code'=>-1,'msg'=>'查无数据']);
        }
        if ($count > 5000) {
            return json_encode(['code'=>-1,'msg'=>'本次导出数据超出系统限制，请重新设置搜索条件']);
        }
        if($info){
            array_splice($info,0,0,'导出条件为:');
            array_splice($info,0,0,'本次共导出'.$count.'条数据');
            array_push($info,'');
            array_push($info,'确定导出请点确认按钮');
            return __success($info);
        }else {
            return __success(['还未设定导出搜索条件，请先通过筛选搜索后再导出数据，未搜索将导出全部数据，单次导出数据条数上限为5000条','','确定导出请点确认按钮']);
        }
    }

    // 获取预存款列表
    public function get_desposit_list() {
        $page = $this->request->get('pageNo','1');
        $pageSize = $this->request->get('pageSize','10');
        $house = $this->request->get('house');
        $mobile = $this->request->get('mobile');
        $bd = $this->request->get('bd');
        $hei = $this->request->get('hei');
        $where = [];
        if ($hei) {
            $user = Db::name('wy_house_user')->where('phone|name','like','%'.$hei.'%')->column('id');
            $us = Db::name('wy_houses')->where('name','like','%'.$hei.'%')->column('id');
            if ($us) {
                $where[] = ['houses_id','in',implode(',',$us)];
            } elseif ($user) {
                $where[] = ['house_user_id','in',implode(',',$user)];
            } else {
                $where[] = ['category|remarks','like','%'.$hei.'%'];
            }
        }
        if ($bd) {
            $where[] = ['bd_info_id','eq',$bd];
        } else {
            $where[] = ['bd_info_id','gt',0];
        }
        $where[] = ['status','eq',1];
        $models = model('deposit_list');
        $data = $models->getList($page,$where,$house,$mobile,$pageSize);
        if(empty($data['data'])) {
            return __error('暂无数据');
        } else {
            foreach($data['data'] as $k => $v) {
                if ($v['type'] == 1) {
                    $data['data'][$k]['category'] = '通用预存款';
                } else {
                    $data['data'][$k]['category'] = '专用预存款('.$v['category'].')';
                }
                $data['data'][$k]['remarks'] = empty($v['remarks']) ? '还没有备注内容' : $v['remarks'];
                $data['data'][$k]['info']='';
                $data['data'][$k]['balance'] = round($v['balance'],2);
                $data['data'][$k]['bd_info_id_text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name');
                $data['data'][$k]['house_user_id_text'] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name');
                $data['data'][$k]['phone'] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('phone');
                if($v['houses_id'] > 0) $data['data'][$k]['info'] = $data['data'][$k]['info'].' '.Db::name('wy_houses')->where('id', $v['houses_id'])->value('name');
                if($v['car_area_id'] > 0) $data['data'][$k]['info'] = $data['data'][$k]['info'].' '.Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
                if($v['car_id'] > 0) $data['data'][$k]['info'] = $data['data'][$k]['info'].' '.Db::name('wy_car')->where('id', $v['car_id'])->value('name');
                $data['data'][$k]['status_text'] = $v['status'] == 1 ? '否' : '是';
            }
            return __success('查询成功',  $data);
        }
    }

    // 获取预存款明细
    public function get_desposit_store() {
        $page = $this->request->get('pageNo','1');
        $pageSize = $this->request->get('pageSize','10');
        $house = $this->request->get('house');
        $mobile = $this->request->get('mobile');
        $bd = $this->request->get('bd');
        $hei = $this->request->get('hei');
        $lei = $this->request->get('lei');
        $type = $this->request->get('type');
        $des_id = $this->request->get('des_id');
        $dets = $this->request->get('dets','');
        $where = [];
        $where1 = [];
        if ($bd) {
            $where[] = ['bd_info_id','eq',$bd];
        } else {
            $where[] = ['bd_info_id','gt',0];
        }
        if ($type) {
            $where[] = ['type','eq',$type];
        }
        $jine_max = @$this->request->get('jine_max');
        $jine_min = @$this->request->get('jine_min');
        if ($jine_max && $jine_min >= 0) {
            $where[] = ['price','between',$jine_min.','.$jine_max];
        }
        $admin = @$this->request->get('admin');
        if ($admin) {
            if ($admin > 0) {
                $where[] = ['come','eq',$admin];
            } else {
                $where[] = ['come','eq','excel导入'];
            }
        }
        $start = @$this->request->get('start_time');
        $end = @$this->request->get('end_time');
        if ($start && $end) {
            $where[] = ['createtime','between',$start.','.$end];
        }
        $cate = @$this->request->get('cate');
        if ($cate) {
            if ($cate == 'excel导入') {
                $did = Db::name('wy_deposit_list')->where('type','=',1)->column('id');
                $where[] = ['deposit_list_id','in',implode(',',$did)];
            } elseif($cate != '所有') {
                $did = Db::name('wy_deposit_list')->where('type','=',2)->where('category','=',$cate)->column('id');
                $where[] = ['deposit_list_id','in',implode(',',$did)];
            }
        }
        if ($hei && $lei == 1) {
            $where1[] = ['status', '=', 1];
            $user = Db::name('wy_house_user')->where('phone|name','like','%'.$hei.'%')->column('id');
            $us = Db::name('wy_houses')->where('name','like','%'.$hei.'%')->column('id');
            if ($user) {
                $where1[] = ['house_user_id','in',implode(',',$user)];
            } elseif ($us) {
                $where1[] = ['houses_id','in',implode(',',$us)];
            } else {
                $where1[] = ['category|remarks','like','%'.$hei.'%'];
            }
            $did = Db::name('wy_deposit_list')->where($where1)->column('id');
            $where[] = ['deposit_list_id','in',implode(',',$did)];
        }
        if ($dets == 1) {
            $uid = Db::name('wy_deposit_list')->where('id',$des_id)->value('house_user_id');
            $des_id = Db::name('wy_deposit_list')->where('house_user_id',$uid)->column('id');
            $where[] = ['deposit_list_id','in',implode(',',$des_id)];
        } else {
            if ($des_id) {
                $where[] = ['deposit_list_id','=',$des_id];
            }
        }
        $models = model('deposit_list_store');
        $data = $models->getList($page,$where,$house,$mobile,$pageSize);
        if(empty($data['data'])) {
            return __error('暂无数据');
        } else {
            $xp = 0;
            $xm = 0;
            $xz = 0;
            foreach($data['data'] as $k => $v) {
                $cate = Db::name('wy_deposit_list')->where('id',$v['deposit_list_id'])->field('houses_id,car_id,car_area_id,category,type')->find();
                $hdt = Db::name('wy_houses')->where('id',$cate['houses_id'])->field('building_id,unit_id,name')->find();
                $build_name = Db::name('wy_building')->where('id',$hdt['building_id'])->value('name') ?: '';
                $unit_name = Db::name('wy_unit')->where('id',$hdt['unit_id'])->value('name') ?: '';
                $bd_name = Db::name('wy_bd_info')->where('id',$v['bd_info_id'])->value('name') ?: '';
                if ($cate['type'] == 1) {
                    $data['data'][$k]['category'] = '通用预存款';
                } else {
                    $data['data'][$k]['category'] = '专用预存款('.$cate['category'].')';
                }
                $data['data'][$k]['cate'] = $cate['category'];
                if ($v['type'] == 1) {
                    $xp += $v['price'];
                    $xm += $v['money'];
                } elseif ($v['type'] == 2) {
                    $xz += $v['price'];
                }
                $t_text = ['1' => '充值', '2' => '抵扣','3'=>'退款'];
                $data['data'][$k]['bd_info_id_text'] = $bd_name.'('.$build_name.$unit_name.')';
                $data['data'][$k]['house'] = $hdt['name'];
                if ($cate['car_id'] > 0) {
                    $data['data'][$k]['house_name'] = Db::name('wy_car')->where('id',$cate['car_id'])->value('name');
                } elseif ($cate['car_area_id'] > 0) {
                    $data['data'][$k]['house_name'] = Db::name('wy_car_area')->where('id',$cate['car_area_id'])->value('name');
                } else {
                    $data['data'][$k]['house_name'] = $hdt['name'];
                }
                $data['data'][$k]['house_user_id_text'] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name');
                $data['data'][$k]['phone'] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('phone');
                $data['data'][$k]['t_text'] = @$t_text[$v['type']];
                $data['data'][$k]['come_text'] = is_numeric($v['come']) ? Db::name('pms_employee')->where('id',$v['come'])->value('name') : $v['come'];
                if ($v['type'] == 1) {
                    $data['data'][$k]['remarks'] = '后台充值；'.$v['remarks'];
                } elseif ($v['type'] == 2) {
                    $data['data'][$k]['remarks'] = '自动抵扣；'.$v['remarks'];
                } elseif ($v['type'] == 3) {
                    $data['data'][$k]['remarks'] = '退款人：'.$v['user_name'].'('.$v['user_phone'].')'.' 备注:'.$v['remarks'];
                }
            }
            $data['xp'] = $xp;
            $data['xm'] = $xm;
            $data['xz'] = $xz;
            return __success('查询成功',  $data);
        }
    }
    public function dep_list_store_se() {
        $param = $this->request->post();
        $house = @$param['house'];
        $mobile = @$param['mobile'];
        $bd = @$param['bd'];
        $hei = @$param['hei'];
        $lei = @$param['lei'];
        $type = @$param['type'];
        $des_id = @$param['des_id'];
        $dets = @$param['dets'];
        $where = [];
        $where1 = [];
        $info = [];
        if ($bd) {
            $where[] = ['bd_info_id','eq',$bd];
            $bd_name = Db::name('wy_bd_info')->where('id','=',$bd)->value('name');
            $info = array_merge($info,['小区：'.$bd_name.';']);
        } else {
            $where[] = ['bd_info_id','gt',0];
        }
        if ($type) {
            $where[] = ['type','eq',$type];
            $t_text = ['1' => '充值', '2' => '抵扣','3'=>'退款'];
            $info = array_merge($info,['类别：'.$t_text[$type].';']);
        }
        $jine_max = @$param['jine_max'];
        $jine_min = @$param['jine_min'];
        if ($jine_max && $jine_min >= 0) {
            $where[] = ['price','between',$jine_min.','.$jine_max];
            $info = array_merge($info,['金额区间：'.$jine_min.'-'.$jine_max.';']);
        }
        $admin = @$param['admin'];
        if ($admin) {
            if ($admin > 0) {
                $where[] = ['come','eq',$admin];
                $ad_name = Db::name('pms_employee')->where('id','=',$admin)->value('name');
                $info = array_merge($info,['来源：'.$ad_name.';']);
            } else {
                $where[] = ['come','eq','excel导入'];
                $info = array_merge($info,['来源：excel导入;']);
            }
        }
        $start = @$param['start_time'];
        $end = @$param['end_time'];
        if ($start && $end) {
            $where[] = ['createtime','between',$start.','.$end];
            $info = array_merge($info,['时间区间：'.$start.'到'.$end.';']);
        }
        $cate = @$param['cate'];
        if ($cate) {
            if ($cate == 'excel导入') {
                $did = Db::name('wy_deposit_list')->where('type','=',1)->column('id');
                $where[] = ['deposit_list_id','in',implode(',',$did)];
                $info = array_merge($info,['预存款类型：excel导入;']);
            } elseif($cate != '所有') {
                $did = Db::name('wy_deposit_list')->where('type','=',2)->where('category','=',$cate)->column('id');
                $where[] = ['deposit_list_id','in',implode(',',$did)];
                $info = array_merge($info,['预存款类型：'.$cate.';']);
            }
        }
        if ($hei && $lei == 1) {
            $info = array_merge($info,['关键字：'.$hei.';']);
            $where1[] = ['status', '=', 1];
            $user = Db::name('wy_house_user')->where('phone|name','like','%'.$hei.'%')->column('id');
            $us = Db::name('wy_houses')->where('name','like','%'.$hei.'%')->column('id');
            if ($user) {
                $where1[] = ['house_user_id','in',implode(',',$user)];
            } elseif ($us) {
                $where1[] = ['houses_id','in',implode(',',$us)];
            } else {
                $where1[] = ['category|remarks','like','%'.$hei.'%'];
            }
            $did = Db::name('wy_deposit_list')->where($where1)->column('id');
            $where[] = ['deposit_list_id','in',implode(',',$did)];
        }
        if ($house && $mobile) {
            if ($house == 1) {
                $h_n = '住户';
            } else {
                $h_n = '房屋';
            }
            $info = array_merge($info,['类型：'.$h_n.';']);
            $info = array_merge($info,['搜索内容：'.$mobile.';']);
        }
        if ($dets == 1) {
            $uid = Db::name('wy_deposit_list')->where('id',$des_id)->value('house_user_id');
            $des_id = Db::name('wy_deposit_list')->where('house_user_id',$uid)->column('id');
            $where[] = ['deposit_list_id','in',implode(',',$des_id)];
        } else {
            if ($des_id) {
                $where[] = ['deposit_list_id','=',$des_id];
            }
        }
        $models = model('deposit_list_store');
        $nod = $models->getList(1,$where,$house,$mobile,10);
        $count = $nod['totalCount'];
        if ($count <= 0) {
            return json_encode(['code'=>-1,'msg'=>'查无数据']);
        }
        if ($count > 5000) {
            return json_encode(['code'=>-1,'msg'=>'本次导出数据超出系统限制，请重新设置搜索条件']);
        }
        if($info){
            array_splice($info,0,0,'导出条件为:');
            array_splice($info,0,0,'本次共导出'.$count.'条数据');
            array_push($info,'');
            array_push($info,'确定导出请点确认按钮');
            return __success($info);
        }else {
            return __success(['还未设定导出搜索条件，请先通过筛选搜索后再导出数据，未搜索将导出全部数据，单次导出数据条数上限为5000条','','确定导出请点确认按钮']);
        }
    }

    // 获取收费项目
    public function get_toll_project() {
        $id=$this->request->post('id');
        if(!@$id){
            $type = $this->request->post('type') ?: 0;
            $name = $this->request->post('name') ?: '';
            $where = [];
            if ($type) {
                $where[] = ['project','=',$type];
            }
            if ($name) {
                $where[] = ['name','like','%'.$name.'%'];
            }
            $data = Db::name('wy_toll_project')->where($where)->select();
            if(empty($data)) {
                return __error('暂无数据');
            } else {
                $text = ['1' => '周期性', '2' => '临时性','3' => '押金性'];
                foreach($data as $k => $v) {
                    $data[$k]['detail'] = Db::name('wy_toll_project_detial')->where('project_id', $v['id'])->select();
                    $list  = [
                        '1' => '单价*数量',
                        '2' => '每户单独输入',
                        '3' => '固定金额',
                        '4' => '自定义公式'
                    ];
                    $data[$k]['project_text'] = $text[$v['project']];
                    foreach($data[$k]['detail'] as $ks => $vs) {

                        if($vs['style'] == '3') {
                            $data[$k]['detail'][$ks]['count_style'] = $list[$vs['style']].':'.$vs['fixed_price'];
                        } else {
                            $data[$k]['detail'][$ks]['count_style'] = $list[$vs['style']];
                        }
                    }
                }
                return __success('查询成功',  $data);
            }
        }else{
            $data=Db::name('wy_toll_project')->where('id',$id)->find();
            return __success('查询成功',  $data);
        }
        
    }

    // 新增收费项目
    public function save_toll_project() {
        $data = $this->request->post();
        unset($data['token']);
        if($data['id']){
            $res=model('TollProject')->isUpdate(true)->save($data);
            return $res ? __success('成功') : __error('编辑失败');
        }else{
            $can_add = Db::name('wy_toll_project')->where('name', $data['name'])->find();
            if(!empty($can_add)) {
                return __error('请勿重复添加数据');
            } else {
                $ad = Db::name('wy_toll_project')->insert($data);
                return $ad >= 1 ? __success('成功') : __error('添加失败');
            }
        }
        
    }

    // 删除项目
    public function del_toll_project() {
        $data = $this->request->post();
        unset($data['token']);
        $is_data = Db::name('wy_toll_project')->where('id', $data['id'])->find();
        if(empty($is_data)) {
            return __error('数据错误');
        } else {
            Db::name('wy_toll_project')->where('id', $data['id'])->delete();
            Db::name('wy_toll_project_detial')->where('project_id', $data['id'])->delete();
            return __success('删除成功');
        }
    }

    // 保存项目详情
    public function save_toll_detail() {
        $data = $this->request->post();
        unset($data['token']);
        $is_data = Db::name('wy_toll_project_detial')->where('name', $data['name'])->find();
        if(!empty($is_data)) {
            return __error('请勿重复添加数据');
        } else {
            $ad = Db::name('wy_toll_project_detial')->insert($data);
            return $ad >= 1 ? __success('成功') : __error('添加失败');
        }
    }

    // 删除收费项目详情
    public function del_toll_detail() {
        $data = $this->request->post();
        unset($data['token']);
        $is_data = Db::name('wy_toll_project_detial')->where('id', $data['id'])->find();
        if(empty($is_data)) {
            return __error('数据错误');
        } else {
            Db::name('wy_toll_project_detial')->where('id', $data['id'])->delete();
            return __success('删除成功');
        }
    }

    // 获取收费项目详情
    public function look_toll_detail() {
        $data = $this->request->post();
        unset($data['token']);
        $is_data = Db::name('wy_toll_project_detial')->alias('d')
        ->join('wy_toll_project p','d.project_id=p.id')
        ->field('d.*,p.project,p.name as p_name')
        ->where('d.id', $data['id'])->find();
        if(empty($is_data)) {
            return __error('数据错误');
        } else {
            return __success('查询成功', $is_data);
        }
    }

    // 更新收费项目详情
    public function update_toll_detail() {
        $data = $this->request->post();
        unset($data['token']);
        $models=model('TollProjectDetial');
        $is_data = $models->where('id', $data['id'])->find();
        if(empty($is_data)) {
            return __error('数据不存在');
        } else {
            $ad = $models->isUpdate(true)->save($data);
            return $ad  ? __success('成功') : __error('更新失败');
        }
    }

    // 获取公式可变参数
    public function get_formula_arg() {
        $data = $this->request->post();
        unset($data['token']);
        $id = Db::name('wy_toll_formula_user')->where('id', $data['id'])->value('wy_toll_formula_name_id');
        $name = Db::name('wy_toll_formula_name')->where('id', $id)->value('unit');
        $datas = getArgs($name);
        return __success('查询成功', $datas);
    }

    // 计算返回值
    public function com_formula_price() {
        $data = $this->request->post();
        unset($data['token']);
        // 获取方法
        $id = Db::name('wy_toll_formula_user')->where('id', $data['id'])->value('wy_toll_formula_name_id');
        $name = Db::name('wy_toll_formula_name')->where('id', $id)->value('unit');
        if(count($data['data']) == 1) {
            return $name($data['data'][0], $data['id']);
        } else {
            return $name($data['data'][0], $data['data'][1], $data['id']);
        }
    }

    // 删除自定义公式
    public function del_formula() {
        $data = $this->request->post();
        unset($data['token']);
        $info = Db::name('wy_toll_formula_user')->where('id', $data['id'])->find();
        if(empty($info)) {
            return __error('id错误');
        } else {
            $st = Db::name('wy_toll_formula_user')->where('id', $data['id'])->delete();
            return $st ? __success('删除成功') : __error('删除失败');
        }
    }

    // 获取所有小区
    public function get_all_bd_info() {
        $data = Db::name('wy_bd_info')->field('id, name')->select();
        if(empty($data)) {
            return __error('数据错误');
        } else {
            return __success('查询成功', $data);
        }
    }

    // 手机号搜索住户
    public function search_user() {
        $data = $this->request->post();
        unset($data['token']);
        $bd_id = $data['bd_id'];
        if(is_numeric($data['search'])){
            $where[] = [['phone','LIKE', $data['search'].'%']];
        }else{
            $where[] = [['name','LIKE', '%'.$data['search'].'%']];
        }
        
        if ($bd_id) {
            $where[] = [['bd_info_id','=',$bd_id]];
        }
        // $house = Db::name('wy_houses')->where('name','like','%'.$data['search'].'%')->where('bd_info_id','=',$bd_id)->column('id');
        $this->userinfo;
        
        $info = Db::name('wy_house_user')->where($where)->field('id,bd_info_id,houses_id, name, phone')->limit(10)->select();
        
        if(empty($info)) {
            return __error('数据错误');
        } else {
            foreach($info as $k => $v) {
                if ($v['bd_info_id'] > 0 && $v['houses_id'] > 0) {
                    $bd = Db::name('wy_bd_info')->where('id',$v['bd_info_id'])->value('name') ?: '';
                    $hou = Db::name('wy_houses')->where('id',$v['houses_id'])->value('name') ?: '';
                    if ($bd_id > 0) {
                        $info[$k]['data'] = $hou.'-'.$v['name'].'-'.$v['phone'];
                    } else {
                        $info[$k]['data'] = $bd.'-'.$hou.'-'.$v['name'].'-'.$v['phone'];
                    }
                } else {
                    $info[$k]['data'] = '非住户-'.$v['name'].'-'.$v['phone'];
                }
                unset($info[$k]['name']);
                unset($info[$k]['phone']);
            }
            return __success('查询成功', $info);
        }
    }

    // 预存款充值
    public function save_deposit() {
        $data = $this->request->post();
        unset($data['token']);
        $param = $data['form'];
        $uid = $param['seUser'];
        $balance = 0;
        if($param['type'] == 1) {
            $bal = Db::name('wy_deposit_list')->where(['house_user_id' => $param['seUser'], 'type' => $param['type']])->field('id,balance')->find();
            $balance = @$bal['balance'];
        } else {
            $bal = Db::name('wy_deposit_list')->where(['house_user_id' => $param['seUser'], 'type' => $param['type'], 'category' => $param['category']])->field('id,balance')->find();
            $balance = @$bal['balance'];
        }
        $user = Db::name('wy_house_user')->where('id',$param['seUser'])->field('houses_id,bd_info_id,money')->find();
        $param['seBd'] = $param['bd_id'];
        unset($param['bd_id']);
//        $param['seBd'] = $user['bd_info_id'];
        $param['houses_id'] = $user['houses_id'];
        $param['status'] = 1;
        if($balance) {
            $param['bd_info_id'] = $param['seBd'];
            $param['house_user_id'] = $param['seUser'];
            $param['balance'] = $balance + $param['price'];
            // $param['balance'] = $balance + $param['price'];
            unset($param['seBd']);
            unset($param['seUser']);
            $did = $bal['id'];
            if ($param['type'] == 1) {
                $status = Db::name('wy_deposit_list')->where(['house_user_id' => $param['house_user_id'], 'type' => $param['type']])->update($param);
            } else {
                $status = Db::name('wy_deposit_list')->where(['house_user_id' => $param['house_user_id'], 'type' => $param['type'],'category' => $param['category']])->update($param);
            }
        } else {
            $param['bd_info_id'] = $param['seBd'];
            $param['house_user_id'] = $param['seUser'];
             $param['balance'] = $param['price'];
            unset($param['seBd']);
            unset($param['seUser']);
            $status = Db::name('wy_deposit_list')->insertGetId($param);
            $did = $status;
        }
        if ($user['money']) {
            Db::name('wy_house_user')->where('id',$uid)->setInc('money',floatval($param['price']));
        } else {
            Db::name('wy_house_user')->where('id',$uid)->setField('money',floatval($param['price']));
        }
        // 生成明细 将下拉菜单用户格式修改一下：小区名称-房号（非住户就显示非住户）-姓名-手机号
        $store['deposit_list_id'] = $did;
        $store['bd_info_id'] = $param['bd_info_id'];
        $store['house_user_id'] = $param['house_user_id'];
        $store['price'] = $param['price'];
        $store['money'] = $param['money'];
        $store['type'] = 1;
        $store['come'] = $this->userinfo['id'];
        $store['createtime'] = date('Y-m-d H:i:s', time());
        $store['remarks'] = $param['remarks'];
        Db::name('wy_deposit_list_store')->insert($store);
        return $status ? __success('充值成功') : __error('充值失败');
    }

    // 退款
    public function cancel_price() {
        $data = $this->request->post();
        unset($data['token']);
        $user = Db::name('wy_deposit_list')->where('id', $data['id'])->field('id, bd_info_id, house_user_id, balance')->find();
        $info['name'] = Db::name('wy_house_user')->where('id', $user['house_user_id'])->column('name, phone');
        $info['bd_info_id'] = $user['bd_info_id'];
        $info['price'] = $data['price'];
        $info['type'] = 3;
        $info['come'] = $this->userinfo['name'];
        $info['createtime'] = date('Y-m-d H:i:s', time());
        $info['remarks'] = "退款人：".$data['user']."，手机号：".$data['phone']."，退款金额：".$data['price']."，退款说明：".$data['message'];
        if($user['balance'] > $data['price']) {
            Db::name('wy_deposit_list_store')->insert($info);
            Db::name('wy_deposit_list')->where('id', $data['id'])->update([
                'balance' => $user['balance'] - $data['price']
            ]);
            return __success('退款成功');
        } else {
            return __error('退款失败');
        }
    }

    // 获取收银台房屋
    public function get_cashier_house() {
        $cm_mg_id = $this->userinfo['cm_message_id']?$this->userinfo['cm_message_id']:'';
        if(@$cm_mg_id){
            $where[]=['id','in',$cm_mg_id];
        }
        $where[] = ['is_deleted', '=', 0];
        $bd = Db::name('wy_bd_info')->where($where)->field('id value, name label')->select();
        foreach($bd as $k => $v) {
            $bd[$k]['children'] = Db::name('wy_building')->where('is_deleted',0)->where('bd_info_id', $v['value'])->field('id value, name label,status')->select();
            foreach($bd[$k]['children'] as $k1 => $v1) {
                switch($v1['status']){
                    case 0:
                        $bd[$k]['children'][$k1]['label']=$v1['label'].'(禁用)';
                        break;
                    case 2:
                        $bd[$k]['children'][$k1]['label']=$v1['label'].'(拆除)';
                        break;
                    case 3:
                        $bd[$k]['children'][$k1]['label']=$v1['label'].'(出售)';
                        break;
                    case 4:
                        $bd[$k]['children'][$k1]['label']=$v1['label'].'(其它)';
                        break;
                }
                
                $bd[$k]['children'][$k1]['children'] = Db::name('wy_unit')->where('is_deleted',0)->where(['bd_info_id' => $v['value'], 'building_id' => $v1['value']])->field('id value, name label')->select();
                foreach($bd[$k]['children'][$k1]['children'] as $k2 => $v2) {
                    $bd[$k]['children'][$k1]['children'][$k2]['children'] = Db::name('wy_houses')->where(['bd_info_id' => $v['value'], 'building_id' => $v1['value'], 'unit_id' => $v2['value']])->field('id value, name label')->select();
                }
            }
        }
        if(empty($bd)) {
            return __error('暂无数据');
        } else {
            return __success('查询成功', $bd);
        }
    }

    // 获取历史缴费账单
    public function get_his_payment() {

        // todo 分页
        $data = $this->request->post();
        unset($data['token']);
        switch($data['type']){
            case 2:
                $wehre=[['car_area_id','=',$data['id']]];
                break;
            case 3:
                $wehre=[['car_id','=',$data['id']]];
                break;
            case 4:
                $wehre[]=[['house_user_id','=',$data['id']]];
                break;
            default:
                $wehre[]=[['house_id','=',$data['id']]];
        }
        $wehre[]=[['pay_status','=',1]];
        $wehre[]=[['is_deleted','=',0]];
        $info = Db::name('wy_no_payment')
            ->where($wehre)
            ->order('pay_time DESC')
            ->page($data['pageNo'],$data['pageSize'])
            ->select();
        $count=Db::name('wy_no_payment')
            ->where($wehre)
            ->count();
        $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
        foreach($info as $k => $v) {
            $remark_list = Db::name('wy_no_payment_remarks')->where('no_payment_id',$v['id'])->column('name');
            $info[$k]['remark_list'] = !empty($remark_list) ? $remark_list : ['还没有备注内容'];
            $info[$k]['info'] = '';
            $info[$k]['type_text'] = $type_text[$v['type']];
            $info[$k]['time'] = $v['start_time'].'/'.$v['end_time'];
            if($info[$k]['bd_info_id'] > 0) $info[$k]['bd_info_id_text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name');
            if($info[$k]['house_id'] > 0) $info[$k]['info'] = $info[$k]['info'].' '.Db::name('wy_houses')->where('id', $v['house_id'])->value('name');
            if($info[$k]['car_area_id'] > 0) $info[$k]['info'] = $info[$k]['info'].' '.Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
            if($info[$k]['car_id'] > 0) $info[$k]['info'] = $info[$k]['info'].' '.Db::name('wy_car')->where('id', $v['car_id'])->value('name');
            if($info[$k]['house_user_id'] > 0) $info[$k]['user_text'] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name');
        }
        return __success('查询成功', ['data'=>$info,'pageSize'=>$data['pageSize'],'pageNo'=>$data['pageNo'],'totalCount'=>$count,'totalPage'=>ceil($count / $data['pageSize'])]);
        
        // $h_type = @$data['h_type'] ?: 0;
        // if ($h_type == 1) {
        //     $wehre[]=[['status','=',1]];
        //     $info = Db::name('wy_payment')
        //         ->where($wehre)
        //         ->order('id DESC')
        //         ->select();
        // } else {
        //     $wehre[]=[['pay_status','=',1]];
        //     $info = Db::name('wy_no_payment')
        //         ->where($wehre)
        //         ->order('createtime DESC')
        //         ->select();
        // }
        // $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
        // if(empty($info)) {
        //     return __error('暂无数据');
        // } else {
        //     foreach($info as $k => $v) {
        //         if ($h_type == 1) {
        //             $info[$k]['style_text'] = $this->style_text($v['pay_style']);
        //             $info[$k]['status_text'] = $this->status_text($v['status']);
        //             $info[$k]['info'] = '';
        //             if($v['bd_info_id'] > 0) $info[$k]['bd_info_id_text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name');
        //             if($v['houses_id'] > 0) $info[$k]['info'] = $info[$k]['info'].' '.Db::name('wy_houses')->where('id', $v['houses_id'])->value('name');
        //             if($v['car_area_id'] > 0) $info[$k]['info'] = $info[$k]['info'].' '.Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
        //             if($v['car_id'] > 0) $info[$k]['info'] = $info[$k]['info'].' '.Db::name('wy_car')->where('id', $v['car_id'])->value('name');
        //             if($v['house_user_id'] > 0) $info[$k]['user_text'] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name');
        //             $info[$k]['action_user'] = is_numeric($v['action_user']) ? Db::name('pms_employee')->where('id', $v['action_user'])->value('name') : $v['action_user'];
        //         } else {
        //             $info[$k]['type_text'] = $type_text[$v['type']];
        //             $info[$k]['info'] = '';
        //             $info[$k]['time'] = $v['start_time'].'/'.$v['end_time'];
        //             if($info[$k]['bd_info_id'] > 0) $info[$k]['bd_info_id_text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name');
        //             if($info[$k]['house_id'] > 0) $info[$k]['info'] = $info[$k]['info'].' '.Db::name('wy_houses')->where('id', $v['house_id'])->value('name');
        //             if($info[$k]['car_area_id'] > 0) $info[$k]['info'] = $info[$k]['info'].' '.Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
        //             if($info[$k]['car_id'] > 0) $info[$k]['info'] = $info[$k]['info'].' '.Db::name('wy_car')->where('id', $v['car_id'])->value('name');
        //             if($info[$k]['house_user_id'] > 0) $info[$k]['user_text'] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name');
        //         }
        //     }
        //     return __success('查询成功', $info);
        // }
    }
    // public function get_his_payment_page() {
    //     $page = $this->request->get('pageNo','1');
    //     $where = [];
    //     $type = $this->request->get('type');
    //     $id = $this->request->get('id');
    //     $h_type = $this->request->get('h_type',0);
    //     switch($type){
    //         case 2:
    //             $where['car_area_id'] = $id;
    //             break;
    //         case 3:
    //             $where['car_id'] = $id;
    //             break;
    //         case 4:
    //             $where['house_user_id'] = $id;
    //             break;
    //         default:
    //             if (in_array($h_type,[1,2,3,4])) {
    //                 $where['houses_id'] = $id;
    //             } else {
    //                 $where['house_id'] = $id;
    //             }
    //     }
    //     if (in_array($h_type,[1,2,3,4])) {
    //         $models = model('payment');
    //         $where['status'] = 1;
    //         $info = $models->getList($page,$where);
    //     } else {
    //         $models = model('no_pay');
    //         $where['pay_status'] = 0;
    //         $info = $models->getList($page,$where);
    //     }
    //     $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
    //     if(empty($info['data'])) {
    //         return __error('暂无数据');
    //     } else {
    //         foreach($info['data'] as $k => $v) {
    //             if (in_array($h_type,[1,2,3,4])) {
    //                 $info['data'][$k]['style_text'] = $this->style_text($v['pay_style']);
    //                 $info['data'][$k]['status_text'] = $this->status_text($v['status']);
    //                 $info['data'][$k]['info'] = '';
    //                 if($v['bd_info_id'] > 0) $info['data'][$k]['bd_info_id_text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name');
    //                 if($v['houses_id'] > 0) $info['data'][$k]['info'] = $info['data'][$k]['info'].' '.Db::name('wy_houses')->where('id', $v['houses_id'])->value('name');
    //                 if($v['car_area_id'] > 0) $info['data'][$k]['info'] = $info['data'][$k]['info'].' '.Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
    //                 if($v['car_id'] > 0) $info['data'][$k]['info'] = $info['data'][$k]['info'].' '.Db::name('wy_car')->where('id', $v['car_id'])->value('name');
    //                 if($v['house_user_id'] > 0) $info['data'][$k]['user_text'] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name');
    //                 $info['data'][$k]['action_user'] = is_numeric($v['action_user']) ? Db::name('pms_employee')->where('id', $v['action_user'])->value('name') : $v['action_user'];
    //             } else {
    //                 $info['data'][$k]['type_text'] = $type_text[$v['type']];
    //                 $info['data'][$k]['info'] = '';
    //                 $info['data'][$k]['time'] = $v['start_time'].'/'.$v['end_time'];
    //                 if($info['data'][$k]['bd_info_id'] > 0) $info['data'][$k]['bd_info_id_text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name');
    //                 if($info['data'][$k]['house_id'] > 0) $info['data'][$k]['info'] = $info['data'][$k]['info'].' '.Db::name('wy_houses')->where('id', $v['house_id'])->value('name');
    //                 if($info['data'][$k]['car_area_id'] > 0) $info['data'][$k]['info'] = $info['data'][$k]['info'].' '.Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
    //                 if($info['data'][$k]['car_id'] > 0) $info['data'][$k]['info'] = $info['data'][$k]['info'].' '.Db::name('wy_car')->where('id', $v['car_id'])->value('name');
    //                 if($info['data'][$k]['house_user_id'] > 0) $info['data'][$k]['user_text'] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name');
    //             }
    //         }
    //         return __success('查询成功', $info);
    //     }
    // }
    public function get_his_payment_page() {
        $page = $this->request->get('pageNo','1');
        $where = [];
        $type = $this->request->get('type');
        $id = $this->request->get('id');
        $h_type = $this->request->get('h_type',0);
        switch($type){
            case 2:
                $where['car_area_id'] = $id;
                break;
            case 3:
                $where['car_id'] = $id;
                break;
            case 4:
                $where['house_user_id'] = $id;
                break;
            default:
                if (in_array($h_type,[1,2,3,4])) {
                    $where['houses_id'] = $id;
                } else {
                    $where['house_id'] = $id;
                }
        }
        if (in_array($h_type,[1,2,3,4])) {
            $models = model('payment');
            $where['status'] = 1;
            $info = $models->getList($page,$where);
        } else {
            $models = model('no_pay');
            $where['pay_status'] = 0;
            $info = $models->getList($page,$where);
        }
        $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
        if(empty($info['data'])) {
            return __error('暂无数据');
        } else {
            foreach($info['data'] as $k => $v) {
                if (in_array($h_type,[1,2,3,4])) {
                    $info['data'][$k]['style_text'] = $this->style_text($v['pay_style']);
                    $info['data'][$k]['status_text'] = $this->status_text($v['status']);
                    $info['data'][$k]['info'] = '';
                    if($v['bd_info_id'] > 0) $info['data'][$k]['bd_info_id_text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name');
                    if($v['houses_id'] > 0) $info['data'][$k]['info'] = $info['data'][$k]['info'].' '.Db::name('wy_houses')->where('id', $v['houses_id'])->value('name');
                    if($v['car_area_id'] > 0) $info['data'][$k]['info'] = $info['data'][$k]['info'].' '.Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
                    if($v['car_id'] > 0) $info['data'][$k]['info'] = $info['data'][$k]['info'].' '.Db::name('wy_car')->where('id', $v['car_id'])->value('name');
                    if($v['house_user_id'] > 0) $info['data'][$k]['user_text'] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name');
                    $info['data'][$k]['action_user'] = is_numeric($v['action_user']) ? Db::name('pms_employee')->where('id', $v['action_user'])->value('name') : $v['action_user'];
                } else {
                    $info['data'][$k]['type_text'] = $type_text[$v['type']];
                    $info['data'][$k]['info'] = '';
                    $info['data'][$k]['time'] = $v['start_time'].'/'.$v['end_time'];
                    if($info['data'][$k]['bd_info_id'] > 0) $info['data'][$k]['bd_info_id_text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name');
                    if($info['data'][$k]['house_id'] > 0) $info['data'][$k]['info'] = $info['data'][$k]['info'].' '.Db::name('wy_houses')->where('id', $v['house_id'])->value('name');
                    if($info['data'][$k]['car_area_id'] > 0) $info['data'][$k]['info'] = $info['data'][$k]['info'].' '.Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
                    if($info['data'][$k]['car_id'] > 0) $info['data'][$k]['info'] = $info['data'][$k]['info'].' '.Db::name('wy_car')->where('id', $v['car_id'])->value('name');
                    if($info['data'][$k]['house_user_id'] > 0) $info['data'][$k]['user_text'] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name');
                }
            }
            return __success('查询成功', $info);
        }
    }
    // 获取收银台收费标准
    public function get_cashier_formula() {
        $info = Db::name('wy_toll_project')->field('id value, name label')->where('project', 2)->select();
        foreach($info as $k => $v) {
            $info[$k]['children'] = Db::name('wy_toll_project_detial')->where(['project_id' => $v['value']])->field('id value, name label')->select();
        }
        if(empty($info)) {
            return __error('暂无数据');
        } else {
            return __success('查询成功', $info);
        }
    }
    public function get_cashier_formula_two() {
        $type = $this->request->post('type') ?: 0;
        if ($type) {
            $info = Db::name('wy_toll_project')->field('id value, name label')->where('project', 'in','1')->select();
        } else {
            $info = Db::name('wy_toll_project')->field('id value, name label')->where('project', 'in','1,2')->select();
        }
        foreach($info as $k => $v) {
            $info[$k]['children'] = Db::name('wy_toll_project_detial')->where(['project_id' => $v['value']])->field('id value, name label')->select();
        }
        if(empty($info)) {
            return __error('暂无数据');
        } else {
            return __success('查询成功', $info);
        }
    }

    // 收银台添加房屋临时收费
    public function save_cashier_house() {
        $data = $this->request->post();
        unset($data['token']);
        if ($data['type'] == 4) {
            $bd_info = Db::name('wy_house_user')->where('id', $data['house_id'])->field('id,name,bd_info_id,building_id,houses_id')->find();
            $user_id = $bd_info['id'];
            $user_name = $bd_info['name'];
            $store = '住户';
            $param['bd_info_id'] = $bd_info['bd_info_id'];
            $param['building_id'] = $bd_info['building_id'];
            $param['house_id'] = $bd_info['houses_id'];
        } elseif ($data['type'] == 3) {
            $bd_info = Db::name('wy_car')->where('id', $data['house_id'])->field('id,name,phone,bd_info_id')->find();
            $user_id = Db::name('wy_house_user')->where('phone',$bd_info['phone'])->value('id');
            $car_name = $bd_info['name'];
            $store = '车辆';
            $param['bd_info_id'] = $bd_info['bd_info_id'];
            $param['car_id'] = $bd_info['id'];
        } elseif ($data['type'] == 2) {
            $bd_info = Db::name('wy_car_area')->where('id', $data['house_id'])->field('id,name,house_user_id,bd_info_id')->find();
            $user_id = $bd_info['house_user_id'];
            $car_area_name = $bd_info['name'];
            $store = '车位';
            $param['bd_info_id'] = $bd_info['bd_info_id'];
            $param['car_area_id'] = $bd_info['id'];
        } else {
            $bd_info = Db::name('wy_houses')->where('id', $data['house_id'])->field('id,name, bd_info_id, building_id')->find();
            $user_id = Db::name('wy_house_user')->where('houses_id',$data['house_id'])->value('id');
            $houses_name = $bd_info['name'];
            $store = '房屋';
            $param['bd_info_id'] = $bd_info['bd_info_id'];
            $param['building_id'] = $bd_info['building_id'];
            $param['house_id'] = $data['house_id'];
        }
        $param['toll_project_detial_id'] = @$data['project'][1];
        $param['name'] = Db::name('wy_toll_project_detial')->where('id','=',@$data['project'][1])->value('name');
        $param['createtime'] = date('Y-m-d H:i', time());
        $param['price'] = $data['money'];
        $param['money'] = $data['money'];
        $param['start_time'] = $data['rangepicker'][0];
        $param['end_time'] = $data['rangepicker'][1];
        $param['one_money'] = $data['price'];
        $param['num'] = $data['num'];
        $param['remarks'] = @$data['remarks']?@$data['remarks']:'';
        $param['type'] = 1;
        $status = Db::name('wy_no_payment')->insertGetId($param);
        // if ($status) {
        //     $param['pay_time'] = date('Y-m-d H:i:s', time());
        //     $param['houses_id'] = @$param['house_id'];
        //     $param['house_user_id'] = $user_id;
        //     unset($param['building_id']);
        //     unset($param['house_id']);
        //     unset($param['createtime']);
        //     unset($param['one_money']);
        //     unset($param['type']);
        //     unset($param['num']);
        //     $param['status'] = 1;
        //     $param['action_user'] = $this->userinfo['name'];
        //     $param['no_payment_id'] = $status;
        //     $pid = Db::name('wy_payment')->insertGetId($param);
        //     $param['payment_id'] = $pid;
        //     $param['num'] = $data['num'];
        //     $param['one_money'] = $data['price'];
        //     $param['user_id'] = $user_id;
        //     $param['type'] = $store;
        //     $param['status'] = '已缴';
        //     if (@$user_name) {
        //         $param['user_name'] = $user_name;
        //     }
        //     if (@$car_name) {
        //         $param['car_name'] = $car_name;
        //     }
        //     if (@$car_area_name) {
        //         $param['car_area_name'] = $car_area_name;
        //     }
        //     if (@$houses_name) {
        //         $param['houses_name'] = $houses_name;
        //     }
        //     unset($param['status']);
        //     unset($param['house_user_id']);
        //     unset($param['bd_info_id']);
        //     unset($param['remarks']);
        //     unset($param['pay_time']);
        //     unset($param['no_payment_id']);
        //     Db::name('wy_payment_store')->insert($param);
        // }
        return $status ? __success('成功') : __error('失败');
    }

    // 根据房屋id获取住户
    public function get_house_user() {
        $data = $this->request->post();
        unset($data['token']);
        $info = Db::name('wy_houses')->where('id', $data['id'])->value('house_user_id');
        $users = Db::name('wy_house_user')->where('id', 'IN', $info)->field('id, name, phone')->select();
        if(empty($users)) {
            return __error('暂无数据');
        } else {
            return __success('查询数据', $users);
        }
    }

    // 房屋交款
    public function house_pay() {
        $data = $this->request->post();
        unset($data['token']);
        $ids = [];
        foreach($data['arr'] as $k => $v) {
            array_push($ids, $v['id']);
        }
        $getseite=Db::name('wy_set_bill')->value('his_check');
        if($getseite){
            $pd_id = $data['arr'][0]['id'];
            $np = Db::name('wy_no_payment')->where('id',$data['arr'][0]['id'])->field('house_id,car_area_id,car_id,house_user_id')->find();
            if ($np['house_id']) {
                $hid = $np['house_id'];
                $where[] = ['house_id','=',$hid];
            } elseif ($np['car_area_id']) {
                $hid = $np['car_area_id'];
                $where[] = ['car_area_id','=',$hid];
            } elseif ($np['car_id']) {
                $hid = $np['car_id'];
                $where[] = ['car_id','=',$hid];
            } elseif ($np['house_user_id']) {
                $hid = $np['house_user_id'];
                $where[] = ['house_user_id','=',$hid];
            }
            foreach ($data['arr'] as $k=>$v) {
                if ($pd_id < $v['id']) {
                    $pd_id = $v['id'];
                }
            }
//            $where[] = ['end_time','<',$pdTime];
            $where[] = ['id','<',$pd_id];
            $where[] = ['pay_status','=',0];
            $where[] = ['is_deleted','=',0];
            $where[] = ['id','not in',$ids];
            $isHashis=Db::name('wy_no_payment')->where($where)->find();
            if($isHashis){
                return __error('需先缴清历史账单后才能缴纳最新账单');
            }
        }
        if(@$data['user']){
            $pay['house_user_id']=@$data['user'];
            $nick = model('wy_house_user')->where('id',@$data['user'])->value('name');
        }else {
           if(!@$data['username']){
               return __error('缴款人不能为空！');
           }
           if(!@$data['phone']){
                return __error('手机号不能为空！');
           }
           $username = @$data['username'];
           $phone = @$data['phone'];
           $usersmodel=model('wy_house_user');
           $userinfos =$usersmodel->where('phone',$phone)->find();
           if($userinfos){
                $usersmodel -> isUpdate(true)->save(['id'=>$userinfos['id'],'name'=>$username]);
                $pay['house_user_id']=$userinfos['id'];
                $data['user']=$userinfos['id'];
           }else{
                $pay['house_user_id']= $data['user']=$usersmodel -> insertGetId(['name'=>$username,'phone'=>$phone]);
           }
            $nick = $username;
        }
        // 获取未缴费账单

        $no_pay_info = Db::name('wy_no_payment')->where('id','IN', $ids)->where('pay_status',0)->select();

        // 生成已缴费账单
        if($no_pay_info[0]['house_id']){
            $pay['houses_id'] = $no_pay_info[0]['house_id'];
            $sty = '房屋';
        }elseif ($no_pay_info[0]['car_area_id']) {
            $pay['car_area_id'] = $no_pay_info[0]['car_area_id'];
            $sty = '车位';
        }elseif($no_pay_info[0]['car_id']) {
            $pay['car_id'] = $no_pay_info[0]['car_id'];
            $sty = '车辆';
        }else {
            $pay['houses_id'] = 0;
            $sty = '房屋';
        }
        
        $name=[];
        foreach($no_pay_info as $k => $v) {
            $flag=1;
            foreach($name as $key=>$value){
                if($value==$v['name']){
                    $flag=0;
                }
            }
            if($flag){
                $name[]=$v['name'];
            }
        }
        $pay['bd_info_id'] = $no_pay_info[0]['bd_info_id'];
        $pay['price'] = $data['allPrice'];
        $pay['name']=implode('/',$name);
        $pay['money'] = $data['user_money'];
        $pay['pay_time'] = date('Y-m-d H:i:s', time());
        $pay['action_user'] = $this->userinfo['name'];
        $pay['pay_style'] = $data['pay_style'];
        $pay['status'] = 1;
        $pay['status'] = 1;
        $pay['deduction'] = @$data['zk'] ? @$data['zk']:'';
        $payId = Db::name('wy_payment')->insertGetId($pay);
        foreach($no_pay_info as $k1 => $v1) {
            // 生成已缴费明细
            $pay_store['payment_id'] = $payId;
            $pay_store['status'] = "已缴";
            $pay_store['type'] = $sty;
            $pay_store['name'] = $v1['name'];
            $pay_store['houses_id'] = $v1['house_id'];
            $pay_store['car_area_id'] = $v1['car_area_id'];
            $pay_store['car_id'] = $v1['car_id'];
            $pay_store['user_id'] = $v1['house_user_id'];
            $pay_store['houses_name'] = model('wy_houses')->where('id',$v1['house_id'])->value('name');
            $pay_store['start_time'] = $v1['start_time'];
            $pay_store['end_time'] = $v1['end_time'];
            $pay_store['one_money'] = $v1['one_money'];
            $pay_store['num'] = $v1['num'];
            $pay_store['price'] = $v1['price'];
            if(@$data['zk']){
                $pay_store['money'] =$v1['money']-$v1['price']*$data['zk']/10;
                $pay_store['deduction'] = $data['zk'];
            }else{
                $pay_store['money']=$v1['money'];
            }
            $pay_store['user_id'] = $data['user'];
            $pay_store['user_name'] = $nick;
            $pay_store['action_user'] = $this->userinfo['name'];
            $pay_store['pay_style'] = $data['pay_style'];
            $pay_store['offer'] = $v1['offer'];
            $pay_store['overdue'] = $v1['overdue'];
            $pay_store['deduction'] = $v1['deduction'];
            Db::name('wy_payment_store')->insert($pay_store);
            // 生成历史缴费账单
            // $his_store['bd_info_id'] = $v1['bd_info_id'];
            // $his_store['house_id'] = $v1['house_id'];
            // $his_store['name'] = $v1['name'];
            // $his_store['start_time'] = $v1['start_time'];
            // $his_store['end_time'] = $v1['end_time'];
            // $his_store['price'] = $v1['one_money'];
            // $his_store['num'] = $v1['num'];
            // $his_store['type'] = '房屋';
            // Db::name('wy_his_payment')->insert($his_store);
            Db::name('wy_no_payment')->where('id', $v1['id'])->update(['pay_status'=>1,'pay_time'=>date('Y-m-d')]);
        }
        return __success('收款成功');
    }

    // 账单设置
    public function set_billing() {
        $data = $this->request->post();
        unset($data['token']);
        $info = Db::name('wy_set_bill')->find();
        if(empty($info)) {
            Db::name('wy_set_bill')->insert($data);
        } else {
            Db::name('wy_set_bill')->where('id', $info['id'])->update($data);
        }
        return __success('设置成功');
    }

    // 获取账单设置
    public function get_set_billing() {
        $info = Db::name('wy_set_bill')->find();
        if(empty($info)) {
            return __error('暂无');
        } else {
            return __success('成功', $info);
        }
    }

    // 设置优惠，滞纳金
    public function set_no_pay_money() {
        $data = $this->request->post();
        unset($data['token']);
        $info=Db::name('wy_no_payment')->where('id', $data['id'])->find();
        $val = $data['value'];
        if ($val > $info['price']) {
            return __error('优惠金额不应该超过应收金额');
        }
        if ($data['type'] == 2) {
            $data['value'] = $info['price'] * $data['value'];
        }
        if($data['name']=='offer'){
            $au = Db::name('wy_auth')->where('id','=',$this->userinfo['wy_auth_id'])->value('menus');
            if ($au) {
                $io = explode(',',$au);
                if (!in_array('68',$io)) {
                    return __error('无优惠权限');
                }
            }
            $n = '优惠金额';
            $updateArr=[$data['name'] => $data['value'],'money'=>$info['price']+$info['overdue']-$data['value']];
            $start_price = $info['offer'];
        }else{
            $n = '滞纳金';
            $updateArr=[$data['name'] => $data['value'],'money'=>(float)$info['price']-$info['offer']+(float)$data['value']];
            $start_price = $info['overdue'];
        }
        Db::name('wy_no_payment')->where('id', $data['id'])->update($updateArr);
        $srr['no_payment_id'] = $data['id'];
        $srr['start_price'] = $start_price;
        $srr['end_price'] = $data['value'];
        $srr['type'] = $data['name'];
        $srr['style'] = $data['type'];
        $srr['remarks'] = $data['bz'];
        $srr['is_deleted'] = 0;
        $srr['in'] = $val;
        $srr['createtime'] = date('Y-m-d H:i:s',time());
        if ($data['bz']) {
            $srr['name'] = $srr['createtime'].'；'.$this->userinfo['name'].'改变'.$n.'为'.$data['value'].'；备注：'.$data['bz'];
        } else {
            $srr['name'] = $srr['createtime'].'；'.$this->userinfo['name'].'改变'.$n.'为'.$data['value'];
        }
        Db::name('wy_no_payment_remarks')->insert($srr);
        return __success('修改成功');
    }

    // 获取临时收费金额
    public function get_project_fix() {
        $data = $this->request->post();
        unset($data['token']);
        $info = Db::name('wy_toll_project_detial')->where(['id' => $data['id']])->find();
        if(empty($info)) {
            return __error('暂无数据');
        } else {
            return __success('查询成功', $info);
        }
    }
    // 获取模板列表
    public function get_toll_model() {
        $param = $this->request->post();
        $where = [];
        $where[] = ['status','=',0];
        if(!empty($param['name'])) $where[] = ['name', 'LIKE', '%'.$param['name'].'%'];
        if(!empty($param['type'])) $where[] = ['type', $param['type']];
        $data = Db::name('wy_toll_model')->where($where)->select();
        foreach ($data as $k=>$v) {
            $data[$k]['type_text'] = $this->model_type_text($v['type']);
            $data[$k]['paper_text'] = $this->model_paper_text($v['paper_type']);
            $data[$k]['layout_text'] = $this->model_layout_text($v['layout']);
        }
        if(empty($data)) {
            return __error('暂无数据');
        } else {
            return __success('查询成功', $data);
        }
    }
    private function model_type_text($val){
        if ($val == 1) {
            return '收据模板';
        } elseif ($val == 2) {
            return '通知单模板';
        }
    }
    private function model_paper_text($val){
        return 'A'.$val;
    }
    private function model_paper_detail($val,$width = 0,$height = 0){
        if ($val == 3) {
            $width = 2970;
            $height = 4200;
        } elseif ($val == 4) {
            $width = 2100;
            $height = 2970;
        } elseif ($val == 5) {
            $width = 148;
            $height = 210;
        }
        return ['width'=>$width,'height'=>$height];
    }
    private function model_layout_text($val){
        if ($val == 2) {
            return '横向';
        } elseif ($val == 1) {
            return '纵向';
        }
    }
    // 添加模板
    public function save_toll_model() {
        $data = $this->request->post();
        unset($data['token']);
        $can_add = Db::name('wy_toll_model')->where('name', $data['name'])->find();
        if(!empty($can_add)) {
            return __error('请勿重复添加数据');
        } else {
            $data['create_time'] = time();
            $ad = Db::name('wy_toll_model')->insert($data);
            return $ad >= 1 ? __success('成功') : __error('添加失败');
        }
    }
    // 获取模板详情
    public function select_toll_model() {
        $data = $this->request->post();
        unset($data['token']);
        $data = Db::name('wy_toll_model')->where('id', $data['id'])->find();
        if(empty($data)) {
            return __error('数据错误');
        } else {
            return __success('查询成功', $data);
        }
    }
    // 更新模板
    public function update_toll_model() {
        $data = $this->request->post();
        unset($data['token']);
        $models=model('WyTollModel');
        $is_data = $models->where('id', $data['id'])->find();
        if(empty($is_data)) {
            return __error('数据不存在');
        } else {
            $ad = $models->isUpdate(true)->save($data);
            return $ad  ? __success('成功') : __error('更新失败');
        }
    }
    public function get_copy_table_list(){
        $ar_id = $this->request->get('ar_id','0');
        $bid = $this->request->get('bid','0');
        // $cm_mg_id = $this->request->get('cm_mg_id','');
        $cm_mg_id = $this->userinfo['cm_message_id']?$this->userinfo['cm_message_id']:'';
        $page = $this->request->get('pageNo','1');
        $pageSize = $this->request->get('pageSize','10');
        $name = $this->request->get('name');
        $bd_id = $this->request->get('bd_id');
        $sf = $this->request->get('sf');
        $start_time = $this->request->get('start_time');
        $end_time = $this->request->get('end_time');
        $gj_name = $this->request->get('gj_name');
        $where = [];
        if ($gj_name || $name) {
            $t_name = empty($gj_name) ? $name : $gj_name;
//            $info_id = Db::name('wy_bd_info')->where('name','like','%'.$t_name.'%')->where('is_deleted','=',0)->column('id');
//            if ($info_id) {
//                $where[] = ['bd_info_id','in',$info_id];
//            }
            $house_id = Db::name('wy_houses')->where('name','like','%'.$t_name.'%')->where('is_deleted','=',0)->column('id');
            $where[] = ['house_id','in',$house_id];
//            $pro_id = model('toll_project')->where('name','=',$t_name)->value('id');
//            if ($pro_id) {
//                $where[] = ['project_id','=',$pro_id];
//            }
//            $pro_det_id = model('toll_project_detial')->where('name','=',$t_name)->value('id');
//            if ($pro_det_id) {
//                $where[] = ['project_detial_id','=',$pro_det_id];
//            }
//            if ($t_name && empty($where)) {
//                $where[] = ['project_id','=',-1];
//            }
        }
        if ($bd_id) {
            $where[] = ['bd_info_id','=',$bd_id];
        }
        if ($sf) {
            $where[] = ['project_detial_id','=',$sf];
        }
        if ($start_time && $end_time) {
            $where[] = ['start_time','<=',$end_time];
            $where[] = ['end_time','>=',$start_time];
        }
        $models=model('copy_table');
        $getdata=$models->getList($ar_id,$cm_mg_id,$bid,$page,$where,$pageSize);
        foreach($getdata['data'] as $k =>$v){
            $getdata['data'][$k]['remarks'] = empty($v['remarks']) ? '还没有备注内容' : $v['remarks'];
            if (date('H',strtotime($v['start_time'])) == 0 && date('i',strtotime($v['start_time'])) == 0) {
                $start = date('Y-m-d',strtotime($v['start_time']));
            } else {
                $start = $v['start_time'];
            }
            if (date('H',strtotime($v['end_time'])) == 0 && date('i',strtotime($v['start_time'])) == 0) {
                $end = date('Y-m-d',strtotime($v['end_time']));
            } else {
                $end = $v['end_time'];
            }
            $getdata['data'][$k]['s_e_time'] = $start.'/'.$end;
            if($v['type']==2){
                
            }else{
                $getdata['data'][$k]['status_text']='正常';
                $getdata['data'][$k]['status']='1';
            }
        }
        return __success('ok',$getdata);
    }
    public function get_copy_table_del(){
        $param = $this->request->post();
        $models_cp=model('copy_table');
        $models_nopay=model('NoPay');
        if($param['type']==1){
            $hasPayed=$models_nopay->where('Batch_num','sd_'.$param['id'])->where('status',1)->find();
            if($hasPayed){
                return __error('有已缴费数据，禁止删除');
            }
            $resD=$models_nopay->where('Batch_num','sd_'.$param['id'])->setField('is_deleted',1);
            $resE=$models_cp->where('id',$param['id'])->setField('is_deleted',1);
            if($resD || $resE){
                return __success('删除成功!');
            }else{
                return __error('删除失败！');
            }
        }else{
            $infos=$models_cp->where('id',$param['id'])->find();
            $allcount=$models_nopay->where('Batch_num','gt_'.$param['id'])->count();
            if($allcount<$infos['exc_num']){
                return __error('账单还未完全生成！');
            }
            $hasPayed=$models_nopay->where('Batch_num','gt_'.$param['id'])->where('status',1)->find();
            if($hasPayed){
                return __error('有已缴费数据，禁止删除');
            }
            $resD=$models_nopay->where('Batch_num','gt_'.$param['id'])->setField('is_deleted',1);
            $resE=$models_cp->where('id',$param['id'])->setField('is_deleted',1);
            if($resD || $resE){
                return __success('删除成功!');
            }else{
                return __error('删除失败！');
            }
        }
    }
    public function add_copy_table (){
        $param = $this->request->post();
        $param['user_id']=$this->userinfo['id'];
        $models=model('copy_table');
        if($param['addType']==1){
            if($param['end_key']<$param['start_key']){
                return __error('结束度数必须大于开始！');
            }
        }
        $rest=$models->addCopyTable($param);
        if($rest){
            if($param['addType']==1){
                return __success('添加成功！');
            }else{
                return __success('添加成功！',$rest);
            }
           
        }else {
            $errInfo=$models->getError()?$models->getError():'添加错误！';
            return __error($errInfo);
        }
    }
    // 抄表导入
    public function copy_table_add_all(){
        $bd_info_id = $this->request->post('bd_info_id');
        $fileList = $this->request->post('fileList');
        $site = $this->request->post('site');
        $xl = explode('.x',$fileList['file']['response']['data']['pathName'])[1];
        echo json_encode(inExcel($fileList['file']['response']['data']['pathName'],$bd_info_id,'wy_copy_table',$site,$xl));
        die;
    }
    // 缴费导入
    public function be_pay_add_all(){
        $bd_info_id = $this->request->post('bd_info_id');
        $fileList = $this->request->post('fileList');
        $site = $this->request->post('site');
        $xl = explode('.x',$fileList['file']['response']['data']['pathName'])[1];
        echo json_encode(inExcel($fileList['file']['response']['data']['pathName'],$bd_info_id,'wy_payment',$site,$xl,$this->userinfo['id']));
        die;
    }
    // 未缴费导入
    public function not_pay_add_all(){
        $bd_info_id = $this->request->post('bd_info_id');
        $fileList = $this->request->post('fileList');
        $site = $this->request->post('site');
        $xl = explode('.x',$fileList['file']['response']['data']['pathName'])[1];
        echo json_encode(inExcel($fileList['file']['response']['data']['pathName'],$bd_info_id,'wy_no_payment',$site,$xl));
        die;
    }
    // 删除缴费账单
    public function pay_more_dec(){
        $data = $this->request->post();
        unset($data['token']);
        $where[] = ['id','in',$data['ids']];
        $where[] = ['is_deleted','=',0];
        $data = Db::name('wy_payment')->where($where)->setField('is_deleted',1);
        if(empty($data)) {
            return __error('删除失败');
        } else {
            return __success('删除成功', $data);
        }
    }
    // 删除模板
    public function del_model(){
        $data = $this->request->post();
        unset($data['token']);
        $data = Db::name('wy_toll_model')->where('id',$data['id'])->setField('status',-1);
        if(empty($data)) {
            return __error('删除失败');
        } else {
            return __success('删除成功', $data);
        }
    }
    public function pay_more_back(){
        $data = $this->request->post();
        unset($data['token']);
        $where[] = ['id','in',$data['ids']];
        $where[] = ['is_deleted','=',1];
        $data = Db::name('wy_payment')->where($where)->setField('is_deleted',0);
        if(empty($data)) {
            return __error('恢复失败');
        } else {
            return __success('恢复成功', $data);
        }
    }
    // 删除未缴费账单
    public function no_pay_more_dec(){
        $data = $this->request->post();
        unset($data['token']);
        $where[] = ['id','in',$data['ids']];
        // $where[] = ['is_deleted','=',0];
        $data = Db::name('wy_no_payment')->where($where)->delete();
        if(empty($data)) {
            return __error('删除失败');
        } else {
            return __success('删除成功', $data);
        }
    }
    public function deposit_detail(){
        $data = $this->request->post();
        unset($data['token']);
        $uid = Db::name('wy_deposit_list')->where('id',$data['id'])->value('house_user_id');
        $info = Db::name('wy_deposit_list')->where('house_user_id',$uid)->field('*,sum(balance) as balance')->find();
        $ty = Db::name('wy_deposit_list')->where('house_user_id',$uid)->where('type',1)->value('balance');
        $zy = Db::name('wy_deposit_list')->where('house_user_id',$uid)->where('type',2)->group('category')->field('*,sum(balance) as balance')->select();
        $info['ty'] = $ty;
        $info['zy'] = $zy;
        if ($info['house_user_id'] > 0) {
            $user = Db::name('wy_house_user')->where('id',$info['house_user_id'])->field('name,phone')->find();
            $info['name'] = $user['name'];
            $info['phone'] = $user['phone'];
        }
        if ($info['bd_info_id'] > 0) $info['bd_info_text'] = Db::name('wy_bd_info')->where('id',$info['bd_info_id'])->value('name');
        if ($info['houses_id'] > 0) {
            $hdt = Db::name('wy_houses')->where('id',$info['houses_id'])->field('bd_info_id,building_id,unit_id,name')->find();
            $info['house_build'] = Db::name('wy_building')->where('id',$hdt['building_id'])->value('name');
            $info['house_unit'] = Db::name('wy_unit')->where('id',$hdt['unit_id'])->value('name');
            $info['house_name'] = $hdt['name'];
        }
        if ($info['car_id'] > 0) $info['car_text'] = Db::name('wy_car')->where('id',$info['car_id'])->value('name');
        if ($info['car_area_id'] > 0) $info['car_area_text'] = Db::name('wy_area_car')->where('id',$info['car_area_id'])->value('name');
        if ($info['type'] == 1) {
            $info['type_text'] = '通用预存款充值';
        } elseif ($info['type'] == 2) {
            $info['type_text'] = '专用预存款充值';
        }
        return __success('查询成功', $info);
    }
    public function deposit_back(){
        $data = $this->request->post();
        unset($data['token']);
        $data = $data['form'];
        $in = Db::name('wy_deposit_list')->where('id',$data['id'])->field('id,bd_info_id,house_user_id,price,balance,status')->find();
        if ($in) {
            if ($in['balance']  < $data['money']) {
                return __error('余额不足');
            }
            $yu = $in['balance'] - $data['money'];
            $vrr['price'] = $yu;
            $vrr['money'] = $yu;
            $vrr['balance'] = $yu;
            if ($yu == 0) {
                $vrr['status'] = 2;
                $info = Db::name('wy_deposit_list')->where('id',$in['id'])->update($vrr);
            } else {
                $info = Db::name('wy_deposit_list')->where('id',$in['id'])->update($vrr);
            }
            if($info) {
                $user = Db::name('wy_house_user')->where('id',$in['house_user_id'])->field('name,phone,money')->find();
                $money = Db::name('wy_house_user')->where('id',$in['house_user_id'])->setDec('money',$in['price']);
                if ($money) {
                    $store['deposit_list_id'] = $data['id'];
                    $store['bd_info_id'] = $in['bd_info_id'];
                    $store['house_user_id'] = $in['house_user_id'];
                    $store['price'] = $data['money'];
                    $store['money'] = $data['money'];
                    $store['type'] = 3;
                    $store['come'] = $this->userinfo['id'];
                    $store['createtime'] = date('Y-m-d H:i:s', time());
                    $store['remarks'] = $data['remarks'];
                    $store['user_name'] = @$user['name'] ?: '';
                    $store['user_phone'] = @$user['phone'] ?: '';
                    Db::name('wy_deposit_list_store')->insert($store);
                    return __success('退款成功');
                } else {
                    Db::name('wy_deposit_list')->where('id',$in['id'])->setField('status',1);
                    return __error('用户扣除失败');
                }
            } else {
                return __error('退款错误');
            }
        } else {
            return __error('没有找到该预存款');
        }
    }
    public function pay_back(){
        $data = $this->request->post();
        unset($data['token']);
        $in = Db::name('wy_payment')->where('id',$data['id'])->field('id,house_user_id,price,status')->find();
        if ($in) {
            $info = Db::name('wy_payment')->where('id',$in['id'])->setField('status',2);
            if($info) {
                $u_m = Db::name('wy_house_user')->where('id',@$in['house_user_id'])->value('money');
                if ($u_m) {
                    $money = Db::name('wy_house_user')->where('id',@$in['house_user_id'])->setInc('money',$in['price']);
                } else {
                    $money = Db::name('wy_house_user')->where('id',@$in['house_user_id'])->setField('money',$in['price']);
                }
                if ($money) {
                    return __success('退款成功');
                } else {
                    Db::name('wy_payment')->where('id',$in['id'])->setField('status',1);
                    return __error('用户扣除失败');
                }
            } else {
                return __error('退款失败');
            }
        } else {
            return __error('没有找到该预存款');
        }
    }
    public function pay_store_back(){
        $data = $this->request->post();
        unset($data['token']);
        if (empty($data['back_tui'])) {
            return __error('请至少选择一项');
        }
        $list = Db::name('wy_payment_store')->where('id','in',$data['back_tui'])->field('id,payment_id,price,status')->select();
        if ($list) {
            $pay_id = 0;
            $sta = false;
            foreach ($list as $k=>$v) {
                $sta = Db::name('wy_payment_store')->where(['status'=>'已缴','id'=>$v['id']])->setField('status','已退');
                $u_m = Db::name('wy_house_user')->where('id',@$v['user_id'])->value('money');
                if ($u_m) {
                    Db::name('wy_house_user')->where('id',@$v['user_id'])->setInc('money',$v['price']);
                } else {
                    Db::name('wy_house_user')->where('id',@$v['user_id'])->setField('money',$v['price']);
                }
                $pay_id = $v['payment_id'];
            }
            $is_t = Db::name('wy_payment')->where('id',$pay_id)->value('status');
            $has = Db::name('wy_payment_store')->where('payment_id','=',$pay_id)->where('status','in',implode(',',['已缴','已退']))->count();
            $has1 = Db::name('wy_payment_store')->where('payment_id','=',$pay_id)->where('status','in',implode(',',['已退']))->count();
            if ($is_t == 1 && $has == $has1) {
                Db::name('wy_payment')->where('id',$pay_id)->setField('status',2);
            }
            if($sta) {
                return __success('退款成功');
            } else {
                return __error('退款失败');
            }
        } else {
            return __error('没有找到该预存款');
        }
    }
    // 预存款打印模板
    public function dep_det() {
        $data = $this->request->post();
        unset($data['token']);
        $modelField = 'id,name,type,paper_type,layout,num,range_t,range_r,range_b,range_l,width,remark';
        $info['model'] = Db::name('wy_toll_model')->where('status',0)->field($modelField)->select();
        foreach ($info['model'] as $k=>$v) {
            $paper_det = $this->model_paper_detail($v['paper_type']);
            $info['model'][$k]['p_width'] = $paper_det['width'];
            $info['model'][$k]['p_height'] = $paper_det['height'];
        }
        return __success('查询成功', $info);
    }
    // 获取预存款套餐列表
    public function get_des_list() {
        $data = $this->request->post();
        unset($data['token']);
        $data['data'] = Db::name('wy_deposit')->field('id,price,money')->select();
        $data['set'] = Db::name('wy_deposit_set')->value('type');
        if(empty($data)) {
            return __error('数据错误');
        } else {
            return __success('查询成功', $data);
        }
    }
    public function model_show(){
        $data = $this->request->post();
        unset($data['token']);
        $det = Db::name('wy_toll_model')->where('id',$data['edit_id'])->find();
        if(empty($det)) {
            return __error('数据错误');
        } else {
            $mod = Db::name('wy_pt_mol')->where('mid',$det['id'])->select();
            $pt = Db::name('wy_pt')->select();
            $det['pt_t'] = [];
            $det['pt_b'] = [];
            $det['pt_f'] = [];
            foreach ($pt as $k=>$v) {
                if ($v['type'] == 1) {
                    array_push($det['pt_t'],$v);
                } elseif ($v['type'] == 2) {
                    array_push($det['pt_b'],$v);
                } elseif ($v['type'] == 3) {
                    array_push($det['pt_f'],$v);
                }
            }
            $det['mod_h'] = [];
            $det['mod_b'] = [];
            $det['mod_f'] = [];
            $t = [];
            foreach ($mod as $k=>$v) {
                if ($v['type'] == 1) {
                    array_push($det['mod_h'],$v);
                } elseif ($v['type'] == 2) {
                    array_push($det['mod_b'],$v);
                    $t[$k]['title'] = $v['name'];
                    $t[$k]['dataIndex'] = $this->os($v['name']);
                } elseif ($v['type'] == 3) {
                    array_push($det['mod_f'],$v);
                }
            }
            $det['mod_x'] = array_merge($t);
            $det['now_time'] = date('Y-m-d H:i',time());
            return __success('查询成功', $det);
        }
    }
    private function os($name = null){
        if ($name) {
            switch ($name){
                case '房屋/车位/车辆':
                    return  'info';
                    break;
                case '楼宇':
                    return  'building';
                    break;
                case '单元':
                    return  'unit';
                    break;
                case '收费项目':
                    return  'name';
                    break;
                case '收费标准':
                    return  'project';
                    break;
                case '开始时间':
                    return  'create_time';
                    break;
                case '结束时间':
                    return  'end_time';
                    break;
                case '起度':
                    return  'info';
                    break;
                case '止度':
                    return  'info';
                    break;
                case '数量':
                    return  'num';
                    break;
                case '单价':
                    return  'one_money';
                    break;
                case '金额':
                    return  'money';
                    break;
                case '优惠':
                    return  'offer';
                    break;
                case '违约金':
                    return  'overdue';
                    break;
                case '应收金额':
                    return  'price';
                    break;
                case '账单期数':
                    return  'info';
                    break;
                case '备注':
                    return  'remarks';
                    break;
                case '预存款抵扣':
                    return  'deduction';
                    break;
                default:
                    return  '';
            }

        } else {
            return '';
        }
    }
    public function set_all_pt(){
        $data = $this->request->get();
        unset($data['token']);
        $list = Db::name('wy_pt')->select();
        return __success('查询成功', $list);
    }
    public function rm_mod_pt(){
        $data = $this->request->get();
        unset($data['token']);
        $id = $data['id'];
        $t = Db::name('wy_pt_mol')->where('id','=',$id)->delete();
        if ($t) {
            return __success('删除成功');
        } else {
            return __error('删除失败');
        }
    }
    public function set_mod_pt(){
        $data = $this->request->get();
        $pt_id = $data['pt_id'];
        $mid = $data['mid'];
        unset($data['token']);
        if ($pt_id) {
            $has = Db::name('wy_pt_mol')->where('mid','=',$mid)->where('pid','=',$pt_id)->value('id');
            if ($has) {
                return __error('已存在');
            }
            $pt = Db::name('wy_pt')->where('id','=',$pt_id)->find();
            $srr['mid'] = $data['mid'];
            $srr['name'] = $data['title'] ?: $pt['name'];
            $srr['pid'] = $pt['id'];
            $srr['type'] = $pt['type'];
            $o = Db::name('wy_pt_mol')->insertGetId($srr);
            if ($o) {
                return __success('设置成功');
            } else {
                return __error('设置错误');
            }
        } else {
            return __error('数据错误');
        }
    }
    public function get_zy() {
        $data = $this->request->post();
        unset($data['token']);
        $list['data'] = Db::name('wy_deposit_zy')->where('status',0)->select();
        if(empty($list)) {
            return __error('数据错误');
        } else {
            foreach ($list['data'] as $k=>$v) {
                $list['data'][$k]['zy_name'] = $v['name'];
            }
            return __success('查询成功', $list);
        }
    }
    // 预存款导入
    public function des_in(){
        $fileList = $this->request->post('fileList');
        $site = $this->request->post('site');
        $bd = $this->request->post('bd_info_id');
        $xl = explode('.x',$fileList['file']['response']['data']['pathName'])[1];
        echo json_encode(inExcel($fileList['file']['response']['data']['pathName'],$bd,'deposit_list',$site,$xl));
        die;
    }
    // 保存预存款自定义
    public function save_dep(){
        $data = $this->request->post();
        unset($data['token']);
        $ids = 0;
        $sta_has = Db::name('wy_deposit_set')->value('id');
        if (!$sta_has) {
            Db::name('wy_deposit_set')->insert(['type'=>1]);
        }
        $sta = Db::name('wy_deposit_set')->field('id,type')->find();
        if ($data['sta'] == 2) {
            foreach ($data['dep'] as $k=>$v) {
                if (!$v['price'] || !$v['money']) {
                    return __error('请填写完整');
                }
                $has = Db::name('wy_deposit')->where(array('money'=>$v['money'],'price'=>$v['price']))->value('id');
                if (!$has) {
                    $srr['money'] = $v['money'];
                    $srr['price'] = $v['price'];
                    $ids = Db::name('wy_deposit')->insert($srr);
                }
            }
            if ($sta['type'] != 2) {
                $ids = Db::name('wy_deposit_set')->where(array('id'=>$sta['id']))->setField('type',2);
            }
        } elseif ($data['sta'] == 1) {
            if ($sta['type'] != 1) {
                $ids = Db::name('wy_deposit_set')->where(array('id'=>$sta['id']))->setField('type',1);
            }
        } elseif ($data['sta'] == 3) {
            if ($sta['type'] != 3) {
                $ids = Db::name('wy_deposit_set')->where(array('id'=>$sta['id']))->setField('type',3);
            }
        }
        if(empty($ids)) {
            return __error('数据错误');
        } else {
            return __success('修改成功', $ids);
        }
    }
    public function save_zy(){
        $data = $this->request->post();
        unset($data['token']);
        $sta_has = '';
        if ($data['dt']) {
            $dt = $data['dt'];
            foreach ($dt as $k=>$v) {
                $sta_has = Db::name('wy_deposit_zy')->where('name',$v['zy_name'])->where('status',0)->value('id');
                if (!$sta_has) {
                    $sta_has = Db::name('wy_deposit_zy')->insert(['name'=>$v['zy_name'],'status'=>0]);
                }
            }
        }
        if (empty($sta_has)) {
            return __error('数据错误');
        } else {
            return __success('修改成功');
        }
    }
    // 删除自定义预存款
    public function del_dep(){
        $data = $this->request->post();
        unset($data['token']);
        $data = Db::name('wy_deposit')->delete($data['id']);
        if(empty($data)) {
            return __error('数据错误');
        } else {
            return __success('删除成功', $data);
        }
    }
    // 删除类型
    public function del_zy(){
        $data = $this->request->post();
        unset($data['token']);
        $data = Db::name('wy_deposit_zy')->delete($data['id']);
        if(empty($data)) {
            return __error('数据错误');
        } else {
            return __success('删除成功', $data);
        }
    }
    //
    public function get_pro_list(){
        $data = $this->request->post();
        unset($data['token']);
        $data = Db::name('wy_toll_project_detail')->where('style','gt',0)->field('id,project_id,name')->select();
        if(empty($data)) {
            return __error('数据错误');
        } else {
            return __success('数据查询成功', $data);
        }
    }

    public function add_others(){
        $param = $this->request->post();
        unset($param['token']);
        if(empty($param['name'])) return __error('收费项目不能为空');
        if(empty($param['bd_info_id'])) return __error('小区不能为空');
        if(empty($param['username'])) return __error('用户姓名不能为空');
        if(empty($param['phone'])) return __error('手机号不能为空');
        if(empty($param['money'])) return __error('收费金额不能为空');
        $userModel=model('wy_house_user');
        $userinfo=$userModel->where('phone',$param['phone'])->find();
        if($userinfo){
            if($userinfo['name']!= $param['username']){
                $userModel->isUpdate(true)->save(['id'=>$userinfo['id'],'name'=>$param['username']]);
            }
            $param['house_user_id']=$userinfo['id'];
            
        }else {
            $userid=$userModel->insertGetId(['name'=>$param['username'],'phone'=>$param['phone']]);
            $param['house_user_id']=$userid;
        }
        unset($param['username']);
        unset($param['phone']);
        $param['price']=$param['money'];
        $param['action_user']=$this->userinfo['name'];
        $param['pay_style']=1;
        $param['pay_time']=date("Y-m-d H:i:s");
        $res= model('wy_payment')->addOhters($param,1);
        if($res){
            return __success('收费成功');
        }else {
            return __success('收费失败');
        }
    }
    public function add_quick(){
        $param = $this->request->post();
        unset($param['token']);
        if(empty($param['bd_info_id'])) return __error('小区不能为空');
        if(empty($param['money'])) return __error('收费金额不能为空');
        // $param['name']='临时停车收费';
        $param['price']=$param['money'];
        $param['action_user']=$this->userinfo['id'];
        // $param['pay_style']=$param['pay_style'];
        $param['pay_time']=date('Y-m-d H:i:s');
        $res= model('wy_payment')->addOhters($param,2);
        if($res){
            return __success('收费成功');
        }else {
            return __success('收费失败');
        }
    }
    //生成收费到指定时间
    public function add_payto_times(){
        $param = $this->request->post();
        $times= $param['pay_time'];
        $house_id=$param['id'];
        $type=$param['type'];
        //获取绑定的收费标准
        $toll_pro_dz=db('wy_toll_bz_middle')->where('houses_id',$house_id)->where('type', $type)->select();
        if (!$toll_pro_dz) {
            return __error('当前房间还没有绑定周期性收费标准，请在绑定周期性收费标准后再试！');
        }
        foreach($toll_pro_dz as $k=>$v){
            //判断收费标准是否存在
            $toll_pro_info=model('toll_project_detial')->where('id',$v['project_detail_id'])->where('is_deleted','0')->find();
            if($toll_pro_info){
                 //判断是否在有效时间内
                if($v['start_time']<=$times && ($v['end_time']>=$times|| $v['is_have_time']==1)){
                    //判断是否在时间周期里
                    if($type==1){
                        $nopayinfo=model('no_pay')->where('house_id',$house_id)->where('toll_project_detial_id','=',$v['project_detail_id'])->find();
                        $infosModel=model('wy_houses');
                    }elseif ($type==2) {
                        $nopayinfo=model('no_pay')->where('car_area_id',$house_id)->where('toll_project_detial_id','=',$v['project_detail_id'])->find();
                        $infosModel=model('wy_car_area');
                    }else{
                        $nopayinfo=model('no_pay')->where('car_id',$house_id)->where('toll_project_detial_id','=',$v['project_detail_id'])->find();
                        $infosModel=model('wy_car');
                    }
                    
                    if(@$nopayinfo){
                        if(@$nopayinfo['end_time']<$times){
                            
                            $endtime=$nopayinfo['end_time'];
                            $time_start=date('Y-m-d',strtotime("$endtime +1 day"));
                            $addarr=[];
                            $houseinfo=$infosModel->where('id',$house_id)->find();
                            $price=model('copy_table')->getNormalMoney($houseinfo,$type,$v,$toll_pro_info);
                            while($time_start<$times){
                                $addNoPay=[
                                    'bd_info_id'=>$houseinfo['bd_info_id'],
                                    'createtime'=>date('Y-m-d H:i:m'),
                                    'price'=>$price,
                                    'money'=>$price,
                                    'one_money'=>$toll_pro_info['price'],
                                    'num'=>$type<=2?$houseinfo['area']:1,
                                    'name'=>$toll_pro_info['name'],
                                    'toll_project_detial_id'=>$v['project_detail_id'],
                                    'start_time'=>$time_start,
                                    'end_time'=>date('Y-m-d',strtotime('-1 day',strtotime('+'.$toll_pro_info['count_week'].' month',strtotime($time_start)))),
                                    'Batch_num'=>''
                                ];
                                // dump($addNoPay);die;
                                if($type==1){
                                    $addNoPay['house_id']=$house_id;
                                    $addNoPay['building_id']=$houseinfo['building_id'];
                                }elseif ($type==2) {
                                    $addNoPay['car_area_id']=$house_id;
                                }else{
                                    $addNoPay['car_id']=$house_id;
                                }
                                $time_start=date('Y-m-d',strtotime('+'.$toll_pro_info['count_week'].' month',strtotime($time_start)));
                                //判断同一个收费项一个时间段是否存在
                                $ishas=DB::name('wy_no_payment')
                                ->where('toll_project_detial_id',$addNoPay['toll_project_detial_id'])
                                ->where('start_time',$addNoPay['start_time'])
                                ->where('end_time',$addNoPay['end_time'])
                                ->find();
                                if(!$ishas){
                                    $addarr[]=$addNoPay;
                                }
                            }
                            $re=db('wy_no_payment')->insertAll($addarr);
                            if (!$re) {
                                if (empty($addarr)) {
                                    return __error('生成缴费记录失败,同一个收费项一个时间段已存在');
                                } else {
                                    return __error('生成缴费记录失败');
                                }
                            }
                        }
                    }else {
                        // dump($toll_pro_dz);die;
                        // $toll_pro_info=model('toll_project_detial')->where('id',$v['project_detail_id'])->find();
                        $time_start=$v['start_time'];
                        $addarr=[];
                        $houseinfo=$infosModel->where('id',$house_id)->find();
                        $price=model('copy_table')->getNormalMoney($houseinfo,$type,$v,$toll_pro_info);
                        while($time_start<$times){
                            $addNoPay=[
                                'bd_info_id'=>$houseinfo['bd_info_id'],
                                'createtime'=>date('Y-m-d H:i:m'),
                                'price'=>$price,
                                'money'=>$price,
                                'one_money'=>$toll_pro_info['price'],
                                'num'=>$type<=2?$houseinfo['area']:1,
                                'name'=>$toll_pro_info['name'],
                                'toll_project_detial_id'=>$v['project_detail_id'],
                                'start_time'=>$time_start,
                                'end_time'=>date('Y-m-d',strtotime('-1 day',strtotime('+'.$toll_pro_info['count_week'].' month',strtotime($time_start)))),
                                'Batch_num'=>''
                            ];
                        
                            if($type==1){
                                $addNoPay['house_id']=$house_id;
                                $addNoPay['building_id']=$houseinfo['building_id'];
                            }elseif ($type==2) {
                                $addNoPay['car_area_id']=$house_id;
                            }else{
                                $addNoPay['car_id']=$house_id;
                            }
                            $time_start=date('Y-m-d',strtotime('+'.$toll_pro_info['count_week'].' month',strtotime($time_start)));
                            $addarr[]=$addNoPay;
                        }
                        $re=db('wy_no_payment')->insertAll($addarr);
                        if (!$re) {
                            return __error('生成缴费记录失败');
                        }
                    }
                }
            }
           
        }
        return __success('ok');
    }
    public function dailyGetList(){
        $param = $this->request->post();
        if(@$param['bd_info_id']){
            $where[] = ['bd_info_id','=',$param['bd_info_id']];
        }else{
            $ar_id = $this->userinfo['pms_ar_id'];
            $cm_mg_id =  $this->userinfo['cm_message_id'];
            if(@$cm_mg_id){
                $where[]=['bd_info_id','in',$cm_mg_id];
            }else {
                $getMgIds=model('wy_bd_info')->dbIds($ar_id);
                if($getMgIds){
                    $where[]=['bd_info_id','in',$getMgIds];
                }else {
                    $where[]=['bd_info_id','=',0];
                }      
            }
        }
        if(@$param['start_time'] && @$param['end_time']){
            $where[] = ['pay_time','>=',$param['start_time']];
            $where[] = ['pay_time','<=',$param['end_time']];
        }
        if (@$param['sou']) {
            $where[] = ['pay_style','=',$param['sou']];
        }
        if(@$param['jine_min'] && @$param['jine_max']){
            $where[] = ['money','>=',$param['jine_min']];
            $where[] = ['money','<=',$param['jine_max']];
        }
        if(!empty($param['name'])) $where[] = ['name','like','%'.$param['name'].'%'];        
        $where[] = ['is_deleted','=',0];
        $models=model('wy_payment');
        
        $status_text = [
            '0' => '',
            '1' => '已收',
            '2' => '已退',
            '3' => '撤销'
        ];
        $list = $models->where($where)->page($param['pageNo'],$param['pageSize'])->order('id desc')->select();
        foreach($list as $k => $v) {
            $list[$k]['style_text'] = payTypeTxt($v['pay_style']);
            $list[$k]['status_text'] = $status_text[$v['status']];
            $list[$k]['nums'] = strtotime($v['pay_time']).$v['id'];
            $list[$k]['pay_time'] = date('Y-m-d',strtotime($v['pay_time']));
            $list[$k]['info'] = '';
            if($v['bd_info_id'] > 0) $list[$k]['bd_info_id_text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name');
            if($v['houses_id'] > 0) $list[$k]['info'] = $list[$k]['info'].' '.Db::name('wy_houses')->where('id', $v['houses_id'])->value('name');
            if($v['car_area_id'] > 0) $list[$k]['info'] = $list[$k]['info'].' '.Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
            if($v['car_id'] > 0) $list[$k]['info'] = $list[$k]['info'].' '.Db::name('wy_car')->where('id', $v['car_id'])->value('name');
            if($v['house_user_id'] > 0) $list[$k]['user_text'] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name');
        }
        $count = $models->where($where)->count();
        return __success('ok',['data'=>$list,'pageSize'=>$param['pageSize'],'pageNo'=>$param['pageNo'],'totalCount'=>$count,'totalPage'=>ceil($count / $param['pageSize'])]);
    }
    public function pro_exp(){
        $param = $this->request->post();
        $where[] = ['is_deleted', '=', 0];
        if (@$param['bd_info_id']) {
            $where[] = ['bd_info_id','=',$param['bd_info_id']];
        }
//        if (@$param['pro_id']) {
//            $where[] = ['project_id','=',$param['pro_id']];
//        }
        if (@$param['pro_id']) {
            $where[] = ['project_detial_id','=',$param['pro_id']];
        }
        if (@$param['start_time'] && @$param['end_time']) {
            $where[] = ['end_time','gt',$param['start_time']];
            $where[] = ['start_time','lt',$param['end_time']];
        }
        if (@$param['name']) {
            $house_id = Db::name('wy_houses')->where('name','like','%'.$param['name'].'%')->where('is_deleted','=',0)->column('id');
            $where[] = ['house_id','in',$house_id];
        }
        if($param['pageNo']==1){
            $bd_info=model('wy_bd_info')->where('id',$param['bd_info_id'])->find();
            $baseData=['ID','小区','房号','收费项目','收费标准','开始时间','结束时间','起度','止度','倍率','费用','备注'];
//            $baseData=['ID','小区','收费项目','收费标准','开始时间','结束时间','公摊','用户'];
            $excParamArr=[$bd_info['name'].'抄表单',$baseData,[]];
            cache('exc_'.$param['token'],$excParamArr);
        }
        $list = model('wy_copy_table')->where($where)->page($param['pageNo'],$param['pageSize'])->order('id desc')->select()->toArray();
        $addExc=[];
        foreach($list as $k => $v) {
            $list[$k]['info']='';
            $addExc[$k][] = $v['id'];
            $addExc[$k][] = Db::name('wy_bd_info')->where('id',$v['bd_info_id'])->value('name') ?: '';
            $addExc[$k][] = Db::name('wy_houses')->where('id',$v['house_id'])->value('name') ?: '';
            $addExc[$k][] = Db::name('wy_toll_project')->where('id',$v['project_id'])->value('name') ?: '';
            $addExc[$k][] = Db::name('wy_toll_project_detial')->where('id',$v['project_detial_id'])->value('name') ?: '';
            $addExc[$k][] = $v['start_time'];
            $addExc[$k][] = $v['end_time'];
            $addExc[$k][] = $v['start_k'];
            $addExc[$k][] = $v['end_k'];
            $addExc[$k][] = $v['times'];
            $addExc[$k][] = $v['ratio'];
            $addExc[$k][] = $v['remarks'];
//            $addExc[$k][] = Db::name('wy_house_user')->where('id',$v['user_id'])->value('phone');
        }
        $tempdataArr=cache('exc_'.$param['token']);
        $tempdatas=array_merge($tempdataArr[2],$addExc);
        $tempdataArr[2]=$tempdatas;
        cache('exc_'.$param['token'],$tempdataArr);
        $count = model('wy_copy_table')->where($where)->count();
        if ($count == 0) {
            return json_encode(['code'=>-1,'msg'=>'查无数据']);
        }
        if ($count > 5000) {
            return json_encode(['code'=>-1,'msg'=>'本次导出数据超出系统限制，请重新设置搜索条件']);
        }
        if($param['pageNo']==ceil($count / $param['pageSize'])){
            $res=exportExcel($tempdataArr[0],$tempdataArr[1],$tempdataArr[2]);
            return json_encode(['code'=>1000,'url'=>$res]);
        }else{
            return __success('ok',['data'=>$list,'pageSize'=>$param['pageSize'],'pageNo'=>$param['pageNo'],'totalCount'=>$count,'totalPage'=>ceil($count / $param['pageSize'])]);
        }
    }
    public function pro_exp_se(){
        $param = $this->request->post();
        $where[] = ['is_deleted', '=', 0];
        if (@$param['bd_info_id']) {
            $where[] = ['bd_info_id','=',$param['bd_info_id']];
        }
        $info = [];
        $bd_info_name = Db::name('wy_bd_info')->where('id',@$param['bd_info_id'])->value('name') ?: '';
        if ($bd_info_name) {
            $info = array_merge($info,['小区：'.$bd_info_name.'小区;']);
        }
//        if (@$param['pro_id']) {
//            $where[] = ['project_id','=',$param['pro_id']];
//        }
        if (@$param['pro_id']) {
            $where[] = ['project_detial_id','=',$param['pro_id']];
        }
        $type_name = Db::name('wy_toll_project_detial')->where('id',@$param['pro_id'])->value('name') ?: '';
        if ($type_name) {
            $info = array_merge($info,['项目/标准：'.$type_name.';']);
        }
        if (@$param['start_time'] && @$param['end_time']) {
            $where[] = ['end_time','gt',$param['start_time']];
            $where[] = ['start_time','lt',$param['end_time']];
            $info = array_merge($info,['开始/结束时间：'.$param['start_time'].'/'.$param['end_time'].';']);
        }
        if (@$param['name']) {
            $house_id = Db::name('wy_houses')->where('name','like','%'.$param['name'].'%')->where('is_deleted','=',0)->column('id');
            $where[] = ['house_id','in',$house_id];
            $info = array_merge($info,['关键词：'.$param['name']]);
        }
        $count = model('wy_copy_table')->where($where)->count();
        if ($count <= 0) {
            return json_encode(['code'=>-1,'msg'=>'查无数据']);
        }
        if ($count > 5000) {
            return json_encode(['code'=>-1,'msg'=>'本次导出数据超出系统限制，请重新设置搜索条件']);
        }
        if($info){
            array_splice($info,0,0,'导出条件为:');
            array_splice($info,0,0,'本次共导出'.$count.'条数据');
            array_push($info,'');
            array_push($info,'确定导出请点确认按钮');
            return __success($info);
        }else {
            return __success(['还未设定导出搜索条件，请先通过筛选搜索后再导出数据，未搜索将导出全部数据，单次导出数据条数上限为5000条','','确定导出请点确认按钮']);
        }
    }
    public function pay_exp(){
        $param = $this->request->post();
        $name = @$param['name'];
        $type = @$param['type'];
        $where = [];
        $bd_id = @$param['bd_id'];
        if ($bd_id) {
            $where[] = ['bd_info_id','=',$bd_id];
        }
        if ($type) {
            if ($type == 1) {
                $where[] = ['houses_id','gt',0];
            } elseif ($type == 2) {
                $where[] = ['car_area_id','gt',0];
            } elseif ($type == 3) {
                $where[] = ['car_id','gt',0];
            } elseif ($type == 4) {
                $where[] = ['house_user_id','gt',0];
            }elseif ($type == 5) {
                $pid = Db::name('wy_payment_store')->where('type','=','其它收费')->column('payment_id');
                $where[] = ['id','in',implode(',',$pid)];
            } elseif ($type ==  6) {
                $pid = Db::name('wy_payment_store')->where('type','=','临时停车费')->column('payment_id');
                $where[] = ['id','in',implode(',',$pid)];
            }
        }
        $start = @$param['start_time'];
        $end = @$param['end_time'];
        if ($start && $end) {
            $where[] = ['pay_time','between',$start.','.$end];
        }
        $jine_max = @$param['jine_max'];
        $jine_min = @$param['jine_min'];
        if ($jine_max && $jine_min >= 0) {
            if ($jine_max == $jine_min) {
                $where[] = ['price','=',$jine_min];
            } else {
                $where[] = ['price','between',$jine_min.','.$jine_max];
            }
        }
        $jine_max_s = @$param['jine_max_s'];
        $jine_min_s = @$param['jine_min_s'];
        if ($jine_max_s && $jine_min_s >= 0) {
            if ($jine_min_s == $jine_max_s) {
                $where[] = ['money','=',$jine_min_s];
            } else {
                $where[] = ['money','between',$jine_min_s.','.$jine_max_s];
            }
        }
        $status = @$param['status'];
        if ($status) {
            $where[] = ['status','=',$status];
        }
        $admin = @$param['admin'];
        if ($admin && $admin != '所有操作员') {
            $where[] = ['action_user','=',$admin];
        }
        $sou = @$param['sou'];
        if ($sou) {
            $where[] = ['pay_style','=',$sou];
        }
        $lab_id = @$param['lab_id'];
        $models = model('payment');
        $list1 = $models->getList($param['pageNo'],$where,$name,$param['pageSize'],false,$lab_id);
        if($param['pageNo']==1){
            $bd_info=model('wy_bd_info')->where('id',$bd_id)->find();
            $baseData=['ID','小区','房屋/车位/车辆','缴费人','应收金额','实收金额','支付时间','操作员','状态','备注'];
            $excParamArr=[$bd_info['name'].'已缴账单',$baseData,[]];
            cache('exc_'.$param['token'],$excParamArr);
        }
        $list = $list1['data'];
        $addExc=[];
        foreach($list as $k => $v) {
            $list[$k]['info']='';
            $addExc[$k][] = $v['id'];
            $addExc[$k][] = Db::name('wy_bd_info')->where('id',$v['bd_info_id'])->value('name') ?: '';
            if ($v['houses_id'] > 0) {
                $info = '房屋:';
            } elseif ($v['car_area_id'] > 0) {
                $info = '车位:';
            } elseif ($v['car_id'] > 0) {
                $info = '车辆:';
            } else {
                $info = $v['name'];
            }
            if($v['houses_id'] > 0) {
                $hs = Db::name('wy_houses')->where('id', $v['houses_id'])->field('building_id,unit_id,name')->find();
                $bd = Db::name('wy_building')->where('id', $hs['building_id'])->value('name') ?: '';
                $ut = Db::name('wy_unit')->where('id', $hs['unit_id'])->value('name') ?: '';
                $info = $info.$bd.'|'.$ut.'|'.$hs['name'];
            }
            if($v['car_area_id'] > 0) $info = $info.' '.Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
            if($v['car_id'] > 0) $info = $info.' '.Db::name('wy_car')->where('id', $v['car_id'])->value('name');
            $addExc[$k][] = $info;
            $xian = '';
            if($v['house_user_id'] > 0) {
                $usr = Db::name('wy_house_user')->where('id', $v['house_user_id'])->field('name,phone')->find();
                $xian = empty($usr) ? '' : $usr['name'].'('.$usr['phone'].')';
            } elseif ($v['houses_id'] > 0 && !$v['house_user_id']) {
                $hu_id = Db::name('wy_houses_user_middle')->where('houses_id',$v['houses_id'])->value('house_user_id');
                $usr = Db::name('wy_house_user')->where('id', $hu_id)->field('name,phone')->find();
                $xian = empty($usr) ? '' : $usr['name'].'('.$usr['phone'].')';
            }
            $addExc[$k][] = $xian;
            $addExc[$k][] = $v['price'];
            $addExc[$k][] = $v['money'];
            $addExc[$k][] = $v['pay_time'];
            $addExc[$k][] = $v['action_user'];
            $addExc[$k][] = $this->status_text($v['status']);
            $addExc[$k][] = $v['remarks'] ?: '还没有备注内容';
        }
        $tempdataArr=cache('exc_'.$param['token']);
        $tempdatas=array_merge($tempdataArr[2],$addExc);
        $tempdataArr[2]=$tempdatas;
        cache('exc_'.$param['token'],$tempdataArr);
        $count = $list1['totalCount'];
        if ($count == 0) {
            return json_encode(['code'=>-1,'msg'=>'查无数据']);
        }
        if ($count > 5000) {
            return json_encode(['code'=>-1,'msg'=>'数据超出5000条']);
        }
        if($param['pageNo']==ceil($count / $param['pageSize'])){
            $res=exportExcel($tempdataArr[0],$tempdataArr[1],$tempdataArr[2]);
            return json_encode(['code'=>1000,'url'=>$res]);
        }else{
            return __success('ok',['data'=>$list,'pageSize'=>$param['pageSize'],'pageNo'=>$param['pageNo'],'totalCount'=>$count,'totalPage'=>ceil($count / $param['pageSize'])]);
        }
    }
    public function dep_list_exp_se(){
        $param = $this->request->post();
        $house = @$param['house'];
        $mobile = @$param['mobile'];
        $bd = @$param['bd'];
        $hei = @$param['hei'];
        $where = [];
        $info = [];
        if ($bd) {
            $where[] = ['bd_info_id','eq',$bd];
            $bd_name = Db::name('wy_bd_info')->where('id','=',$bd)->value('name');
            $info = array_merge($info,['小区：'.$bd_name]);
        } else {
            $where[] = ['bd_info_id','gt',0];
        }
        if ($hei) {
            $info = array_merge($info,['关键词：'.$hei]);
            $user = Db::name('wy_house_user')->where('phone|name','like','%'.$hei.'%')->column('id');
            $us = Db::name('wy_houses')->where('name','like','%'.$hei.'%')->column('id');
            if ($us) {
                $where[] = ['houses_id','in',implode(',',$us)];
            } elseif ($user) {
                $where[] = ['house_user_id','in',implode(',',$user)];
            } else {
                $where[] = ['category|remarks','like','%'.$hei.'%'];
            }
        }
        if ($house && $mobile) {
            if ($house == 1) {
                $h_n = '住户';
            } else {
                $h_n = '房屋';
            }
            $info = array_merge($info,['类型：'.$h_n.';']);
            $info = array_merge($info,['搜索内容：'.$mobile.';']);
        }
        $where[] = ['status','eq',1];
        $models = model('deposit_list');
        $nod = $models->getList(1,$where,$house,$mobile,10);
        $count = $nod['totalCount'];
        if ($count <= 0) {
            return json_encode(['code'=>-1,'msg'=>'查无数据']);
        }
        if ($count > 5000) {
            return json_encode(['code'=>-1,'msg'=>'本次导出数据超出系统限制，请重新设置搜索条件']);
        }
        if($info){
            array_splice($info,0,0,'导出条件为:');
            array_splice($info,0,0,'本次共导出'.$count.'条数据');
            array_push($info,'');
            array_push($info,'确定导出请点确认按钮');
            return __success($info);
        }else {
            return __success(['还未设定导出搜索条件，请先通过筛选搜索后再导出数据，未搜索将导出全部数据，单次导出数据条数上限为5000条','','确定导出请点确认按钮']);
        }
    }
    public function dep_list_exp(){
        $param = $this->request->post();
        $where[] = ['status', '=', 1];
        $bd_id = @$param['bd_info_id'];
        $hei = @$param['hei'];
        $house = @$param['house'];
        $mobile = @$param['mobile'];
        if ($bd_id) {
            $where[] = ['bd_info_id','eq',$bd_id];
        } else {
            $where[] = ['bd_info_id','gt',0];
        }
        if ($hei) {
            $user = Db::name('wy_house_user')->where('phone|name','like','%'.$hei.'%')->column('id');
            $us = Db::name('wy_houses')->where('name','like','%'.$hei.'%')->column('id');
            if ($us) {
                $where[] = ['houses_id','in',implode(',',$us)];
            } elseif ($user) {
                $where[] = ['house_user_id','in',implode(',',$user)];
            } else {
                $where[] = ['category|remarks','like','%'.$hei.'%'];
            }
        }
        $where[] = ['status','eq',1];
        $models = model('deposit_list');
        $list1 = $models->getList($param['pageNo'],$where,$house,$mobile,$param['pageSize']);
        $list = $list1['data'];
        if($param['pageNo']==1){
            $bd_info=model('wy_bd_info')->where('id',$param['bd_info_id'])->find();
            $baseData=['ID','小区','房号','住户','手机号','预存余额','备注'];
            $excParamArr=[$bd_info['name'].'预存款',$baseData,[]];
            cache('exc_'.$param['token'],$excParamArr);
        }
//        $list = model('deposit_list')->where($where)->page($param['pageNo'],$param['pageSize'])->order('id desc')->group('house_user_id')->field('*,sum(balance) as balance')->select()->toArray();
        $addExc=[];
        foreach($list as $k => $v) {
            $list[$k]['info']='';
            $addExc[$k][] = $v['id'];
            $addExc[$k][] = Db::name('wy_bd_info')->where('id',$v['bd_info_id'])->value('name') ?: '';
            $addExc[$k][] = Db::name('wy_houses')->where('id',$v['houses_id'])->value('name') ?: '';
            $addExc[$k][] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name');
            $addExc[$k][] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('phone');
//            $addExc[$k][] = $v['category'];
            $addExc[$k][] = $v['balance'];
            $addExc[$k][] = $v['remarks'] ?: '';
        }
        $tempdataArr=cache('exc_'.$param['token']);
        $tempdatas=array_merge($tempdataArr[2],$addExc);
        $tempdataArr[2]=$tempdatas;
        cache('exc_'.$param['token'],$tempdataArr);
//        $count = model('deposit_list')->where($where)->group('house_user_id')->count();
        $count = $list1['totalCount'];
        if ($count == 0) {
            return json_encode(['code'=>-1,'msg'=>'查无数据']);
        }
        if ($count > 5000) {
            return json_encode(['code'=>-1,'msg'=>'数据超出5000条']);
        }
        if($param['pageNo']==ceil($count / $param['pageSize'])){
            $res=exportExcel($tempdataArr[0],$tempdataArr[1],$tempdataArr[2]);
            return json_encode(['code'=>1000,'url'=>$res]);
        }else{
            return __success('ok',['data'=>$list,'pageSize'=>$param['pageSize'],'pageNo'=>$param['pageNo'],'totalCount'=>$count,'totalPage'=>ceil($count / $param['pageSize'])]);
        }
    }
    public function dep_list_store_exp(){
        $param = $this->request->post();
        $house = @$param['house'];
        $mobile = @$param['mobile'];
        $bd = @$param['bd'];
        $hei = @$param['hei'];
        $lei = @$param['lei'];
        $type = @$param['type'];
        $des_id = @$param['des_id'];
        $dets = @$param['dets'];
        $where = [];
        $where1 = [];
        if ($bd) {
            $where[] = ['bd_info_id','eq',$bd];
        } else {
            $where[] = ['bd_info_id','gt',0];
        }
        if ($type) {
            $where[] = ['type','eq',$type];
        }
        $jine_max = @$param['jine_max'];
        $jine_min = @$param['jine_min'];
        if ($jine_max && $jine_min >= 0) {
            $where[] = ['price','between',$jine_min.','.$jine_max];
        }
        $admin = @$param['admin'];
        if ($admin) {
            if ($admin > 0) {
                $where[] = ['come','eq',$admin];
            } else {
                $where[] = ['come','eq','excel导入'];
            }
        }
        $start = @$param['start_time'];
        $end = @$param['end_time'];
        if ($start && $end) {
            $where[] = ['createtime','between',$start.','.$end];
        }
        $cate = @$param['cate'];
        if ($cate) {
            if ($cate == 'excel导入') {
                $did = Db::name('wy_deposit_list')->where('type','=',1)->column('id');
                $where[] = ['deposit_list_id','in',implode(',',$did)];
            } elseif($cate != '所有') {
                $did = Db::name('wy_deposit_list')->where('type','=',2)->where('category','=',$cate)->column('id');
                $where[] = ['deposit_list_id','in',implode(',',$did)];
            }
        }
        if ($hei && $lei == 1) {
            $where1[] = ['status', '=', 1];
            $user = Db::name('wy_house_user')->where('phone|name','like','%'.$hei.'%')->column('id');
            $us = Db::name('wy_houses')->where('name','like','%'.$hei.'%')->column('id');
            if ($user) {
                $where1[] = ['house_user_id','in',implode(',',$user)];
            } elseif ($us) {
                $where1[] = ['houses_id','in',implode(',',$us)];
            } else {
                $where1[] = ['category|remarks','like','%'.$hei.'%'];
            }
            $did = Db::name('wy_deposit_list')->where($where1)->column('id');
            $where[] = ['deposit_list_id','in',implode(',',$did)];
        }
        if ($dets == 1) {
            $uid = Db::name('wy_deposit_list')->where('id',$des_id)->value('house_user_id');
            $des_id = Db::name('wy_deposit_list')->where('house_user_id',$uid)->column('id');
            $where[] = ['deposit_list_id','in',implode(',',$des_id)];
        } else {
            if ($des_id) {
                $where[] = ['deposit_list_id','=',$des_id];
            }
        }
        $models = model('deposit_list_store');
        $list1 = $models->getList($param['pageNo'],$where,$house,$mobile,$param['pageSize']);
        if($param['pageNo']==1){
//            $bd_info=model('wy_bd_info')->where('id',$param['bd_info_id'])->find();
            $baseData=['ID','小区/楼宇','房号/车位号/车牌号','住户','手机号','金额','类型','来源','时间','预存款类型','备注'];
            $excParamArr=['预存款',$baseData,[]];
            cache('exc_'.$param['token'],$excParamArr);
        }
        $list = $list1['data'];
//        $list = model('deposit_list_store')->where($where)->page($param['pageNo'],$param['pageSize'])->order('id desc')->select()->toArray();
        $addExc=[];
        $t_text = ['1' => '充值', '2' => '抵扣','3'=>'退款'];
        foreach($list as $k => $v) {
            $remarks = '';
            $cate = Db::name('wy_deposit_list')->where('id',$v['deposit_list_id'])->field('houses_id,category,type')->find();
            $hdt = Db::name('wy_houses')->where('id',$cate['houses_id'])->field('building_id,unit_id,name')->find();
            $build_name = Db::name('wy_building')->where('id',$hdt['building_id'])->value('name') ?: '';
            $unit_name = Db::name('wy_unit')->where('id',$hdt['unit_id'])->value('name') ?: '';
            $bd_name = Db::name('wy_bd_info')->where('id',$v['bd_info_id'])->value('name') ?: '';
            if ($cate['type'] == 1) {
                $category = '通用预存款';
            } else {
                $category = '专用预存款('.$cate['category'].')';
            }
            $come_text = is_numeric($v['come']) ? Db::name('pms_employee')->where('id',$v['come'])->value('name') : $v['come'];
            $list[$k]['info']='';
            $addExc[$k][] = $v['id'];
            $addExc[$k][] = $bd_name.'('.$build_name.$unit_name.')';
            $addExc[$k][] = $hdt['name'];
            $us = Db::name('wy_house_user')->where('id', $v['house_user_id'])->field('name,phone')->find();
            $addExc[$k][] = $us['name'];
            $addExc[$k][] = $us['phone'];
            $addExc[$k][] = $v['price'];
            $addExc[$k][] = @$t_text[$v['type']];
            $addExc[$k][] = $come_text;
            $addExc[$k][] = $v['createtime'];
            $addExc[$k][] = $category;
            if ($v['type'] == 1) {
                $remarks = '后台充值；'.$v['remarks'];
            } elseif ($v['type'] == 2) {
                $remarks = '自动抵扣；'.$v['remarks'];
            } elseif ($v['type'] == 3) {
                $remarks = '退款人：'.$v['user_name'].'('.$v['user_phone'].')'.' 备注:'.$v['remarks'];
            }
            $addExc[$k][] = $remarks;
        }
        $tempdataArr=cache('exc_'.$param['token']);
        $tempdatas=array_merge($tempdataArr[2],$addExc);
        $tempdataArr[2]=$tempdatas;
        $count = $list1['totalCount'];
        cache('exc_'.$param['token'],$tempdataArr);
        if ($count == 0) {
            return json_encode(['code'=>-1,'msg'=>'查无数据']);
        }
        if ($count > 5000) {
            return json_encode(['code'=>-1,'msg'=>'数据超出5000条']);
        }
        if($param['pageNo']==ceil($count / $param['pageSize'])){
            $res=exportExcel($tempdataArr[0],$tempdataArr[1],$tempdataArr[2]);
            return json_encode(['code'=>1000,'url'=>$res]);
        }else{
            return __success('ok',['data'=>$list,'pageSize'=>$param['pageSize'],'pageNo'=>$param['pageNo'],'totalCount'=>$count,'totalPage'=>ceil($count / $param['pageSize'])]);
        }
    }
    public function pay_store_exp(){
        $param = $this->request->post();
        $name = $this->request->post('name');
        $bd_id = $this->request->post('bd_info_id');
        $type = $this->request->post('type');
        $where = [];
        if ($bd_id) {
            $bd_ids = Db::name('wy_payment')->where('bd_info_id','=',$bd_id)->where('is_deleted','=',0)->column('id');
            $where[] = ['payment_id','in',implode(',',$bd_ids)];
        }
        if ($type) {
            $all_type = ['1'=>'房屋','2'=>'车位','3'=>'车辆','4'=>'住户','5'=>'租金','6'=>'其它收费','7'=>'临时停车费'];
            $where[] = ['type','=',$all_type[$type]];
        }
        $start = $this->request->post('start_time');
        $end = $this->request->post('end_time');
        if ($start && $end) {
            $pays = model('payment')->where('pay_time','between',$start.','.$end)->column('id');
            if ($pays) {
                $where[] = ['payment_id','in',implode(',',$pays)];
            }
        }
        $jine_max = $this->request->post('jine_max');
        $jine_min = $this->request->post('jine_min');
        if ($jine_max && $jine_min >= 0) {
            if ($jine_min == $jine_max) {
                $where[] = ['price','=',$jine_min];
            } else {
                $where[] = ['price','between',$jine_min.','.$jine_max];
            }
        }
        $jine_max_s = $this->request->post('jine_max_s');
        $jine_min_s = $this->request->post('jine_min_s');
        if ($jine_max_s && $jine_min_s >= 0) {
            if ($jine_min_s == $jine_max_s) {
                $where[] = ['money','=',$jine_min_s];
            } else {
                $where[] = ['money','between',$jine_min_s.','.$jine_max_s];
            }
        }
        $status = $this->request->post('status');
        if ($status) {
            $all_sta = ['1'=>'已缴','2'=>'退款','3'=>'撤销'];
            $where[] = ['status','=',$all_sta[$status]];
        }
        $admin = $this->request->post('admin');
        if ($admin) {
            $where[] = ['action_user','=',$admin];
        }
        $sou = $this->request->post('sou');
        if ($sou) {
            $where[] = ['pay_style','=',$sou];
        }
        $bd_id = $this->request->post('bd_id');
        $label = $this->request->post('lab_id');
        if ($bd_id) {
            $hous = model('wy_houses')->where('bd_info_id',$bd_id)->column('id');
            if ($hous) {
                $where[] = ['houses_id','in',implode(',',$hous)];
            }
        }
        if($param['pageNo']==1){
            $baseData=['ID','类型','小区','房号/车位号/车牌','费用名称/时间','状态/支付时间','数量','应收金额','优惠','滞纳金','折扣','实收金额','缴费人','收款方式','操作员','备注'];
//            $baseData=['ID','类型','小区','房号/车位号/车牌','费用名称','开始时间','结束时间','应收金额','实收金额','折扣','支付时间','缴费人','收款方式','操作员','状态','备注'];
            $excParamArr=['已缴账单明细',$baseData,[]];
            cache('exc_'.$param['token'],$excParamArr);
        }
        $models = model('payment_store');
        $list1 = $models->getList($param['pageNo'],$where,$name,$param['pageSize'],$label);
        $list = $list1['data'];
        $addExc=[];
        foreach($list as $k => $v) {
//            $data_info = '';
//            if($v['houses_id'] > 0) $data_info = Db::name('wy_houses')->where('id', $v['houses_id'])->value('name');
//            if($v['car_id'] > 0) $data_info = Db::name('wy_car')->where('id', $v['car_id'])->value('name');
//            if($v['car_area_id'] > 0) $data_info = Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
            $pay = Db::name('wy_payment')->where('id', $v['payment_id'])->field('pay_time,deduction,bd_info_id,remarks')->find();
            $bd_info_id = $pay['bd_info_id'];

            $bd_info_name = Db::name('wy_bd_info')->where('id', $bd_info_id)->value('name') ?: '';
            if ($v['houses_id'] > 0) {
                $info = '房屋:';
            } elseif ($v['car_area_id'] > 0) {
                $info = '车位:';
            } elseif ($v['car_id'] > 0) {
                $info = '车辆:';
            } else {
                $info = '';
            }
            if($v['houses_id'] > 0) {
                $hs = Db::name('wy_houses')->where('id', $v['houses_id'])->field('building_id,unit_id,name')->find();
                $bd = Db::name('wy_building')->where('id', $hs['building_id'])->value('name') ?: '';
                $ut = Db::name('wy_unit')->where('id', $hs['unit_id'])->value('name') ?: '';
                $info = $info.$bd.'|'.$ut.'|'.$hs['name'];
            }
            if($v['car_id'] > 0) {
                $car_name = Db::name('wy_car')->where('id', $v['car_id'])->value('name') ?: '';
                $info = $info.' '.$car_name;
            }
            if($v['car_area_id'] > 0) {
                $car_area_name = Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name') ?: '';
                $info = $info.' '.$car_area_name;
            }
            if($v['user_id'] > 0) {
                $usr = Db::name('wy_house_user')->where('id', $v['user_id'])->field('name,phone')->find();
                $data['data'][$k]['user_id_name'] = $usr['name'].'('.$usr['phone'].')';
                $data['data'][$k]['u_name'] = $usr['name'];
                $data['data'][$k]['u_phone'] = '('.$usr['phone'].')';
            }
            $addExc[$k][] = $v['id'];
            $addExc[$k][] = $v['type'];
            $addExc[$k][] = $bd_info_name;
            $addExc[$k][] = $info;
            $addExc[$k][] = $v['name'].';'."\r\n".$v['start_time'].'/'."\r\n".$v['end_time'];
            $addExc[$k][] = $v['status']."\r\n".$pay['pay_time'];
            $addExc[$k][] = $v['num'];
            $addExc[$k][] = $v['price'];
            $addExc[$k][] = $v['offer'];
            $addExc[$k][] = $v['overdue'];
            $addExc[$k][] = $v['deduction'];
            $addExc[$k][] = $v['money'];
            $addExc[$k][] = Db::name('wy_house_user')->where('id', $v['user_id'])->value('name') ?: '';
            $addExc[$k][] = $this->style_text($v['pay_style']);
            $addExc[$k][] = is_numeric($v['action_user']) ? Db::name('pms_employee')->where('id', $v['action_user'])->value('name') : $v['action_user'];
            $addExc[$k][] = $pay['remarks'];
        }
        $tempdataArr=cache('exc_'.$param['token']);
        $tempdatas=array_merge($tempdataArr[2],$addExc);
        $tempdataArr[2]=$tempdatas;
        cache('exc_'.$param['token'],$tempdataArr);
        $count = $list1['totalCount'];
        if ($count == 0) {
            return json_encode(['code'=>-1,'msg'=>'查无数据']);
        }
        if ($count > 5000) {
            return json_encode(['code'=>-1,'msg'=>'数据超出5000条']);
        }
        if($param['pageNo']==ceil($count / $param['pageSize'])){
            $res=exportExcel($tempdataArr[0],$tempdataArr[1],$tempdataArr[2]);
            return json_encode(['code'=>1000,'url'=>$res]);
        }else{
            return __success('ok',['data'=>$list,'pageSize'=>$param['pageSize'],'pageNo'=>$param['pageNo'],'totalCount'=>$count,'totalPage'=>ceil($count / $param['pageSize'])]);
        }
    }
    public function not_pay_exp(){
        $param = $this->request->post();
        $where[] = ['is_deleted', '=', 0];
        if (@$param['bd_id']) {
            $where[] = ['bd_info_id','=',$param['bd_id']];
        }
        if (@$param['message']) {
            $where[] = ['name','=',$param['message']];
        }
        if (@$param['type'] == 1) {
            $where[] = ['type','in','0,1'];
        } else{
            if ($param['type'] > 1) {
                $where[] = ['type','=',$param['type']];
            }
        }
        if (@$param['pro_det_id']) {
            $where[] = ['toll_project_detial_id','=',$param['pro_det_id']];
        }
        $lab_id = @$param['lab_id'];
        if ($lab_id) {
//            $uids = Db::name('wy_labels_middle')->where('labels_id',$lab_id)->column('house_user_id');
//            $hid = Db::name('wy_house_user')->where('id','in',implode(',',$uids))->column('houses_id');
//            $where[] = ['house_id','in',implode(',',$hid)];
        }
        $name = @$param['name'];
        $start = @$param['start_time'];
        $end = @$param['end_time'];
        if ($start && $end) {
            $where[] = ['pay_time','between',$start.','.$end];
        }
        $jine_max = @$param['jine_max'];
        $jine_min = @$param['jine_min'];
        if ($jine_max && $jine_min >= 0) {
            if ($jine_min == $jine_max) {
                $where[] = ['price','=',$jine_min];
            } else {
                $where[] = ['price','between',$jine_min.','.$jine_max];
            }
        }
        $gj_name_value = @$param['gj_name_value'];
        if ($gj_name_value) {
            $name = $gj_name_value;
        }
        $status = @$param['status'];
        if ($status) {
            $where[] = ['status','=',$status];
        }
        $sou = @$param['sou'];
        if ($sou) {
            $where[] = ['pay_style','=',$sou];
        }
        $lab_id = @$param['lab_id'];
        if ($lab_id) {
//            $uids = Db::name('wy_labels_middle')->where('labels_id',$lab_id)->column('house_user_id');
//            $hid = Db::name('wy_house_user')->where('id','in',implode(',',$uids))->column('houses_id');
//            $where[] = ['house_id','in',implode(',',$hid)];
        }
        $where[] = ['pay_status','=',0];
        if($param['pageNo']==1){
            $bd_info=model('wy_bd_info')->where('id',$param['bd_id'])->find();
            $baseData=['ID','小区','楼栋','单元','房号/车位号/车牌号','类型','收费名称','应收金额','应缴金额','开始时间','结束时间','单价','数量','优惠','滞纳金','预存款抵扣','备注'];
            $excParamArr=[$bd_info['name'].'未缴账单',$baseData,[]];
            cache('exc_'.$param['token'],$excParamArr);
        }

        $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
        $models = model('no_pay');
        $list1 = $models->getList($param['pageNo'],$where,$name,$param['pageSize'],$lab_id);
        $list = $list1['data'];
        $addExc=[];
        $hsMd = model('wy_houses');
        foreach($list as $k => $v) {
            $list[$k]['info']='';
            $addExc[$k][] = $v['id'];
            $addExc[$k][] = @Db::name('wy_bd_info')->where('id',$v['bd_info_id'])->value('name') ?: '';
            $hu = $hsMd->where('id',$v['house_id'])->field('building_id,unit_id,area,name')->find();
            $addExc[$k][] = @Db::name('wy_building')->where('id',@$hu['building_id'])->value('name') ?: '';
            $addExc[$k][] = @Db::name('wy_unit')->where('id',@$hu['unit_id'])->value('name') ?: '';
            if ($v['car_area_id'] > 0) {
                $addExc[$k][] = @Db::name('wy_car_area')->where('id',$v['car_area_id'])->value('name') ?: '';
            } elseif ($v['car_id'] > 0) {
                $addExc[$k][] = @Db::name('wy_car')->where('id',$v['car_id'])->value('name') ?: '';
            } else {
                $addExc[$k][] = @$hu['name'] ?: '';
            }
            $addExc[$k][] = $type_text[$v['type']];
            $addExc[$k][] = $v['name'];
            $addExc[$k][] = $v['price'];
            $addExc[$k][] = $v['money'];
            $addExc[$k][] = $v['start_time'];
            $addExc[$k][] = $v['end_time'];
            $addExc[$k][] = $v['one_money'];
            $num = $v['num'];
            if($v['house_id'] > 0) {
                if ($v['num'] > 0) {
                } else {
                    $num = $hu['area'];
                }
            }
            if ($v['one_money'] == 0) {
                $num = 0;
            }
            $addExc[$k][] = $num;
            $addExc[$k][] = $v['offer'];
            $addExc[$k][] = $v['overdue'];
            $addExc[$k][] = $v['deduction'];
            $addExc[$k][] = $v['remarks'];
        }
        $tempdataArr=cache('exc_'.$param['token']);
        $tempdatas=array_merge($tempdataArr[2],$addExc);
        $tempdataArr[2]=$tempdatas;
        cache('exc_'.$param['token'],$tempdataArr);
        $count = $list1['totalCount'];
        if ($count == 0) {
            return json_encode(['code'=>-1,'msg'=>'查无数据']);
        }
        if($param['pageNo']==ceil($count / $param['pageSize'])){
            $res=exportExcel($tempdataArr[0],$tempdataArr[1],$tempdataArr[2]);
            return json_encode(['code'=>1000,'url'=>$res]);
        }else{
            return __success('ok',['data'=>$list,'pageSize'=>$param['pageSize'],'pageNo'=>$param['pageNo'],'totalCount'=>$count,'totalPage'=>ceil($count / $param['pageSize'])]);
        }
    }
    public function not_pay_exp_se(){
        $param = $this->request->post();
        $where[] = ['is_deleted', '=', 0];
        $info = [];
        if (@$param['type'] == 1) {
            $where[] = ['type','in','0,1'];
            $info = array_merge($info,['类型：房屋;']);
        } else{
            if ($param['type'] > 1) {
                $where[] = ['type','=',$param['type']];
                $type_text = ['0'=>'房屋','1' => '房屋', '2' => '车辆', '3' => '车位', '4' => '住户'];
                $info = array_merge($info,['类型：'.$type_text[$param['type']].';']);
            }
        }
        if (@$param['bd_id']) {
            $where[] = ['bd_info_id','=',$param['bd_id']];
            $bd_info_name = Db::name('wy_bd_info')->where('id',$param['bd_id'])->value('name');
            $info = array_merge($info,['小区：'.$bd_info_name.'小区;']);
        }
        if (@$param['pro_det_id']) {
            $where[] = ['toll_project_detial_id','=',$param['pro_det_id']];
            $tname = Db::name('wy_toll_project_detial')->where('id',$param['pro_det_id'])->value('name');
            $info = array_merge($info,['项目/标准：'.$tname.';']);
        }
        $lab_id = @$param['lab_id'];
        if ($lab_id) {
//            $uids = Db::name('wy_labels_middle')->where('labels_id',$lab_id)->column('house_user_id');
//            $hid = Db::name('wy_house_user')->where('id','in',implode(',',$uids))->column('houses_id');
//            $where[] = ['house_id','in',implode(',',$hid)];
            $baiq = Db::name('wy_labels')->where('id',$lab_id)->value('name');
            $info = array_merge($info,['数据标签：'.$baiq.';']);
        }
        $jine_max = @$param['jine_max'];
        $jine_min = @$param['jine_min'];
        if ($jine_max && $jine_min >= 0) {
            $where[] = ['price','between',$jine_min.','.$jine_max];
            $info = array_merge($info,['应收金额区间：'.$jine_min.'-'.$jine_max.';']);
        }
        $name = @$param['name'];
        if ($name) {
            $info = array_merge($info,['关键词：'.$name.';']);
        }
        $where[] = ['pay_status','=',0];
        $models = model('no_pay');
        $nod = $models->getList(1,$where,$name,10,$lab_id);
        $count = $nod['totalCount'];
        if ($count <= 0) {
            return json_encode(['code'=>-1,'msg'=>'查无数据']);
        }
        if ($count > 5000) {
            return json_encode(['code'=>-1,'msg'=>'本次导出数据超出系统限制，请重新设置搜索条件']);
        }
        if($info){
            array_splice($info,0,0,'导出条件为:');
            array_splice($info,0,0,'本次共导出'.$count.'条数据');
            array_push($info,'');
            array_push($info,'确定导出请点确认按钮');
            return __success($info);
        }else {
            return __success(['还未设定导出搜索条件，请先通过筛选搜索后再导出数据，未搜索将导出全部数据，单次导出数据条数上限为5000条','','确定导出请点确认按钮']);
        }
    }
    public function pay_exp_se(){
        $param = $this->request->post();
        $name = @$param['name'];
        $type = @$param['type'];
        $info = [];
        $where = [];
        $bd_id = @$param['bd_id'];
        if ($bd_id) {
            $bd_info_name = Db::name('wy_bd_info')->where('id',$bd_id)->value('name');
            $where[] = ['bd_info_id','=',$bd_id];
            $info = array_merge($info,['小区：'.$bd_info_name.';']);
        }
        if ($type) {
            if ($type == 1) {
                $where[] = ['houses_id','gt',0];
                $info = array_merge($info,['类型：房屋;']);
            } elseif ($type == 2) {
                $where[] = ['car_area_id','gt',0];
                $info = array_merge($info,['类型：车位;']);
            } elseif ($type == 3) {
                $where[] = ['car_id','gt',0];
                $info = array_merge($info,['类型：车辆;']);
            } elseif ($type == 4) {
                $where[] = ['house_user_id','gt',0];
                $info = array_merge($info,['类型：用户;']);
            } elseif ($type == 5) {
                $pid = Db::name('wy_payment_store')->where('type','=','其它收费')->column('payment_id');
                $where[] = ['id','in',implode(',',$pid)];
                $info = array_merge($info,['类型：其它收费;']);
            } elseif ($type ==  6) {
                $pid = Db::name('wy_payment_store')->where('type','=','临时停车费')->column('payment_id');
                $where[] = ['id','in',implode(',',$pid)];
                $info = array_merge($info,['类型：临时停车费;']);
            }
        }
        $start = @$param['start_time'];
        $end = @$param['end_time'];
        if ($start && $end) {
            $where[] = ['pay_time','between',$start.','.$end];
            $info = array_merge($info,['时间区间：'.$start.'到'.$end.';']);
        }
        $jine_max = @$param['jine_max'];
        $jine_min = @$param['jine_min'];
        if ($jine_max && $jine_min >= 0) {
            if ($jine_max == $jine_min) {
                $where[] = ['price','=',$jine_min];
            } else {
                $where[] = ['price','between',$jine_min.','.$jine_max];
            }
            $info = array_merge($info,['应收金额区间：'.$jine_min.'-'.$jine_max.';']);
        }
        $jine_max_s = @$param['jine_max_s'];
        $jine_min_s = @$param['jine_min_s'];
        if ($jine_max_s && $jine_min_s >= 0) {
            if ($jine_min_s == $jine_max_s) {
                $where[] = ['money','=',$jine_min_s];
            } else {
                $where[] = ['money','between',$jine_min_s.','.$jine_max_s];
            }
            $info = array_merge($info,['实收金额区间：'.$jine_min_s.'-'.$jine_max_s.';']);
        }
        $status = @$param['status'];
        if ($status) {
            $where[] = ['status','=',$status];
            $all_sta = ['1'=>'已缴','2'=>'退款','3'=>'撤销'];
            $info = array_merge($info,['状态：'.$all_sta[$status].';']);
        }
        $admin = @$param['admin'];
        if ($admin && $admin != '所有操作员') {
            $where[] = ['action_user','=',$admin];
            $info = array_merge($info,['操作员：'.$admin.';']);
        }
        $sou = @$param['sou'];
        if ($sou) {
            $where[] = ['pay_style','=',$sou];
            $info = array_merge($info,['收款方式：'.$this->style_text($sou).';']);
        }
        $lab_id = @$param['lab_id'];
        if ($lab_id) {
//            $lab = Db::name('wy_labels_middle')->where('id',$lab_id)->value('labels_id');
            $lab_name = Db::name('wy_labels')->where('id',$lab_id)->value('name');
            $info = array_merge($info,['数据标签：'.$lab_name.';']);
        }
        $models = model('payment');
        $nod = $models->getList(1,$where,$name,10,false,$lab_id);
        $count = $nod['totalCount'];
        if ($count <= 0) {
            return json_encode(['code'=>-1,'msg'=>'查无数据']);
        }
        if ($count > 5000) {
            return json_encode(['code'=>-1,'msg'=>'本次导出数据超出系统限制，请重新设置搜索条件']);
        }
        if($info){
            array_splice($info,0,0,'导出条件为:');
            array_splice($info,0,0,'本次共导出'.$count.'条数据');
            array_push($info,'');
            array_push($info,'确定导出请点确认按钮');
            return __success($info);
        }else {
            return __success(['还未设定导出搜索条件，请先通过筛选搜索后再导出数据，未搜索将导出全部数据，单次导出数据条数上限为5000条','','确定导出请点确认按钮']);
//            return __success('还未设定导出搜索条件,确定导出请点确认按钮');
        }
    }
    public function daily_exp_se(){
        $param = $this->request->post();
        $info = [];
        if(@$param['bd_info_id']){
            $where[] = ['bd_info_id','=',$param['bd_info_id']];
            $bd_name = Db::name('wy_bd_info')->where('id','=',$param['bd_info_id'])->value('name');
            $info = array_merge($info,['导出小区：'.$bd_name.';']);
        }
        if(@$param['start_time'] && @$param['end_time']){
            $info = array_merge($info,['时间区间：'.$param['start_time'].'到'.$param['end_time'].';']);
            $where[] = ['pay_time','>=',$param['start_time']];
            $where[] = ['pay_time','<=',$param['end_time']];
        }
        if (@$param['sou']) {
            $info = array_merge($info,['收款方式：'.payTypeTxt($param['sou']).';']);
            $where[] = ['pay_style','=',$param['sou']];
        }
        if(@$param['jine_min'] && @$param['jine_max']){
            $info = array_merge($info,['金额区间：'.$param['jine_min'].'到'.$param['jine_max'].';']);
            $where[] = ['money','>=',$param['jine_min']];
            $where[] = ['money','<=',$param['jine_max']];
        }
        if(!empty($param['name'])) $where[] = ['name','like','%'.$param['name'].'%'];
        $where[] = ['is_deleted','=',0];
        $models = model('payment');
        $nod = $models->getList(1,$where,'',10,'');
        $count = $nod['totalCount'];
        if ($count <= 0) {
            return json_encode(['code'=>-1,'msg'=>'查无数据']);
        }
        if ($count > 5000) {
            return json_encode(['code'=>-1,'msg'=>'本次导出数据超出系统限制，请重新设置搜索条件']);
        }
        if($info){
            array_splice($info,0,0,'导出条件为:');
            array_splice($info,0,0,'本次共导出'.$count.'条数据');
            array_push($info,'');
            array_push($info,'确定导出请点确认按钮');
            return __success($info);
        }else {
            return __success(['还未设定导出搜索条件，请先通过筛选搜索后再导出数据，未搜索将导出全部数据，单次导出数据条数上限为5000条','','确定导出请点确认按钮']);
        }

    }
    public function dailyExc(){
        $param = $this->request->post();
        if(@$param['bd_info_id']){
            $where[] = ['bd_info_id','=',$param['bd_info_id']];
        }else{
            return __error('请选择小区！');
        }
        if(@$param['start_time'] && @$param['end_time']){
            $where[] = ['pay_time','>=',$param['start_time']];
            $where[] = ['pay_time','<=',$param['end_time']];
        }
        if (@$param['sou']) {
            $where[] = ['pay_style','=',$param['sou']];
        }
        if(@$param['jine_min'] && @$param['jine_max']){
            $where[] = ['money','>=',$param['jine_min']];
            $where[] = ['money','<=',$param['jine_max']];
        }
        if(!empty($param['name'])) $where[] = ['name','like','%'.$param['name'].'%'];        
        $where[] = ['is_deleted','=',0];
        $models=model('wy_payment');
        $modelsStor=model('wy_payment_store');
    
        $status_text = [
            '0' => '',
            '1' => '已收',
            '2' => '已退',
            '3' => '撤销'
        ];
        if($param['pageNo']==1){
            //构造标头
            $ids=$models->where($where)->column('id');//查找所有符合条件的id
            $litTime=$modelsStor->where('payment_id','in',$ids)->min('start_time',false);
            $maxTime=$modelsStor->where('payment_id','in',$ids)->max('end_time',false);
            $miniYear= date('Y',strtotime($litTime));
            $maxYear= date('Y',strtotime($maxTime));
            $topYearArr=[];
            $monthDate=[];
            for($i=(int)$miniYear;$i<=$maxYear;$i++){
                $topYearArr[]=$i;
                for($j=1;$j<=12;$j++){
                    $monthDate[]=$j.'月';
                }
            }
            //小区信息
            $bd_info=model('wy_bd_info')->where('id',$param['bd_info_id'])->find();
            
            //创建第二行标头
            $baseData=['序号','开票日期','收据/发票号','收款项目','业主姓名','物业小区','房号/车号/车位','面积/吨数','计费时间','缴费起始日期','缴费终止日期','单价','应收金额','实收金额','差额','备注'];
            // $res=exportExcel($bd_info['name'].'台帐',array_merge($baseData,$monthDate),[],$topYearArr);
            $excParamArr=[$bd_info['name'].'台帐',array_merge($baseData,$monthDate),[],$topYearArr];
            cache('exc_'.$param['token'],$excParamArr);
            cache('exc_minYear_'.$param['token'],$miniYear);
            cache('exc_maxYear_'.$param['token'],$maxYear);
        }
        $list = $models->where($where)->page($param['pageNo'],$param['pageSize'])->order('id desc')->select()->toArray();
        $addExc=[];
        foreach($list as $k => $v) {
            $list[$k]['info']='';
            $addExc[$k][]=$v['id'];
            $addExc[$k][]=date('Y-m-d',strtotime($v['pay_time']));
            $addExc[$k][]=strtotime($v['pay_time']).$v['id'];
            $addExc[$k][]=$v['name'];
            $addExc[$k][]=$v['house_user_id'] > 0?Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name'):'';
            $addExc[$k][]=$v['bd_info_id'] > 0?Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name'):'';
            if($v['houses_id'] > 0) $list[$k]['info'] = $list[$k]['info'].' '.Db::name('wy_houses')->where('id', $v['houses_id'])->value('name');
            if($v['car_area_id'] > 0) $list[$k]['info'] = $list[$k]['info'].' '.Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
            if($v['car_id'] > 0) $list[$k]['info'] = $list[$k]['info'].' '.Db::name('wy_car')->where('id', $v['car_id'])->value('name');
            $addExc[$k][]=$list[$k]['info'];
            $addExc[$k][]=$modelsStor->where('payment_id',$v['id'])->sum('num');
            $addExc[$k][]='';
            $addExc[$k][]=$modelsStor->where('payment_id',$v['id'])->min('start_time',false)?date('Y-m-d',strtotime($modelsStor->where('payment_id',$v['id'])->min('start_time',false))):'';
            $addExc[$k][]=$modelsStor->where('payment_id',$v['id'])->max('end_time',false)?date('Y-m-d',strtotime($modelsStor->where('payment_id',$v['id'])->max('end_time',false))):'';
            $addExc[$k][]=$modelsStor->where('payment_id',$v['id'])->avg('one_money');
            $addExc[$k][]=$v['price'];
            $addExc[$k][]=$v['money'];
            $addExc[$k][]=(float)$v['price']-(float)$v['money'];
            $addExc[$k][]=payTypeTxt($v['pay_style']);
            $miniYear=cache('exc_minYear_'.$param['token']);
            $maxYear=cache('exc_maxYear_'.$param['token']);
            $addExcTemp=[];
            for($i=(int)$miniYear;$i<=$maxYear;$i++){
                for($j=1;$j<=12;$j++){
                    $tems=$i.'-'.$j;
                    $start_time=$tems.'-01';
                    $end_time=date('Y-m-t',strtotime($start_time));
                    $prisarr=$modelsStor->where('payment_id',$v['id'])->where('start_time','>=',$start_time)->where('start_time','<=',$end_time)->field('id,money')->select()->toArray();
                    if($prisarr){
                        $pris=0;
                        foreach($prisarr as $kks=>$vvs){
                            $pris+=$vvs['money'];
                        }
                        $addExcTemp[]=$pris;
                    }else{
                        $addExcTemp[]='';
                    }
                }

            }

            $newExctemp=getVgaPriceFortz($addExcTemp);
            
            for($i=0;$i<count($newExctemp);$i++){
                $addExc[$k][]=$newExctemp[$i]; 
            }
            // dump($addExc);die;
            //TODO 待完善 收费日期细节
            // $list[$k]['style_text'] = $style_text[$v['pay_style']];
            // $list[$k]['status_text'] = $status_text[$v['status']];
            // $list[$k]['nums'] = strtotime($v['pay_time']).$v['id'];
            // $list[$k]['info'] = '';
            // if($v['bd_info_id'] > 0) $list[$k]['bd_info_id_text'] = Db::name('wy_bd_info')->where('id', $v['bd_info_id'])->value('name');
            // if($v['houses_id'] > 0) $list[$k]['info'] = $list[$k]['info'].' '.Db::name('wy_houses')->where('id', $v['houses_id'])->value('name');
            // if($v['car_area_id'] > 0) $list[$k]['info'] = $list[$k]['info'].' '.Db::name('wy_car_area')->where('id', $v['car_area_id'])->value('name');
            // if($v['car_id'] > 0) $list[$k]['info'] = $list[$k]['info'].' '.Db::name('wy_car')->where('id', $v['car_id'])->value('name');
            // if($v['house_user_id'] > 0) $list[$k]['user_text'] = Db::name('wy_house_user')->where('id', $v['house_user_id'])->value('name');
        }
        $tempdataArr=cache('exc_'.$param['token']);
        $tempdatas=array_merge($tempdataArr[2],$addExc);
        $tempdataArr[2]=$tempdatas;
        cache('exc_'.$param['token'],$tempdataArr);
        $count = $models->where($where)->count();
        if($param['pageNo']==ceil($count / $param['pageSize'])){
            $res=exportExcel($tempdataArr[0],$tempdataArr[1],$tempdataArr[2],$tempdataArr[3]);
            return json_encode(['code'=>1000,'url'=>$res]);
        }else{
            if($list){
                return __success('ok',['data'=>$list,'pageSize'=>$param['pageSize'],'pageNo'=>$param['pageNo'],'totalCount'=>$count,'totalPage'=>ceil($count / $param['pageSize'])]);
            }else{
                return __error('数据为空！');
            }
            
        }
        
    }
}
function getVgaPriceFortz($arr)
{
    $hasCount=0;
    $allcount=0;
    $hasKey=[];
    $realHasKey=[];
    $realCount=0;
    foreach($arr as $k=>$v){
        
        if(is_numeric($v)){
            $hasCount++;
            $hasKey[]=$k;
            if($v!=0)$realCount++;$realHasKey[]=$k;
            $allcount+=$v;
        }
    }
    if($hasCount>1){
        if($realCount==$hasCount){
            return $arr;
        }else{
            
            $noeprice = round($allcount/$hasCount,2);
            
            foreach($hasKey as $k=>$v){
                $arr[$v]=$noeprice;
            }
            $arr[$hasKey[count($hasKey)-1]]=$allcount-$noeprice*$hasCount+$noeprice;
            return $arr;
        }
    }else{
        return $arr;
    }
    

}