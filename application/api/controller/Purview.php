<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 2019-10-13
 * Time: 23:04
 * 权限管理菜单
 */

namespace app\api\controller;
use app\common\controller\ApiController;
use think\Db;
class Purview extends ApiController
{
    private $wy_auth;
    private $pms_employee;

    function __construct(){
        parent::__construct();
        $this->wy_auth = model('wy_auth');
        $this->pms_employee = Db::name('pms_employee');
    }

    // 群组列表
    public function groupList() {
        $param = $this->request->param();
        $where[] = ['pms_ar_id','=',$this->userinfo['pms_ar_id']];
        $list = $this->wy_auth->where($where)->page(empty($param['page']) ? 1 : $param['page'],20)->select();
        $count = $this->wy_auth->where($where)->count();
        $info = ['total'=>$count,'pageSize'=>20,'current'=>$param['page'],'page_sum'=>ceil($count / 20)];
        return __success('ok',['data'=>$list,'info'=>$info]);
    }

    // 添加or编辑群组
    public function operationGroup() {
        $param = $this->request->param();
        unset($param['token']);
        if(empty($param['name'])) return __error('群组名称不可为空');
        if(empty($param['pms_ar_id'])) return __error('所属物业不可为空');
        if($param['id'] == 0){
            $res = $this->wy_auth->strict(false)->insert($param,true,true);
            return $res ? __success('添加成功') : __error('添加失败');
        }else{
            $res = $this->wy_auth->isUpdate(true)->save($param);
            return $res ? __success('修改成功') : __error('修改失败');
        }
    }

    // 获取权限组菜单
    public function getTree() {
        $menu = Db::name('wy_web_menu')->field('id,pid,title')->order('id,sort')->select();
        $oneMenu = [];
        foreach ($menu as $key=>$value) if($value['pid'] === 0) array_push($oneMenu,$value);
        foreach ($oneMenu as $k=>$v){
            $oneMenu[$k]['children'] = [];
            foreach ($menu as $mk=>$mv) {
                if($mv['pid'] == $v['id']) array_push($oneMenu[$k]['children'],$mv);
            }
            foreach ($oneMenu[$k]['children'] as $ck=>$cv){
                $oneMenu[$k]['children'][$ck]['children'] = [];
                foreach ($menu as $mk=>$mv) {
                    if($mv['pid'] == $cv['id']) array_push($oneMenu[$k]['children'][$ck]['children'],$mv);
                }
            }
        }
        return __success('ok',$oneMenu);
    }

    // 设置群组权限
    public function setGroupPurview() {
        $id = $this->request->param('id');
        $menus = $this->request->param('menus');
        $upRes = Db::name('wy_auth')->where('id',$id)->update(['menus'=>$menus]);
        return $upRes !== false ? __success('设置成功') : __error('设置失败');
    }

    // 删除群组
    public function delGroup() {
        $param = $this->request->param();
        unset($param['token']);
        if(empty($param['id'])) return __error('参数错误');
        // 查询当前组下是否有成员
        $employee = $this->pms_employee->where(['wy_auth_id'=>$param['id']])->find();
        if(!empty($employee)) return __error('当前群组存在员工,若要删除群组,请先删除员工');
        $delRes = $this->wy_auth->where('id',$param['id'])->delete();
        return !empty($delRes) ? __success('删除成功') : __error('删除失败');
    }

    // 获取当前物业所有群组和小区
    public function getAllGroupAndCommunity() {
        $param = $this->request->param();
        $where[] = ['pms_ar_id','=',$this->userinfo['pms_ar_id']];
        // 小区列表
        $community = Db::name('wy_bd_info')->where($where)->field('id value,name title')->select();
        // 群组列表
        $group = $this->wy_auth->where($where)->field('id value,name title')->select();
        return __success('ok',['group'=>$group,'community'=>$community]);
    }

    // 角色列表
    public function roleList(){
        $param = $this->request->param();
        $where[] = ['e.pms_ar_id','=',$this->userinfo['pms_ar_id']];
        $where[] = ['status','=','1'];
        $where[] = ['is_deleted','=',0];
        $list = $this->pms_employee->alias('e')->join('wy_auth m','e.wy_auth_id = m.id')->where($where)->field('e.id,e.name,e.mobile,e.wy_auth_id,m.name as wy_auth_name,e.cm_message_id')->page(empty($param['page']) ? 1 : $param['page'],20)->select();
        $count = $this->pms_employee->alias('e')->join('wy_auth a','e.wy_auth_id = a.id')->where($where)->count();
        $info = ['total'=>$count,'pageSize'=>20,'current'=>$param['page'],'page_sum'=>ceil($count / 20)];
        return __success('ok',['data'=>$list,'info'=>$info]);
    }

    // 添加or编辑角色
    public function operationRole() {
        $param = $this->request->param();
        unset($param['token']);
        if(empty($param['name'])) return __error('姓名不可为空');
        if(empty($param['mobile'])) return __error('手机号不可为空');
        if(empty($param['wy_auth_id'])) return __error('所属群组不可为空');
        $param['cm_message_id'] = implode(',',$param['cm_message_id']);
        if($param['id'] == 0){
            if(empty($param['password'])) return __error('密码不可为空');
            $param['status'] = '1';
            $param['password'] = password($param['password']);
            $param['pms_ar_id'] = $this->userinfo['pms_ar_id'];
            $param['create_unit'] = 2;
            $res = $this->pms_employee->strict(false)->insert($param,true,true);
            return $res ? __success('添加成功') : __error('添加失败');
        }else{
            if(!empty($param['password'])) $param['password'] = password($param['password']);
            else unset($param['password']);
            $upRes = $this->pms_employee->update($param);
            return $upRes !== false ? __success('修改成功') : __error('修改失败');
        }
    }

    // 删除角色
    public function delRole() {
        $param = $this->request->param();
        if(empty($param['id'])) return __error('参数错误');
        $upRes = $this->pms_employee->where(['id'=>$param['id']])->update(['is_deleted'=>1]);
        return $upRes !== false ? __success('删除成功') : __error('删除失败');
    }
}