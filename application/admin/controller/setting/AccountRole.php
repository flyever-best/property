<?php

namespace app\admin\controller\setting;

use app\common\controller\AdminController;

use think\Controller;
use think\Request;

/**
 * 固定角色
 */
class AccountRole extends AdminController
{
    
    /**
     * PmsAuth模型对象
     */
    protected $model = null;

    public function __construct() {
            parent::__construct();
            $this->model = model('PmsAuth');
                    $this->view->assign("statusList", $this->model->getStatusList());
        }
    
    /**
     * 查看
     */
    public function index()
    {
        if ($this->request->get('type') == 'ajax') {
            $page = $this->request->get('page', 1);
            $limit = $this->request->get('limit', 10);
            $search = (array)$this->request->get('search', []);
            return json($this->model->getPmsAuthList($page, $limit, $search));
        }
        $basic_data = [
            'title' => '固定角色',
            'data'  => '',
        ];
        return $this->fetch('', $basic_data);
    }

    /**
     * 添加
     * @return mixed
     */
    public function add() {
        if (!$this->request->isPost()) {
            $basic_data = [
                'title' => '添加固定角色',
            ];
            return $this->fetch('add', $basic_data);
        } else {
            $post = $this->request->post();

            //验证数据
            $validate = $this->validate($post, 'app\admin\validate\PmsAuth.add');
            if (true !== $validate) return __error($validate);

            //保存数据,返回结果
            return $this->model->addData($post);
        }
    }

    /**
     * 修改
     * @return mixed|string|\think\response\Json
     */
    public function edit() {
        if (!$this->request->isPost()) {

            //查找所需修改的固定角色
            $data = $this->model->where('id', $this->request->get('id'))->find();
            if (empty($data)) return msg_error('暂无数据，请重新刷新页面！');

            //基础数据
            $basic_data = [
                'title'    => '修改固定角色',
                'info' => $data,
            ];
            return $this->fetch('edit', $basic_data);
        } else {
            $post = $this->request->post();

            //验证数据
            $validate = $this->validate($post, 'app\admin\validate\PmsAuth.edit');
            if (true !== $validate) return __error($validate);

            //保存数据,返回结果
            return $this->model->editData($post);
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function del() {
        $get = $this->request->get();

        //验证数据
        if (!is_array($get['id'])) {
            $validate = $this->validate($get, 'app\admin\validate\PmsAuth.del');
            if (true !== $validate) return __error($validate);
        }

        //执行删除操作
        return $this->model->delData($get['id']);
    }

    /**
     * 更改状态
     * @return \think\response\Json
     */
    public function status() {
        $get = $this->request->get();

        //验证数据
        $validate = $this->validate($get, 'app\admin\validate\PmsAuth.status');
        if (true !== $validate) return __error($validate);

        //判断状态
        $status = $this->model->where('id', $get['id'])->value('status');
        $status == 1 ? list($msg, $status) = ['启用成功', $status = 0] : list($msg, $status) = ['禁用成功', $status = 1];

        //执行更新操作操作
        $update = $this->model->where('id', $get['id'])->update(['status' => $status]);

        if ($update >= 1) return __success($msg);
        return __error('数据有误，请刷新重试！');
    }

    /**
     * 授权信息
     * @return mixed|string|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function authorize() {
        if (!$this->request->isPost()) {

            //查找所需授权角色
            $auth = $this->model->where('id', $this->request->get('id'))->find();
            if (empty($auth)) return msg_error('暂无数据，请重新刷新页面！');

            $node = model('pms_menu')->where(['status' => 1])->order('sort asc')->select();

            $auth_node = model('pms_auth_node')->where(['auth' => $auth['id']])->select();

            foreach ($node as &$vo) {
                $i = 0;
                foreach ($auth_node as $al) {
                    $vo['id'] == $al['node'] && $i++;
                }
                $i == 0 ? $vo['is_checked'] = false : $vo['is_checked'] = true;
            }

            //基础数据
            $basic_data = [
                'title' => '角色授权',
                'auth'  => $auth,
                'node'  => $node,
            ];
            $this->assign($basic_data);

            return $this->fetch();
        } else {
            $post = $this->request->post();
            empty($post['node_id']) && $post['node_id'] = [];

            //验证数据
            $validate = $this->validate($post, 'app\admin\validate\Auth.authorize');
            if (true !== $validate) return __error($validate);

            $insertAll = [];
            foreach ($post['node_id'] as $vo) {
                $insertAll[] = [
                    'auth' => $post['auth_id'],
                    'node' => $vo,
                ];
            }

            //清空菜单缓存
            clear_menu();

            //清空旧数据
            model('pms_auth_node')->where(['auth' => $post['auth_id']])->delete();
            //保存数据,返回结果
            return model('pms_auth_node')->authorize($insertAll);
        }
    }
}
