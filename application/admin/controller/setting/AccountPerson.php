<?php

namespace app\admin\controller\setting;

use app\common\controller\AdminController;

use think\Controller;
use think\Request;

/**
 * 员工
 */
class Accountperson extends AdminController
{
    
    /**
     * PmsEmployee模型对象
     */
    protected $model = null;

    public function __construct() {
            parent::__construct();
            $this->model = model('PmsEmployee');
                    $this->view->assign("pmsAuthIdList", $this->model->getPmsAuthIdList());
        $this->view->assign("pmsArIdList", $this->model->getPmsArIdList());
        $this->view->assign("statusList", $this->model->getStatusList());
        }
    
    /**
     * 查看
     */
    public function index()
    {
        if ($this->request->get('type') == 'ajax') {
            $page = $this->request->get('page', 1);
            $limit = $this->request->get('limit', 10);
            $search = (array)$this->request->get('search', []);
            return json($this->model->getPmsEmployeeList($page, $limit, $search));
        }
        $basic_data = [
            'title' => '员工',
            'data'  => '',
        ];
        return $this->fetch('', $basic_data);
    }

    /**
     * 添加
     * @return mixed
     */
    public function add() {
        if (!$this->request->isPost()) {
            $basic_data = [
                'title' => '添加员工',
            ];
            return $this->fetch('add', $basic_data);
        } else {
            $post = $this->request->post();

            //验证数据
            $validate = $this->validate($post, 'app\admin\validate\PmsEmployee.add');
            if (true !== $validate) return __error($validate);

            //保存数据,返回结果
            $post['create_unit'] = 1;
            return $this->model->addData($post);
        }
    }

    /**
     * 修改
     * @return mixed|string|\think\response\Json
     */
    public function edit() {
        if (!$this->request->isPost()) {

            //查找所需修改的员工
            $data = $this->model->where('id', $this->request->get('id'))->find();
            if (empty($data)) return msg_error('暂无数据，请重新刷新页面！');

            //基础数据
            $basic_data = [
                'title'    => '修改员工',
                'info' => $data,
            ];
            return $this->fetch('edit', $basic_data);
        } else {
            $post = $this->request->post();

            //验证数据
            $validate = $this->validate($post, 'app\admin\validate\PmsEmployee.edit');
            if (true !== $validate) return __error($validate);

            //保存数据,返回结果
            return $this->model->editData($post);
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function del() {
        $get = $this->request->get();

        //验证数据
        if (!is_array($get['id'])) {
            $validate = $this->validate($get, 'app\admin\validate\PmsEmployee.del');
            if (true !== $validate) return __error($validate);
        }

        //执行删除操作
        return $this->model->delData($get['id']);
    }

    /**
     * 更改状态
     * @return \think\response\Json
     */
    public function status() {
        $get = $this->request->get();

        //验证数据
        $validate = $this->validate($get, 'app\admin\validate\PmsEmployee.status');
        if (true !== $validate) return __error($validate);

        //判断状态
        $status = $this->model->where('id', $get['id'])->value('status');
        $status == 1 ? list($msg, $status) = ['启用成功', $status = 0] : list($msg, $status) = ['禁用成功', $status = 1];

        //执行更新操作操作
        $update = $this->model->where('id', $get['id'])->update(['status' => $status]);

        if ($update >= 1) return __success($msg);
        return __error('数据有误，请刷新重试！');
    }

    public function authorizeHotel() {
        if (!$this->request->isPost()) {
            //查找所需授权角色
            $auth = model('pmsEmployee')->where('id', $this->request->get('id'))->find();
            if($this->request->get('type') == '1'){
                $page = $this->request->get('page',1);
                $limit = $this->request->get('limit',20);
                if (empty($auth)) return msg_error('暂无数据，请重新刷新页面！');
                $node = model('CmMessage')->where(['status' => 1,'pms_ar_id' => $auth['pms_ar_id']])->page($page,$limit)->select();
                $count = model('CmMessage')->where(['status' => 1, 'pms_ar_id' => $auth['pms_ar_id']])->count();

                $auth_hotel = explode(',',$auth['cm_message_id']);

                foreach ($node as &$vo) {
                    $i = 0;
                    foreach ($auth_hotel as $al) {
                        $vo['id'] == $al && $i++;
                    }
                    $i == 0 ? $vo['is_checked'] = false : $vo['is_checked'] = true;
                }
                $list = [
                    'page_current' => $page,
                    'page_sum'     => ceil($count / $limit),
                    'data'  => $node,
                ];
                return __success('查询成功!',$list);
            }
            //查找所需授权角色
            // $auth = $this->model->where('id', $this->request->get('id'))->find();
            // if (empty($auth)) return msg_error('暂无数据，请重新刷新页面！');
            //基础数据
            $basic_data = [
                'title' => '酒店授权',
                'auth'  => $auth,
                'data' => '',
            ];
            $this->assign($basic_data);
            $this->assign('auth',$auth);
//            $this->assign('cityList',db('system_area')->where('level',2)->column('id,name'));
            return $this->fetch('');
        } else {
            $post = $this->request->post();
            empty($post['node_id']) && $post['node_id'] = [];

            //验证数据
            $validate = $this->validate($post, 'app\admin\validate\Auth.authorize');
            if (true !== $validate) return __error($validate);

            $insertAll['cm_message_id'] = implode(',',$post['node_id']);
            $insertAll['id'] = $post['auth_id'];
            $res=model('pmsEmployee')->isUpdate(true)->save($insertAll);
            if($res){
                return __success('保存成功！');
            }else{
                return __error('保存失败！');
            }
        }
    }

}
