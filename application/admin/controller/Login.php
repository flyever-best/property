<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\facade\Cache;

class Login extends Controller {

    /**
     * User模型对象
     */
    protected $model = null;

    /**
     * 初始化
     * Login constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->model = model('User');
        $action = $this->request->action();
        if (!empty(session('user.id')) && $action !== 'out' && $action !== 'change') return $this->redirect('@admin');
    }
    public function pro_exp(){
        $data = $this->request->get();
        $xlsCell = array(
            array('id','编号'),
            array('bd_info_id_text','小区'),
            array('project_id_text','收费项目'),
            array('project_detial_id_text','收费标准'),
            array('start_time','开始时间'),
            array('end_time','结束时间'),
            array('ratio','公摊'),
            array('phone','用户'),
        );
        $where[] = ['is_deleted', '=', 0];
        $where[] = ['bd_info_id','=',$data['bd_info_id']];
        $where[] = ['project_detial_id','=',$data['pdt']];
        $list = db('wy_copy_table')->where($where)->order(['id' => 'desc'])->select();
        $bdMd = db('wy_bd_info');
        $proMd = db('wy_toll_project');
        $detMd = db('wy_toll_project_detial');
        $userMd = db('wy_house_user');
        foreach ($list as $k=>$v) {
            $list[$k]['bd_info_id_text'] = $bdMd->where('id',$v['bd_info_id'])->value('name');
            $list[$k]['project_id_text'] = $proMd->where('id',$v['project_id'])->value('name');
            $list[$k]['project_detial_id_text'] = $detMd->where('id',$v['project_detial_id'])->value('name');
            $list[$k]['phone'] = $userMd->where('id',$v['user_id'])->value('phone');
        }
        $xlsName = "抄表录入导出数据";
        $xlsData = $list;
        exportExcel2($xlsName,$xlsCell,$xlsData);
    }
    public function pay_exp(){
        $data = $this->request->get();
        $xlsCell = array(
            array('id','编号'),
            array('bd_info_id_text','小区'),
            array('houses_id_text','房屋'),
            array('houses_user_id_text','用户'),
            array('name','收费名称'),
            array('price','应收金额'),
            array('money','实收金额'),
            array('pay_style_text','支付类型'),
            array('status_text','支付状态'),
            array('pay_time','支付时间'),
            array('remarks','备注'),
        );
        $style_text = [
            '1' => '线下支付-现金',
            '2' => '线下支付-支票',
            '3' => '线下支付-银行转账',
            '4' => '线下支付-pos机刷卡',
            '5' => '线下支付-支付宝直接转账',
            '6' => '线下支付-微信直接转账',
            '7' => '小区收款码-支付宝',
            '8' => '小区收款码-微信'
        ];
        $status_text = [
            '1' => '已收',
            '2' => '已退',
            '3' => '撤销'
        ];
        $where[] = ['is_deleted', '=', 0];
        $where[] = ['bd_info_id','=',$data['bd_info_id']];
        $list = db('wy_payment')->where($where)->order(['id' => 'desc'])->select();
        $bdMd = db('wy_bd_info');
        $hsMd = db('wy_houses');
        $hsuMd = db('wy_house_user');
        foreach ($list as $k=>$v) {
            $list[$k]['bd_info_id_text'] = $bdMd->where('id',$v['bd_info_id'])->value('name');
            $list[$k]['houses_id_text'] = $hsMd->where('id',$v['houses_id'])->value('name');
            $list[$k]['houses_user_id_text'] = $hsuMd->where('id',$v['house_user_id'])->value('phone');
            $list[$k]['pay_style_text'] = $style_text[$v['pay_style']];
            $list[$k]['status_text'] = $status_text[$v['status']];
        }
        $xlsName = "缴费账单导出数据";
        $xlsData = $list;
        exportExcel2($xlsName,$xlsCell,$xlsData);
    }
    public function not_pay_exp(){
        $data = $this->request->get();
        $xlsCell = array(
            array('id','编号'),
            array('bd_info_id_text','小区'),
            array('house_id_text','房屋'),
            array('house_user_id_text','用户'),
            array('type_text','类型'),
            array('name','收费名称'),
            array('price','应收金额'),
            array('money','应缴金额'),
            array('start_time','开始时间'),
            array('end_time','结束时间'),
            array('one_money','单价'),
            array('num','数量'),
            array('offer','优惠'),
            array('overdue','滞纳金'),
            array('deduction','预存款抵扣'),
            array('remarks','备注'),
        );
        $type_text = [
            '1' => '房屋',
            '2' => '车辆',
            '3' => '车位',
            '4' => '住户'
        ];
        $where[] = ['is_deleted', '=', 0];
        $where[] = ['bd_info_id','=',$data['bd_info_id']];
        $list = db('wy_no_payment')->where($where)->where('pay_status',0)->order(['id' => 'desc'])->select();
        $bdMd = db('wy_bd_info');
        $hsMd = db('wy_houses');
        $hsuMd = db('wy_house_user');
        foreach ($list as $k=>$v) {
            $list[$k]['type_text'] = $type_text[$v['type']];
            $list[$k]['bd_info_id_text'] = $bdMd->where('id',$v['bd_info_id'])->value('name');
            $list[$k]['house_id_text'] = $hsMd->where('id',$v['house_id'])->value('name');
            $list[$k]['house_user_id_text'] = $hsuMd->where('id',$v['house_user_id'])->value('phone');
        }
        $xlsName = "未缴费账单导出数据";
        $xlsData = $list;
        exportExcel2($xlsName,$xlsCell,$xlsData);
    }
    /**
     * 后台登录
     * @return mixed|\think\response\Json
     */
    public function index() {
        if ($this->request->isGet()) {

            //基础数据
            $basic_data = [
                'title' => '智慧物业管理系统',
                'data'  => '',
            ];
            $this->assign($basic_data);

            return $this->fetch('');
        } else {
            $post = $this->request->post();

            //判断是否开启验证码登录选择验证规则
            $SysInfo = Cache::get('SysInfo');
            $SysInfo['VercodeType'] != 1 ? $validate_type = 'app\admin\validate\Login.index_off' : $validate_type = 'app\admin\validate\Login.index_on';

            //验证参数
            $validate = $this->validate($post, $validate_type);
            if (true !== $validate) {
                return __error($validate);
            }

            //判断登录是否成功
            $login = $this->model->login($post['username'], $post['password']);
            if ($login['code'] == 1) {
                isset($login['user']['id']) ? $user_id = $login['user']['id'] : $user_id = '';
                \app\admin\service\LogService::loginLog($user_id, 1, 0, "【账号登录】{$login['msg']}");
                return __error($login['msg']);
            }

            //储存session数据
            $login['user']['login_at'] = time();
            session('user', $login['user']);
            \app\admin\service\LogService::loginLog($login['user']['id'], 1, 1, '【账号登录】登录成功，正在进入系统！');
            return __success($login['msg']);
        }
    }

    /**
     * 切换账户
     * @return mixed
     */
    public function change() {
        if ($this->request->isGet()) {

            //基础数据
            $basic_data = [
                'title' => '垣通网络科技后台登录',
                'data'  => '',
            ];
            $this->assign($basic_data);

            return $this->fetch('index');
        }
    }

    /**
     * 退出登录
     * @return \think\response\Json
     */
    public function out() {

        //记录日志
        \app\admin\service\LogService::loginLog(session('user.id'), 0, 1, "【主动退出】正在退出后台系统！");

        //删除自身菜单缓存
        Cache::rm(session('user.id') . '_AdminMenu');

        //清空sesion数据
        session('user', null);

        return msg_success('退出登录成功', url('@admin/login'));
    }
}