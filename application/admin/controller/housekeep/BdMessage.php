<?php

namespace app\admin\controller\housekeep;

use app\common\controller\AdminController;

use think\Controller;
use think\Request;

/**
 * 楼栋信息
 */
class Bdmessage extends AdminController
{
    
    /**
     * BdMessage模型对象
     */
    protected $model = null;

    public function __construct() {
            parent::__construct();
            $this->model = model('BdMessage');
                    $this->view->assign("cmMessageIdList", $this->model->getCmMessageIdList());
        $this->view->assign("statusList", $this->model->getStatusList());
        }
    
    /**
     * 查看
     */
    public function index()
    {
        if ($this->request->get('type') == 'ajax') {
            $page = $this->request->get('page', 1);
            $limit = $this->request->get('limit', 10);
            $search = (array)$this->request->get('search', []);
            $periodId = $this->request->get('period_id');
            $cmId = $this->request->get('cm_id');
            return json($this->model->getBdMessageList($page, $limit, $search, $cmId, $periodId));
        }
        $basic_data = [
            'title' => '楼栋信息',
            'data'  => '',
        ];
        $this->assign('periodId', $this->request->get('period_id'));
        $this->assign('cmId', $this->request->get('cm_id'));
        return $this->fetch('housekeep/bd_message/index', $basic_data);
    }

    /**
     * 添加
     * @return mixed
     */
    public function add() {
        if (!$this->request->isPost()) {
            $basic_data = [
                'title' => '添加楼栋信息',
            ];
            return $this->fetch('add', $basic_data);
        } else {
            $post = $this->request->post();

            //验证数据
            $validate = $this->validate($post, 'app\admin\validate\BdMessage.add');
            if (true !== $validate) return __error($validate);

            //保存数据,返回结果
            return $this->model->addData($post);
        }
    }

    /**
     * 修改
     * @return mixed|string|\think\response\Json
     */
    public function edit() {
        if (!$this->request->isPost()) {

            //查找所需修改的楼栋信息
            $data = $this->model->where('id', $this->request->get('id'))->find();
            if (empty($data)) return msg_error('暂无数据，请重新刷新页面！');

            //基础数据
            $basic_data = [
                'title'    => '修改楼栋信息',
                'info' => $data,
            ];
            return $this->fetch('edit', $basic_data);
        } else {
            $post = $this->request->post();

            //验证数据
            $validate = $this->validate($post, 'app\admin\validate\BdMessage.edit');
            if (true !== $validate) return __error($validate);

            //保存数据,返回结果
            return $this->model->editData($post);
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function del() {
        $get = $this->request->get();

        //验证数据
        if (!is_array($get['id'])) {
            $validate = $this->validate($get, 'app\admin\validate\BdMessage.del');
            if (true !== $validate) return __error($validate);
        }

        //执行删除操作
        return $this->model->delData($get['id']);
    }

    /**
     * 更改状态
     * @return \think\response\Json
     */
    public function status() {
        $get = $this->request->get();

        //验证数据
        $validate = $this->validate($get, 'app\admin\validate\BdMessage.status');
        if (true !== $validate) return __error($validate);

        //判断状态
        $status = $this->model->where('id', $get['id'])->value('status');
        $status == 1 ? list($msg, $status) = ['启用成功', $status = 0] : list($msg, $status) = ['禁用成功', $status = 1];

        //执行更新操作操作
        $update = $this->model->where('id', $get['id'])->update(['status' => $status]);

        if ($update >= 1) return __success($msg);
        return __error('数据有误，请刷新重试！');
    }

}
