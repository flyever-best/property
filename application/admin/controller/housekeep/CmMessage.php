<?php

namespace app\admin\controller\housekeep;

use app\common\controller\AdminController;

use think\Controller;
use think\Request;
use think\Db;

/**
 * 小区信息管理
 */
class Cmmessage extends AdminController
{
    
    /**
     * CmMessage模型对象
     */
    protected $model = null;

    public function __construct() {
            parent::__construct();
            $this->model = model('CmMessage');
                    $this->view->assign("pmsArIdList", $this->model->getPmsArIdList());
        $this->view->assign("statusList", $this->model->getStatusList());
        }
    
    /**
     * 查看
     */
    public function index()
    {
        if ($this->request->get('type') == 'ajax') {
            $page = $this->request->get('page', 1);
            $limit = $this->request->get('limit', 10);
            $search = (array)$this->request->get('search', []);
            return json($this->model->getCmMessageList($page, $limit, $search, $this->__getAuthArMessage()));
        }
        $basic_data = [
            'title' => '小区信息管理',
            'data'  => '',
        ];
        return $this->fetch('', $basic_data);
    }

    /**
     * 添加
     * @return mixed
     */
    public function add() {
        if (!$this->request->isPost()) {
            $basic_data = [
                'title' => '添加小区信息管理',
            ];
            return $this->fetch('add', $basic_data);
        } else {
            $post = $this->request->post();
            //验证数据
            $validate = $this->validate($post, 'app\admin\validate\CmMessage.add');
            if (true !== $validate) return __error($validate);

            //保存数据,返回结果
            return $this->model->addData($post);
        }
    }

    /**
     * 修改
     * @return mixed|string|\think\response\Json
     */
    public function edit() {
        if (!$this->request->isPost()) {

            //查找所需修改的小区信息管理
            $data = $this->model->where('id', $this->request->get('id'))->find();
            if (empty($data)) return msg_error('暂无数据，请重新刷新页面！');

            //基础数据
            $basic_data = [
                'title'    => '修改小区信息管理',
                'info' => $data,
            ];
            return $this->fetch('edit', $basic_data);
        } else {
            $post = $this->request->post();

            //验证数据
            $validate = $this->validate($post, 'app\admin\validate\CmMessage.edit');
            if (true !== $validate) return __error($validate);

            //保存数据,返回结果
            return $this->model->editData($post);
        }
    }

    /**
     * 删除
     * @return \think\response\Json
     */
    public function del() {
        $get = $this->request->get();

        //验证数据
        if (!is_array($get['id'])) {
            $validate = $this->validate($get, 'app\admin\validate\CmMessage.del');
            if (true !== $validate) return __error($validate);
        }

        //执行删除操作
        return $this->model->delData($get['id']);
    }

    /**
     * 更改状态
     * @return \think\response\Json
     */
    public function status() {
        $get = $this->request->get();

        //验证数据
        $validate = $this->validate($get, 'app\admin\validate\CmMessage.status');
        if (true !== $validate) return __error($validate);

        //判断状态
        $status = $this->model->where('id', $get['id'])->value('status');
        $status == 1 ? list($msg, $status) = ['启用成功', $status = 0] : list($msg, $status) = ['禁用成功', $status = 1];

        //执行更新操作操作
        $update = $this->model->where('id', $get['id'])->update(['status' => $status]);

        if ($update >= 1) return __success($msg);
        return __error('数据有误，请刷新重试！');
    }



    // 三级联动，一级信息
    public function __getCityOption() {
        $data = Db::name('district')->where('pid', 1)->column('id, district');
        return __success('查询成功', $data);
    }

    // 三级联动，二级
    public function __getCountyOption() {
        $pid = $this->request->post('pid');
        $data = Db::name('district')->where('pid', $pid)->column('id, district');
        if(empty($data) && $pid != null) {
            $info = Db::name('district')->where('id', $pid)->column('id, district');
            return __success('查询成功', $info);
        } else {
            return __success('查询成功', $data);
        }
    }

    // 三级联动，三级
    public function __getAreaOption() {
        $pid = $this->request->post('pid');
        $data = Db::name('district')->where('pid', $pid)->column('id, district');
        return __success('查询成功', $data);
    }

    // 获取所有小区
    public function getAllCmMessage() {
        $data = model('CmMessage')->where([['status', '=', 1], ['is_deleted', '=', 0]])->select();
        return __success('查询成功', $data);
    }

    // 接受设置的小区
    public function getUserSetCm() {
        $cmId = $this->request->get('cmId');
        session('cmId', $cmId, 'think');
        if(!empty(session('cmId','', 'think'))) {
            return __success('设置成功');
        } else {
            return __error('设置失败');
        }
    }

}
