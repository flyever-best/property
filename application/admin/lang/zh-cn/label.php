<?php

return [
    'Id'  =>  'Id',
    'Label_type_id'  =>  '分类',
    'Name'  =>  '名称',
    'Status'  =>  '禁用：0=禁用，1=不禁用',
    'Is_deleted'  =>  '是否删除'
];
