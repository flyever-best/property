<?php

return [
    'Id'  =>  'id',
    'Name'  =>  '标签名称',
    'Status'  =>  '状态',
    'Status 0'  =>  '禁用',
    'Status 1'  =>  '正常',
    'Is_deleted'  =>  '是否删除'
];
