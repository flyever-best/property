<?php

return [
    'Id'  =>  'Id',
    'Label_type_id'  =>  '分类',
    'Name'  =>  '名称',
    'Status'  =>  '状态',
    'Status 0'  =>  '禁用',
    'Status 1'  =>  '正常',
    'Is_deleted'  =>  '是否删除'
];
