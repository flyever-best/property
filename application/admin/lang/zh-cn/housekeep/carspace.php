<?php

return [
    'Id'  =>  '车位id',
    'Name'  =>  '名称',
    'Nickname'  =>  '姓名',
    'Mobile'  =>  '手机号',
    'Status'  =>  '状态',
    'Status 0'  =>  '禁用',
    'Status 1'  =>  '正常',
    'Is_deleted'  =>  '是否删除'
];
