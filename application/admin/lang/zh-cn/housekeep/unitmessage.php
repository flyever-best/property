<?php

return [
    'Id'  =>  'Id',
    'Info'  =>  '单元',
    'Bd_message_id'  =>  '所属楼栋',
    'Unit'  =>  '标识',
    'Status'  =>  '状态',
    'Status 0'  =>  '禁用',
    'Status 1'  =>  '正常',
    'Is_deleted'  =>  '是否删除'
];
