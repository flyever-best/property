<?php

return [
    'Id'  =>  '编号',
    'Bd_num'  =>  '楼栋号',
    'Unit_num'  =>  '单元号',
    'Houses_num'  =>  '房号',
    'Houses_type'  =>  '房产类型',
    'Houses_type 0'  =>  '多层住宅',
    'Houses_type 1'  =>  '高层住宅',
    'Houses_area'  =>  '建筑面积',
    'Houses_admin'  =>  '房管员',
    'Pro_type'  =>  '房屋类型',
    'Name'  =>  '业主姓名',
    'User_unit'  =>  '身份证号',
    'Checktime'  =>  '入住日期',
    'Phone'  =>  '手机号',
    'Spare_phone'  =>  '备用手机号',
    'Work'  =>  '工作单位',
    'Car_unit'  =>  '车牌号',
    'Car_unit_other'  =>  '车牌号2',
    'Remarks'  =>  '备注',
    'Err_msg'  =>  '错误原因'
];
