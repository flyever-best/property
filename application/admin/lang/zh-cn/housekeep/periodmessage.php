<?php

return [
    'Id'  =>  'id',
    'Name'  =>  '区期名称',
    'Unit'  =>  '区期编号',
    'Status'  =>  '状态',
    'Status 0'  =>  '正常',
    'Status 1'  =>  '禁用',
    'Is_deleted'  =>  '是否删除'
];
