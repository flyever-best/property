<?php

return [
    'Id'  =>  '编号',
    'Cm_num'  =>  '小区',
    'Bd_num'  =>  '楼号',
    'Period_num'  =>  '区号',
    'Unit_num'  =>  '单元',
    'Houses_num'  =>  '户号',
    'Name'  =>  '姓名',
    'Phone'  =>  '手机号',
    'User_unit'  =>  '身份证号',
    'Houses_area'  =>  '面积',
    'Public_num'  =>  '公摊系数',
    'Num_area'  =>  '系数面积',
    'Type'  =>  '类型',
    'Type 1'  =>  '商户',
    'Type 2'  =>  '住宅',
    'Labels_id'  =>  '房屋标签',
    'In_time'  =>  '入住时间',
    'In_message'  =>  '入住信息',
    'Pay_time'  =>  '交费时间',
    'Remarks'  =>  '备注',
    'Status'  =>  '状态',
    'Status 0'  =>  '禁用',
    'Status 1'  =>  '正常',
    'Is_deleted'  =>  '是否删除'
];
