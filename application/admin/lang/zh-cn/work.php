<?php

return [
    'Id'  =>  '工单id',
    'Houses_info_id'  =>  '提交人',
    'Work_image'  =>  '照片',
    'Limited_time'  =>  '截止处理时间',
    'Title'  =>  '工单标题',
    'Content'  =>  '工单内容',
    'Progress'  =>  '工单进度',
    'Progress 1'  =>  '待分配',
    'Progress 2'  =>  '待完成',
    'Progress 3'  =>  '已完成',
    'Type'  =>  '工单类型',
    'Type 1'  =>  '基础工单',
    'Type 2'  =>  '投诉工单',
    'X_info'  =>  'x',
    'Y_info'  =>  'y',
    'Is_deleted'  =>  '逻辑删除',
    'Createtime'  =>  '工单创建时间',
    'Actiontime'  =>  '最后一次操作时间'
];
