<?php

return [
    'Id'  =>  '编号',
    'Name'  =>  '名称',
    'System_user_id'  =>  '绑定管理员',
    'Username'  =>  '账号',
    'Password'  =>  '密码',
    'Status'  =>  '状态',
    'Status 0'  =>  '禁用',
    'Status 1'  =>  '正常',
    'Is_deleted'  =>  '是否删除'
];
