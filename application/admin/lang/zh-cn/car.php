<?php

return [
    'Id'  =>  '车辆id',
    'Name'  =>  '车牌号',
    'Createtime'  =>  '登记时间',
    'Status'  =>  '状态',
    'Status 0'  =>  '禁用',
    'Status 1'  =>  '正常',
    'Is_deleted'  =>  '是否删除'
];
