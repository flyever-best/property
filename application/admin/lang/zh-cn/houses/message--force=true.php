<?php

return [
    'Id'  =>  'id',
    'Houses_id'  =>  '房屋id',
    'Name'  =>  '户主姓名',
    'Phone'  =>  '联系方式',
    'Status'  =>  '关注状态',
    'Reg_status'  =>  '注册状态',
    'Is_deleted'  =>  '是否删除'
];
