<?php

return [
    'Id'  =>  'Id',
    'Cm_message_id'  =>  '小区id',
    'Area'  =>  '房屋面积',
    'Numbers'  =>  '户数',
    'Type'  =>  '房屋类型',
    'Name'  =>  '房屋标识',
    'Public_price'  =>  '公摊费系数',
    'Status'  =>  '房屋状态',
    'Status 0'  =>  '禁用',
    'Status 1'  =>  '正常',
    'Is_deleted'  =>  '是否删除'
];
