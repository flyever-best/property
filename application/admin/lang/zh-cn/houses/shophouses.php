<?php

return [
    'Id'  =>  'Id',
    'Area'  =>  '房屋面积',
    'Numbers'  =>  '户数',
    'Type'  =>  '房屋类型',
    'Name'  =>  '房屋标识',
    'Public_price'  =>  '公摊费系数',
    'Status'  =>  '房屋状态',
    'Is_deleted'  =>  '是否删除'
];
