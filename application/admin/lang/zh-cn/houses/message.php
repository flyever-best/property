<?php

return [
    'Id'  =>  'id',
    'Bd_message_id'  =>  '所属楼栋',
    'Unit_message_id'  =>  '所属单元',
    'Unit'  =>  '标识',
    'Name'  =>  '户主姓名',
    'Phone'  =>  '联系方式',
    'Status'  =>  '关注状态',
    'Status 0'  =>  '未关注',
    'Status 1'  =>  '已关注',
    'Reg_status'  =>  '注册状态',
    'Reg_status 0'  =>  '未注册',
    'Reg_status 1'  =>  '已注册',
    'Is_deleted'  =>  '是否删除'
];
