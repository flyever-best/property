<?php

return [
    'Id'  =>  '编号',
    'Name'  =>  '导入表格名称',
    'Createtime'  =>  '导入时间',
    'User'  =>  '操作人',
    'Err_msg'  =>  '失败/总数',
    'Status'  =>  '导入状态',
    'Status 0'  =>  '未完成',
    'Status 1'  =>  '完成',
    'Is_deleted'  =>  '是否删除'
];
