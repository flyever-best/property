<?php

return [
    'Id'  =>  '编号',
    'Pms_ar_id'  =>  '物业公司',
    'Cm_message_id'  =>  ' 小区',
    'Fee_items_id'  =>  '类型',
    'Type'  =>  '分类',
    'Type 1'  =>  '周期行费用',
    'Type 2'  =>  '走表费用',
    'Name'  =>  '名称',
    'Standard'  =>  '标准',
    'Unit'  =>  '单位',
    'Price'  =>  '金额',
    'Ext'  =>  '扩展',
    'Is_deleted'  =>  '是否删除'
];
