<?php

return [
    'Id'  =>  'id',
    'Type'  =>  '收费类型',
    'Name'  =>  '费用名称',
    'Price'  =>  '收费价格',
    'Createtime'  =>  '创建日期',
    'Endtime'  =>  '截止日期',
    'Remarks'  =>  '备注',
    'Is_deleted'  =>  '是否删除',
    'Coupon_id'  =>  '优惠卷'
];
