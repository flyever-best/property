<?php

return [
    'Id'  =>  'Id',
    'Type'  =>  '优惠类型',
    'Content'  =>  '优惠内容',
    'Is_deleted'  =>  '是否删除',
    'Name'  =>  '优惠卷标题',
    'Status'  =>  '状态'
];
