<?php

return [
    'Id'  =>  'Id',
    'Name'  =>  '小区名称',
    'Pms_ar_id'  =>  '所属物业',
    'City'  =>  '所在地区',
    'Street'  =>  '所属街道办事处',
    'Police'  =>  '所处派出所',
    'Number'  =>  '小区编号',
    'Phone'  =>  '办公电话',
    'Is_deleted'  =>  '是否删除',
    'Status'  =>  '状态',
    'Status 0'  =>  '禁用',
    'Status 1'  =>  '正常'
];
