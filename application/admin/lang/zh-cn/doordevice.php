<?php

return [
    'Id'  =>  'Id',
    'Type'  =>  '门禁类型',
    'Size'  =>  '门禁尺寸',
    'Effect_type'  =>  '门禁功能',
    'Position'  =>  '门禁位置',
    'Canphone'  =>  '是否支持电话呼叫',
    'Is_deleted'  =>  '是否删除'
];
