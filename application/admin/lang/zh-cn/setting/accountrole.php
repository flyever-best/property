<?php

return [
    'Id'  =>  'Id',
    'Title'  =>  '权限名称',
    'Remark'  =>  '备注说明',
    'Status'  =>  '状态',
    'Status 0'  =>  '禁用',
    'Status 1'  =>  '启用',
    'Is_deleted'  =>  '是否删除',
    'Createtime'  =>  '创建时间'
];
