<?php

return [
    'Id'  =>  '员工ID',
    'Name'  =>  '姓名',
    'Mobile'  =>  '手机号',
    'Password'  =>  '密码',
    'Pms_auth_id'  =>  '员工权限',
    'Pms_ar_id'  =>  '所属物业',
    'Status'  =>  '状态',
    'Status 0'  =>  '禁用',
    'Status 1'  =>  '正常',
    'Is_deleted'  =>  '是否删除'
];
