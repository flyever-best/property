<?php

return [
    'Id'  =>  'Id',
    'Area'  =>  '房屋面积',
    'Name'  =>  '户主姓名',
    'Numbers'  =>  '户数',
    'Type'  =>  '房屋类型',
    'Identity'  =>  '房屋标识',
    'Public_price'  =>  '公摊费系数',
    'Is_deleted'  =>  '是否删除'
];
