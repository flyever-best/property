<?php

return [
    'Id'  =>  '工单id',
    'User_id'  =>  '处理人',
    'Limited_time'  =>  '限时',
    'Title'  =>  '工单标题',
    'Member_id'  =>  '工单创建人',
    'Content'  =>  '工单内容',
    'Progress'  =>  '工单进度
1 未派单
2 已派单
3 已接单
4 已超时
5 已结单',
    'Type'  =>  '工单类型
1 正常 2 投诉',
    'Createtime'  =>  '工单创建时间',
    'Actiontime'  =>  '最后一次操作时间',
    'Is_deleted'  =>  '逻辑删除
1 删除 0 正常'
];
