<?php

return [
    'Id'  =>  '车位id',
    'Car_id'  =>  '所属车辆',
    'Cm_message_id'  =>  '所属小区',
    'Space'  =>  '车位坐标',
    'Createtime'  =>  '创建时间',
    'Endtime'  =>  '有效截止时间',
    'Status'  =>  '状态',
    'Status 0'  =>  '禁用',
    'Status 1'  =>  '正常',
    'Is_deleted'  =>  '是否删除'
];
