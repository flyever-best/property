<?php

namespace app\admin\model;

use app\common\service\ModelService;

class HousesInfo extends ModelService
{
    // 表名
    protected $table = 'houses_info';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'type_text',
        'cm_message_id_text',
        'bd_message_id_text',
        'unit_message_id_text',
        'houses_id_text',
        'status_text',
        'reg_status_text'
    ];

    /**
     * 获取HousesInfo列表数据
     * @param int   $page
     * @param int   $limit
     * @param array $search
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getHousesInfoList($page = 1, $limit = 10, $search = []) {
        $where = [['is_deleted', '=', 0]];

        //搜索条件
        foreach ($search as $key => $value) {
            if (!empty($value)) {
                switch ($key) {
                    case 'username':
                        $member_id = model('member')->where([['username', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'nickname':
                        $member_id = model('member')->where([['nickname', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'createtime':
                        $value_list = explode(" - ", $value);
                        $where[] = [$key, 'BETWEEN', ["{$value_list[0]} 00:00:00", "{$value_list[1]} 23:59:59"]];
                        break;
                    default:
                        !empty($value) && $where[] = [$key, 'LIKE', '%' . $value . '%'];
                }
            }
        }

        $count = self::where($where)->count();
        $data = self::where($where)->page($page, $limit)->order(['id' => 'desc'])->select();
            empty($data) ? $msg = '暂无数据！' : $msg = '查询成功！';
        $info = [
            'limit'        => $limit,
            'page_current' => $page,
            'page_sum'     => ceil($count / $limit),
        ];
        $list = [
            'code'  => 0,
            'msg'   => $msg,
            'count' => $count,
            'info'  => $info,
            'data'  => $data,
        ];
        return $list;
    }
    

    
    public function getTypeList()
    {
        return ['1' => __('Type 1'),'2' => __('Type 2'),'3' => __('Type 3')];
    }     


    public function getCmMessageIdList()
    {
        if(cache('cm_message')){
            return cache('cm_message');
        }else{
            $cm_message =  db('cm_message')->where(array('status'=>1,'is_deleted'=>0))->column('id,name');
            cache('cm_message',$cm_message);
            return $cm_message;
        }
    }


    public function getBdMessageIdList()
    {
        if(cache('bd_message')){
            return cache('bd_message');
        }else{
            $bd_message =  db('bd_message')->where(array('status'=>1,'is_deleted'=>0))->column('id,name');
            cache('bd_message',$bd_message);
            return $bd_message;
        }
    }


    public function getUnitMessageIdList()
    {
        if(cache('unit_message')){
            return cache('unit_message');
        }else{
            $unit_message =  db('unit_message')->where(array('status'=>1,'is_deleted'=>0))->column('id,unit');
            cache('unit_message',$unit_message);
            return $unit_message;
        }
    }


    public function getHousesIdList()
    {
        if(cache('houses')){
            return cache('houses');
        }else{
            $houses =  db('houses')->where(array('status'=>1,'is_deleted'=>0))->column('id,name');
            cache('houses',$houses);
            return $houses;
        }
    }

    public function getStatusList()
    {
        return ['0' => __('Status 0'),'1' => __('Status 1')];
    }     

    public function getRegStatusList()
    {
        return ['0' => __('Reg_status 0'),'1' => __('Reg_status 1')];
    }     


    public function getTypeTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['type'];
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getCmMessageIdTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['cm_message_id'];
        $list = $this->getCmMessageIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getBdMessageIdTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['bd_message_id'];
        $list = $this->getBdMessageIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getUnitMessageIdTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['unit_message_id'];
        $list = $this->getUnitMessageIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getHousesIdTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['houses_id'];
        $list = $this->getHousesIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['status'];
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getRegStatusTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['reg_status'];
        $list = $this->getRegStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
