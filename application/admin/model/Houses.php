<?php

namespace app\admin\model;

use app\common\service\ModelService;

class Houses extends ModelService
{
    // 表名
    protected $table = 'houses';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'cm_message_id_text',
        'numbers_text',
        'status_text',
        'bd_message_id_text',
        'unit_message_id_text'
    ];

    /**
     * 获取Houses列表数据
     * @param int   $page
     * @param int   $limit
     * @param array $search
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getHousesList($page = 1, $limit = 10, $search = [],  $cmId=0, $unitId=0, $bdId=0, $periodId=0) {
        $where = [];
        if($cmId == 0 && $unitId == 0 && $bdId == 0 && $periodId == 0) {
            $where = [['is_deleted', '=', 0]];
        } else {
            array_push($where, ['period_message_id', '=', $periodId], ['bd_message_id', '=', $bdId], ['unit_message_id', '=', $unitId], ['cm_message_id', '=', $cmId]);
        }

        //搜索条件
        foreach ($search as $key => $value) {
            if (!empty($value)) {
                switch ($key) {
                    case 'username':
                        $member_id = model('member')->where([['username', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'nickname':
                        $member_id = model('member')->where([['nickname', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'createtime':
                        $value_list = explode(" - ", $value);
                        $where[] = [$key, 'BETWEEN', ["{$value_list[0]} 00:00:00", "{$value_list[1]} 23:59:59"]];
                        break;
                    default:
                        !empty($value) && $where[] = [$key, 'LIKE', '%' . $value . '%'];
                }
            }
        }

        $count = self::where($where)->count();
        $data = self::where($where)->page($page, $limit)->order(['id' => 'desc'])->select()->toArray();
            empty($data) ? $msg = '暂无数据！' : $msg = '查询成功！';
        $info = [
            'limit'        => $limit,
            'page_current' => $page,
            'page_sum'     => ceil($count / $limit),
        ];
        $list = [
            'code'  => 0,
            'msg'   => $msg,
            'count' => $count,
            'info'  => $info,
            'data'  => $data,
        ];
        return $list;
    }
    

    

    public function getCmMessageIdList()
    {
        if(cache('cm_message')){
            return cache('cm_message');
        }else{
            $cm_message =  db('cm_message')->where(array('status'=>0,'is_deleted'=>0))->column('id,name');
            cache('cm_message',$cm_message);
            return $cm_message;
        }
    }

    public function getNumbersList()
    {
        return ['1' => '1户型','2' => '2户型','3' => '3户型','4' => '4户型','5' => '5户型'];
    }     

    public function getStatusList()
    {
//        return ['0' => __('Status 0'),'1' => __('Status 1')];
        return ['0' => '禁用','1' => '正常'];
    }


    public function getCmMessageIdTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['cm_message_id'];
        $list = $this->getCmMessageIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getNumbersTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['numbers'];
        $list = $this->getNumbersList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['status'];
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getBdMessageIdTextAttr($value, $data)
    {
        if(cache('bd_message')) {
            return cache('bd_message');
        } else {
            $info = db('bd_message')->where(['status' => 1, 'is_deleted' => 0, 'id' => $data['bd_message_id']])->value('name');
            cache('bd_message', $info);
            return $info;
        }
    }

    public function getUnitMessageIdTextAttr($value, $data)
    {
        if(cache('unit_message')) {
            return cache('unit_message');
        } else {
            $info = db('unit_message')->where(['status' => 1, 'is_deleted' => 0, 'id' => $data['unit_message_id']])->value('unit');
            cache('unit_message', $info);
            return $info;
        }
    }



}
