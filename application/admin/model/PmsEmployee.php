<?php

namespace app\admin\model;

use app\common\service\ModelService;

class PmsEmployee extends ModelService
{
    // 表名
    protected $table = 'pms_employee';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'pms_auth_id_text',
        'pms_ar_id_text',
        'status_text'
    ];

    /**
     * 获取PmsEmployee列表数据
     * @param int   $page
     * @param int   $limit
     * @param array $search
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getPmsEmployeeList($page = 1, $limit = 10, $search = []) {
        $where = [['is_deleted', '=', 0]];

        //搜索条件
        foreach ($search as $key => $value) {
            if (!empty($value)) {
                switch ($key) {
                    case 'username':
                        $member_id = model('member')->where([['username', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'nickname':
                        $member_id = model('member')->where([['nickname', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'createtime':
                        $value_list = explode(" - ", $value);
                        $where[] = [$key, 'BETWEEN', ["{$value_list[0]} 00:00:00", "{$value_list[1]} 23:59:59"]];
                        break;
                    default:
                        !empty($value) && $where[] = [$key, 'LIKE', '%' . $value . '%'];
                }
            }
        }

        $count = self::where($where)->count();
        $data = self::where($where)->page($page, $limit)->order(['id' => 'desc'])->select();
            empty($data) ? $msg = '暂无数据！' : $msg = '查询成功！';
        $info = [
            'limit'        => $limit,
            'page_current' => $page,
            'page_sum'     => ceil($count / $limit),
        ];
        $list = [
            'code'  => 0,
            'msg'   => $msg,
            'count' => $count,
            'info'  => $info,
            'data'  => $data,
        ];
        return $list;
    }
    

    

    public function getPmsAuthIdList()
    {
        if(cache('pms_auth')){
            return cache('pms_auth');
        }else{
            $pms_auth =  db('pms_auth')->where(array('status'=>0,'is_deleted'=>0))->column('id,title');
            cache('pms_auth',$pms_auth);
            return $pms_auth;
        }
    }


    public function getPmsArIdList()
    {
        if(cache('pms_ar')){
            return cache('pms_ar');
        }else{
            $pms_ar =  db('pms_ar')->where(array('status'=>1,'is_deleted'=>0))->column('id,name');
            cache('pms_ar',$pms_ar);
            return $pms_ar;
        }
    }

    public function getStatusList()
    {
        return ['0' => __('Status 0'),'1' => __('Status 1')];
    }     


    public function getPmsAuthIdTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['pms_auth_id'];
        $list = $this->getPmsAuthIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getPmsArIdTextAttr($value, $data)
    {
        $value = $value ? $value : $data['pms_ar_id'];
        $list = $this->getPmsArIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['status'];
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
