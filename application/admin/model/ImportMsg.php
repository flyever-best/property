<?php

namespace app\admin\model;

use app\common\service\ModelService;

class ImportMsg extends ModelService
{
    // 表名
    protected $table = 'import_msg';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'type_text',
        'labels_id_text',
        'status_text',
        'cm_text',
        'bd_text',
        'period_text',
        'unit_text'
    ];

    /**
     * 获取ImportMsg列表数据
     * @param int   $page
     * @param int   $limit
     * @param array $search
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function getImportMsgList($page = 1, $limit = 10, $search = [],  $cmId=0, $unitId=0, $bdId=0, $periodId=0) {
        $where = [];        
        if($cmId == 0 && $unitId == 0 && $bdId == 0 && $periodId == 0) {
            $where = [['is_deleted', '=', 0]];
        } else {
            $bdMsg = model('BdMessage')->where('id', $bdId)->value('unit');
            $periodMsg = model('PeriodMessage')->where('id', $periodId)->value('unit');
            $unitMsg = model('UnitMessage')->where('id', $unitId)->value('info');
            array_push($where, ['period_num', '=', $periodMsg], ['bd_num', '=', $bdMsg], ['unit_num', '=', $unitMsg]);
        }
        //搜索条件
        foreach ($search as $key => $value) {
            if (!empty($value)) {
                switch ($key) {
                    case 'username':
                        $member_id = model('member')->where([['username', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'nickname':
                        $member_id = model('member')->where([['nickname', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'createtime':
                        $value_list = explode(" - ", $value);
                        $where[] = [$key, 'BETWEEN', ["{$value_list[0]} 00:00:00", "{$value_list[1]} 23:59:59"]];
                        break;
                    default:
                        !empty($value) && $where[] = [$key, 'LIKE', '%' . $value . '%'];
                }
            }
        }

        $count = self::where($where)->count();
        $data = self::where($where)->page($page, $limit)->order(['id' => 'desc'])->select();
            empty($data) ? $msg = '暂无数据！' : $msg = '查询成功！';
        $info = [
            'limit'        => $limit,
            'page_current' => $page,
            'page_sum'     => ceil($count / $limit),
        ];
        $list = [
            'code'  => 0,
            'msg'   => $msg,
            'count' => $count,
            'info'  => $info,
            'data'  => $data,
        ];
        return $list;
    }
    

    
    public function getTypeList()
    {
        return ['1' => __('Type 1'),'2' => __('Type 2')];
    }     


    public function getLabelsIdList()
    {
        if(cache('labels')){
            return cache('labels');
        }else{
            $labels =  db('labels')->where(array('status'=>0,'is_deleted'=>0))->column('id,name');
            cache('labels',$labels);
            return $labels;
        }
    }

    public function getStatusList()
    {
        return ['0' => __('Status 0'),'1' => __('Status 1')];
    }     


    public function getTypeTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['type'];
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getLabelsIdTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['labels_id'];
        $list = $this->getLabelsIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['status'];
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getCmTextAttr($value, $data)
    {        
        return model('CmMessage')->where('id', $data['cm_num'])->value('name');
    }

    public function getBdTextAttr($value, $data)
    {        
        return model('BdMessage')->where('unit', $data['bd_num'])->value('name');
    }
    public function getPeriodTextAttr($value, $data)
    {        
        return model('PeriodMessage')->where('id', $data['period_num'])->value('name');
    }
    public function getUnitTextAttr($value, $data)
    {   
        $bdId = model('BdMessage')->where([['cm_message_id', '=', $data['cm_num']], ['unit', '=', $data['bd_num']]])->value('id');
        return model('UnitMessage')->where([['info', '=', $data['unit_num']], ['bd_message_id', '=', $bdId]])->value('unit');
    }






}
