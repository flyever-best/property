<?php

namespace app\admin\model;

use app\common\service\ModelService;

class DoorDevice extends ModelService
{
    // 表名
    protected $table = 'door_device';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'effect_type_text',
        'canphone_text'
    ];

    /**
     * 获取DoorDevice列表数据
     * @param int   $page
     * @param int   $limit
     * @param array $search
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getDoorDeviceList($page = 1, $limit = 10, $search = []) {
        $where = [['is_deleted', '=', 0]];

        //搜索条件
        foreach ($search as $key => $value) {
            if (!empty($value)) {
                switch ($key) {
                    case 'username':
                        $member_id = model('member')->where([['username', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'nickname':
                        $member_id = model('member')->where([['nickname', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'createtime':
                        $value_list = explode(" - ", $value);
                        $where[] = [$key, 'BETWEEN', ["{$value_list[0]} 00:00:00", "{$value_list[1]} 23:59:59"]];
                        break;
                    default:
                        !empty($value) && $where[] = [$key, 'LIKE', '%' . $value . '%'];
                }
            }
        }

        $count = self::where($where)->count();
        $data = self::where($where)->page($page, $limit)->order(['id' => 'desc'])->select();
            empty($data) ? $msg = '暂无数据！' : $msg = '查询成功！';
        $info = [
            'limit'        => $limit,
            'page_current' => $page,
            'page_sum'     => ceil($count / $limit),
        ];
        $list = [
            'code'  => 0,
            'msg'   => $msg,
            'count' => $count,
            'info'  => $info,
            'data'  => $data,
        ];
        return $list;
    }
    

    
    public function getEffectTypeList()
    {
        return ['1' => __('Effect_type 1'),'2' => __('Effect_type 2'),'3' => __('Effect_type 3'),'4' => __('Effect_type 4'),'5' => __('Effect_type 5')];
    }     

    public function getCanphoneList()
    {
        return ['1' => __('Canphone 1'),'2' => __('Canphone 2')];
    }     


    public function getEffectTypeTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['effect_type'];
        $list = $this->getEffectTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getCanphoneTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['canphone'];
        $list = $this->getCanphoneList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
