<?php

namespace app\admin\model;

use app\common\service\ModelService;

class CmMessage extends ModelService
{
    // 表名
    protected $table = 'cm_message';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'pms_ar_id_text',
        'status_text',
        'city_text',
        'county_text',
        'area_text'
    ];

    /**
     * 获取CmMessage列表数据
     * @param int   $page
     * @param int   $limit
     * @param array $search
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getCmMessageList($page = 1, $limit = 10, $search = [], $auth) {
        $where = [['is_deleted', '=', 0]];
        if($auth !== null && count($auth) > 0) array_push($where, ['pms_ar_id', 'IN', $auth]);
        //搜索条件
        foreach ($search as $key => $value) {
            if (!empty($value)) {
                switch ($key) {
                    case 'username':
                        $member_id = model('member')->where([['username', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'nickname':
                        $member_id = model('member')->where([['nickname', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'createtime':
                        $value_list = explode(" - ", $value);
                        $where[] = [$key, 'BETWEEN', ["{$value_list[0]} 00:00:00", "{$value_list[1]} 23:59:59"]];
                        break;
                    default:
                        !empty($value) && $where[] = [$key, 'LIKE', '%' . $value . '%'];
                }
            }
        }

        $count = self::where($where)->count();
        $data = self::where($where)->page($page, $limit)->order(['id' => 'desc'])->select();
            empty($data) ? $msg = '暂无数据！' : $msg = '查询成功！';
        $info = [
            'limit'        => $limit,
            'page_current' => $page,
            'page_sum'     => ceil($count / $limit),
        ];
        $list = [
            'code'  => 0,
            'msg'   => $msg,
            'count' => $count,
            'info'  => $info,
            'data'  => $data,
        ];
        return $list;
    }
    

    

    public function getPmsArIdList()
    {
        if(cache('pms_ar')){
            return cache('pms_ar');
        }else{
            $pms_ar =  db('pms_ar')->where(array('status'=>1,'is_deleted'=>0))->column('id,name');
            cache('pms_ar',$pms_ar);
            return $pms_ar;
        }
    }

    public function getCityList()
    {
        if(cache('city')){
            return cache('city');
        }else{
            $city =  db('district')->column('id,district');
            cache('city',$city);
            return $city;
        }
    }

    public function getCountyList()
    {
        if(cache('county')){
            return cache('county');
        }else{
            $county =  db('district')->column('id,district');
            cache('county',$county);
            return $county;
        }
    }

    public function getAreaList()
    {
        if(cache('area')){
            return cache('area');
        }else{
            $area =  db('district')->column('id,district');
            cache('area',$area);
            return $area;
        }
    }

    public function getStatusList()
    {
        return ['0' => __('Status 0'),'1' => __('Status 1')];
    }     


    public function getPmsArIdTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['pms_ar_id'];
        $list = $this->getPmsArIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['status'];
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getCityTextAttr($value, $data)
    {
        $value = $value ? $value : $data['city'];
        $list = $this->getCityList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getCountyTextAttr($value, $data)
    {
        $value = $value ? $value : $data['county'];
        $list = $this->getCountyList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getAreaTextAttr($value, $data)
    {
        $value = $value ? $value : $data['area'];
        $list = $this->getAreaList();
        return isset($list[$value]) ? $list[$value] : '';
    }



}
