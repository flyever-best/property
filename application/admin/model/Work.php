<?php

namespace app\admin\model;

use app\common\service\ModelService;

class Work extends ModelService
{
    // 表名
    protected $table = 'work';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'timestamp';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'houses_info_id_text',
        'progress_text',
        'type_text'
    ];

    /**
     * 获取Work列表数据
     * @param int   $page
     * @param int   $limit
     * @param array $search
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getWorkList($page = 1, $limit = 10, $search = []) {
        $where = [['is_deleted', '=', 0]];

        //搜索条件
        foreach ($search as $key => $value) {
            if (!empty($value)) {
                switch ($key) {
                    case 'username':
                        $member_id = model('member')->where([['username', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'nickname':
                        $member_id = model('member')->where([['nickname', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'createtime':
                        $value_list = explode(" - ", $value);
                        $where[] = [$key, 'BETWEEN', ["{$value_list[0]} 00:00:00", "{$value_list[1]} 23:59:59"]];
                        break;
                    default:
                        !empty($value) && $where[] = [$key, 'LIKE', '%' . $value . '%'];
                }
            }
        }

        $count = self::where($where)->count();
        $data = self::where($where)->page($page, $limit)->order(['id' => 'desc'])->select();
            empty($data) ? $msg = '暂无数据！' : $msg = '查询成功！';
        $info = [
            'limit'        => $limit,
            'page_current' => $page,
            'page_sum'     => ceil($count / $limit),
        ];
        $list = [
            'code'  => 0,
            'msg'   => $msg,
            'count' => $count,
            'info'  => $info,
            'data'  => $data,
        ];
        return $list;
    }
    

    

    public function getHousesInfoIdList()
    {
        if(cache('houses_info')){
            return cache('houses_info');
        }else{
            $houses_info =  db('houses_info')->where(array('status'=>0,'is_deleted'=>0))->column('id,name');
            cache('houses_info',$houses_info);
            return $houses_info;
        }
    }

    public function getProgressList()
    {
        return ['1' => __('Progress 1'),'2' => __('Progress 2'),'3' => __('Progress 3')];
    }     

    public function getTypeList()
    {
        return ['1' => __('Type 1'),'2' => __('Type 2')];
    }     


    public function getHousesInfoIdTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['houses_info_id'];
        $list = $this->getHousesInfoIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getProgressTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['progress'];
        $list = $this->getProgressList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getTypeTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['type'];
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
