<?php

namespace app\admin\model;

use app\common\service\ModelService;

class FeeType extends ModelService
{
    // 表名
    protected $table = 'fee_type';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'pms_ar_id_text',
        'cm_message_id_text',
        'fee_items_id_text',
        'type_text'
    ];

    /**
     * 获取FeeType列表数据
     * @param int   $page
     * @param int   $limit
     * @param array $search
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getFeeTypeList($page = 1, $limit = 10, $search = []) {
        $where = [['is_deleted', '=', 0]];

        //搜索条件
        foreach ($search as $key => $value) {
            if (!empty($value)) {
                switch ($key) {
                    case 'username':
                        $member_id = model('member')->where([['username', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'nickname':
                        $member_id = model('member')->where([['nickname', 'LIKE', "%{$value}%"]])->column('id');
                        $where[] = ['member_id', 'IN', $member_id];
                        break;
                    case 'createtime':
                        $value_list = explode(" - ", $value);
                        $where[] = [$key, 'BETWEEN', ["{$value_list[0]} 00:00:00", "{$value_list[1]} 23:59:59"]];
                        break;
                    default:
                        !empty($value) && $where[] = [$key, 'LIKE', '%' . $value . '%'];
                }
            }
        }

        $count = self::where($where)->count();
        $data = self::where($where)->page($page, $limit)->order(['id' => 'desc'])->select();
            empty($data) ? $msg = '暂无数据！' : $msg = '查询成功！';
        $info = [
            'limit'        => $limit,
            'page_current' => $page,
            'page_sum'     => ceil($count / $limit),
        ];
        $list = [
            'code'  => 0,
            'msg'   => $msg,
            'count' => $count,
            'info'  => $info,
            'data'  => $data,
        ];
        return $list;
    }
    

    

    public function getPmsArIdList()
    {
        if(cache('pms_ar')){
            return cache('pms_ar');
        }else{
            $pms_ar =  db('pms_ar')->where(array('status'=>0,'is_deleted'=>0))->column('id,name');
            cache('pms_ar',$pms_ar);
            return $pms_ar;
        }
    }


    public function getCmMessageIdList()
    {
        if(cache('cm_message')){
            return cache('cm_message');
        }else{
            $cm_message =  db('cm_message')->where(array('status'=>0,'is_deleted'=>0))->column('id,name');
            cache('cm_message',$cm_message);
            return $cm_message;
        }
    }


    public function getFeeItemsIdList()
    {
        if(cache('fee_items')){
            return cache('fee_items');
        }else{
            $fee_items =  db('fee_items')->where(array('is_deleted'=>0,'type'=>1))->column('id,name');
            cache('fee_items',$fee_items);
            return $fee_items;
        }
    }
    public function getFeeItemsIdGoList(){
        return retGoValue();
    }
    public function getTypeList()
    {
        return ['1' => __('Type 1'),'2' => __('Type 2')];
    }     


    public function getPmsArIdTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['pms_ar_id'];
        $list = $this->getPmsArIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getCmMessageIdTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['cm_message_id'];
        $list = $this->getCmMessageIdList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getFeeItemsIdTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['fee_items_id'];
        if($data['type']=='1'){
            $list = $this->getFeeItemsIdList();
        }else{
            $list = $this->getFeeItemsIdGoList();
        }
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getTypeTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['type'];
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
