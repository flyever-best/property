<?php
//后台公共文件


if (!function_exists('__log')) {

    /**
     * 写入系统日志
     * @param $data 数据
     * @param $type 日志类型
     */
    function __log($data, $type) {
        app('Log')::write(json_encode($data, JSON_UNESCAPED_UNICODE), $type);
    }
}

if (!function_exists('replace_menu_title')) {

    /**
     * 格式化菜单名称进行输出
     * @param $var 变量名
     * @param int $number 循环次数
     * @return string
     */
    function replace_menu_title($var, $number = 1) {
        $prefix = '';
        for ($i = 1; $i < $number; $i++) {
            $prefix .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        }
        return $prefix . $var;
    }
}

if (!function_exists('exportExcel')) {
    /**
     * excel 表格导出
     * @param $expTitle     表头名
     * @param $expCellName  表字段数据
     * @param $expTableData 表格数据
     * @param $download     是否保存下载
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Reader_Exception
     */
    function exportExcel($expTitle, $expCellName, $expTableData, $download = false){
        $fileName = $expTitle.date('_Y年m月d日H点i分s秒');//文件名
        $cellNum = count($expCellName);//数据总列数
        $dataNum = count($expTableData);//数据总行数
        // 实例化一个Excel文档
        $objPHPExcel = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $cellName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

        $objPHPExcel->getActiveSheet(0)->mergeCells('A1:'.$cellName[$cellNum-1].'1');//合并单元格
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $expTitle);//第一表头
        // 第一/二表头数据居中
        $objPHPExcel->getActiveSheet(0)->getStyle('A1:'.$cellName[$cellNum-1].'1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet(0)->getStyle('A2:'.$cellName[$cellNum-1].'2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        // 第一/二表头字体加粗
        $objPHPExcel->getActiveSheet(0)->getStyle('A1:'.$cellName[$cellNum-1].'1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet(0)->getStyle('A2:'.$cellName[$cellNum-1].'2')->getFont()->setBold(true);
        for($i=0;$i<$cellNum;$i++){
            // 表头赋值
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i].'2', $expCellName[$i][1]);
            // 设置列自动宽度
            // $objPHPExcel->getActiveSheet(0)->getColumnDimension($cellName[$i])->setAutoSize(true);
            // 设置列宽度
            $objPHPExcel->getActiveSheet(0)->getColumnDimension($cellName[$i])->setWidth(14);
        }
        // 循环导出数据
        for($i=0;$i<$dataNum;$i++){
            for($j=0;$j<$cellNum;$j++){
                $objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j].($i+3), $expTableData[$i][$expCellName[$j][0]]);
            }
        }
        // 声明下载类型
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        // 下载形式及文件名
        // attachment新窗口打印 - inline本窗口浏览
        header("Content-Disposition: attachment;filename=".$fileName.".xls");
        // 禁止缓存
        header('Cache-Control: max-age=0');
        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, 'Xls');
        if($download === true){
            // 保存到服务器本地
            $path = ROOT_PATH.'public/static/excel/';
            if(!is_dir($path)){
                mkdir($path, 0777, true);
            }
            $file = $expTitle.'_'.date('Y年m月d日H点i分s秒').'.xls';
            $filePath = $path.$file;
            $objWriter->save($filePath);
            return HomeDomain().'/static/excel/'.$file;
            // return request()->domain().'/static/excel/'.$file;
        }else{
            $objWriter->save('php://output');
            exit;
        }
    }
    function exportExcel2($expTitle, $expCellName, $expTableData){
        $xlsTitle = iconv('utf-8', 'gb2312', $expTitle);//文件名称
        $fileName = $expTitle.date('_Y年m月d日H点i分s秒');//or $xlsTitle 文件名称可根据自己情况设定
        $cellNum = count($expCellName);
        $dataNum = count($expTableData);

        $objPHPExcel = new PHPExcel();
        $cellName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');

        $objPHPExcel->getActiveSheet(0)->mergeCells('A1:'.$cellName[$cellNum-1].'1');//合并单元格
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $expTitle.'  导出时间:'.date('Y-m-d H:i:s'));
        for($i=0;$i<$cellNum;$i++){
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i].'2', $expCellName[$i][1]);
        }
        // Miscellaneous glyphs, UTF-8
        for($i=0;$i<$dataNum;$i++){
            for($j=0;$j<$cellNum;$j++){
                $objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j].($i+3), $expTableData[$i][$expCellName[$j][0]]);
            }
        }

        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name='.$xlsTitle.'.xls');
        header('Content-Disposition:attachment;filename='.$expTitle.$fileName.'.xls');//attachment新窗口打印inline本窗口打印
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }
}