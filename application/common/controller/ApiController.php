<?php

namespace app\common\controller;


use app\common\service\AuthService;
use app\common\service\QiniuService;
use think\Controller;
use think\Db;
use think\facade\Cache;


class ApiController extends Controller {

    /**
     * 构造函数
     * BlogController constructor.
     */
    protected $userinfo=[];
    protected $token='';
    public function __construct() {
        parent::__construct();
        $token=$this->request->param('token');
        if($token){
            $res = checkToken($token);
            // $res['data']['cm_message_id']='1,2';
            // updateUserInfoByToken($token,$res);
            // $res = checkToken($token);
            if ($res['code'] != 0) {
                echo json_encode(['code' => 10010, 'msg' => 'token错误', 'data' => []]);die;
            }
            // dump($res);die;
            $userinfo=$res['data'];
            $this->userinfo=$userinfo;
            $this->token=$token;
        }else {
            echo json_encode(['code' => 1, 'msg' => '用户未登录', 'data' => []]);die;
        }
    }

}