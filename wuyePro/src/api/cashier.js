import { axios } from '@/utils/request'

// 搜索房屋
export function search_info (parameter) {
    return axios({
        url: '/cashier/search_info',
        method: 'post',
        data: parameter
    })
}

// 房屋信息

export function house_info (parameter) {
    return axios({
        url: '/cashier/house_info',
        method: 'post',
        data: parameter
    })
}
//获取欠费记录
export function house_info_no_pay (parameter) {
    return axios({
        url: '/cashier/get_no_pay_info',
        method: 'post',
        data: parameter
    })
}
export function pay_det (parameter) {
    return axios({
        url: '/cashier/pay_det',
        method: 'post',
        data: parameter
    })
}
// 获取计算公式
export function getPayFormulaList(parameter) {
    return axios({
        url: '/cashier/get_pay_formula_list',
        method: 'get',
        params:parameter
    })
}

// 保存自定义公式
export function saveTollFormula(parameter) {
    return axios({
        url: '/cashier/save_toll_formula',
        method: 'post',
        data: parameter
    })
}

// 获取自定义公式列表
export function getTollFormula(parameter) {
    return axios({
        url: '/cashier/get_toll_formula',
        method: 'post',
        data: parameter
    })
}

// 获取未缴费账单
export function getNoPayMentList(parameter) {
    return axios({
        url: '/cashier/get_no_pay_list',
        method: 'post',
        data: parameter
    })
}

// 获取已缴费账单
export function getPayMentList(parameter) {
    return axios({
        url: '/cashier/get_pay_list',
        method: 'post',
        data: parameter
    })
}

// 获取预存款列表
export function getDespositList(parameter) {
    return axios({
        url: '/cashier/get_desposit_list',
        method: 'post',
        data: parameter
    })
}


// 获取预存款明细
export function getDespositStore(parameter) {
    return axios({
        url: '/cashier/get_desposit_store',
        method: 'post',
        data: parameter
    })
}

// 获取收费项目
export function getTollProject(parameter) {
    return axios({
        url: '/houses/get_toll_project',
        method: 'post',
        data: parameter
    })
}

// 新增收费项目
export function saveTollProject(parameter) {
    return axios({
        url: '/cashier/save_toll_project',
        method: 'post',
        data: parameter
    })
}

// 删除收费项目
export function delTollProject(parameter) {
    return axios({
        url: '/cashier/del_toll_project',
        method: 'post',
        data: parameter
    })
}

// 保存收费项目详情
export function saveTollDetail(parameter) {
    return axios({
        url: '/cashier/save_toll_detail',
        method: 'post',
        data: parameter
    })
}


// 删除收费项目详情
export function delTollDetail(parameter) {
    return axios({
        url: '/cashier/del_toll_detail',
        method: 'post',
        data: parameter
    })
}

// 查看收费项目详情
export function lookTollDetail(parameter) {
    return axios({
        url: '/cashier/look_toll_detail',
        method: 'post',
        data: parameter
    })
}

// 更新收费项目详情
export function updateTollDetail(parameter) {
    return axios({
        url: '/cashier/update_toll_detail',
        method: 'post',
        data: parameter
    })
}

// 获取公式参数
export function getFormulaArg(parameter) {
    return axios({
        url: '/cashier/get_formula_arg',
        method: 'post',
        data: parameter
    })
}

// 计算金额
export function comFormulaPrice(parameter) {
    return axios({
        url: '/cashier/com_formula_price',
        method: 'post',
        data: parameter
    })
}

// 删除自定义公式
export function delFormula(parameter) {
    return axios({
        url: '/cashier/del_formula',
        method: 'post',
        data: parameter
    })
}

// 获取所有小区
export function getAllBdinfo(parameter) {
    return axios({
        url: '/cashier/get_all_bd_info',
        method: 'post',
        data: parameter
    })
}

// 搜索手机号住户
export function searchUser(parameter) {
    return axios({
        url: '/cashier/search_user',
        method: 'post',
        data: parameter
    })
}

// 预存款充值
export function saveDeposit(parameter) {
    return axios({
        url: '/cashier/save_deposit',
        method: 'post',
        data: parameter
    })
}
// 预存款退款
export function depositBack(parameter) {
    return axios({
        url: '/cashier/deposit_back',
        method: 'post',
        data: parameter
    })
}
export function payBack(parameter) {
    return axios({
        url: '/cashier/pay_back',
        method: 'post',
        data: parameter
    })
}
export function payStoreBack(parameter) {
    return axios({
        url: '/cashier/pay_store_back',
        method: 'post',
        data: parameter
    })
}
// 收银台房屋数据
export function getCashierHouse(parameter) {
    return axios({
        url: '/cashier/get_cashier_house',
        method: 'post',
        data: parameter
    })
}

// 历史欠费
export function getHisPayment(parameter) {
    return axios({
        url: '/cashier/get_his_payment',
        method: 'post',
        data: parameter
    })
}


// 收银台收费标准
export function getCashierFormula(parameter) {
    return axios({
        url: '/cashier/get_cashier_formula',
        method: 'post',
        data: parameter
    })
}
// 收银台收费标准two
export function getCashierFormulaTwo(parameter) {
    return axios({
        url: '/cashier/get_cashier_formula_two',
        method: 'post',
        data: parameter
    })
}
// 添加临时收费
export function saveCashierHouse(parameter) {
    return axios({
        url: '/cashier/save_cashier_house',
        method: 'post',
        data: parameter
    })
}


// 获取房屋住户
export function getHouseUser(parameter) {
    return axios({
        url: '/cashier/get_house_user',
        method: 'post',
        data: parameter
    })
}


// 房屋交款
export function housePay(parameter) {
    return axios({
        url: '/cashier/house_pay',
        method: 'post',
        data: parameter
    })
}
// 账单设置
export function setBilling(parameter) {
    return axios({
        url: '/cashier/set_billing',
        method: 'post',
        data: parameter
    })
}
// 获取账单设置
export function getBilling(parameter) {
    return axios({
        url: '/cashier/get_set_billing',
        method: 'post',
        data: parameter
    })
}
// 设置优惠，滞纳金
export function setNoPayMoney(parameter) {
    return axios({
        url: '/cashier/set_no_pay_money',
        method: 'post',
        data: parameter
    })
}

// 获取临时收费金额
export function getProjectFix(parameter) {
    return axios({
        url: '/cashier/get_project_fix',
        method: 'post',
        data: parameter
    })
}
// 获取模板列表
export function getTollModel(parameter) {
    return axios({
        url: '/cashier/get_toll_model',
        method: 'post',
        data: parameter
    })
}
// 保存模板
export function saveTollModel(parameter) {
    return axios({
        url: '/cashier/save_toll_model',
        method: 'post',
        data: parameter
    })
}
// 查看模板详情
export function selectTollModel(parameter) {
    return axios({
        url: '/cashier/select_toll_model',
        method: 'post',
        data: parameter
    })
}
// 更新模板详情
export function updateTollModel(parameter) {
    return axios({
        url: '/cashier/update_toll_model',
        method: 'post',
        data: parameter
    })
}
// 删除缴费
export function moreDec(parameter) {
    return axios({
        url: '/cashier/pay_more_dec',
        method: 'post',
        data: parameter
    })
}
// 删除模板
export function delModel(parameter) {
    return axios({
        url: '/cashier/del_model',
        method: 'post',
        data: parameter
    })
}
export function moreBack(parameter) {
    return axios({
        url: '/cashier/pay_more_back',
        method: 'post',
        data: parameter
    })
}
// 删除缴费
export function moreNoDec(parameter) {
    return axios({
        url: '/cashier/no_pay_more_dec',
        method: 'post',
        data: parameter
    })
}
// 预存款打印模板
export function dep_det (parameter) {
    return axios({
        url: '/cashier/dep_det',
        method: 'post',
        data: parameter
    })
}
// 预存款导入
export function moneyIN(parameter) {
    return axios({
        url: '/cashier/des_in',
        method: 'post',
        data: parameter
    })
}
// 预存款套餐列表
export function getDesList (parameter) {
    return axios({
        url: '/cashier/get_des_list',
        method: 'post',
        data: parameter
    })
}
// 预存款类型列表
export function getZy (parameter) {
    return axios({
        url: '/cashier/get_zy',
        method: 'post',
        data: parameter
    })
}
// 保存预存款套餐
export function saveDep (parameter) {
    return axios({
        url: '/cashier/save_dep',
        method: 'post',
        data: parameter
    })
}
// 保存预存款类型
export function saveZy (parameter) {
    return axios({
        url: '/cashier/save_zy',
        method: 'post',
        data: parameter
    })
}
// 删除预存款套餐
export function delDep (parameter) {
    return axios({
        url: '/cashier/del_dep',
        method: 'post',
        data: parameter
    })
}
// 删除类型
export function delZy(parameter) {
    return axios({
        url: '/cashier/del_zy',
        method: 'post',
        data: parameter
    })
}