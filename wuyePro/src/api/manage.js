import { axios } from '@/utils/request'

const api = {
  user: '/user',
  role: '/role',
  service: '/service',
  permission: '/permission',
  permissionNoPager: '/permission/no-pager',
  orgTree: '/org/tree',
  getCType: '/fee/get_c_type'
}

export default api

export function getUserList (parameter) {
  return axios({
    url: api.user,
    method: 'get',
    params: parameter
  })
}

export function getRoleList (parameter) {
  return axios({
    url: api.role,
    method: 'get',
    params: parameter
  })
}

export function getServiceList (parameter) {
  return axios({
    url: api.service,
    method: 'get',
    params: parameter
  })
}

export function getPermissions (parameter) {
  return axios({
    url: api.permissionNoPager,
    method: 'get',
    params: parameter
  })
}

export function getOrgTree (parameter) {
  return axios({
    url: api.orgTree,
    method: 'get',
    params: parameter
  })
}

// id == 0 add     post
// id != 0 update  put
export function saveService (parameter) {
  return axios({
    url: api.service,
    method: parameter.id === 0 ? 'post' : 'put',
    data: parameter
  })
}
export function getCfeeType (parameter) {
  return axios({
    url: api.getCType,
    method: 'get',
    params: parameter
  })
}
export function getFeeTyepItem (parameter) {
  return axios({
    url: '/fee/get_type_item',
    method: 'get',
    params: parameter
  })
}
export function saveFeeType (parameter) {
  return axios({
    url: '/fee/save_fee_type',
    method: typeof parameter.saveData.id === 'undefined' ? 'post' : 'put',
    data: parameter
  })
}
export function delFeeTypeItem (parameter) {
  return axios({
    url: '/fee/del_fee_type',
    method: 'delete',
    data: parameter
  })
}
export function getDiscountList (parameter) {
  return axios({
    url: '/discount/get_list',
    method: 'get',
    params: parameter
  })
}
export function saveDiscount (parameter) {
  return axios({
    url: '/discount/save_item',
    method: typeof parameter.saveData.id === 'undefined' ? 'post' : 'put',
    data: parameter
  })
}
export function delDiscount (parameter) {
  return axios({
    url: '/discount/del_item',
    method: 'delete',
    data: parameter
  })
}
export function getTree (parameter) {
  return axios({
    url: '/fee/getTree',
    method: 'get',
    params: parameter
  })
}
export function makeOrder (parameter) {
  return axios({
    url: '/order/make_order',
    method: 'post',
    data: parameter
  })
}
export function searchFee(parameter){
  return axios({
    url: '/fee/search_fee',
    method: 'get',
    params: parameter
  })
}
export function getHouseInfo(parameter){
  return axios({
    url: '/fee/house_info',
    method: 'get',
    params: parameter
  })
}
export function getHouseFeeLists(parameter){
  return axios({
    url: '/fee/house_info_fee_list',
    method: 'get',
    params: parameter
  })
}
export function GetFeeOrerBase(parameter){
  return axios({
    url: '/fee/get_fee_order_base',
    method: 'get',
    params: parameter
  })
}
export function submitOrders(parameter){
  return axios({
    url: '/order/submit_order',
    method: 'post',
    data: parameter
  })
}

