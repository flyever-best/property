/* eslint-disable no-cond-assign,quotes,no-unused-vars */
import Vue from 'vue'
import { login, getInfo, logout } from '@/api/login'
import { ACCESS_TOKEN } from '@/store/mutation-types'
import { welcome } from '@/utils/util'

const user = {
  state: {
    token: '',
    name: '',
    welcome: '',
    avatar: '',
    roles: [],
    info: {},
    messageList: [],
    nowMg: '',
    pmsArId: '',
    nowMgName: ''
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, { name, welcome }) => {
      state.name = name
      state.welcome = welcome
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_INFO: (state, info) => {
      state.info = info
    },
    SET_MESSAGELIST: (state, info) => {
      state.messageList = info
    },
    SET_NOWMG: (state, info) => {
      state.nowMg = info
    },
    SET_NOWMGNAME: (state, info) => {
      state.nowMgName = info
    },
    SET_PMSARID: (state, info) => {
      state.pmsArId = info
    }
  },

  actions: {
    // 登录
    Login ({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        login(userInfo).then(response => {
          const result = response
          // eslint-disable-next-line eqeqeq
          if (result.code == 0) {
            Vue.ls.set(ACCESS_TOKEN, result.data.token, 7 * 24 * 60 * 60 * 1000)
            commit('SET_TOKEN', result.data.token)
            resolve(result)
          } else {
            reject(result.msg)
          }
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetInfo ({ commit }) {
      return new Promise((resolve, reject) => {
        // eslint-disable-next-line no-unused-vars
        const token = Vue.ls.get(ACCESS_TOKEN)
        getInfo(token).then(response => {
          const result = response
          if (result.code == 0) {
            commit('SET_ROLES', result.data.role)
            commit('SET_INFO', result.data)
            commit('SET_NAME', { name: result.data.name, welcome: welcome() })
            commit('SET_AVATAR', result.avatar)
            // commit('SET_MESSAGELIST', result.data.message_list)
            // const defmgId = Vue.ls.get('MG_ID_' + result.data.pms_ar_id)
            commit('SET_NOWMG', result.data.cm_message_id)
            // if (defmgId) {
            //   commit('SET_NOWMG', Vue.ls.get('MG_ID_' + result.data.pms_ar_id))
            //   commit('SET_NOWMGNAME', Vue.ls.get('MG_NAME_' + result.data.pms_ar_id))
            // } else {
            //   commit('SET_NOWMG', result.data.message_list[0].id)
            //   commit('SET_NOWMGNAME', result.data.message_list[0].name)
            //   Vue.ls.set('MG_ID_' + result.data.pms_ar_id, result.data.message_list[0].id)
            //   Vue.ls.set('MG_NAME_' + result.data.pms_ar_id, result.data.message_list[0].name)
            // }
            commit('SET_PMSARID', result.data.pms_ar_id)
            resolve(response)
          } else {
            reject(result.msg)
          }
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    Logout ({ commit, state }) {
      return new Promise((resolve) => {
        commit('SET_TOKEN', '')
        commit('SET_ROLES', [])
        Vue.ls.remove(ACCESS_TOKEN)

        logout(state.token).then(() => {
          resolve()
        }).catch(() => {
          resolve()
        })
      })
    },
    // 切换小区
    changeMg ({ commit, state }, ids) {
      return new Promise((resolve) => {
        commit('SET_NOWMG', ids)
        Vue.ls.set('MG_ID_' + state.pmsArId, ids)
        const msList = state.messageList
        let msgname = ''
        for (var i in msList) {
          if (msList[i].id === ids) {
            msgname = msList[i].name
          }
        }
        commit('SET_NOWMGNAME', msgname)
        Vue.ls.set('MG_NAME_' + state.pmsArId, msgname)
        resolve()
      })
    },
    // 设置小区id
    setPmsArId ({ commit, state }, id) {
      return new Promise((resolve) => {
        commit('SET_PMSARID', id)
        resolve()
      })
    },
    // 设置小区id
    setMsName ({ commit, state }, name) {
      return new Promise((resolve) => {
        commit('SET_NOWMGNAME', name)
        resolve()
      })
    }
  }
}

export default user
