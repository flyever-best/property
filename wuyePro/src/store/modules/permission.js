import { constantRouterMap,notFoundRouter } from '@/config/router.config'
import { RouteView, PageView, BlankLayout, BasicLayout } from '@/layouts'

const routerComponents = {
  BasicLayout,
  RouteView,
  BlankLayout,
  PageView,
  dataUserEditPwd:() => import('@/views/data/user/editPwd'), // 修改密码
  dashboardAnalysis:() => import('@/views/dashboard/Analysis'), // 数据可视化
  dataCommunityIndex:() => import('@/views/data/community/index'), // 小区列表
  dataCommunityOperation:() => import('@/views/data/community/operation'), // 添加小区
  dataCommunityExtend:() => import('@/views/data/community/extend'), // 小区其他信息
  dataBuildingIndex:() => import('@/views/data/building/index'), // 楼宇管理
  dataBuildingOperation:() => import('@/views/data/building/operation'), // 添加楼宇
  dataBuildingUnit:() => import('@/views/data/building/unit'), // 单元管理
  dataHousesIndex:() => import('@/views/data/houses/index'), // 房屋管理
  dataHousesOperation:() => import('@/views/data/houses/operation'), // 添加房屋
  dataHousesExtend:() => import('@/views/data/houses/extend'), // 房屋其他信息
  dataHousesDetail:() => import('@/views/data/houses/detail'), // 房屋详情
  dataHousesImport:() => import('@/views/data/houses/import'), // 导入房屋
  dataHousesBz:() => import('@/views/data/houses/bz'), // 房屋添加收费标准
  dataParkingIndex:() => import('@/views/data/parking/index'), // 车位管理
  dataParkingOperation:() => import('@/views/data/parking/operation'), // 添加车位
  dataParkingImport:() => import('@/views/data/parking/import'), // 导入车位
  dataParkingDetail:() => import('@/views/data/parking/detail'), // 车位详情
  dataParkingBz:() => import('@/views/data/parking/bz'), // 车位添加收费标准
  dataVehicleIndex:() => import('@/views/data/vehicle/index'), // 车辆管理
  dataVehicleOperation:() => import('@/views/data/vehicle/operation'), // 添加车辆
  dataVehicleExtend:() => import('@/views/data/vehicle/extend'), // 车辆其他信息
  dataVehicleImport:() => import('@/views/data/vehicle/import'), // 导入车辆
  dataVehicleDetail:() => import('@/views/data/vehicle/detail'), // 车辆详情
  dataVehicleBz:() => import('@/views/data/vehicle/bz'), // 车辆添加收费标准
  dataHouseholdIndexIndex:() => import('@/views/data/household/index/index'), // 住户管理
  dataHouseholdIndexOperation:() => import('@/views/data/household/index/operation'), // 添加住户
  dataHouseholdIndexImport:() => import('@/views/data/household/index/import'), // 住户导入
  dataHouseholdIndexDetail:() => import('@/views/data/household/index/detail'), // 住户详情
  dataHouseholdNonindexIndex:() => import('@/views/data/household/nonindex/index'), // 非住户管理
  dataHouseholdNonindexOperation:() => import('@/views/data/household/nonindex/operation'), // 添加非住户
  dataHouseholdNonindexDetail:() => import('@/views/data/household/nonindex/detail'), // 非住户详情
  dataLabelsIndex:() => import('@/views/data/labels/index'), // 数据标签
  dataLabelsBindList:() => import('@/views/data/labels/bindList'), // 绑定住户列表
  chargeCashierIndex:() => import('@/views/charge/cashier/index'), // 常规收费
  chargeCashierOthers:() => import('@/views/charge/cashier/others'), // 其他收费
  chargeCashierQuick:() => import('@/views/charge/cashier/quick'), // 临时停车费
  chargeCashierHouseDetail:() => import('@/views/charge/cashier/houseDetail'), // 收银台房屋详情
  chargeCashierCarDetail:() => import('@/views/charge/cashier/carDetail'), // 收银台车辆详情
  chargeCashierCarAreaDetail:() => import('@/views/charge/cashier/carAreaDetail'), // 收银台车位详情
  chargeCashierHouseUserDetail: () => import('@/views/charge/cashier/houseUserDetail'), // 收银台住户详情
  chargeCopyTableIndex: () => import('@/views/charge/copy_table/index'), // 抄表录入
  chargeCopyTableImport: () => import('@/views/charge/copy_table/import'), // 抄表录入导入
  chargeNotPayIndex: () => import('@/views/charge/not_pay/index'), // 未缴账单
  chargeBePayDetail: () => import('@/views/charge/be_pay/detail'), // 详情缴费
  chargeNotPayImport: () => import('@/views/charge/not_pay/import'), // 未缴账单导入
  chargeBePayIndex: () => import('@/views/charge/be_pay/index'), // 已缴账单
  chargeBePayImport: () => import('@/views/charge/be_pay/import'), // 已缴账单导入
  chargePaymentNoticeIndex: () => import('@/views/charge/payment_notice/index'), // 缴费通知
  chargePreDepositDetail: () => import('@/views/charge/pre_deposit/detail'), // 预存款详情
  chargePreDepositIndex: () => import('@/views/charge/pre_deposit/index'), // 预存款管理
  chargePreDepositIn: () => import('@/views/charge/pre_deposit/in'), // 导入预存款
  chargeTollIndex: () => import('@/views/charge/toll/index'), // 收费设置
  chargeTollTollAddFormula: () => import('@/views/charge/toll/tollAddFormula'), // 添加公式
  chargeTollTollProjectAdd: () => import('@/views/charge/toll/tollProjectAdd'), // 添加收费项目
  chargeTollTollDetailAdd: () => import('@/views/charge/toll/tollDetailAdd'), // 添加收费标准
  chargeTollTollAddModel: () => import('@/views/charge/toll/tollAddModel'), // 添加模板
  chargeStatisticsDailyIndex: () => import('@/views/statistics/daily/index'), // 业务收款台账
  purviewGroup: () => import('@/views/purview/group'), // 群组管理
  purviewRole: () => import('@/views/purview/role'), // 用户管理
}

function generator(routerMap, parent) {
  return routerMap.map(item => {
    const currentRouter = {
      // 路由地址 动态拼接生成如 /dashboard/workplace
      path: `${item.path}`,
      // 路由名称，建议唯一
      name: item.key || '',
      // 该路由对应页面的 组件
      component: routerComponents[item.component],
      // 是否强制显示为item
      hideChildrenInMenu:item.hideChildrenInMenu ? true : false,
      // 是否显示菜单
      hidden:item.hidden ? true : false,
      // meta: 页面标题, 菜单图标, 页面权限(供指令权限用，可去掉)
      meta: { title: item.title, icon: item.icon || undefined,hiddenHeaderContent:item.hiddenHeaderContent ? true : false,keepAlive:item.keepAlive ? true : false }
    }
    // 为了防止出现后端返回结果不规范，处理有可能出现拼接出两个 反斜杠
    currentRouter.path = currentRouter.path.replace('//', '/')
    // 重定向
    item.redirect && (currentRouter.redirect = item.redirect)
    // 是否有子菜单，并递归处理
    if (item.children && item.children.length > 0) {
      // Recursion
      currentRouter.children = generator(item.children, currentRouter)
    }
    return currentRouter
  })
}

const permission = {
  state: {
    routers: constantRouterMap,
    addRouters: []
  },
  mutations: {
    SET_ROUTERS: (state, routers) => {
      state.addRouters = routers
      state.routers = constantRouterMap.concat(routers)
    }
  },
  actions: {
    GenerateRoutes ({ commit }, data) {
      return new Promise(resolve => {
        console.log(data)
        const router = generator(data)
        router.push(notFoundRouter);
        commit('SET_ROUTERS', router)
        resolve()
      })
    }
  }
}

export default permission
