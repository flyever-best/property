import util from './commom_util';

class methods {
    // 私有方法
    constructor(){

    }

    /**
     * 时间戳格式化
     * @param date  '时间戳'
     * @param extra 'Y-m-d H:i:s'
     * @return '2019-03-19 18:23:01'
     */
    time_format(date = new Date().getTime(),extra = 'Y-m-d H:i:s') {
        const D = new Date(date);
        const time = {
            'Y': D.getFullYear(),
            'm': D.getMonth() + 1,
            'd': D.getDate(),
            'H': D.getHours(),
            'i': D.getMinutes(),
            's': D.getSeconds()
        };
        const key = extra.split(/\W/);
        let _date;
        for (const k of key) {
            time[k] = time[k] < 10 ? '0' + time[k] : time[k];
            _date = extra.replace(k, time[k]);
            extra = _date;
        }
        return _date;
    }

    /**
     * 判断是否数据是否为空
     * @param any '任何数据'
     * @return undefined, null ,'', NaN, [], {}, 0, false 为 true，其他为 false
     */
    isEmpty(any) {
        if (util.isTrueEmpty(any)) return true;
        if (util.isRegExp(any)) {
            return false;
        } else if (util.isDate(any)) {
            return false;
        } else if (util.isError(any)) {
            return false;
        } else if (util.isArray(any)) {
            return any.length === 0;
        } else if (util.isString(any)) {
            return any.length === 0;
        } else if (util.isNumber(any)) {
            return any === 0;
        } else if (util.isBoolean(any)) {
            return !any;
        } else if (util.isObject(any)) {
            for (const key in any) {
                return false && key;
            }
            return true;
        }
        return false;
    }

    /**
     * 判断是否为手机号
     * @param '手机号'
     * @return {boolean}
     */
    isMobile(str) {
        let mobileReg = /^[1][2-9][0-9]{9}$/;
        return mobileReg.test(str) ? true : false;
    }

    /**
     * 判断是否为护照
     * @param '护照'
     * @returns {boolean}
     */
    isPassport(str){
        let passportReg = /^1[45][0-9]{7}$|([P|p|S|s]\d{7}$)|([S|s|G|g]\d{8}$)|([Gg|Tt|Ss|Ll|Qq|Dd|Aa|Ff]\d{8}$)|([H|h|M|m]\d{8,10})$/;
        return passportReg.test(str) ? true : false;
    }

    /**
     * 判断是否为身份证号
     * @param '身份证号'
     * @returns {{msg: string, code: boolean}} msg->提示信息 code->true成功 false失败
     */
    isIdCard(code){
        let city = {11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江 ",31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北 ",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏 ",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外 "};
        let tip = "正确";
        let pass= true;

        if(!code || !/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i.test(code)){
            tip = "身份证号格式错误";
            pass = false;
        }else if(!city[code.substr(0,2)]){
            tip = "地址编码错误";
            pass = false;
        } else{
            //18位身份证需要验证最后一位校验位
            if(code.length == 18){
                code = code.split('');
                //∑(ai×Wi)(mod 11)
                //加权因子
                let factor = [ 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 ];
                //校验位
                let parity = [ 1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2 ];
                let sum = 0;
                let ai = 0;
                let wi = 0;
                for (let i = 0; i < 17; i++) {
                    ai = code[i];
                    wi = factor[i];
                    sum += ai * wi;
                }
                let last = parity[sum % 11];
                if(parity[sum % 11] != code[17]){
                    tip = "校验位错误";
                    pass = false;
                }
            }
        }
        return {code:pass,msg:tip};
    }

    /**
     * 获取指定日期是周几
     * @param time '时间戳'
     * @return 日 一 二 三 四 五 六
     */
    week(time = new Date().getTime()) {
        return `日一二三四五六`.charAt(new Date(time).getDay());
    }


}

export default new methods();
