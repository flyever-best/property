// eslint-disable-next-line
import { UserLayout, BasicLayout, RouteView, BlankLayout, PageView } from '@/layouts'
import { bxAnaalyse } from '@/core/icons'

const routerComponents = {
  RouteView,
  BlankLayout,
  PageView,
  dataUserEditPwd:() => import('@/views/data/user/editPwd'), // 修改密码
  dashboardAnalysis:() => import('@/views/dashboard/Analysis'), // 控制台
  dataCommunityIndex:() => import('@/views/data/community/index'), // 小区列表
  dataCommunityOperation:() => import('@/views/data/community/operation'), // 添加小区
  dataCommunityExtend:() => import('@/views/data/community/extend'), // 小区其他信息
  dataBuildingIndex:() => import('@/views/data/building/index'), // 楼宇管理
  dataBuildingOperation:() => import('@/views/data/building/operation'), // 添加楼宇
  dataBuildingUnit:() => import('@/views/data/building/unit'), // 单元管理
  dataHousesIndex:() => import('@/views/data/houses/index'), // 房屋管理
  dataHousesOperation:() => import('@/views/data/houses/operation'), // 添加房屋
  dataHousesExtend:() => import('@/views/data/houses/extend'), // 房屋其他信息
  dataHousesDetail:() => import('@/views/data/houses/detail'), // 房屋详情
  dataHousesImport:() => import('@/views/data/houses/import'), // 导入房屋
  dataHousesBz:() => import('@/views/data/houses/bz'), // 房屋添加收费标准
  dataParkingIndex:() => import('@/views/data/parking/index'), // 车位管理
  dataParkingOperation:() => import('@/views/data/parking/operation'), // 添加车位
  dataParkingImport:() => import('@/views/data/parking/import'), // 导入车位
  dataParkingDetail:() => import('@/views/data/parking/detail'), // 车位详情
  dataParkingBz:() => import('@/views/data/parking/bz'), // 车位添加收费标准
  dataVehicleIndex:() => import('@/views/data/vehicle/index'), // 车辆管理
  dataVehicleOperation:() => import('@/views/data/vehicle/operation'), // 添加车辆
  dataVehicleExtend:() => import('@/views/data/vehicle/extend'), // 车辆其他信息
  dataVehicleImport:() => import('@/views/data/vehicle/import'), // 导入车辆
  dataVehicleDetail:() => import('@/views/data/vehicle/detail'), // 车辆详情
  dataVehicleBz:() => import('@/views/data/vehicle/bz'), // 车辆添加收费标准
  dataHouseholdIndexIndex:() => import('@/views/data/household/index/index'), // 住户管理
  dataHouseholdIndexOperation:() => import('@/views/data/household/index/operation'), // 添加住户
  dataHouseholdIndexImport:() => import('@/views/data/household/index/import'), // 住户导入
  dataHouseholdIndexDetail:() => import('@/views/data/household/index/detail'), // 住户详情
  dataHouseholdNonindexIndex:() => import('@/views/data/household/nonindex/index'), // 非住户管理
  dataHouseholdNonindexOperation:() => import('@/views/data/household/nonindex/operation'), // 添加非住户
  dataHouseholdNonindexDetail:() => import('@/views/data/household/nonindex/detail'), // 非住户详情
  dataLabelsIndex:() => import('@/views/data/labels/index'), // 数据标签
  dataLabelsBindList:() => import('@/views/data/labels/bindList'), // 绑定住户列表
  chargeCashierIndex:() => import('@/views/charge/cashier/index'), // 常规收费
  chargeCashierOthers:() => import('@/views/charge/cashier/others'), // 其他收费
  chargeCashierQuick:() => import('@/views/charge/cashier/quick'), // 临时停车费
  chargeCashierHouseDetail:() => import('@/views/charge/cashier/houseDetail'), // 收银台房屋详情
  chargeCashierCarDetail:() => import('@/views/charge/cashier/carDetail'), // 收银台车辆详情
  chargeCashierCarAreaDetail:() => import('@/views/charge/cashier/carAreaDetail'), // 收银台车位详情
  chargeCashierHouseUserDetail: () => import('@/views/charge/cashier/houseUserDetail'), // 收银台住户详情
  chargeCopyTableIndex: () => import('@/views/charge/copy_table/index'), // 抄表录入
  chargeCopyTableImport: () => import('@/views/charge/copy_table/import'), // 抄表录入导入
  chargeNotPayIndex: () => import('@/views/charge/not_pay/index'), // 未缴账单
  chargeNotPayImport: () => import('@/views/charge/not_pay/import'), // 未缴账单导入
    chargeBePayDetail: () => import('@/views/charge/be_pay/detail'), // 详情缴费
  chargeBePayIndex: () => import('@/views/charge/be_pay/index'), // 已缴账单
  chargeBePayImport: () => import('@/views/charge/be_pay/import'), // 已缴账单导入
  chargePaymentNoticeIndex: () => import('@/views/charge/payment_notice/index'), // 缴费通知
  chargePreDepositDetail: () => import('@/views/charge/pre_deposit/detail'), // 预存款详情
  chargePreDepositIndex: () => import('@/views/charge/pre_deposit/index'), // 预存款管理
  chargePreDepositIn: () => import('@/views/charge/pre_deposit/in'), // 导入预存款
  chargeTollIndex: () => import('@/views/charge/toll/index'), // 收费设置
  chargeTollTollAddFormula: () => import('@/views/charge/toll/tollAddFormula'), // 添加公式
  chargeTollTollProjectAdd: () => import('@/views/charge/toll/tollProjectAdd'), // 添加收费项目
  chargeTollTollDetailAdd: () => import('@/views/charge/toll/tollDetailAdd'), // 添加收费标准
  chargeTollTollAddModel: () => import('@/views/charge/toll/tollAddModel'), // 添加模板
  chargeStatisticsDailyIndex: () => import('@/views/statistics/daily/index'), // 业务收款台账
  purviewGroup: () => import('@/views/purview/group'), // 群组管理
  purviewRole: () => import('@/views/purview/role'), // 用户管理
}



export const asyncRouterMap = [

  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: '首页' },
    redirect: '/dashboard',
    children: [
      // dashboard
      {
        path: '/data/user/editPwd',
        name: 'data_user_edit_pwd',
        hidden: true,
        component: routerComponents['dataUserEditPwd'],
        meta:{ title: '修改密码' },
      },
      {
        path: '/dashboard',
        name: 'dashboard',
        redirect: '/dashboard/analysis',
        component: routerComponents['RouteView'],
        meta: { title: '仪表盘', keepAlive: true, icon: bxAnaalyse },
        children: [
          {
            path: '/dashboard/analysis',
            name: 'Analysis',
            component: routerComponents['dashboardAnalysis'],
            meta: { title: '控制台', keepAlive: false }
          }
        ]
      },
      {
        path: '/data',
        name: 'data',
        component:routerComponents['PageView'],
        redirect: '/data/community',
        meta:{ title:'数据中心', icon:'cloud' },
        children:[
          {
            path: '/data/community',
            name: 'data_community_index',
            component: routerComponents['dataCommunityIndex'],
            meta:{ title: '小区列表' }
          },
          {
            path: '/data/community/operation/:id',
            name: 'data_community_operation',
            hidden: true,
            component: routerComponents['dataCommunityOperation'],
            meta:{ title: '添加小区' },
          },
          {
            path: '/data/community/extend/:id',
            name: 'data_community_extend',
            hidden: true,
            component: routerComponents['dataCommunityExtend'],
            meta:{ title: '小区其它信息' },
          },
          {
            path: '/data/building',
            name: 'data_building_index',
            component: routerComponents['dataBuildingIndex'],
            meta:{ title: '楼宇管理' }
          },
          {
            path: '/data/building/operation/:id',
            name: 'data_building_operation',
            hidden: true,
            component: routerComponents['dataBuildingOperation'],
            meta:{ title: '添加楼宇' },
          },
          {
            path: '/data/building/unit/:id',
            name: 'data_building_unit',
            hidden: true,
            component: routerComponents['dataBuildingUnit'],
            meta:{ title: '单元管理' },
          },
          {
            path: '/data/houses/:id',
            name: 'data_houses_index',
            component: routerComponents['dataHousesIndex'],
            meta:{ title: '房屋管理' }
          },
          {
            path: '/data/houses/operation/:id',
            name: 'data_houses_operation',
            hidden: true,
            component: routerComponents['dataHousesOperation'],
            meta:{ title: '添加房屋' },
          },
          {
            path: '/data/houses/extend/:id',
            name: 'data_houses_extend',
            hidden:true,
            component: routerComponents['dataHousesExtend'],
            meta:{ title: '房屋其它信息' },
          },
          {
            path: '/data/houses/detail/:id',
            name: 'data_houses_detail',
            hidden:true,
            component: routerComponents['dataHousesDetail'],
            meta:{ title: '房屋详情' },
          },
          {
            path: '/data/houses/import',
            name: 'house-import',
            hidden:true,
            component: routerComponents['dataHousesImport'],
            meta:{ title: '导入房屋' },
          },
          {
            path: '/data/houses/bz/:id',
            name: 'data_houses_bz',
            hidden:true,
            component: routerComponents['dataHousesBz'],
            meta:{ title: '添加收费标准' },
          },
          {
            path: '/data/parking',
            name: 'data_parking_index',
            component: routerComponents['dataParkingIndex'],
            meta:{ title: '车位管理' }
          },
          {
            path: '/data/parking/operation/:id',
            name: 'data_parking_operation',
            hidden: true,
            component: routerComponents['dataParkingOperation'],
            meta:{ title: '添加车位' },
          },
          {
            path: '/data/parking/import',
            name: 'parking-import',
            hidden:true,
            component: routerComponents['dataParkingImport'],
            meta:{ title: '导入车位' },
          },
          {
            path: '/data/parking/detail/:id',
            name: 'data_parking_detail',
            hidden: true,
            component: routerComponents['dataParkingDetail'],
            meta:{ title: '车位详情' },
          },
          {
            path: '/data/parking/bz/:id',
            name: 'data_parking_bz',
            hidden: true,
            component: routerComponents['dataParkingBz'],
            meta:{ title: '添加收费标准' },
          },
          {
            path: '/data/vehicle',
            name: 'data_vehicle_index',
            component: routerComponents['dataVehicleIndex'],
            meta:{ title: '车辆管理' }
          },
          {
            path: '/data/vehicle/operation/:id',
            name: 'data_vehicle_operation',
            hidden: true,
            component: routerComponents['dataVehicleOperation'],
            meta:{ title: '添加车辆' },
          },
          {
            path: '/data/vehicle/extend/:id',
            name: 'data_vehicle_extend',
            hidden:true,
            component: routerComponents['dataVehicleExtend'],
            meta:{ title: '车辆其它信息' },
          },
          {
            path: '/data/vehicle/import',
            name: 'vehicle-import',
            hidden:true,
            component: routerComponents['dataVehicleImport'],
            meta:{ title: '导入车辆' },
          },
          {
            path: '/data/vehicle/detail/:id',
            name: 'data_vehicle_detail',
            hidden:true,
            component: routerComponents['dataVehicleDetail'],
            meta:{ title: '车辆详情' },
          },
          {
            path: '/data/vehicle/bz/:id',
            name: 'data_vehicle_bz',
            hidden: true,
            component: routerComponents['dataVehicleBz'],
            meta:{ title: '添加收费标准' },
          },
          {
            path: '/data/household',
            redirect:'/data/household/index',
            component: routerComponents['BlankLayout'],
            meta:{ title: '客户管理' },
            children:[
              {
                path:'/data/household/index',
                name:'data_household_index',
                hideChildrenInMenu:true,
                component: routerComponents['dataHouseholdIndexIndex'],
                meta:{ title: '住户管理' },
              },
              {
                path: '/data/household/operation/:id',
                name: 'data_household_operation',
                hidden: true,
                component: routerComponents['dataHouseholdIndexOperation'],
                meta:{ title: '添加住户' },
              },
              {
                path: '/data/household/import',
                name: 'household-import',
                hidden:true,
                component: routerComponents['dataHouseholdIndexImport'],
                meta:{ title: '住户导入' },
              },
              {
                path: '/data/household/detail/:id',
                name: 'data_household_detail',
                hidden: true,
                component: routerComponents['dataHouseholdIndexDetail'],
                meta:{ title: '住户详情' },
              },
              {
                path:'/data/household/nonindex',
                name:'data_household_nonindex',
                component: routerComponents['dataHouseholdNonindexIndex'],
                meta:{ title: '非住户管理' },
              },
              {
                path: '/data/household/nonindex/operation/:id',
                name: 'data_household_nonindex_operation',
                hidden: true,
                component: routerComponents['dataHouseholdNonindexOperation'],
                meta:{ title: '添加非住户' },
              },
              {
                path: '/data/household/nonindex/detail/:id',
                name: 'data_household_nonindex_detail',
                hidden: true,
                component: routerComponents['dataHouseholdNonindexDetail'],
                meta:{ title: '非住户详情' },
              },
            ]
          },
          {
            path: '/data/labels',
            name: 'data_labels_index',
            component: routerComponents['dataLabelsIndex'],
            meta:{ title: '数据标签' }
          },
          {
            path: '/data/labels/bindList/:id',
            name:'data_labels_bindList',
            hidden:true,
            component: routerComponents['dataLabelsBindList'],
            meta:{ title: '绑定列表',hiddenHeaderContent:true }
          }
        ]
      },
      {
        path:'/charge',
        name:'charge',
        component:routerComponents['PageView'],
        redirect: '/charge/cashier',
        meta:{ title:'收费管理', icon:'property-safety' },
        children:[
          {
            path:'/charge/cashier',
            component: routerComponents['BlankLayout'],
            meta:{ title: '收银台' },
            children:[
              {
                path:'/charge/cashier/index',
                name:'charge_cashier_index',
                component: routerComponents['chargeCashierIndex'],
                meta:{ title: '常规收费' },
              },
              {
                path:'/charge/cashier/others',
                name:'charge_cashier_others',
                component: routerComponents['chargeCashierOthers'],
                meta:{ title: '其它收费' },
              },
              {
                path:'/charge/cashier/quick',
                name:'charge_cashier_quick',
                component: routerComponents['chargeCashierQuick'],
                meta:{ title: '临时停车费' },
              }

            ]
          },

          // 收银台房屋详情
          {
            path:'/charge/cashier/detail/:house_id',
            name:'charge_cashier_houseDetail',
            hidden: true,
            component: routerComponents['chargeCashierHouseDetail'],
            meta:{ title: '收银台' }
          },

          // 收银台车辆详情
          {
            path:'/charge/cashier/car_detail/:house_id',
            name:'charge_cashier_carDetail',
            hidden: true,
            component: routerComponents['chargeCashierCarDetail'],
            meta:{ title: '收银台' }
          },

          // 收银台车位详情
          {
            path:'/charge/cashier/car_area_detail/:house_id',
            name:'charge_cashier_carAreaDetail',
            hidden: true,
            component: routerComponents['chargeCashierCarAreaDetail'],
            meta:{ title: '收银台' }
          },

          // 收银台住户详情
          {
            path:'/charge/cashier/house_user_detail/:house_id',
            name:'charge_cashier_houseUserDetail',
            hidden: true,
            component: routerComponents['chargeCashierHouseUserDetail'],
            meta:{ title: '收银台' }
          },
          {
            path:'/charge/copy_table',
            name:'charge_copy_table_index',
            component: routerComponents['chargeCopyTableIndex'],
            meta:{ title: '抄表录入' }
          },
          {
            path:'/charge/copy_table_import',
            name:'charge_copy_table_import',
            hidden: true,
            component: routerComponents['chargeCopyTableImport'],
            meta:{ title: '抄表录入导入' }
          },
          {
            path:'/charge/not_pay',
            name:'charge_not_pay_index',
            component: routerComponents['chargeNotPayIndex'],
            meta:{ title: '未缴账单' }
          },
          {
            path:'/charge/not_pay_import',
            name:'charge_not_pay_import',
            hidden: true,
            component: routerComponents['chargeNotPayImport'],
            meta:{ title: '未缴账单导入' }
          },
          {
            path:'/charge/be_pay',
            name:'charge_be_pay_index',
            component: routerComponents['chargeBePayIndex'],
            meta:{ title: '已缴账单' }
          },
            {
                path:'/charge/be_pay/detail/:id',
                name:'charge_be_pay_detail',
                hidden: true,
                component: routerComponents['chargeBePayDetail'],
                meta:{ title: '已缴详情' }
            },
          {
            path:'/charge/be_pay_import',
            name:'charge_be_pay_import',
            hidden: true,
            component: routerComponents['chargeBePayImport'],
            meta:{ title: '已缴账单导入' }
          },
          {
            path:'/charge/payment_notice',
            name:'charge_payment_notice_index',
              hidden:true,
            component: routerComponents['chargePaymentNoticeIndex'],
            meta:{ title: '缴费通知' }
          },
          {
            path:'/charge/pre_deposit_det',
            hidden: true,
            name:'charge_pre_deposit_detail',
            component: routerComponents['chargePreDepositDetail'],
            meta:{ title: '预存款详情' }
          },
          {
            path:'/charge/pre_deposit',
            name:'charge_pre_deposit_index',
            component: routerComponents['chargePreDepositIndex'],
            meta:{ title: '预存款管理' }
          },
          {
            path:'/charge/pre_deposit_in',
            name:'charge_pre_deposit_in',
            hidden: true,
            component: routerComponents['chargePreDepositIn'],
            meta:{ title: '导入预存款' }
          },
          {
            path:'/charge/toll',
            name:'charge_toll_index',
            component: routerComponents['chargeTollIndex'],
            meta:{ title: '收费设置' }
          },
          {
            path:'/charge/toll/add_formula/:id',
            name:'charge_toll_add_formula',
            hidden: true,
            component: routerComponents['chargeTollTollAddFormula'],
            meta:{ title: '添加公式' }
          },
          {
            path:'/charge/toll/project_add/:id',
            name:'toll_project_add',
            hidden: true,
            component: routerComponents['chargeTollTollProjectAdd'],
            meta:{ title: '添加收费项目' }
          },
          {
            path:'/charge/toll/detail_add/:edit_id',
            name:'charge_toll_detail_add',
            hidden: true,
            component: routerComponents['chargeTollTollDetailAdd'],
            meta:{ title: '添加收费标准' }
          },
          {
            path:'/charge/toll/add_model',
            name:'charge_toll_add_model',
            hidden: true,
            component: routerComponents['chargeTollTollAddModel'],
            meta:{ title: '添加模板' }
          },
        ]
      },
      {
        path:'/statistics',
        name:'statistics',
        component:routerComponents['PageView'],
        redirect:'/statistics/daily',
        meta:{ title:'报表统计', icon:'bar-chart' },
        children:[
          {
            path:'/statistics/daily',
            name:'daily_index',
            component: routerComponents['chargeStatisticsDailyIndex'],
            meta:{ title: '业务收款台账' }
          }
        ]
      },
      {
        path:'/purview',
        name:'purview',
        component:routerComponents['PageView'],
        redirect:'/purview/group',
        meta:{ title:'权限设置', icon:'usergroup-add' },
        children:[
          {
            path:'/purview/group',
            name:'purview_group',
            component: routerComponents['purviewGroup'],
            meta:{ title: '群组管理' }
          },
          {
            path:'/purview/role',
            name:'purview_role',
            component: routerComponents['purviewRole'],
            meta:{ title: '用户管理' }
          }
        ]
      },
      // {
      //   path: '/fee',
      //   name: 'fee',
      //   redirect: '/fee/pay_dashboard/',
      //   component: PageView,
      //   meta: { title: '物业收费', icon: 'property-safety' },
      //   children: [
      //     {
      //       path: '/fee/pay_dashboard',
      //       name: 'pay_dashboard',
      //       component: () => import('@/views/fee/pay_dashboard'),
      //       redirect: '/fee/pay_dashboard/index',
      //       meta: { title: '物业收银台', keepAlive: false },
      //       children: [
      //         {
      //           path: '/fee/pay_dashboard/index',
      //           name: 'feeIndexs',
      //           component: () => import('@/views/fee/dashboard/index'),
      //           meta: { title: '开单' },
      //         },
      //         {
      //           path: '/fee/pay_dashboard/refundpage',
      //           name: 'feeRefundpage',
      //           component: () => import('@/views/fee/dashboard/refundpage'),
      //           meta: { title: '退单' }
      //         },
      //         {
      //           path: '/list/search/checkorderpage',
      //           name: 'feeCheckorderpage',
      //           component: () => import('@/views/fee/dashboard/checkorderpage'),
      //           meta: { title: '查单' }
      //         },
      //         {
      //           path: '/fee/detail/house',
      //           name: 'houseInfo',
      //           hidden: true,
      //           component: () => import('@/views/fee/detail/house'),
      //           meta: { title: '房间详情' }
      //         },
      //         {
      //           path: '/fee/result/success',
      //           name: 'FeeResultSuccess',
      //           hidden: true,
      //           component: () => import(/* webpackChunkName: "result" */ '@/views/fee/result/Success'),
      //           meta: { title: '成功', keepAlive: false, hiddenHeaderContent: true}
      //         },
      //         {
      //           path: '/fee/result/fail',
      //           name: 'FeeResultFail',
      //           hidden: true,
      //           component: () => import(/* webpackChunkName: "result" */ '@/views/fee/result/Error'),
      //           meta: { title: '失败', keepAlive: false, hiddenHeaderContent: true}
      //         }
      //       ]
      //     },
      //     {
      //       path: '/fee/item',
      //       name: 'feeItem',
      //       component: () => import('@/views/fee/item'),
      //       meta: { title: '收费项目管理', keepAlive: false }
      //     },
      //     {
      //       path: '/fee/discount',
      //       name: 'feeDiscount',
      //       component: () => import('@/views/fee/discount'),
      //       meta: { title: '收费项目优惠设定', keepAlive: false }
      //     },
      //     {
      //       path: '/fee/fee_template',
      //       name: 'fee_template',
      //       component: () => import('@/views/fee/fee_template'),
      //       meta: { title: '票据模板', keepAlive: false },
      //       hideChildrenInMenu: true,
      //       children: [
      //         {
      //           path: '/fee/fee_template/add',
      //           name: 'fee_template_add',
      //           component: () => import('@/views/fee/fee_template_add'),
      //           meta: { title: '新增收费模板' }
      //         },
      //         {
      //           path: '/fee/fee_template/edit',
      //           name: 'fee_template_edit',
      //           component: () => import('@/views/fee/fee_template_edit'),
      //           meta: { title: '编辑收费模板' }
      //         }
      //       ]
      //     },
      //     {
      //       path: '/fee/infoimport',
      //       name: 'infoimport',
      //       component: () => import('@/views/fee/infoimport'),
      //       redirect: '/fee/infoimport/warter',
      //       meta: { title: '线下信息管理', keepAlive: true },
      //       children: [
      //         {
      //           path: '/fee/infoimport/warter',
      //           name: 'InfoimportWarter',
      //           component: () => import('../views/fee/infoimport/warter'),
      //           meta: { title: '水费' }
      //         },
      //         {
      //           path: '/fee/infoimport/power',
      //           name: 'InfoimportPower',
      //           component: () => import('../views/fee/infoimport/power'),
      //           meta: { title: '电费' }
      //         },
      //         {
      //           path: '/fee/infoimport/gas',
      //           name: 'InfoimportGas',
      //           component: () => import('../views/fee/infoimport/gas'),
      //           meta: { title: '煤气费' }
      //         },
      //         {
      //           path: '/fee/infoimport/public',
      //           name: 'InfoimportPublic',
      //           component: () => import('../views/fee/infoimport/public'),
      //           meta: { title: '公摊费' }
      //         }
      //       ]
      //     },
      //     {
      //       path: '/fee/create_fee_order',
      //       name: 'create_fee_order',
      //       component: () => import('@/views/fee/create_fee_order'),
      //       meta: { title: '一键生成物业收费单', keepAlive: false }
      //     }
      //   ]
      // },
      //   {
      //   path: '/work_order',
      //   redirect: '/work_order/order-list',
      //   component: PageView,
      //   meta: { title: '物业工单', icon: 'ordered-list' },
      //   children: [
      //     {
      //       path: '/work_order/order-list',
      //       name: 'order-list',
      //       component: () => import('@/views/work_order/order-list'),
      //       meta: { title: '基础工单', keepAlive: false }
      //     },
      //     {
      //       path: '/work_order/complaint-order',
      //       name: 'complaint-order',
      //       component: () => import('@/views/work_order/complaint-order'),
      //       meta: { title: '投诉工单', keepAlive: false }
      //     }
      //   ]
      // },
      // {
      //   path: '/report',
      //   redirect: '/report/fee-report',
      //   component: PageView,
      //   meta: { title: '数据报表', icon: 'ordered-list' },
      //   children: [
      //     {
      //       path: '/report/fee-report',
      //       name: 'fee-report',
      //       component: () => import('@/views/report/fee-report'),
      //       meta: { title: '收费报表', keepAlive: false }
      //     },
      //     {
      //       path: '/report/household-report',
      //       name: 'household-report',
      //       component: () => import('@/views/report/household-report'),
      //       meta: { title: '住户报表', keepAlive: false }
      //     },
      //     {
      //       path: '/report/car-report',
      //       name: 'car-report',
      //       component: () => import('@/views/report/car-report'),
      //       meta: { title: '停车场收费报表', keepAlive: false }
      //     }
      //   ]
      // },
      // {
      //   path: '/wiki',
      //   redirect: '/wiki/document',
      //   component: PageView,
      //   meta: { title: '知识库', icon: 'zhihu' },
      //   children: [
      //     {
      //       path: '/wiki/document',
      //       name: 'wikidoc',
      //       component: () => import('@/views/wiki/document'),
      //       meta: { title: '文档下载', keepAlive: false }
      //     }
      //   ]
      // },
      // account
      // {
      //   path: '/account',
      //   component: RouteView,
      //   redirect: '/account/center',
      //   name: 'account',
      //   meta: { title: '个人页', icon: 'user', keepAlive: true, permission: [ 'user' ] },
      //   children: [
      //     {
      //       path: '/account/center',
      //       name: 'center',
      //       component: () => import('@/views/account/center/Index'),
      //       meta: { title: '个人中心', keepAlive: true, permission: [ 'user' ] }
      //     },
      //     {
      //       path: '/account/settings',
      //       name: 'settings',
      //       component: () => import('@/views/account/settings/Index'),
      //       meta: { title: '个人设置', hideHeader: true, permission: [ 'user' ] },
      //       redirect: '/account/settings/base',
      //       hideChildrenInMenu: true,
      //       children: [
      //         {
      //           path: '/account/settings/base',
      //           name: 'BaseSettings',
      //           component: () => import('@/views/account/settings/BaseSetting'),
      //           meta: { title: '基本设置', hidden: true, permission: [ 'user' ] }
      //         },
      //         {
      //           path: '/account/settings/security',
      //           name: 'SecuritySettings',
      //           component: () => import('@/views/account/settings/Security'),
      //           meta: { title: '安全设置', hidden: true, keepAlive: true, permission: [ 'user' ] }
      //         },
      //         {
      //           path: '/account/settings/custom',
      //           name: 'CustomSettings',
      //           component: () => import('@/views/account/settings/Custom'),
      //           meta: { title: '个性化设置', hidden: true, keepAlive: true, permission: [ 'user' ] }
      //         },
      //         {
      //           path: '/account/settings/binding',
      //           name: 'BindingSettings',
      //           component: () => import('@/views/account/settings/Binding'),
      //           meta: { title: '账户绑定', hidden: true, keepAlive: true, permission: [ 'user' ] }
      //         },
      //         {
      //           path: '/account/settings/notification',
      //           name: 'NotificationSettings',
      //           component: () => import('@/views/account/settings/Notification'),
      //           meta: { title: '新消息通知', hidden: true, keepAlive: true, permission: [ 'user' ] }
      //         }
      //       ]
      //     }
      //   ]
      // }
    ]
  },
  {
    path: '*', redirect: '/404', hidden: true
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/user',
    component: UserLayout,
    redirect: '/user/login',
    hidden: true,
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Login')
      },
      {
        path: 'register',
        name: 'register',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Register')
      },
      {
        path: 'register-result',
        name: 'registerResult',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/RegisterResult')
      }
    ]
  },

  {
    path: '/test',
    component: BlankLayout,
    redirect: '/test/home',
    children: [
      {
        path: 'home',
        name: 'TestHome',
        component: () => import('@/views/Home')
      }
    ]
  },

  {
    path: '/404',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
  }

]

export const notFoundRouter = {
  path: '*', redirect: '/404', hidden: true
}